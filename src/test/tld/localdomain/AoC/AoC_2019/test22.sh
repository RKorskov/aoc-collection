#! /bin/sh
# -*- coding: utf-8; indent-tabs-mode: nil; -*-
# -*- eval: (set-language-environment Russian) -*-
# Time-stamp: <2019-12-24 18:51:04 roukoru>

rm *.class
javac Day22.java
[[ $? -ne 0 ]] && exit $?

function test_deal_new() {
    printf "Deal new:\n"
    printf "result:\n9 8 7 6 5 4 3 2 1 0\n"
    printf "deal into new stack\n" | java Day22 10
}

function test_cut() {
    printf "Cut:\n"
    printf "result:\n3 4 5 6 7 8 9 0 1 2\n"
    printf "cut 3" | java Day22 10
    printf "result:\n6 7 8 9 0 1 2 3 4 5\n"
    printf "cut -4" | java Day22 10
}

function test_deal_inc() {
    printf "Deal inc:\n"
    printf "result:\n0 7 4 1 8 5 2 9 6 3\n"
    printf "deal with increment 3\n" | java Day22 10
}

function test_compound() {
    printf "Composite tests:\n"
    printf "result:\n0 3 6 9 2 5 8 1 4 7\n"
    printf "deal with increment 7\ndeal into new stack\ndeal into new stack\n" | java Day22 10
    printf "result:\n3 0 7 4 1 8 5 2 9 6\n"
    printf "cut 6\ndeal with increment 7\ndeal into new stack\n" | java Day22 10

    printf "result:\n6 3 0 7 4 1 8 5 2 9\n"
    printf "deal with increment 7\ndeal with increment 9\ncut -2\n" | java Day22 10

    printf "result:\n9 2 5 8 1 4 7 0 3 6\n"
    printf "deal into new stack\ncut -2\ndeal with increment 7\ncut 8\ncut -4\ndeal with increment 7\ncut 3\ndeal with increment 9\ndeal with increment 3\ncut -1\n" | java Day22 10
}

test_deal_new
test_cut
test_deal_inc
test_compound
