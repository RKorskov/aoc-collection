#! /bin/sh
# -*- coding: utf-8; indent-tabs-mode: nil; -*-
# -*- eval: (set-language-environment Russian) -*-
# Time-stamp: <2019-12-23 12:46:38 roukoru>

rm *.class
javac Day09.java
[[ $? -ne 0 ]] && exit $?

function test_io() {
    printf "IN / OUT tests\n"
    printf "should print: 7 42\n"
    printf "3,0,4,0,3,0,4,0,99" | java Day09 7 42
    printf "should print: 17\n"
    printf "104,17,99" | java Day09 7 42
    printf "should print: 1729\n"
    printf "204,3,99,1729" | java Day09 0
    printf "should print: 91\n"
    printf "203,5,204,5,99,0" | java Day09 91
}

function test_addmul() {
    printf "ADD / MUL tests\n"
    printf "should print: 2\n"
    printf "1,0,0,0,4,0,99" | java Day09 7 42
    printf "should print: 2 67\n"
    printf "2,7,8,7,4,7,99,6,7" | java Day09 7 42
}

function test_jmp() {
    printf "JZ / JNZ tests\n"
    printf "should print: 11\n"
    printf "1006,9,6,4,0,99,104,11,99,0" | java Day09 7 42
    printf "should print: 1005\n"
    printf "1005,9,6,4,0,99,104,11,99,0" | java Day09 7 42
}

function test_cmp() {
    printf "CMP / CMPL tests\n"
    printf "should print: 0\n"
    printf "3,9,8,9,10,9,4,9,99,-1,8" | java Day09 7
    printf "should print: 1\n"
    printf "3,9,8,9,10,9,4,9,99,-1,8" | java Day09 8
    printf "should print: 1\n"
    printf "3,9,7,9,10,9,4,9,99,-1,8" | java Day09 7
    printf "should print: 0\n"
    printf "3,9,7,9,10,9,4,9,99,-1,8" | java Day09 8
    printf "should print: 1\n"
    printf "3,3,1108,-1,8,3,4,3,99" | java Day09 8
    printf "should print: 0\n"
    printf "3,3,1108,-1,8,3,4,3,99" | java Day09 7
    printf "should print: 1\n"
    printf "3,3,1107,-1,8,3,4,3,99" | java Day09 7
    printf "should print: 0\n"
    printf "3,3,1107,-1,8,3,4,3,99" | java Day09 8
    printf "should print: 0\n"
    printf "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9" | java Day09 0
    printf "should print: 1\n"
    printf "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9" | java Day09 7
    printf "should print: 0\n"
    printf "3,3,1105,-1,9,1101,0,0,12,4,12,99,1" | java Day09 0
    printf "should print: 1\n"
    printf "3,3,1105,-1,9,1101,0,0,12,4,12,99,1" | java Day09 7
    printf "should print: 999\n"
    printf "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99" | java Day09 5
    printf "should print: 1000\n"
    printf "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99" | java Day09 8
    printf "should print: 1001\n"
    printf "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99" | java Day09 11
}

function test_mem() {
    printf "sparse memory access tests\n"
    printf "should print: 1125899906842624\n"
    printf "104,1125899906842624,99" | java Day09 7 6 42
    printf "should print: 1219070632396864\n"
    printf "1102,34915192,34915192,7,4,7,99,0" | java Day09 7 6 42
    printf "should print: '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99'\n"
    printf "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99" | java Day09 7 6 42
}

function test_base() {
    printf "MVB tests\n"
    printf "$0 todo\n"
}

test_io
test_addmul
test_jmp
test_cmp
test_mem
test_base
