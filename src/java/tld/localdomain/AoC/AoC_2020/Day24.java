package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.stream.IntStream;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 24: Lobby Layout
 *
 * As you enter the lobby, you discover a small problem: the floor is
 * being renovated. You can't even reach the check-in desk until
 * they've finished installing the new tile floor.
 *
 * The tiles are all hexagonal; they need to be arranged in a hex grid
 * with a very specific color pattern. Not in the mood to wait, you
 * offer to help figure out the pattern.
 *
 * A member of the renovation crew gives you a list of the tiles that
 * need to be flipped over (your puzzle input). Each line in the list
 * identifies a single tile that needs to be flipped by giving a
 * series of steps starting from a reference tile in the very center
 * of the room. (Every line starts from the same reference tile.)
 */
public class Day24 {
    public static void main(final String[] args) {
        HexAxialDir[][] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * Because the tiles are hexagonal, every tile has six neighbors:
     * east, southeast, southwest, west, northwest, and
     * northeast. These directions are given in your list,
     * respectively, as e, se, sw, w, nw, and ne. A tile is identified
     * by a series of these directions with no delimiters; for
     * example, esenee identifies the tile you land on if you start at
     * the reference tile and then move one tile east, one tile
     * southeast, one tile northeast, and one tile east.
     */
    private static HexAxialDir[][] readParseInput(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        List<HexAxialDir[]> los = new ArrayList<>();
        sc.useDelimiter("[\n ]");
        for(;sc.hasNext();) {
            String str = sc.next().trim();
            if(str.length() > 0)
                los.add(HexAxialDir.parse(str));
        }
        return los.toArray(new HexAxialDir[0][]);
    }

    /**
     * Go through the renovation crew's list and determine which tiles
     * they need to flip. After all of the instructions have been
     * followed, how many tiles are left with the black side up?
     */
    private static int part1(final HexAxialDir[][] dirs) {
        Map<HexAxial, TileColour> tiles = initTileset(dirs);
        int sum = 0;
        for(TileColour tc : tiles.values())
            if(tc == TileColour.BLACK)
                ++sum;
        return sum;
    }

    private static Map<HexAxial,TileColour> initTileset(final HexAxialDir[][] dirs) {
        Map<HexAxial, TileColour> tiles = new HashMap<>();
        for(HexAxialDir[] diRow : dirs) {
            TileWalker tw = new TileWalker();
            for(HexAxialDir dir : diRow)
                tw.move(dir);
            HexAxial lt = new HexAxial(tw.q, tw.r, tw.s);
            TileColour mt = tiles.get(lt);
            if(mt == null)
                mt = TileColour.BLACK;
            else
                mt = TileColour.flip(mt);
            tiles.put(lt, mt);
        }
        return tiles;
    }

    /**
     * Every day, the tiles are all flipped according to the following
     * rules:
     *
     * - Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
     * - Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
     *
     * Here, tiles immediately adjacent means the six tiles directly
     * touching the tile in question.
     *
     * The rules are applied simultaneously to every tile; put another
     * way, it is first determined which tiles need to be flipped,
     * then they are all flipped at the same time.
     *
     * How many tiles will be black after 100 days?
     */
    private static int part2(final HexAxialDir[][] dirs) {
        Map<HexAxial, TileColour> tiles = initTileset(dirs);
        for(int i = 0; i < 100; ++i)
            tiles = morphTileset(tiles);
        int sum = 0;
        for(TileColour tc : tiles.values())
            if(tc == TileColour.BLACK)
                ++sum;
        return sum;
    }

    /**
     * The tiles are all flipped according to the following rules:
     *
     * - Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
     * - Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
     *
     * Here, tiles immediately adjacent means the six tiles directly
     * touching the tile in question.
     *
     * The rules are applied simultaneously to every tile.
     */
    private static Map<HexAxial,TileColour> morphTileset(final Map<HexAxial,TileColour> tiles) {
        return null;
    }
}

enum HexAxialDir {
    SE, E, NE, NW, W, SW;

    public static HexAxialDir[] parse(final String src) {
        List<HexAxialDir> lld = new ArrayList<>();
        for(int i = 0; i < src.length(); ++i) {
            HexAxialDir d = getMinorDir(src, i);
            if(d != null) {
                lld.add(d);
                ++i;
            }
            else switch(src.charAt(i)) {
            case 'e':
                lld.add(HexAxialDir.E);
                break;
            case 'w':
                lld.add(HexAxialDir.W);
                break;
            }
        }
        return lld.toArray(new HexAxialDir[0]);
    }

    private static HexAxialDir getMinorDir(final String dir, final int pos) {
        if((dir.length() - pos) > 1)
            switch(dir.substring(pos, pos+2).toUpperCase()) {
            case "SE":
                return HexAxialDir.SE;
            case "NE":
                return HexAxialDir.NE;
            case "SW":
                return HexAxialDir.SW;
            case "NW":
                return HexAxialDir.NW;
            }
        return null;
    }
}

/**
 * https://www.redblobgames.com/grids/hexagons/
 */
class HexAxial {
    final int q, r, s;

    HexAxial(int q, int r, int s) {
        this.q = q;
        this.r = r;
        this.s = s;
    }

    HexAxial() { this(0,0,0); }

    public String toString() {
        return String.format("(%d %d %d)", q, r, s);
    }

    public int hashCode() { return Objects.hash(q, r, s); }

    public boolean equals​(Object obj) {
        if(obj == null || !(obj instanceof HexAxial)) return false;
        HexAxial hao = (HexAxial)obj;
        return q == hao.q && r == hao.r && s == hao.s;
    }
}

enum TileColour {
    BLACK, WHITE;

    public static TileColour flip(final TileColour state) {
        switch(state) {
        case BLACK:
            return WHITE;
        case WHITE:
            return BLACK;
        }
        return null; // should not happen...
    }
}

/**
 * https://www.redblobgames.com/grids/hexagons/
 * https://stackoverflow.com/questions/5084801/manhattan-distance-between-tiles-in-a-hexagonal-grid
 * https://math.stackexchange.com/questions/2254655/hexagon-grid-coordinate-system
 * https://gamedev.stackexchange.com/questions/102032/map-coordinates-system?newreg=16a46d867bda4991a9e651dfe715dfa9
 */
class TileWalker { // ? HexCursor
    int q, r, s;

    TileWalker() {
        this.q = 0;
        this.r = 0;
        this.s = 0;
    }

    public void move(final HexAxialDir dir) {
        switch(dir) {
        case E:
            ++q;
            break;
        case W:
            --q;
            break;
        case SE:
            ++r;
            break;
        case SW:
            --q;
            ++r;
            break;
        case NE:
            ++q;
            --r;
            break;
        case NW:
            --r;
            break;
        }
    }

    public String toString() {
        return String.format("(%d %d %d)", q, r, s);
    }
}
