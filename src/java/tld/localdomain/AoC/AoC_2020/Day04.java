package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 4: Passport Processing
 */
public class Day04 {
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * Each passport is represented as a sequence of key:value pairs
     * separated by spaces or newlines. Passports are separated by
     * blank lines.
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        StringBuffer acc = new StringBuffer();
        for(;sc.hasNext();) {
            String src = sc.next();
            if(src == null) continue;
            if(src.length() > 0) {
                if(acc.length() > 0)
                    acc.append(' ');
                acc.append(src.trim());
            }
            else {
                loi.add(acc.toString());
                acc = new StringBuffer();
            }
        }
        if(acc.length() > 0)
            loi.add(acc.toString());
        return loi.toArray(new String[0]);
    }

    /**
     * Count the number of valid passports - those that have all
     * required fields. Treat cid as optional. In your batch file, how
     * many passports are valid?
     */
    private static int part1(final String[] src) {
        int cnt = 0;
        for(String spp : src) {
            PassProc pp = PassProc.create(spp);
            if(pp.ecl == null || pp.pid == null || pp.eyr == null //
               || pp.hcl == null || pp.byr == null || pp.iyr == null //
               || pp.hgt == null)
                continue;
            ++cnt;
        }
        return cnt;
    }

    private final static String[] EYE_COLOR = "amb blu brn gry grn hzl oth".split(" ");

    /**
     * You can continue to ignore the cid field, but each other field
     * has strict rules about what values are valid for automatic
     * validation:
     *
     * - byr (Birth Year) - four digits; at least 1920 and at most 2002.
     * - iyr (Issue Year) - four digits; at least 2010 and at most 2020.
     * - eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
     * - hgt (Height) - a number followed by either cm or in:
     *   If cm, the number must be at least 150 and at most 193.
     *   If in, the number must be at least 59 and at most 76.
     * - hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
     * - ecl (Eye Color) - exactly one of: `amb blu brn gry grn hzl oth`.
     * - pid (Passport ID) - a nine-digit number, including leading zeroes.
     * - cid (Country ID) - ignored, missing or not.
     *
     * Count the number of valid passports - those that have all
     * required fields and valid values. Continue to treat cid as
     * optional. In your batch file, how many passports are valid?
     */
    private static int part2(final String[] src) {
        int cnt = 0;
        for(String spp : src) {
            PassProc pp = PassProc.create(spp);
            if(pp.ecl == null || pp.pid == null || pp.eyr == null //
               || pp.hcl == null || pp.byr == null || pp.iyr == null //
               || pp.hgt == null)
                continue;
            boolean fv = true;
            try {
                int n = Integer.parseInt(pp.byr);
                if(pp.byr.length() != 4 || n < 1920 || n > 2002) continue;
                n = Integer.parseInt(pp.iyr);
                if(pp.iyr.length() != 4 || n < 2010 || n > 2020) continue;
                n = Integer.parseInt(pp.eyr);
                if(pp.eyr.length() != 4 || n < 2020 || n > 2030) continue;
                if(pp.hgt.length() > 2) {
                    int h = Integer.parseInt(pp.hgt.substring(0, pp.hgt.length() - 2));
                    switch(pp.hgt.substring(pp.hgt.length() - 2, pp.hgt.length())) {
                    case "cm":
                        if(h < 150 || h > 193)
                            fv = false;
                        break;
                    case "in":
                        if(h < 59 || h > 76)
                            fv = false;
                        break;
                    default:
                        continue;
                    }
                }
                if(pp.hcl.length() == 7 && pp.hcl.charAt(0) == '#')
                    Integer.parseInt(pp.hcl.substring(1), 16); // exception on invalid value
                else
                    continue;
                if(Arrays.stream(EYE_COLOR)
                   .filter(c -> c.equals(pp.ecl))
                   .count() != 1)
                    continue;
                if(pp.pid.length() == 9)
                    Integer.parseInt(pp.pid); // exception on invalid value
                else
                    continue;
                // cid
            }
            catch(Exception ex) {
                fv = false;
                System.out.println("InvNum : " + spp);
            }
            if(fv)
                ++cnt;
        }
        return cnt;
    }
}

/**
 * Each passport is represented as a sequence of key:value pairs
 * separated by spaces or newlines. Passports are separated by blank
 * lines.
 *
 * - byr (Birth Year)
 * - iyr (Issue Year)
 * - eyr (Expiration Year)
 * - hgt (Height)
 * - hcl (Hair Color)
 * - ecl (Eye Color)
 * - pid (Passport ID)
 * - cid (Country ID)
 */
class PassProc {
    final String ecl, pid, eyr, hcl, byr, iyr, cid, hgt;

    private PassProc(String ecl, String pid, String eyr, String hcl,
                     String byr, String iyr, String cid, String hgt) {
        this.ecl = ecl;
        this.pid = pid;
        this.eyr = eyr;
        this.hcl = hcl;
        this.byr = byr;
        this.iyr = iyr;
        this.cid = cid;
        this.hgt = hgt;
    }

    public static PassProc create(final String src) {
        String ecl = null, pid = null, eyr = null, hcl = null,
            byr = null, iyr = null, cid = null, hgt = null;
        String[] flds = src.trim().split("\\s+");
        for(String fld : flds) {
            String[] kv = fld.split(":");
            if(kv.length < 2) {
                System.err.println(src);
                continue;
            }
            switch(kv[0]) {
            case "ecl":
                ecl = kv[1];
                break;
            case "pid":
                pid = kv[1];
                break;
            case "eyr":
                eyr = kv[1];
                break;
            case "hcl":
                hcl = kv[1];
                break;
            case "byr":
                byr = kv[1];
                break;
            case "iyr":
                iyr = kv[1];
                break;
            case "cid":
                cid = kv[1];
                break;
            case "hgt":
                hgt = kv[1];
                // break;
            }
        }
        return new PassProc(ecl, pid, eyr, hcl, byr, iyr, cid, hgt);
    }
}
