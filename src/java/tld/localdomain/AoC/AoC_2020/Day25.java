package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 25: Combo Breaker
 *
 * The handshake used by the card and the door involves an operation
 * that transforms a subject number. To transform a subject number,
 * start with the value 1. Then, a number of times called the loop
 * size, perform the following steps:
 *
 * - Set the value to itself multiplied by the subject number.
 * - Set the value to the remainder after dividing the value by 20201227.
 *
 * The card always uses a specific, secret loop size when it
 * transforms a subject number. The door always uses a different,
 * secret loop size.
 *
 * The cryptographic handshake works like this:
 *
 * - The card transforms the subject number of 7 according to the card's secret loop size. The result is called the card's public key.
 * - The door transforms the subject number of 7 according to the door's secret loop size. The result is called the door's public key.
 * - The card and door use the wireless RFID signal to transmit the two public keys (your puzzle input) to the other device. Now, the card has the door's public key, and the door has the card's public key. Because you can eavesdrop on the signal, you have both public keys, but neither device's loop size.
 * - The card transforms the subject number of the door's public key according to the card's loop size. The result is the encryption key.
 * - The door transforms the subject number of the card's public key according to the door's loop size. The result is the same encryption key as the card calculated.
 */
public class Day25 {
    public static void main(final String[] args) {
        final int pubCard = Integer.parseInt(args[0]),
            pubDoor = Integer.parseInt(args[1]);
        System.out.println(part1(pubCard, pubDoor));
        System.out.println(part2(pubCard, pubDoor));
    }

    /**
     * What encryption key is the handshake trying to establish?
     */
    private static long part1(final int pubCard, final int pubDoor) {
        int loopSizeCard = -1, loopSizeDoor = -1;
        long n = 1;
        for(int i = 0; i < Integer.MAX_VALUE; ++i) {
            n = encode1(n, 7);
            if(n == pubCard && loopSizeCard < 0)
                loopSizeCard = i + 1;
            if(n == pubDoor && loopSizeDoor < 0)
                loopSizeDoor = i + 1;
            if(loopSizeDoor > 0 && loopSizeCard > 0)
                break;
        }
        System.out.printf("loop size : card %d : door %d\n", loopSizeDoor, loopSizeCard);
        long skey = -1;
        if(loopSizeDoor > 0) {
            skey = 1;
            for(int i = 0; i < loopSizeDoor; ++i)
                skey = encode1(skey, pubCard);
            System.out.printf("skey door loop %d\n", skey);
        }
        if(loopSizeCard > 0) {
            skey = 1;
            for(int i = 0; i < loopSizeCard; ++i)
                skey = encode1(skey, pubDoor);
            System.out.printf("skey card loop %d\n", skey);
        }
        return skey;
    }

    /**
     * The handshake used by the card and the door involves an
     * operation that transforms a subject number. To transform a
     * subject number, start with the value 1. Then, a number of times
     * called the loop size, perform the following steps:
     *
     * - Set the value to itself multiplied by the subject number.
     * - Set the value to the remainder after dividing the value by 20201227.
     */
    private static long encode1(final long num0, final int subject) {
        return (num0 * subject) % 20201227;
    }
}
