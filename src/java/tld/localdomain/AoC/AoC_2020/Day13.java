package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 13: Shuttle Search
 */
public class Day13 {
    public static void main(final String[] args) {
        __BusSchedule src = readParseInput(args);
        System.out.println(part1(src.timestamp, src.busID));
        System.out.println(part2(src.timestamp, src.busID));
    }

    /**
     * 939
     * 7,13,x,x,59,x,31,19
     */
    private static __BusSchedule readParseInput(final String[] args) {
        List<Integer> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("[\n ,]");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            try {
                loi.add(Integer.parseInt(src));
            }
            catch(Exception ex) {
                loi.add(0);
            }
        }
        return new __BusSchedule(loi.get(0),
                                 loi.stream().skip(1).mapToInt(i->(int)i) //
                                 .toArray());
    }

    /**
     * What is the ID of the earliest bus you can take to the airport
     * multiplied by the number of minutes you'll need to wait for
     * that bus?
     */
    private static int part1(final int timestamp, final int[] busID) {
        int tmin = Integer.MAX_VALUE, bid = -1;
        for(int i = 0; i < busID.length; ++i) {
            if(busID[i] < 1) continue; // skip missing, I will need tham in 2nd part
            int tdif = (timestamp / busID[i] + 1) * busID[i] - timestamp;
            if(tdif < tmin) {
                tmin = tdif;
                bid = busID[i];
            }
        }
        return tmin * bid;
    }

    /**
     * What is the earliest timestamp such that all of the listed bus
     * IDs depart at offsets matching their positions in the list?
     */
    private static long part2(final int timestamp, final int[] busID) {
        // t%busID[i] == (busID[i]-i)
        for(int i = 0; i < busID.length; ++i) {
            if(busID[i] < 1) continue;
            int tadj = busID[i] - i;
        }
        return prod;
    }
}

class __BusSchedule {
    final int timestamp;
    final int[] busID;

    __BusSchedule(int timestamp, int[] busID) {
        this.timestamp = timestamp;
        this.busID = busID;
    }
}
