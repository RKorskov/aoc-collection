package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 8: Handheld Halting
 *
 * The boot code is represented as a text file with one instruction
 * per line of text. Each instruction consists of an operation (acc,
 * jmp, or nop) and an argument (a signed number like +4 or -20).
 *
 * - `acc` increases or decreases a single global value called the accumulator by the value given in the argument. For example, `acc +7` would increase the accumulator by 7. The accumulator starts at 0. After an acc instruction, the instruction immediately below it is executed next.
 * - `jmp` jumps to a new instruction relative to itself. The next instruction to execute is found using the argument as an offset from the jmp instruction; for example, `jmp +2` would skip the next instruction, `jmp +1` would continue to the instruction immediately below it, and `jmp -20` would cause the instruction 20 lines above to be executed next.
 * - `nop` stands for No OPeration - it does nothing. The instruction immediately below it is executed next.
 */
public class Day08 {
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * The boot code is represented as a text file with one
     * instruction per line of text. Each instruction consists of an
     * operation (acc, jmp, or nop) and an argument (a signed number
     * like +4 or -20).
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            //if(src == null) continue;
            if(src.length() > 0)
                loi.add(src);
        }
        return loi.toArray(new String[0]);
    }

    /**
     * Run your copy of the boot code. Immediately before any
     * instruction is executed a second time, what value is in the
     * accumulator?
     */
    private static int part1(final String[] src) {
        long[] bitmap = new long[src.length / 64 + 1];
        int a = 0;
        for(int ip = 0; ip < src.length;) {
            if(getBitmap(bitmap, ip))
                break;
            setBitmap(bitmap, ip);
            String[] cmd = src[ip].split("\\s+");
            switch(cmd[0]) {
            case "jmp":
                ip += Integer.parseInt(cmd[1]);
                break;
            case "acc":
                a += Integer.parseInt(cmd[1]);
            case "nop":
                ++ip;
                break;
            }
        }
        return a;
    }

    private static void setBitmap(long[] bitmap, final int pos) {
        final int idx = pos / 64,
            bit = pos % 64;
        bitmap[idx] |= 1L << bit;
    }

    private static boolean getBitmap(long[] bitmap, final int pos) {
        final int idx = pos / 64,
            bit = pos % 64;
        return (bitmap[idx] & (1L << bit)) != 0;
    }

    /**
     * The program is supposed to terminate by attempting to execute
     * an instruction immediately after the last instruction in the
     * file. By changing exactly one jmp or nop, you can repair the
     * boot code and make it terminate correctly.
     *
     * Fix the program so that it terminates normally by changing
     * exactly one jmp (to nop) or nop (to jmp).
     * What is the value of the accumulator after the program
     * terminates?
     */
    private static int part2(final String[] src) {
        String[] hotfix = Arrays.copy(src);
        // fixme!
        return part1(hotfix);
    }
}
