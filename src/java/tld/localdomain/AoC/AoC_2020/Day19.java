package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Strevaltron;
import tld.localdomain.AoC.util.StrevaltronState;
import tld.localdomain.AoC.util.Utils;

/**
 * Day 19: Monster Messages
 *
 * Your goal is to determine the number of messages that completely
 * match rule 0. In the above example, ababbb and abbbab match, but
 * bababa, aaabbb, and aaaabbb do not, producing the answer 2. The
 * whole message must match all of rule 0; there can't be extra
 * unmatched characters in the message. (For example, aaaabbb might
 * appear to match rule 0 above, but it has an extra unmatched b on
 * the end.)
 *
 * How many messages completely match rule 0?
 */
public class Day19 {
    public static void main(final String[] args) {
        Map<Integer,Message19Rule> ruleset = new HashMap<>();
        List<String> _messages = new ArrayList<>();
        readParseInput(args, ruleset, _messages);
        String[] messages = _messages.toArray(new String[0]);
        System.out.println(part1(ruleset, messages));
        System.out.println(part2(ruleset, messages));
    }

    private static final Pattern patMessage = Pattern.compile("^([ab]+)$");

    /**
     * 0: 4 1 5
     * 1: 2 3 | 3 2
     * 4: "a"
     *
     * ababbb
     */
    private static void readParseInput(final String[] args,
                                           final Map<Integer,Message19Rule> rules,
                                           final List<String> messages) {
        String[] src = readInput(args);
        for(String str : src) {
            if(str.length() < 1) continue;
            Matcher m = patMessage.matcher(str);
            if(m.find()) {
                messages.add(m.group(1));
                continue;
            }
            try {
                Message19Rule mr = Message19Rule.fromString(str);
                rules.put(mr.getId(), mr);
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private static String[] readInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();)
            loi.add(sc.next().trim());
        return loi.toArray(new String[0]);
    }

    /**
     * How many messages completely match rule 0?
     */
    private static int part1(final Map<Integer,Message19Rule> ruleset,
                             final String[] messages) {
        int mcount = 0;
        for(String message : messages) {
            Strevaltron sm = new Strevaltron(message);
            if (validate(ruleset, 0, sm) && sm.isEOF()) {
                ++mcount;
                System.out.printf("%s PASS\n", message);
            } else
                System.out.printf("%s FAIL\n", message);
        }
        return mcount;
    }

    /**
     * validate the message by applying to it the rule0id from ruleset
     * todo: trailing chars
     */
    private static boolean validate(final Map<Integer,Message19Rule> ruleset,
                                    final int rule0id,
                                    final Strevaltron message) {
        Message19Rule rule = ruleset.get(rule0id);
        if(rule.isFinale())
            return rule.test(message.get());
        for(int[] subset : rule.getSubrules()) {
            boolean vf = true;
            StrevaltronState state = message.getState();
            for(int rid : subset) {
                vf = validate(ruleset, rid, message);
                if(!vf) break;
            }
            if(vf) return true;
            message.rewind(state);
        }
        return false;
    }

    /**
     */
    private static int part2(final Map<Integer,Message19Rule> ruleset,
                             final String[] messages) {
        return -1;
    }
}

/**
 * 0: 1 2 3
 * 1: "a"
 * 2: 1 3 | 3 1
 * 3: "b"
 */
class Message19Rule {
    private static final Pattern patChar = Pattern.compile("^(\\d+): \"(.)\"$");
    private static final Pattern patList = Pattern.compile("^(\\d+): ([ 0-9|]+)$");

    final int id;
    final List<int[]> rids;
    final char chr;

    private Message19Rule(int id, List<int[]> rids) { // 1: 2 3 | 4 5 6 | ...
        this.id = id;
        this.rids = rids;
        this.chr = 0;
    }

    private Message19Rule(int id, int[] rules) { // 1: 2 3 4 ...
        this.id = id;
        this.rids = new ArrayList<>();
        this.rids.add(Arrays.copyOf(rules, rules.length));
        this.chr = 0;
    }

    private Message19Rule() {
        id = -1;
        rids = new ArrayList<>();
        chr = 0;
    }

    private Message19Rule(int id, char c) { // 1: "a"
        this.id = id;
        this.rids = new ArrayList<>();
        this.chr = c;
    }

    public int getId() { return id; }
    public List<int[]> getSubrules() { return rids; } // ? deep copy
    public boolean isFinale() { return chr != 0; }
    public boolean test(char c) { return chr !=0 && chr == c; }

    public String toString() {
        if(chr != 0 && (rids == null || rids.isEmpty()))
            return String.format("%d: \"%c\"", id, chr);
        StringBuilder sb = new StringBuilder();
        sb.append(id);
        sb.append(": ");
        boolean sep = false;
        for(int[] altset : rids) {
            if(sep)
                sb.append(" |");
            else
                sep = true;
            for(int n : altset) {
                sb.append(' ');
                sb.append(n);
            }
        }
        return sb.toString();
    }

    public static Message19Rule fromString(final String src) {
        Matcher m = patChar.matcher(src);
        if(m.find())
            return new Message19Rule(Integer.parseInt(m.group(1)),
                                     m.group(2).charAt(0));
        m = patList.matcher(src);
        m.find();
        String[] rgs = m.group(2).split(" +\\| +");
        List<int[]> lrs = new ArrayList<>();
        for(String str : rgs) {
            int[] t = Arrays.stream(str.split(" +"))
                .mapToInt(Integer::parseInt).toArray();
            lrs.add(t);
        }
        return new Message19Rule(Integer.parseInt(m.group(1)), lrs);
    }
}

/*
class Rule19Tree { // ... 34?
    final int id; // 7:
    List<Rule19Tree[]> childs; // 1 2 | 3 4 5 | ...
    final char finale; // "あ"

    Rule19Tree() {
        id = -1;
        childs = new ArrayList<>();
        finale = 0;
    }

    public static Rule19Tree build(List<Message19Rule> ruleset) {
        return new Rule19Tree();
    }
}
*/
