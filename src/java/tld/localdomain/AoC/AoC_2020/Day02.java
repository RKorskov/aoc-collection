package tld.localdomain.AoC.AoC_2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 2: Password Philosophy
 *
 * Their password database seems to be a little corrupted: some of the
 * passwords wouldn't have been allowed by the Official Toboggan
 * Corporate Policy that was in effect when they were chosen.
 *
 * To try to debug the problem, they have created a list (your puzzle
 * input) of passwords (according to the corrupted database) and the
 * corporate policy when that password was set.
 *
 * Each line gives the password policy and then the password. The
 * password policy indicates the lowest and highest number of times a
 * given letter must appear for the password to be valid. For example,
 * 1-3 a means that the password must contain a at least 1 time and at
 * most 3 times.
 *
 * How many passwords are valid according to their policies?
 */
public class Day02 {
    public static void main(final String[] args) {
        PassPhil[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * 1-4 v: nvvv
     * 6-9 h: hhthplhgmpzsmhhxhh
     * 6-7 r: rrtrrrgrgcc
     * 10-15 h: sdbhvbhfjhwllmrpdv
     */
    private static PassPhil[] readParseInput(final String[] args) {
        List<PassPhil> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            loi.add(new PassPhil(src));
        }
        return loi.toArray(new PassPhil[0]);
    }

    /**
     * How many passwords are valid according to their policies?
     */
    private static int part1(final PassPhil[] src) {
        int cnt = 0;
        for(PassPhil pp : src) {
            int n = (int)(pp.pwd.chars().filter(c -> c == pp.chr).count());
            if(n >= pp.from && n <= pp.to)
                ++cnt;
        }
        return cnt;
    }

    /**
     * Each policy actually describes two positions in the password,
     * where 1 means the first character, 2 means the second
     * character, and so on. (Be careful; Toboggan Corporate Policies
     * have no concept of "index zero"!) Exactly one of these
     * positions must contain the given letter. Other occurrences of
     * the letter are irrelevant for the purposes of policy
     * enforcement.
     *
     * How many passwords are valid according to the new
     * interpretation of the policies?
     */
    private static int part2(final PassPhil[] src) {
        int cnt = 0;
        for(PassPhil pp : src) {
            final char c1 = pp.pwd.charAt(pp.from - 1);
            final char c2 = pp.pwd.charAt(pp.to - 1);
            final boolean f1 = c1 == pp.chr, f2 = c2 == pp.chr;
            if(f1^f2) ++cnt;
        }
        return cnt;
    }
}


/**
 * 1-4 v: nvvv
 * 6-9 h: hhthplhgmpzsmhhxhh
 * 6-7 r: rrtrrrgrgcc
 * 10-15 h: sdbhvbhfjhwllmrpdv
 */
class PassPhil {
    final String pwd;
    final char chr;
    final int from, to;

    final static Pattern pat = Pattern.compile("^([0-9]+)-([0-9]+) (.): (\\w+)$");

    PassPhil(String src) {
        Matcher m = pat.matcher(src);
        if(!m.find())
            System.out.println(src);
        from = Integer.parseInt(m.group(1));
        to = Integer.parseInt(m.group(2));
        chr = m.group(3).charAt(0);
        pwd = m.group(4);
    }
}
