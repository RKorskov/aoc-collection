package tld.localdomain.AoC.AoC_2020;

// import tld.localdomain.AoC.util.Utils;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Day 23: Crab Cups
 *
 * The cups will be arranged in a circle and labeled clockwise (your
 * puzzle input). For example, if your labeling were 32415, there
 * would be five cups in the circle; going clockwise around the circle
 * from the first cup, the cups would be labeled 3, 2, 4, 1, 5, and
 * then back to 3 again.
 *
 * Before the crab starts, it will designate the first cup in your
 * list as the current cup. The crab is then going to do 100 moves.
 *
 * Each move, the crab does the following actions:
 *
 * - The crab picks up the three cups that are immediately clockwise of
 *   the current cup. They are removed from the circle; cup spacing is
 *   adjusted as necessary to maintain the circle.
 *
 * - The crab selects a destination cup: the cup with a label equal to
 *   the current cup's label minus one. If this would select one of the
 *   cups that was just picked up, the crab will keep subtracting one
 *   until it finds a cup that wasn't just picked up. If at any point
 *   in this process the value goes below the lowest value on any cup's
 *   label, it wraps around to the highest value on any cup's label
 *   instead.
 *
 * - The crab places the cups it just picked up so that they are
 *   immediately clockwise of the destination cup. They keep the same
 *   order as when they were picked up.
 *
 * - The crab selects a new current cup: the cup which is immediately
 *   clockwise of the current cup.
 */
public class Day23 {
    public static void main(final String[] args) {
        String src = args[0];
        int n = args.length < 2 ? 100 : Integer.parseInt(args[1]);
        System.out.println(part1(src, n));
        System.out.println(part2(src, n));
    }

    /**
     * Using your labeling, simulate 100 moves.
     * What are the labels on the cups after cup 1?
     */
    private static String part1(final String cupSetup, final int turns) {
        D23CupRing ring = new D23CupRing(cupSetup);
        for(int i = 1; i <= turns; ++i) {
            ring.take();
            ring.selectDest();
            ring.place();
            ring.next();
        }
        return ring.tail1();
    }

    /**
     * the crab starts arranging many cups in a circle on your raft -
     * one million (1000000) in total.
     *
     * Your labeling is still correct for the first few cups; after
     * that, the remaining cups are just numbered in an increasing
     * fashion starting from the number after the highest number in
     * your list and proceeding one by one until one million is
     * reached. (For example, if your labeling were 54321, the cups
     * would be numbered 5, 4, 3, 2, 1, and then start counting up
     * from 6 until one million is reached.) In this way, every number
     * from one through one million is used exactly once.
     *
     * The crab is going to do ten million (10000000) moves!
     *
     * Determine which two cups will end up immediately clockwise of
     * cup 1. What do you get if you multiply their labels together?
     */
    private static long part2(final String cupSetup, final int turns) {
        System.out.println("optmize me!");
        // use indices of the array as the labels of the previous cup,
        // and the value as the label of the following cup
        // ... also...
        // Using a dict for storing the links between the numbers
        // requires only 3 updates per cycle instead of shifting a big
        // array.
        System.exit(-1);
        D23CupRing2 ring = new D23CupRing2(cupSetup);
        ring.extend(100_0000);
        for(int j = 1; j <= 1000; ++j) {
            System.out.println(j);
            for(int i = 1; i <= 10000; ++i) { // ~50s per loop
                ring.take();
                ring.selectDest();
                ring.place();
                ring.next();
            }
        }
        return ring.productAfter1();
    }
}

/**
 * ring buffer, quirky and unsafe implementation
 */
class D23CupRing {
    List<Integer> ring;
    // int cp; // current (cup) pointer
    int cpl; // current (cup) label
    int dest; // destination cup label
    int[] stash;

    D23CupRing() {
        ring = new LinkedList<>(); // deque, list, queue, and more!
        cpl = -1;
        dest = -1;
        stash = null;
    }

    D23CupRing(String seed) {
        this();
        seed.chars().map(c->c-'0').forEach(ring::add);
        cpl = ring.get(0); // Before the crab starts, it will designate the first cup in your list as the current cup.
    }

    /**
     * Your labeling is still correct for the first few cups; after
     * that, the remaining cups are just numbered in an increasing
     * fashion starting from the number after the highest number in
     * your list and proceeding one by one until one million is
     * reached. (For example, if your labeling were 54321, the cups
     * would be numbered 5, 4, 3, 2, 1, and then start counting up
     * from 6 until one million is reached.) In this way, every number
     * from one through one million is used exactly once.
     */
    public void extend(final int n) {
        int m = ring.stream().mapToInt(Integer::intValue).max().orElse(-1);
        IntStream.rangeClosed(m, n).forEach(ring::add);
    }

    /**
     * 1. The crab picks up the three cups that are immediately
     * clockwise of the current cup. They are removed from the circle;
     * cup spacing is adjusted as necessary to maintain the circle.
     */
    public void take() {
        stash = new int[3];
        final int cp = ring.indexOf(cpl) + 1;
        for(int i = 0; i < 3; ++i)
            stash[i] = ring.remove(cp < ring.size() ? cp : 0);
    }

    /**
     * 2. The crab selects a destination cup:
     * 2.1. the cup with a label equal to the current cup's label minus one.
     * 2.2. If this would select one of the cups that was just picked
     * up, the crab will keep subtracting one until it finds a cup
     * that wasn't just picked up.
     * 2.3. If at any point in this process the value goes below the
     * lowest value on any cup's label, it wraps around to the highest
     * value on any cup's label instead.
     */
    public void selectDest() {
        // fixme
        int dst = cpl - 1; // 2.1
        for(;;) {
            if(dst < 1) // 2.3
                dst = ring.stream().mapToInt(Integer::intValue).max().orElse(-1);
                    // .max((p,q) -> p==q?0: (p<q?-1:1)).get();
            if(isStashed(dst)) // 2.2
                --dst;
            else
                break;
        }
        dest = dst;
    }

    private boolean isStashed(final int val) {
        if(stash == null || stash.length < 3) return false;
        return stash[0] == val || stash[1] == val || stash[2] == val;
    }

    /**
     * 3. The crab places the cups it just picked up so that they are
     * immediately clockwise of the destination cup. They keep the
     * same order as when they were picked up.
     */
    public void place() {
        int i = 0;
        int dst = ring.indexOf(dest);
        if(dst < 0) {
            System.err.printf("Oops!\n dest %d [%d]\n ring %s\n stash %s\n",
                              dest, dst, ring.toString(), Arrays.toString(stash));
            System.exit(-1);
        }
        try {
            ring.add(++dst, stash[i++]);
            ring.add(++dst, stash[i++]);
            ring.add(++dst, stash[i]);
            // cleanup
            stash = null;
            dest = -1;
        }
        catch (Exception ex) {
            System.err.printf("Oops!\n dest %d [%d]\n ring %s\n stash %s\n",
                              dest, dst, ring.toString(), Arrays.toString(stash));
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * 0. Before the crab starts, it will designate the first cup in
     * your list as the current cup.
     *
     * 4. The crab selects a new current cup: the cup which is
     * immediately clockwise of the current cup.
     */
    public void next() {
        int cp = ring.indexOf(cpl) + 1;
        if(cp >= ring.size())
            cp = 0;
        cpl = ring.get(cp);
    }

    /**
     * for cups:  5 (8) 3  7  4  1  9  2  6,
     * the cups clockwise from 1 are labeled 9, 2, 6, 5, and so on,
     * producing 92658374.
     * @return cup labels on the cups after cup 1
     */
    public String tail1() {
        StringBuilder sb = new StringBuilder();
        for(int i = ring.indexOf(1) + 1;
            i >= ring.size() || ring.get(i) != 1;
            ++i) {
            if(i >= ring.size())
                i = -1;
            else
                sb.append(ring.get(i));
        }
        return sb.toString();
    }

    /**
     * Determine which two cups will end up immediately clockwise of
     * cup 1. What do you get if you multiply their labels together?
     */
    public long productAfter1() {
        final int i1 = ring.indexOf(1);
        return ((long)ring.get(i1+1)) * ring.get(i1+2);
    }

    public String toString() {
        return String.format("cups %s\n current (%s)\n stash %s\n dest %d",
                             ring.toString(), cpl,
                             (stash == null) ? "[]" : Arrays.toString(stash),
                             dest);
    }
}

/**
 * ring buffer, part|try 2
 *
 * use indices of the array as the labels of the previous cup, and the
 * value as the label of the following cup
 *
 * ... also...
 *
 * Using a dict for storing the links between the numbers requires
 * only 3 updates per cycle instead of shifting a big array
 */
class D23CupRing2 {
    List<Integer> ring;
    // int cp; // current (cup) pointer
    int cpl; // current (cup) label
    int dest; // destination cup label
    int[] stash;

    D23CupRing2() {
        ring = new LinkedList<>(); // deque, list, queue, and more!
        cpl = -1;
        dest = -1;
        stash = null;
    }

    D23CupRing2(String seed) {
        this();
        List<Integer> li = new ArrayList<>();
        seed.chars().map(c->c-'0').forEach(li::add);
        cpl = ring.get(0); // Before the crab starts, it will designate the first cup in your list as the current cup.
    }

    public void extend(final int i) {
    }

    public long productAfter1() {
        return 0;
    }

    public void take() {
    }

    public void selectDest() {
    }

    public void place() {
    }

    public void next() {
    }
}
