package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.stream.IntStream;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 10: Adapter Array
 *
 * If you use every adapter in your bag at once, what is the
 * distribution of joltage differences between the charging
 * outlet, the adapters, and your device?
 */
public class Day10 {
    public static void main(final String[] args) {
        int[] src = readParseInput(args);
        Arrays.sort(src);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     */
    private static int[] readParseInput(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        List<Integer> loi = new ArrayList<>();
        for(;sc.hasNext();) {
            try {
                int n = Integer.parseInt(sc.next());
                loi.add(n);
            }
            catch(Exception ex) {
                loi.add(0);
            }
        }
        return loi.stream().mapToInt(v->(int)v).toArray();
    }

    /**
     * Find a chain that uses all of your adapters to connect the
     * charging outlet to your device's built-in adapter and count the
     * joltage differences between the charging outlet, the adapters,
     * and your device. What is the number of 1-jolt differences
     * multiplied by the number of 3-jolt differences?
     *
     * @param adapters  sorted list of adapters (output joltage)
     */
    private static int part1(final int[] adapters) {
        int jolts = 0;
        int j1 = 0, j3 = 1; // build-in adapter have +3 difference
        for(int i = 0; i < adapters.length; ++i) {
            final int jdif = adapters[i] - jolts;
            if(jdif <= 3) {
                jolts = adapters[i];
                if(jdif == 1) ++j1;
                else if(jdif == 3) ++j3;
            }
        }
        return j1 * j3;
    }

    /**
     * To completely determine whether you have enough adapters,
     * you'll need to figure out how many different ways they can be
     * arranged. Every arrangement needs to connect the charging
     * outlet to your device. The previous rules about when adapters
     * can successfully connect still apply.
     *
     * What is the total number of distinct ways you can arrange the
     * adapters to connect the charging outlet to your device?
     */
    private static int part2(final int[] adapters) {
        return -1;
    }
}
