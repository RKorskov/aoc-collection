package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 5: Binary Boarding
 *
 */
public class Day05 {
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            if(src == null) continue;
            if(src.length() > 0)
                loi.add(src);
        }
        return loi.toArray(new String[0]);
    }

    /**
     * Instead of zones or groups, this airline uses binary space
     * partitioning to seat people. A seat might be specified like
     * `FBFBBFFRLR`, where `F` means "front", `B` means "back", `L`
     * means "left", and `R` means "right".
     *
     * As a sanity check, look through your list of boarding
     * passes. What is the highest seat ID on a boarding pass?
     */
    private static int part1(final String[] src) {
        int hsid = 0;
        for(String spp : src) {
            int sid = getSeatID(spp);
            if(sid > hsid)
                hsid = sid;
        }
        return hsid;
    }

    private static int getSeatID(final String passcode) {
        int sid = -1, row0 = 0, row1 = 127, col0 = 0, col1 = 7;
        for(char c : passcode.toCharArray()) {
            int dc = (col1 - col0 + 1) / 2,
                dr = (row1 - row0 + 1) / 2;
            switch(c) {
            case 'L':
                col1 -= dc;
                break;
            case 'R':
                col0 += dc;
                break;
            case 'F':
                row1 -= dr;
                break;
            case 'B':
                row0 += dr;
                break;
            }
        }
        sid = row0 * 8 + col0;
        return sid;
    }

    /**
     * It's a completely full flight, so your seat should be the only
     * missing boarding pass in your list. However, there's a catch:
     * some of the seats at the very front and back of the plane don't
     * exist on this aircraft, so they'll be missing from your list as
     * well.
     *
     * Your seat wasn't at the very front or back, though; the seats
     * with IDs +1 and -1 from yours will be in your list.
     *
     * What is the ID of your seat?
     */
    private static int part2(final String[] src) {
        int[] seats = Arrays.stream(src) //
            .mapToInt(s -> getSeatID(s)).sorted().toArray();
        for(int i = 1; i < seats.length; ++i)
            if((seats[i] - seats[i-1]) == 2)
                return seats[i] - 1;
        return -1;
    }
}
