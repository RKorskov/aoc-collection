package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 14: Docking Data
 */
public class Day14 {
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
     * mem[8] = 11
     * mem[7] = 101
     * mem[8] = 0
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            loi.add(src);
        }
        return loi.toArray(new String[0]);
    }

    private static Pattern patMask = Pattern.compile("^mask = ([10X]{36})$");
    private static Pattern patMem = Pattern.compile("^mem\\[(\\d+)\\] = (\\d+)$");

    /**
     * Execute the initialization program. What is the sum of all
     * values left in memory after it completes?
     */
    private static long part1(final String[] src) {
        Map<Integer,Long> mii = new HashMap<>();
        DockMask mask = null;
        for(String cmd : src) {
            Matcher m = patMask.matcher(cmd);
            if(m.find()) {
                mask = new DockMask(m.group(1));
                continue;
            }
            m = patMem.matcher(cmd);
            if(!m.find()) continue;
            final Integer addr = Integer.parseInt(m.group(1));
            long val = Long.parseLong(m.group(2));
            val = mask.apply(val);
            mii.put(addr, val);
        }
        return mii.values().stream().mapToLong(v->v).sum();
    }

    /**
     * Execute the initialization program using an emulator for a
     * version 2 decoder chip. What is the sum of all values left in
     * memory after it completes?
     */
    private static long part2(final String[] src) {
        return -1;
    }
}

class DockMask {
    final long maskOne, maskZero;

    DockMask(String mask) {
        long m0 = 0, m1 = 0;
        for(byte c : mask.getBytes()) {
            m1 = (m1 << 1);
            m0 = (m0 << 1);
            switch(c) {
            case '0':
                break;
            case '1':
                m1 |= 1;
                m0 |= 1;
                break;
            case 'X':
            default:
                m0 |= 1;
                break;
            }
        }
        maskOne  = m1;
        maskZero = m0;
    }

    public long apply(final long val) {
        return (val | maskOne) & maskZero;
    }

    public String toString() {
        return Long.toBinaryString(maskOne) + " " + Long.toBinaryString(maskZero);
    }
}
