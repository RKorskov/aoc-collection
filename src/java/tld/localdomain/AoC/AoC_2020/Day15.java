package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 15: Rambunctious Recitation
 *
 * Input data:
 * 5,1,9,18,13,8,0
 */
public class Day15 {
    public static void main(final String[] args) {
        int[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    private static int[] readParseInput(final String[] args) {
        List<Integer> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("[, ]");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            loi.add(Integer.parseInt(src));
        }
        return loi.stream().mapToInt(v->v).toArray(); // toArray(new int[0]);
    }

    /**
     * - If that was the first time the number has been spoken, the current player says 0.
     * - Otherwise, the number had been spoken before; the current player announces how many turns apart the number is from when it was previously spoken.
     *
     * Given your starting numbers, what will be the 2020th number spoken?
     */
    private static int part1(final int[] src) {
        Map<Integer,MemoRamRec> mim = new HashMap<>();
        int turnCount = 0;
        for(int n : src)
            mim.put(n, new MemoRamRec(++turnCount));
        int last = src[src.length - 1];
        ++turnCount;
        for(;turnCount <= 2020; ++turnCount) {
            MemoRamRec lns = mim.get(last);
            if(lns == null)
                last = 0;
            else
                last = lns.diff();
            lns = mim.get(last);
            if(lns == null)
                lns = new MemoRamRec(turnCount);
            else
                lns.update(turnCount);
            mim.put(last, lns);
        }
        return last;
    }

    /**
     * Given your starting numbers, what will be the 30000000th number spoken?
     */
    private static long part2(final int[] src) {
        Map<Long,MemoRamRecL> mim = new HashMap<>();
        long turnCount = 0;
        for(int n : src)
            mim.put((long)n, new MemoRamRecL(++turnCount));
        long last = src[src.length - 1];
        ++turnCount;
        for(;turnCount <= 3000_0000L; ++turnCount) {
            MemoRamRecL lns = mim.get(last);
            if(lns == null)
                last = 0;
            else
                last = lns.diff();
            lns = mim.get(last);
            if(lns == null)
                lns = new MemoRamRecL(turnCount);
            else
                lns.update(turnCount);
            mim.put(last, lns);
        }
        return last;
    }
}

class MemoRamRec {
    int last, before;

    MemoRamRec() { this(0,0); }

    MemoRamRec(int first) { this(first, first); }

    MemoRamRec(int last, int before) {
        this.last = last;
        this.before = before;
    }

    public void update(final int last) {
        this.before = this.last;
        this.last = last;
    }

    public int diff() { return last - before; }

    public String toString() { return String.format("[%d,%d]", before, last); }
}

class MemoRamRecL {
    long last, before;

    MemoRamRecL() { this(0,0); }

    MemoRamRecL(long first) { this(first, first); }

    MemoRamRecL(long last, long before) {
        this.last = last;
        this.before = before;
    }

    public void update(final long last) {
        this.before = this.last;
        this.last = last;
    }

    public long diff() { return last - before; }

    public String toString() { return String.format("[%d,%d]", before, last); }
}
