package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 6: Custom Customs
 *
 * The form asks a series of 26 yes-or-no questions marked `a` through
 * `z`. All you need to do is identify the questions for which anyone
 * in your group answers "yes".
 */
public class Day06 {
    public static void main(final String[] args) {
        List<String[]> src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     */
    private static List<String[]> readParseInput(final String[] args) {
        List<String[]> lor = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> acc = new ArrayList<>();
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src == null) continue;
            if(src.length() > 0)
                acc.add(src.trim());
            else {
                lor.add(acc.toArray(new String[0]));
                acc.clear(); // acc = new ArrayList<>();
            }
        }
        if(!acc.isEmpty())
            lor.add(acc.toArray(new String[0]));
        return lor;
    }

    /**
     * For each group, count the number of questions to which _anyone_
     * answered "yes". What is the sum of those counts?
     */
    private static int part1(final List<String[]> src) {
        int cnt = 0;
        for(String[] spp : src)
            cnt += Arrays.stream(spp) //
                .reduce("", (acc, str) -> acc + str) //
                .chars().distinct().count();
        return cnt;
    }

    /**
     * For each group, count the number of questions to which
     * _everyone_ answered "yes". What is the sum of those counts?
     */
    private static int part2(final List<String[]> src) {
        int cnt = 0;
        for(String[] spp : src)
            cnt += Integer.bitCount(
                                    Arrays.stream(spp)
                                    .mapToInt(str->str.chars()
                                              .reduce(0,(p,q)->p|(1<<(q-'a'))))
                                    .reduce(0xFFFFFFFF,(p,q)->p&q)
                                    );
        return cnt;
    }
}
