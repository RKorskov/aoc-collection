package tld.localdomain.AoC.AoC_2020;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

import java.util.*;

/**
 * After decoding the satellite messages, you discover that the data
 * actually contains many small images created by the satellite's
 * camera array. The camera array consists of many cameras; rather
 * than produce a single square image, they produce many smaller
 * square image tiles that need to be reassembled back into a single
 * image.
 *
 * Each camera in the camera array returns a single monochrome image
 * tile with a random unique ID number. The tiles (your puzzle input)
 * arrived in a random order.
 *
 * Worse yet, the camera array appears to be malfunctioning: each
 * image tile has been rotated and flipped to a random
 * orientation. Your first task is to reassemble the original image by
 * orienting the tiles so they fit together.
 *
 * To show how the tiles should be reassembled, each tile's image data
 * includes a border that should line up exactly with its adjacent
 * tiles. All tiles have this border, and the border lines up exactly
 * when the tiles are both oriented correctly. Tiles at the edge of
 * the image also have this border, but the outermost edges won't line
 * up with any other tiles.
 */
public class Day20 {
    public static void main(final String[] args) {
        String[] strs = readLines(args);
        Map<Integer,int[]> src = parseData(strs);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    private static String[] readLines(final String[] args) {
        List<String> los = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String str = sc.next().trim();
            if(str.length() > 0)
                los.add(str);
        }
        return los.toArray(new String[0]);
    }

    private static final Pattern patHeader = Pattern.compile("^Tile (\\d{4}):$"),
        patData = Pattern.compile("^[.#]{10}$");
    private static final int TILE_SIZE = 10;

    /**
     * Tile 1091:
     * ..#.##.##.
     * .........#
     * #.#......#
     * .###..#...
     * #.#.......
     * ##......##
     * ......#..#
     * #.#....#..
     * #...#....#
     * #####....#
     *
     * 1019: {
     * 0010110110b,
     * 0010110111b,
     * 1111100001b,
     * 0110011011b
     * } // edges
     */
    private static Map<Integer,int[]> parseData(final String[] src) {
        Map<Integer,int[]> tiles = new HashMap<>();
        List<String> rawdata = new ArrayList<>();
        int hid = -1;
        for(String str : src) {
            Matcher m = patHeader.matcher(str);
            if(m.find()) {
                if(hid >= 0) {
                    int[] tile = parseRawData(rawdata);
                    if(tile.length == TILE_SIZE)
                        tiles.put(hid, tile);
                    rawdata.clear();
                }
                hid = Integer.parseInt(m.group(1));
                continue;
            }
            m = patData.matcher(str);
            if(m.find() && hid >= 0)
                rawdata.add(str);
        }
        return tiles;
    }

    private static int[] parseRawData(List<String> rawdata) {
        int[] image = new int[rawdata.size()];
        for(int i = 0; i < rawdata.size(); ++i)
            // yech!
            image[i] = Integer.parseUnsignedInt(rawdata.get(i) //
                                                .replace('.', '0') //
                                                .replace('#', '1'), 2);
        return image;
    }

    private static int[] getEdges(final int[] image) {
        int[] edges = new int[4];
        edges[Tile20Grid.E_NORTH] = image[0];
        edges[Tile20Grid.E_SOUTH] = image[TILE_SIZE-1];
        for(int i = 0; i < TILE_SIZE; ++i) {
            edges[Tile20Grid.E_EAST] = //
                (edges[Tile20Grid.E_EAST] << 1) | (image[i] & 1);
            edges[Tile20Grid.E_WEST] |= (image[i] & 0x200) >> i;
        }
        return edges;
    }

    /**
     * Assemble the tiles into an image.
     * What do you get if you multiply together the IDs of the four
     * corner tiles?
     *
     * 0. допустим, что все тайлы однозначно соответствуют другим;
     * 1. взять тайл (T1) из очереди и использовать его в ядро матрицы;
     * 2. взять (Ti) из очереди;
     * 3. проверять ядро и Ti на взаимное прилегание, перебирая возможные комбинации из поворотов (4), отражений (4) и прилеганий (4) для Ti к свободным сторонам ядра;
     * 4.1. если Ti не сосед, то вернуть Ti в конец очереди;
     * 4.2. если Ti сосед, то добавть Ti в матрицу.
     * 5. если очередь не пуста, то шаг_2;
     */
    private static long part1(final Map<Integer,int[]> tiles) {
        Deque<Integer> deque = new ArrayDeque<>(); // free tiles queue
        Map<Integer,Tile20Grid> tiling = new HashMap<>(); // continuously placed tiles
        deque.addAll(tiles.keySet());
        int coreID = deque.pop();
        tiling.put(coreID, new Tile20Grid(coreID, tiles.get(coreID)));
        // WIP
        /*
         * 0. если deque пуста, то перейти к подсчёту произведения
         * 1. взять (следующий) фрагмент P из tiling (присоединённые фрагменты)
         * 2. если P не имеет вакантных сторон, то ш.1
         * 3. взять (следующий) фрагмент Q из deque (неприсоединившиеся фрагменты)
         * 4. если Q не является соседним с P, то поместить его в unfit (отложенные фрагменты)
         */
        for(;!deque.isEmpty();) {
            final int loop_protect = deque.size();
            Deque<Integer> unfit = new ArrayDeque<>(); // leftovers
            for(;!deque.isEmpty();) {
                for(int ctileID : tiling.keySet()) {
                    Tile20Grid ctile = tiling.get(ctileID);
                    if(!ctile.haveVacancy()) continue;
                    final int ntid = deque.pop();
                    final Tile20Grid candid = new Tile20Grid(ntid, tiles.get(ntid));
                    if(evalTileFitness(ctile, candid)) {
                        tiling.put(ntid, candid);
                        break;
                    }
                    unfit.push(ntid);
                }
            }
            if(loop_protect == unfit.size()) return -1; // error loop! nothing was changed
            deque = unfit; // copy?
        }
        return evalCornerProduct(tiling);
    }

    /**
     * What do you get if you multiply together the IDs of the four
     * corner tiles?
     */
    private static long evalCornerProduct(final Map<Integer,Tile20Grid> tiling) {
        long prod = 1;
        for(Tile20Grid tile: tiling.values()) {
            int cnt = 0;
            //for(Tile20Grid t: tile.links[i])
            for(int i = 0; i < 4; ++i)
                if(tile.getLink(i) == null)
                    ++cnt;
            if(cnt == 3)
                prod *= tile.getID();
        }
        return prod;
    }

    /**
     * if neighbor may be put adjacent any side of core, add it as link
     * @return true, if neighbor may be put adjacent core
     */
    private static boolean evalTileFitness(Tile20Grid core,
                                           Tile20Grid neighbor) {
        for(int a = 0; a < 2; ++a) { // flapping
            for(int i = 0; i < 2; ++i) { // flipping
                for(int r = 0; r < 4; ++r) { // rotating
                    if(core.fit(neighbor))
                        return true;
                    neighbor.rotate();
                }
                neighbor.flip();
            }
            neighbor.flap();
        }
        return false;
    }

    /**
     */
    private static int part2(final Map<Integer,int[]> src) {
        return -1;
    }
}

class Tile20Grid {
    final static public int E_NORTH = 0, E_EAST = 1, E_SOUTH = 2, E_WEST = 3;
    final private int tile_id;
    private short[] edges; // centered by `shl 3` to simplify mirroring code
    private Tile20Grid[] links; // 南, 北, 西, 东; // neighbour links

    Tile20Grid(int id) {
        this.tile_id = id;
        this.edges = new short[4];
        this.links = new Tile20Grid[4];
    }

    Tile20Grid(int id, int[] edges) {
        this(id);
        for(int i = 0; i < 4; ++i)
            this.edges[i] = (short)(edges[i] << 3);
    }

    public int getID() { return tile_id; }
    public int getEdge(final int dir) { return edges[dir]; }
    public Tile20Grid getLink(final int dir) { return links[dir]; }

    /**
     * @return true, if this tile have 4 neighbours
     */
    public boolean haveVacancy() {
        for(Tile20Grid t: links)
            if(t == null)
                return true;
        return false;
    }

    public void flip() { // 南北
        mirror(E_NORTH, E_SOUTH, E_EAST, E_WEST);
    }

    public void flap() { // 东西
        mirror(E_EAST, E_WEST, E_NORTH, E_SOUTH);
    }

    private void mirror(int i0, int i1, int i2, int i3) { // 南北 | 东西
        final int t = edges[i0];
        edges[i0] = edges[i1];
        edges[i1] = (short)t;
        edges[i2] = Utils.reverse16(edges[i2]);
        edges[i3] = Utils.reverse16(edges[i3]);
    }

    public void rotate() { // to left by pi/2
        final short t_south = edges[E_SOUTH];
        edges[E_SOUTH] = edges[E_WEST];
        edges[E_WEST]  = Utils.reverse16(edges[E_NORTH]);
        edges[E_NORTH] = edges[E_EAST];
        edges[E_EAST]  = Utils.reverse16(t_south);
    }

    /**
     * performing check if given tile may fit by some vacant side.
     * No tile transformation is performed.
     * If tiles fit, neighbour link updated
     */
    public boolean fit(Tile20Grid tile) {
        if(links[E_SOUTH] == null && edges[E_SOUTH] == tile.getEdge(E_NORTH)) {
            links[E_SOUTH] = tile;
            return true;
        }
        if(links[E_EAST] == null && edges[E_EAST] == tile.getEdge(E_WEST)) {
            links[E_EAST] = tile;
            return true;
        }
        if(links[E_NORTH] == null && edges[E_NORTH] == tile.getEdge(E_SOUTH)) {
            links[E_NORTH] = tile;
            return true;
        }
        if(links[E_WEST] == null && edges[E_WEST] == tile.getEdge(E_EAST)) {
            links[E_WEST] = tile;
            return true;
        }
        return false;
    }
}
