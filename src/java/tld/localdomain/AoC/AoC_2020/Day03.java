package tld.localdomain.AoC.AoC_2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 3: Toboggan Trajectory
 *
 * Due to the local geology, trees in this area only grow on exact
 * integer coordinates in a grid. You make a map (your puzzle input)
 * of the open squares (.) and trees (#) you can see.
 *
 * These aren't the only trees, though; due to something you read
 * about once involving arboreal genetics and biome stability, the
 * same pattern repeats to the right many times.
 *
 * You start on the open square (.) in the top-left corner and need to
 * reach the bottom (below the bottom-most row on your map).
 *
 * The toboggan can only follow a few specific slopes (you opted for a
 * cheaper model that prefers rational numbers); start by counting all
 * the trees you would encounter for the slope right 3, down 1:
 *
 * From your starting position at the top-left, check the position
 * that is right 3 and down 1. Then, check the position that is right
 * 3 and down 1 from there, and so on until you go past the bottom of
 * the map.
 *
 * Starting at the top-left corner of your map and following a slope
 * of right 3 and down 1, how many trees would you encounter?
 */
public class Day03 {
    public static void main(final String[] args) {
        char[][] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     */
    private static char[][] readParseInput(final String[] args) {
        List<char[]> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            if(src != null && src.length() > 0)
                loi.add(src.toCharArray());
        }
        return loi.toArray(new char[0][]);
    }

    /**
     * Starting at the top-left corner of your map and following a
     * slope of right 3 and down 1, how many trees would you
     * encounter?
     */
    private static int part1(final char[][] grid) {
        return countTrees(grid, 1, 3);
    }

    /**
     * Determine the number of trees you would encounter if, for each
     * of the following slopes, you start at the top-left corner and
     * traverse the map all the way to the bottom:
     *
     * - Right 1, down 1.
     * - Right 3, down 1. (This is the slope you already checked.)
     * - Right 5, down 1.
     * - Right 7, down 1.
     * - Right 1, down 2.
     *
     * What do you get if you multiply together the number of trees
     * encountered on each of the listed slopes?
     */
    private static long part2(final char[][] grid) {
        long cnt = countTrees(grid, 1, 1);
        cnt *= countTrees(grid, 1, 3);
        cnt *= countTrees(grid, 1, 5);
        cnt *= countTrees(grid, 1, 7);
        cnt *= countTrees(grid, 2, 1);
        return cnt;
    }

    private static int countTrees(final char[][] grid,
                                  final int dy, final int dx) {
        int cnt = 0;
        for(int x = 0, y = 0; y < grid.length; y+=dy, x=(x+dx)%grid[0].length)
            if(grid[y][x] == '#')
                ++cnt;
        return cnt;
    }
}
