package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 16: Ticket Translation
 */
public class Day16 {
    public static void main(final String[] args) {
        TicketData src = readParseInput(args);
        System.out.println(part1(src.ranges, src.yourTicket, src.nearbyTickets));
        System.out.println(part2(src.ranges, src.yourTicket, src.nearbyTickets));
    }

    /**
     * departure location: 42-570 or 579-960
     * zone: 30-629 or 637-968
     *
     * your ticket:
     * 79,149,97,163
     *
     * nearby tickets:
     * 945,134,238,221,801,428,519,660,523,537,481,156,845,873,52
     */
    private static TicketData readParseInput(final String[] args) {
        String[] src = readInput(args);
        List<Range2> lr2 = new ArrayList<>();
        List<Integer> utic = new ArrayList<>();
        List<int[]> nrtics = new ArrayList<>();
        boolean yourTicket = false;
        boolean nearbyTickets = false;
        for(String str : src) {
            if(str.length() < 1) continue;
            switch(str) {
            case "your ticket:":
                yourTicket = true;
                nearbyTickets = false;
                continue;
            case "nearby tickets:":
                yourTicket = false;
                nearbyTickets = true;
                continue;
            }
            if(yourTicket) {
                Arrays.stream(str.split(",")) //
                    .mapToInt(s->Integer.parseInt(s)) //
                    .forEach(utic::add);
                continue;
            }
            if(nearbyTickets) {
                nrtics.add(Arrays.stream(str.split(",")) //
                           .mapToInt(s->Integer.parseInt(s)) //
                           .toArray());
                continue;
            }
            // ranges always came first
            Range2 r2 = Range2.fromString(str);
            if(r2 != null) {
                lr2.add(r2);
                continue;
            }
        }
        final Range2[] ar2 = lr2.toArray(new Range2[0]);
        final int[] uta = utic.stream().mapToInt(v->v).toArray();
        final int[][] nts = nrtics.toArray(new int[0][]);
        // sanity check...
        if(ar2.length != uta.length)
            return null;
        for(int[] nt : nts)
            if(ar2.length != nt.length)
                return null;
        return new TicketData(ar2, uta, nts);
    }

    private static String[] readInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();)
            loi.add(sc.next().trim());
        return loi.toArray(new String[0]);
    }

    /**
     * Consider the validity of the nearby tickets you scanned. What
     * is your ticket scanning error rate?
     */
    private static int part1(final Range2[] ranges,
                             final int[] yourTicket,
                             final int[][] nearbyTickets) {
        int sum = 0;
        for(int[] tic : nearbyTickets) {
            for(int val : tic) {
                boolean vf = false;
                for(Range2 rng : ranges)
                    vf |= rng.check(val);
                if(!vf) {
                    sum += val;
                    break;
                }
            }
        }
        return sum;
    }

    /**
     * Use the remaining valid tickets to determine which field is which.
     *
     * Using the valid ranges for each field, determine what order the
     * fields appear on the tickets. The order is consistent between
     * all tickets: if seat is the third field, it is the third field
     * on every ticket, including your ticket.
     *
     * Once you work out which field is which, look for the six fields
     * on your ticket that start with the word departure. What do you
     * get if you multiply those six values together?
     */
    private static int part2(final Range2[] ranges,
                             final int[] yourTicket,
                             final int[][] nearbyTickets) {
        return -1;
    }
}

class TicketData {
    final Range2[] ranges;
    final int[] yourTicket;
    final int[][] nearbyTickets;

    TicketData(Range2[] ranges, int[] yourTicket, int[][] nearbyTickets) {
        this.ranges = ranges;
        this.yourTicket = yourTicket;
        this.nearbyTickets = nearbyTickets;
    }

    public String toString() {
        return String.format("");
    }
}

/**
 * zone: 30-629 or 637-968
 */
class Range2 {
    final String name;
    final int rh0, rh1, rt2, rt3;

    private static Pattern pat = Pattern.compile("^([a-z ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)$");

    Range2(String name, int r0, int r1, int r2, int r3) {
        this.name = name;
        rh0 = r0;
        rh1 = r1;
        rt2 = r2;
        rt3 = r3;
    }

    public boolean check(final int val) {
        return rh0 <= val && val <= rh1 || rt2 <= val && val <= rt3;
    }

    public String toString() {
        return String.format("%s : %d %d : %d %d", name, rh0, rh1, rt2, rt3);
    }

    public static Range2 fromString(final String src) {
        Matcher m = pat.matcher(src);
        if(m.find())
            return new Range2(m.group(1),
                              Integer.parseInt(m.group(2)),
                              Integer.parseInt(m.group(3)),
                              Integer.parseInt(m.group(4)),
                              Integer.parseInt(m.group(5)));
        else
            return null; // exception?
    }
}

