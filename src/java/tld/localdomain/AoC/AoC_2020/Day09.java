package tld.localdomain.AoC.AoC_2020;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.stream.IntStream;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 9: Encoding Error
 */
public class Day09 {
    public static void main(final String[] args) {
        XMASData src = readParseInput(args);
        System.out.println(part1(src.data, src.preambleLength));
        System.out.println(part2(src.data, src.preambleLength));
    }

    /**
     * XMAS starts by transmitting a preamble of 25 numbers. After
     * that, each number you receive should be the sum of any two of
     * the 25 immediately previous numbers. The two numbers will have
     * different values, and there might be more than one such pair.
     */
    private static XMASData readParseInput(final String[] args) {
        final int limitSkip = args.length > 1 ? Integer.parseInt(args[1]) : 25;
        Scanner sc = Utils.getScanner(args);
        List<Integer> loi = new ArrayList<>();
        //sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            try {
                int n = Integer.parseInt(sc.next());
                loi.add(n);
            }
            catch(Exception ex) {;}
        }
        return new XMASData(loi.stream().mapToInt(v->(int)v).toArray(),
                            limitSkip);
    }

    /**
     * The first step of attacking the weakness in the XMAS data is to
     * find the first number in the list (after the preamble) which is
     * not the sum of two of the 25 numbers before it. What is the
     * first number that does not have this property?
     */
    private static int part1(final int[] src, final int preambleLength) {
        for(int k = preambleLength; k < src.length; ++k) {
            int n = src[k];
            boolean isNotIn = true;
            factoring:
            for(int i = k - preambleLength; i < (k - 1); ++i) {
                for(int j = i + 1; j < k; ++j) {
                    if(i == j) continue;
                    if(src[i] + src[j] == n) {
                        isNotIn = false;
                        break factoring;
                    }
                }
            }
            if(isNotIn)
                return n;
        }
        return -1;
    }

    /**
     * The final step in breaking the XMAS encryption relies on the
     * invalid number you just found: you must find a contiguous set
     * of at least two numbers in your list which sum to the invalid
     * number from step 1.
     *
     * To find the encryption weakness, add together the smallest and
     * largest number in this contiguous range;
     *
     * What is the encryption weakness in your XMAS-encrypted list of
     * numbers?
     */
    private static int part2(final int[] src, final int preambleLength) {
        final int n = part1(src, preambleLength);
        if(n < 0) return -1;
        for(int i = 0; i < (src.length-1); ++i) {
            int sum = src[i];
            for(int j = i+1; j < src.length && sum < n; ++j) {
                sum += src[j];
                // if(sum > n) break; // {sum -= src[i]; ++i; if(i == j) ++j; if(j >= src.length) break;}
                if(sum == n)
                    return rangeMin(src, i, j) + rangeMax(src, i, j);
            }
        }
        return -1;
    }

    private static int rangeMin(final int[] src, final int p, final int q) {
        int rm = src[p];
        for(int i = p+1; i <= q; ++i)
            rm = Math.min(rm, src[i]);
        return rm;
    }

    private static int rangeMax(final int[] src, final int p, final int q) {
        int rm = src[p];
        for(int i = p+1; i <= q; ++i)
            rm = Math.max(rm, src[i]);
        return rm;
    }
}

final class XMASData {
    final int[] data;
    final int preambleLength;
    XMASData(int[] data, int preambleLength) {
        this.data = data;
        this.preambleLength = preambleLength;
    }
}
