package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 12: Rain Risk
 *
 * The navigation instructions (your puzzle input) consists of a
 * sequence of single-character actions paired with integer input
 * values. After staring at them for a few minutes, you work out what
 * they probably mean:
 *
 *  Action N means to move north by the given value.
 *  Action S means to move south by the given value.
 *  Action E means to move east by the given value.
 *  Action W means to move west by the given value.
 *  Action L means to turn left the given number of degrees.
 *  Action R means to turn right the given number of degrees.
 *  Action F means to move forward by the given value in the direction
 *  the ship is currently facing.
 *
 * The ship starts by facing east. Only the L and R actions change the
 * direction the ship is facing. (That is, if the ship is facing east
 * and the next instruction is N10, the ship would move north 10
 * units, but would still move east if the following action were F.)
 */
public class Day12 {
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * L180 L270 L90 R180 R270 R90
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        //sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            loi.add(src);
        }
        return loi.toArray(new String[0]);
    }

    /**
     * Figure out where the navigation instructions lead.  What is the
     * Manhattan distance between that location and the ship's
     * starting position?
     */
    private static int part1(final String[] src) {
        FerryShip ship = new FerryShip();
        for(String cmd : src)
            ship.move(cmd);
        return Math.abs(ship.posX) + Math.abs(ship.posY);
    }

    /**
     * Almost all of the actions indicate how to move a waypoint which
     * is relative to the ship's position.
     *
     * The waypoint starts 10 units east and 1 unit north relative to
     * the ship. The waypoint is relative to the ship; that is, if the
     * ship moves, the waypoint moves with it.
     *
     * Figure out where the navigation instructions actually
     * lead. What is the Manhattan distance between that location and
     * the ship's starting position?
     */
    private static int part2(final String[] src) {
        return -1;
    }
}

class FerryShip {
    int posX, posY;
    int dir; // (L|R){90,180,270}
    int wpx, wpy;

    FerryShip() {
        posX = 0;
        posY = 0;
        dir = 0;
        wpx = 10;
        wpy = 1;
    }

    public String toString() {
        return String.format("%d %d  %d", posX, posY, dir);
    }

    public void move(final String cmd) {
        final int dist = Integer.parseInt(cmd.substring(1));
        switch(cmd.charAt(0)) {
        case 'N':
            posY += dist;
            break;
        case 'S':
            posY -= dist;
            break;
        case 'E':
            posX += dist;
            break;
        case 'W':
            posX -= dist;
            break;
        case 'L':
            dir += dist;
            break;
        case 'R':
            dir -= dist;
            break;
        case 'F':
            switch(dir) {
            case 0:
                posX += dist;
                break;
            case 90:
                posY += dist;
                break;
            case 180:
                posX -= dist;
                break;
            case 270:
                posY -= dist;
                break;
            }
            break;
        }
        dir %= 360;
        if(dir < 0) dir += 360;
    }

    public void move2waypoint(final String cmd) {
        final int dist = Integer.parseInt(cmd.substring(1));
        switch(cmd.charAt(0)) {
        case 'N':
            posY += dist;
            break;
        case 'S':
            posY -= dist;
            break;
        case 'E':
            posX += dist;
            break;
        case 'W':
            posX -= dist;
            break;
        case 'L':
            dir += dist;
            break;
        case 'R':
            dir -= dist;
            break;
        case 'F':
            switch(dir) {
            case 0:
                posX += dist;
                break;
            case 90:
                posY += dist;
                break;
            case 180:
                posX -= dist;
                break;
            case 270:
                posY -= dist;
                break;
            }
            break;
        }
        dir %= 360;
        if(dir < 0) dir += 360;
    }
}
