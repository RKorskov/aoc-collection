package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 7: Handy Haversacks
 *
 * Due to recent aviation regulations, many rules (your puzzle input)
 * are being enforced about bags and their contents; bags must be
 * color-coded and must contain specific quantities of other
 * color-coded bags.
 *
 * You have a shiny gold bag. If you wanted to carry it in at least
 * one other bag, how many different bag colors would be valid for the
 * outermost bag? (In other words: how many colors can, eventually,
 * contain at least one shiny gold bag?)
 */
public class Day07 {
    public static void main(final String[] args) {
        BagRule[] src = readParseInput(args);
        final String root = "shiny gold"; // ? arg1
        System.out.println(part1(root, src));
        System.out.println(part2(root, src));
    }

    /**
     * drab tan bags contain 4 clear gold bags.
     * vibrant lime bags contain 3 faded gold bags, 3 plaid aqua bags, 2 clear black bags.
     * dotted black bags contain no other bags.
     */
    private static BagRule[] readParseInput(final String[] args) {
        List<BagRule> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            BagRule rule = BagRule.create(src);
            if(rule != null)
                loi.add(rule);
        }
        return loi.toArray(new BagRule[0]);
    }

    /**
     * How many bag colors can eventually contain at least one shiny
     * gold bag? (The list of rules is quite long; make sure you get
     * all of it.)
     */
    private static int part1(final String root, final BagRule[] src) {
        T18Tree<String> tree = buildBagTree(root, src);
        return countLeaves(tree);
    }

    private static T18Tree<String> buildBagTree(final String rootPayload,
                                               final BagRule[] ruleSrc) {
        T18Tree<String> root = new T18Tree<>(rootPayload);
        for(BagRule rule : ruleSrc) {
            if(rule.isContain(rootPayload)) {
                T18Tree<String> child;
                child = buildBagTree(rule.color, ruleSrc);
                root.add(child);
            }
        }
        return root;
    }

    private static int countLeaves(final T18Tree<String> root) {
        int lcnt = 0;
        for(T18Tree<String> child : root.getChilds()) {
            if(child.isLeaf())
                ++lcnt;
            else
                lcnt += countLeaves(child);
        }
        return lcnt;
    }

    /**
     */
    private static int part2(final String root, final BagRule[] src) {
        int bagCount = 0;
        return bagCount;
    }
}

/**
 * drab tan bags contain 4 clear gold bags.
 * vibrant lime bags contain 3 faded gold bags, 3 plaid aqua bags, 2 clear black bags.
 * dotted black bags contain no other bags.
 */
class BagRule {
    final String color;
    final Map<String,Integer> ruleset; // color : amount

    private BagRule() {
        this(null, null);
    }

    private BagRule(String bagColor) {
        this(bagColor, null);
    }

    private BagRule(String bagColor, Map<String,Integer> rules) {
        this.color = bagColor;
        this.ruleset = rules;
    }

    public boolean isContain(final String bagColor) {
        if(ruleset == null || ruleset.isEmpty()) return false;
        return ruleset.keySet().stream().anyMatch(clr -> clr.equals(bagColor));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(color);
        sb.append(" : {");
        for(String k : ruleset.keySet()) {
            sb.append(k);
            sb.append(' ');
            sb.append(ruleset.get(k));
            sb.append(", ");
        }
        sb.delete(sb.length() - 2, sb.length()); // fixme
        sb.append('}');
        return sb.toString();
    }

    private static final Pattern //
        patBag = Pattern.compile("^([a-z ]+) bags contain .+.$"),
        patRule = Pattern.compile("(\\d) ([a-z ]+) bags[,.]"),
        patTerm = Pattern.compile("no other bags");

    public static BagRule create(final String src) {
        Matcher m = patBag.matcher(src);
        if(!m.find()) return null; // exception?
        String bagColor = m.group(1);
        m = patTerm.matcher(src);
        if(m.find())
            return new BagRule(bagColor);
        Map<String,Integer> rules = new HashMap<>(); // color, amount
        m = patRule.matcher(src);
        for(;m.find();)
            rules.put(m.group(1), Integer.parseInt(m.group(2)));
        return new BagRule(bagColor, rules);
    }
}

/**
 * n-ary tree with given payload.
 * naive implementation :/
 */
class T18Tree <T> {
    private T18Tree<T> parent;
    private final List<T18Tree<T>> childs;
    private final T payload;

    T18Tree(T payload) {
        parent = null;
        childs = new ArrayList<>();
        this.payload = payload;
    }

    T18Tree() { this(null); }

    public void setParent(T18Tree<T> parent) { this.parent = parent; }
    public T18Tree<T> getParent() { return parent; }
    public T getPayload() { return payload; }
    public List<T18Tree<T>> getChilds() { return childs; }
    public boolean isLeaf() { return childs == null || childs.isEmpty();}
    public boolean isRoot() { return parent == null; }

    public void add(T18Tree<T> child) {
        childs.add(child);
        child.setParent(this);
    }

    public void add(T payload) {
        T18Tree<T> child = new T18Tree<>(payload);
        childs.add(child);
        child.setParent(this);
    }
}
