package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 11: Seating System
 */
public class Day11 {
    public static void main(final String[] args) {
        SeatState[][] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * L.LLLLL.LL
     * ..L.L.....
     */
    private static SeatState[][] readParseInput(final String[] args) {
        List<SeatState[]> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            SeatState[] lss = new SeatState[src.length()];
            for(int i = 0; i < src.length(); ++i)
                lss[i] = SeatState.fromChar(src.charAt(i));
            loi.add(lss);
        }
        return loi.toArray(new SeatState[0][]);
    }

    /**
     * Simulate your seating area by applying the seating rules
     * repeatedly until no seats change state. How many seats end up
     * occupied?
     */
    private static int part1(final SeatState[][] src) {
        SeatState[][] grid = new SeatState[src.length][];
        for(int i = 0; i < src.length; ++i)
            grid[i] = Arrays.copyOf(src[i], src[i].length);
        int sum = 0;
        for(boolean wasChanged = true; wasChanged;) {
            wasChanged = false;
            sum = 0;
            SeatState[][] ng = new SeatState[grid.length][];
            for(int i = 0; i < grid.length; ++i)
                ng[i] = new SeatState[grid[i].length];
            for(int j = 0; j < grid.length; ++j) {
                for(int i = 0; i < grid[j].length; ++i) {
                    ng[j][i] = checkIn(grid, i, j);
                    wasChanged |= ng[j][i] != grid[j][i];
                    if(ng[j][i] == SeatState.OCCUPIED)
                        ++sum;
                }
            }
            grid = ng; // ?swap
            // printGrid(grid); System.out.println();
        }
        return sum;
    }

    /**
     * All decisions are based on the number of occupied seats
     * adjacent to a given seat (one of the eight positions
     * immediately up, down, left, right, or diagonal from the
     * seat). The following rules are applied to every seat
     * simultaneously:
     *
     * - If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
     * - If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
     * - Otherwise, the seat's state does not change.
     *
     * Floor (.) never changes; seats don't move, and nobody sits on
     * the floor.
     */
    private static SeatState checkIn(final SeatState[][] grid,
                                     final int x, final int y) {
        switch(grid[y][x]) {
        case FLOOR:
            return SeatState.FLOOR;
        case EMPTY:
            if(seatCount(grid, x, y) == 0)
                return SeatState.OCCUPIED;
            else
                return SeatState.EMPTY;
        case OCCUPIED:
            if(seatCount(grid, x, y) > 3)
                return SeatState.EMPTY;
            else
                return SeatState.OCCUPIED;
        }
        return null;
    }

    /**
     * All decisions are based on the number of occupied seats
     * adjacent to a given seat (one of the eight positions
     * immediately up, down, left, right, or diagonal from the
     * seat).
     */
    private static int seatCount(final SeatState[][] grid,
                                 final int x, final int y) {
        int sum = 0;
        // loop or iff...? me dunno. yff.
        if(get(grid, y+1, x+0) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y-1, x+0) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y+0, x+1) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y+0, x-1) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y+1, x+1) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y-1, x-1) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y-1, x+1) == SeatState.OCCUPIED)
            ++sum;
        if(get(grid, y+1, x-1) == SeatState.OCCUPIED)
            ++sum;
        return sum;
    }

    private static SeatState get(final SeatState[][] grid,
                                 final int y, final int x) {
        if(y < 0 || y >= grid.length || x < 0 || x >= grid[y].length)
            return SeatState.FLOOR;
        return grid[y][x];
    }

    private static void printGrid(final SeatState[][] grid) {
        for(int j = 0; j < grid.length; ++j) {
            for(int i = 0; i < grid[j].length; ++i) {
                char c;
                switch(grid[j][i]) {
                case EMPTY:
                    c = 'L';
                    break;
                case OCCUPIED:
                    c = '#';
                    break;
                //case FLOOR:
                default:
                    c = '.';
                }
                System.out.print(c);
            }
            System.out.println();
        }
    }

    /**
     */
    private static int part2(final SeatState[][] src) {
        return -1;
    }
}

enum SeatState {
    FLOOR, EMPTY, OCCUPIED;

    public static SeatState fromChar(final char src) {
        switch(src) {
        case '.':
            return FLOOR;
        case 'L':
            return EMPTY;
        case '#':
            return OCCUPIED;
        }
        return null;
    }
}
