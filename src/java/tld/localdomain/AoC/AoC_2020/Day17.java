package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 17: Conway Cubes
 *
 * During a cycle, all cubes simultaneously change their state
 * according to the following rules:
 *
 * - If a cube is active and exactly 2 or 3 of its neighbors are also
 *   active, the cube remains active. Otherwise, the cube becomes
 *   inactive.
 * - If a cube is inactive but exactly 3 of its neighbors are active,
 *   the cube becomes active. Otherwise, the cube remains inactive.
 *
 * The engineers responsible for this experimental energy source would
 * like you to simulate the pocket dimension and determine what the
 * configuration of cubes should be at the end of the six-cycle boot
 * process.
 */
public class Day17 {
    public static void main(final String[] args) {
        String[] src = readLines(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * .###..#.
     */
    private static Cell3D[] readData(final String[] src) {
        List<Cell3D> cmap = new ArrayList<>();
        String[] data = readLines(src);
        for(int y = 0; y < data.length; ++y)
            for(int x = 0; x < data[y].length(); ++x)
                if(data[y].charAt(x) == '#')
                    cmap.add(new Cell3D(x, y, 0));
        return cmap.toArray(new Cell3D[0]);
    }

    private static String[] readLines(final String[] args) {
        List<String> los = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();)
            los.add(sc.next().trim());
        return los.toArray(new String[0]);
    }

    /**
     * Starting with your given initial configuration, simulate six
     * cycles. How many cubes are left in the active state after the
     * sixth cycle?
     */
    private static long part1(final String[] src) {
        Cell3D[] startCells = readData(src);
        Grid3D grid3d = new Grid3D(startCells);
        for(int rep = 0; rep < 6; ++rep)
            grid3d.update();
        return grid3d.count();
    }

    /**
     */
    private static int part2(final String[] src) {
        return -1;
    }
}

class Cell3D {
    final int x, y, z;

    Cell3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

enum CubeState {
    ACTIVE, INACTIVE
}

class Grid3D {
    public static final int[] NEIGHBORS = {0,1, 1,1, 1,0, 1,-1, 0,-1, -1,-1, -1,0, -1,1};

    Map<Cell3D,CubeState> grid3d;
    private int step;
    private boolean step_complete;

    Grid3D() {
        grid3d = new HashMap<>();
        step = 0;
        step_complete = true;
    }

    Grid3D(Cell3D[] cells) {
        this();
        for(Cell3D cell: cells)
            grid3d.put(cell, CubeState.ACTIVE);
    }

    /**
     * removes inactive cubes
     */
    public void clean() {
        for(Cell3D cell: grid3d.keySet())
            if(grid3d.get(cell) == CubeState.INACTIVE)
                grid3d.remove(cell);
    }

    public boolean get(final Cell3D cell) {
        return grid3d.get(cell) == CubeState.ACTIVE;
    }

    public boolean get(final int x, final int y, final int z) {
        return grid3d.get(new Cell3D(x, y, z)) == CubeState.ACTIVE;
    }

    public Collection<CubeState> getValues() {
        return grid3d.values();
    }

    public void set(final Cell3D cell, final CubeState state) {
        grid3d.put(cell, state);
    }

    public void set(final int x, final int y, final int z,
                    final CubeState state) {
        grid3d.put(new Cell3D(x, y, z), state);
    }

    public long count() {
        return grid3d.values().stream().filter(v->v == CubeState.ACTIVE).count();
    }

    public void update() {
        ++step;
        step_complete = false;
        Map<Cell3D,CubeState> nmap = new HashMap<>();
        for(Cell3D cell : grid3d.keySet()) {
            nmap.put(cell, testCell(cell) ? CubeState.ACTIVE : CubeState.INACTIVE);
            // how about new inactive to active cells?
            if(get(cell)) {
                List<Cell3D> newBorns = lookAround(cell);
                for(Cell3D newCell : newBorns)
                    nmap.put(newCell, CubeState.ACTIVE);
            }
        }
        grid3d = nmap;
        step_complete = true;
    }

    /**
     * - If a cube is active and exactly 2 or 3 of its neighbors are also
     *   active, the cube remains active. Otherwise, the cube becomes
     *   inactive.
     * - If a cube is inactive but exactly 3 of its neighbors are active,
     *   the cube becomes active. Otherwise, the cube remains inactive.
     */
    public boolean testCell(final Cell3D cell) { // not a best name at all...
        int count = 0;
        for(int n = 0; n < NEIGHBORS.length; n += 2) {
            final int x = cell.x + NEIGHBORS[n],
                y = cell.y + NEIGHBORS[n+1];
            for(int z = -1; z < 2; ++z) {
                // todo
                Cell3D c3d = new Cell3D(x, y, z);
                if(get(c3d))
                    ++count;
            }
            if(get(cell.x, cell.y, cell.z - 1)) ++count;
            if(get(cell.x, cell.y, cell.z + 1)) ++count;
        }
        return count == 3 || get(cell) && count == 2;
    }

    /**
     * implementation of rule2:
     * - If a cube is inactive but exactly 3 of its neighbors are active,
     *   the cube becomes active. Otherwise, the cube remains inactive.
     *
     * @return array of cells with new active cubes
     */
    private List<Cell3D> lookAround(final Cell3D cell) {
        List<Cell3D> ncd = new ArrayList<>();
        // todo
        for(int n = 0; n < Grid3D.NEIGHBORS.length; n += 2) {
            final int x = cell.x + Grid3D.NEIGHBORS[n],
                    y = cell.y + Grid3D.NEIGHBORS[n+1];
            for(int z = -1; z < 2; ++z) {
                Cell3D c3d = new Cell3D(x, y, z);
                if(testCell(c3d))
                    ncd.add(c3d);
            }
        }
        return ncd;
    }
}
