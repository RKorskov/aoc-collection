package tld.localdomain.AoC.AoC_2020;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Day 21: Allergen Assessment
 *
 * You don't speak the local language, so you can't read any
 * ingredients lists. However, sometimes, allergens are listed in a
 * language you do understand. You should be able to use this
 * information to determine which ingredient contains which allergen
 * and work out which foods are safe to take with you on your trip.
 *
 * You start by compiling a list of foods (your puzzle input), one
 * food per line. Each line includes that food's ingredients list
 * followed by some or all of the allergens the food contains.
 *
 * Each allergen is found in exactly one ingredient. Each ingredient
 * contains zero or one allergen. Allergens aren't always marked; when
 * they're listed (as in (contains nuts, shellfish) after an
 * ingredients list), the ingredient that contains each listed
 * allergen will be somewhere in the corresponding ingredients
 * list. However, even if an allergen isn't listed, the ingredient
 * that contains that allergen could still be present: maybe they
 * forgot to label it, or maybe it was labeled in a language you don't
 * know.
 *
 * Determine which ingredients cannot possibly contain any of the
 * allergens in your list. How many times do any of those ingredients
 * appear?
 */
public class Day21 {
    public static void main(final String[] args) {
        String[] strs = readLines(args);
        D21Menu src = parseInput(strs);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
     */
    private static String[] readLines(final String[] args) {
        List<String> los = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String str = sc.next().trim();
            if (str.length() > 0)
                los.add(str);
        }
        return los.toArray(new String[0]);
    }

    /**
     * mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
     */
    private static D21Menu parseInput(final String[] src) {
        D21Menu foods = new D21Menu();
        for(String str: src)
            try {
                foods.add(new D21Dish(str));
            }
            catch (Exception ex) {
                System.err.println(str);
            }
        return foods;
    }

    /**
     * Determine which ingredients cannot possibly contain any of the
     * allergens in your list. How many times do any of those
     * ingredients appear?
     *
     * looks like a some sort of Einstein's puzzle
     */
    private static int part1(final D21Menu foods) {
        D21Matrix dict = distinctize(foods);
        for(D21Dish dish: foods.dishes)
            dict.updateA2I(dish);
        dict.filterA2I();
        dict.print();
        int cnt = 0;
        for(String iname: dict.ingredients) {
            String a = dict.getAllergen(iname);
            System.out.printf("%s : %s\n", iname, a);
            if(a == null || a.equals(""))
                for(D21Dish dish: foods.dishes)
                    if(dish.ingredients.contains(iname))
                        ++cnt;
        }
        return cnt;
    }

    /**
     * leaves only non-duplicated occurences
     * @return ingredients - allergens matrix
     */
    private static D21Matrix distinctize(final D21Menu src) {
        List<String> acc0 = new ArrayList<>(); // ingredients
        List<String> acc1 = new ArrayList<>(); // allergens
        for(D21Dish t: src.dishes) {
            acc0.addAll(t.ingredients);
            acc1.addAll(t.allergens);
        }
        acc0 = acc0.stream().distinct().collect(Collectors.toList());
        acc1 = acc1.stream().distinct().collect(Collectors.toList());
        return new D21Matrix(acc0, acc1);
    }

    /**
     * Arrange the ingredients alphabetically by their allergen and
     * separate them by commas to produce your canonical dangerous
     * ingredient list. (There should not be any spaces in your
     * canonical dangerous ingredient list.) In the above example,
     * this would be mxmxvkd,sqjhc,fvjkl.
     *
     * Time to stock your raft with supplies. What is your canonical
     * dangerous ingredient list?
     */
    private static int part2(final D21Menu foods) {
        return -1;
    }
}

class D21Matrix {
    final List<String> ingredients, allergens;
    Map<String,String> dictI2A; // mappings of ingredient to allergen
    Map<String,Set<String>> dictA2I; // mappings of allergen to ingredient dictionary
    //boolean[][] grid;

    D21Matrix() {
        ingredients = new ArrayList<>();
        allergens = new ArrayList<>();
        dictI2A = new HashMap<>();
        dictA2I = new HashMap<>();
    }

    D21Matrix(List<String> ings, List<String> alls) {
        this();
        if(ings != null)
            ingredients.addAll(ings);
        if(alls != null)
            allergens.addAll(alls);
        // for(String key: ings) dictI2A.put(key, null); // fill it later, on 1st call of getAllergen
        for(String key: alls)
            dictA2I.put(key, new HashSet<>());
    }

    /**
     * updating A2I dictionary, according to given dish.
     * contracting list of possible namings of given allergen
     */
    public void updateA2I(D21Dish dish) {
        for(String key: dish.allergens) {
            Set<String> src = dictA2I.get(key);
            if(src.isEmpty()) {
                src.addAll(dish.ingredients);
                continue;
            }
            if(src.size() == 1)
                continue;
            // actual update by ANDing of src and dish.allergens
            Set<String> res = new HashSet<>();
            for(String str: dish.ingredients)
                if(src.contains(str))
                    res.add(str);
            if(src.size() > res.size())
                dictA2I.put(key, res);
        }
    }

    /**
     * filtering list of allergen namings by removing already known
     * names from set of possibilities
     */
    public void filterA2I() {
        Set<String> aknown = new HashSet<>(); // allergens with known names
        Set<String> miria = null; // allergens with unknown names
        Set<String> keys = null;
        for(miria = dictA2I.keySet(); !miria.isEmpty();) {
            keys = miria;
            miria = new HashSet<>();
            for (String key : keys) {
                if (dictA2I.get(key).size() == 1)
                    aknown.add(key);
                else
                    miria.add(key);
            }
            for(String akn: aknown) {
                String name = dictA2I.get(akn).iterator().next(); // ugh!
                for(String alr: miria) {
                    Set<String> set = dictA2I.get(alr);
                    if(set.contains(name))
                        set.remove(name);
                }
            }
        }
    }

    /**
     * reverse mappings
     */
    private void buildI2A() {
        for(String iname: ingredients)
            dictI2A.put(iname, "");
        for(String aname: dictA2I.keySet()) {
            final String iname = dictA2I.get(aname).iterator().next();
            dictI2A.put(iname, aname);
        }
    }

    public String getAllergen(String ingredient) {
        if(dictI2A.isEmpty())
            buildI2A();
        return dictI2A.get(ingredient);
    }

    public void print() {
        for(String key: dictA2I.keySet())
            System.out.printf("%s : %s\n", key, dictA2I.get(key).toString());
    }
}

class D21Menu {
    final List<D21Dish> dishes;

    D21Menu() {
        // ingredients = new ArrayList<>();
        // allergens = new ArrayList<>();
        dishes = new ArrayList<>();
    }

    public boolean add(final D21Dish dish) {
        return dish != null && dishes.add(dish);
    }
}

class D21Dish {
    final List<String> ingredients, allergens;
    private static final Pattern pList = Pattern.compile("^(.+) \\(contains (.+)\\)$");

    /*
    D21Dish() {
        // ingredients = new ArrayList<>();
        // allergens = new ArrayList<>();
        // ingredients = new String[0];
        // allergens = new String[0];
    }
    */

    D21Dish(String src) {
        Matcher m = pList.matcher(src);
        m.find();
        ingredients = Arrays.asList(m.group(1).split(" +"));
        allergens = Arrays.asList(m.group(2).split(" *, +"));
    }
}
