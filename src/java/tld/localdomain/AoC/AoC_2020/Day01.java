package tld.localdomain.AoC.AoC_2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import tld.localdomain.AoC.util.Utils;

/**
 * Day 1: Report Repair
 *
 * Before you leave, the Elves in accounting just need you to fix your
 * expense report (your puzzle input); apparently, something isn't
 * quite adding up.
 *
 * Specifically, they need you to find the two entries that sum to
 * 2020 and then multiply those two numbers together.
 *
 * Find the two entries that sum to 2020; what do you get if you
 * multiply them together?
 */
public class Day01 {
    public static void main(final String[] args) {
        int[] aas = readParseInput(args);
        System.out.println(part1(aas));
        System.out.println(part2(aas));
    }

    private static int[] readParseInput(final String[] args) {
        List<Integer> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        for(;sc.hasNext();) {
            String src = sc.next();
            loi.add(Integer.parseInt(src));
        }
        return loi.stream().mapToInt(x->(int)x).toArray();
    }

    /**
     * Find the two entries that sum to 2020; what do you get if you
     * multiply them together?
     */
    private static int part1(final int[] src) {
        for(int i = 0; i < src.length; ++i)
            for(int j = i+1; j < src.length; ++j)
                if((src[i] + src[j]) == 2020)
                    return src[i] * src[j];
        return -1;
    }

    /**
     * In your expense report, what is the product of the three
     * entries that sum to 2020?
     */
    private static int part2(final int[] src) {
        // fixme! eek!
        for(int i = 0; i < src.length; ++i)
            for(int j = i+1; j < src.length; ++j)
                for(int k = j+1; k < src.length; ++k)
                    if((src[i] + src[j] + src[k]) == 2020)
                        return src[i] * src[j] * src[k];
        return -1;
    }

}

