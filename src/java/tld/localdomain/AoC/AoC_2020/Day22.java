package tld.localdomain.AoC.AoC_2020;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import tld.localdomain.AoC.util.Utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Day 22: Crab Combat
 *
 * Before the game starts, split the cards so each player has their
 * own deck (your puzzle input). Then, the game consists of a series
 * of rounds: both players draw their top card, and the player with
 * the higher-valued card wins the round. The winner keeps both cards,
 * placing them on the bottom of their own deck so that the winner's
 * card is above the other card. If this causes a player to have all
 * of the cards, they win, and the game ends.
 */
public class Day22 {
    public static void main(final String[] args) {
        String[] strs = readLines(args);
        List<Deque<Integer>> src = parseInput(strs);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     */
    private static String[] readLines(final String[] args) {
        List<String> los = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        //sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String str = sc.next().trim();
            if (str.length() > 0)
                los.add(str);
        }
        return los.toArray(new String[0]);
    }

    private final static int __MAX_PLAYERS__ = 2;

    /**
     * Player 1: 9 2 6 3 1
     * Player 2: 5 8 4 7 10
     */
    private static List<Deque<Integer>> parseInput(final String[] src) {
        List<Deque<Integer>> data = new ArrayList<>(); // player deques
        int pc = 0; // player count
        Deque<Integer> ns = new ArrayDeque<>();
        for(String str: src) {
            if(str.equals("Player")) continue;
            if(str.charAt(str.length() - 1) == ':') { // next player deque
                if(ns.isEmpty()) continue;
                data.add(ns);
                if(pc >= __MAX_PLAYERS__) break; // fail-safe breaker
                ns = new ArrayDeque<>();
                continue;
            }
            ns.add(Integer.parseInt(str));
        }
        if(pc < __MAX_PLAYERS__)
            data.add(ns);
        return data;
    }

    /**
     * Play the small crab in a game of Combat using the two decks you
     * just dealt. What is the winning player's score?
     */
    private static long part1(final List<Deque<Integer>> deques) {
        Deque<Integer> p1 = new ArrayDeque(deques.get(0)),
            p2 = new ArrayDeque(deques.get(1));
        // System.out.println(p1); System.out.println(p2);
        for(;!(p1.isEmpty() || p2.isEmpty());) {
            final int n1 = p1.pop();
            final int n2 = p2.pop();
            if(n1 > n2) {
                p1.add(n1);
                p1.add(n2);
            }
            else if(n1 < n2) {
                p2.add(n2);
                p2.add(n1);
            }
        }
        // System.out.println(p1); System.out.println(p2);
        return p1.isEmpty() ? evalScore(p2) : evalScore(p1);
    }

    /**
     * Once the game ends, you can calculate the winning player's
     * score. The bottom card in their deck is worth the value of the
     * card multiplied by 1, the second-from-the-bottom card is worth
     * the value of the card multiplied by 2, and so on.
     */
    private static long evalScore(final Deque<Integer> deque) {
        D22Quasiton dqt = D22Quasiton.create(deque.size());
        return deque.stream()
            .mapToLong(t -> t.longValue() * dqt.gec())
            .sum();
    }

    /**
     */
    private static long part2(final List<Deque<Integer>> deques) {
        return -1;
    }
}

class D22Quasiton {
    protected int val;
    private D22Quasiton() {this(0);}
    private D22Quasiton(int n) {val = n;}
    public static D22Quasiton create() {return new D22Quasiton();}
    public static D22Quasiton create(int n) {return new D22Quasiton(n);}
    // public int get() {return val;}
    // public int gic() {return val++;}
    public int gec() {return val--;}
}
