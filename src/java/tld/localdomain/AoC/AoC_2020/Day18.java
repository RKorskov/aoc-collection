package tld.localdomain.AoC.AoC_2020;

import java.util.*;
import java.util.OptionalLong;
// import java.util.regex.Pattern;
// import java.util.regex.Matcher;
// import java.util.stream.Stream;
// import java.util.function.Supplier;

import tld.localdomain.AoC.util.Strevaltron;
import tld.localdomain.AoC.util.Utils;

/**
 * Day 18: Operation Order
 *
 * The homework (your puzzle input) consists of a series of
 * expressions that consist of addition (+), multiplication (*), and
 * parentheses ((...)). Just like normal math, parentheses indicate
 * that the expression inside must be evaluated before it can be used
 * by the surrounding expression. Addition still finds the sum of the
 * numbers on both sides of the operator, and multiplication still
 * finds the product.
 *
 * However, the rules of operator precedence have changed. Rather than
 * evaluating multiplication before addition, the operators have the
 * same precedence, and are evaluated left-to-right regardless of the
 * order in which they appear.
 */
public class Day18 {
    // 选择恐惧症 xuǎnzé kǒngjù zhèng, чаще 选择困难症 xuǎnzé kùnnán zhèng
    public static void main(final String[] args) {
        String[] src = readParseInput(args);
        System.out.println(part1(src));
        System.out.println(part2(src));
    }

    /**
     * ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
     */
    private static String[] readParseInput(final String[] args) {
        List<String> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src.length() < 1) continue;
            loi.add(src);
        }
        return loi.toArray(new String[0]);
    }

    /**
     * Before you can help with the homework, you need to understand
     * it yourself. Evaluate the expression on each line of the
     * homework; what is the sum of the resulting values?
     */
    private static long part1(final String[] src) {
        long sum = 0;
        for(String str : src)
            sum += eval4(str);
        return sum;
    }

    private static OptionalLong evalOp(final Tree18Operation op,
                                      final OptionalLong arg0, final long arg1) {
        if(arg0 == null || arg0.isEmpty()) // || op == null || op == Tree18Operation.NOP)
            return OptionalLong.of(arg1);
        switch(op) {
        case ADD:
            return OptionalLong.of(arg0.getAsLong() + arg1);
        case MUL:
            return OptionalLong.of(arg0.getAsLong() * arg1);
        default:
            return OptionalLong.of(arg1);
        }
    }

    /**
     * ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
     */
    private static long eval4(final String src) {
        //System.out.println(src);
        final OptionalLong val = __eval4(new Strevaltron(src));
        return val.orElse(0);
    }

    private static OptionalLong __eval4(final Strevaltron src) {
        Tree18Operation op = Tree18Operation.NOP;
        OptionalLong acc = OptionalLong.empty();
        for(char c = ' '; c != 0; c = src.get()) {
            switch(c) { // rework as chain of responsibility?
            case ' ': // skip spaces
                break;
            case ')':
                return acc;
                // break;
            case '(':
                final OptionalLong opval = __eval4(src);
                acc = evalOp(op, acc, opval.orElse(0));
                break;
            case '+':
                op = Tree18Operation.ADD;
                break;
            case '*':
                op = Tree18Operation.MUL;
                break;
            default: { // 0..9
                final int val = c - '0';
                acc = evalOp(op, acc, val);
                op = Tree18Operation.NOP;
            }
            }
        }
        return acc;
    }

    /**
     * addition is evaluated before multiplication
     */
    private static long part2(final String[] src) {
        return -1;
    }
}

/**
 * GRP for parentheses
 */
enum Tree18Operation {
    ADD, SUB, MUL, DIV, NOP, GRP;
}
