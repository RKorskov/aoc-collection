package tld.localdomain.AoC.util;

public class Strevaltron { // small utility class for evaluations over strings
    String str;
    int cptr = 0;

    public Strevaltron(String src) { str = src; }

    public char get() { return cptr < str.length() ? str.charAt(cptr++) : 0; }
    public boolean isEOF() { return cptr >= str.length(); }

    public StrevaltronState getState() {
        return new StrevaltronState(cptr, str.hashCode());
    }

    public boolean rewind(StrevaltronState state) {
        if(state != null && state.strHash == str.hashCode() //
           && state.cptr >= 0 //
           && state.cptr < str.length()) {
            cptr = state.cptr;
            return true;
        }
        return false;
    }
}
