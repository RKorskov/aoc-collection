package tld.localdomain.AoC.util;

public class Tuple <P,Q> {
    final P p;
    final Q q;

    public Tuple(P p, Q q) {
        this.p = p;
        this.q = q;
    }

    public P get0() { return p; }
    public Q get1() { return q; }
}
