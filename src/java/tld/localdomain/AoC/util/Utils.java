package tld.localdomain.AoC.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Utils {
    public static Scanner getScanner(final String[] args) {
        Scanner sc = null;
        if(args.length < 1 || args[0].equals("-"))
            sc = new Scanner(System.in);
        else {
            Path fpin = Path.of(args[0]);
            if(Files.exists(fpin))
                try {
                    sc = new Scanner(fpin);
                } catch (IOException ex) {
                    sc = null;
                }
            if(sc == null)
                sc = new Scanner(args[0]);
        }
        if(sc != null)
            sc.useDelimiter("\\s+");
        return sc;
    }

    /**
     * reversing bit order
     */
    public static short reverse16(final int val) {
        int rev = ((val & 0xAAAA) >> 1) | ((val & 0x5555) << 1);
        rev = ((rev & 0xCCCC) >> 2) | ((rev & 0x3333) << 2);
        rev = ((rev & 0xF0F0) >> 4) | ((rev & 0x0F0F) << 4);
        return (short)(((rev & 0xFF00) >> 8) | ((rev & 0x00FF) << 8));
    }
}

