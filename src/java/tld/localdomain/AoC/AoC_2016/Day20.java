package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Day 20: Firewall Rules
 *
 * You'd like to set up a small hidden computer here so you can use it to get back into the network later. However, the corporate firewall only allows communication with certain external IP addresses.
 * You've retrieved the list of blocked IPs from the firewall, but the list seems to be messy and poorly maintained, and it's not clear which IPs are allowed. Also, rather than being written in dot-decimal notation, they are written as plain 32-bit integers, which can have any value from 0 through 4294967295, inclusive.
 * For example, suppose only the values 0 through 9 were valid, and that you retrieved the following blacklist:
 *
 * ```text
 * 5-8
 * 0-2
 * 4-7
 * ```
 *
 * The blacklist specifies ranges of IPs (inclusive of both the start and end value) that are not allowed. Then, the only IPs that this firewall allows are 3 and 9, since those are the only numbers not in any range.
 * Given the list of blocked IPs you retrieved from the firewall (your puzzle input), what is the lowest-valued IP that is not blocked?
 */
public class Day20 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static long part1(final String[] args) {
        List<LongTuple> fwrs = readData(args);
        boolean rf = true;
        for(;rf;) {
            for (int i = 0; i < fwrs.size(); ++i) {
                LongTuple ti = fwrs.get(i);
                if (ti == null) continue;
                for (int j = i + 1; j < fwrs.size(); ++j) {
                    LongTuple tj = fwrs.get(j);
                    if (tj == null) continue;
                    LongTuple tij = joinRanges(ti, tj);
                    if (tij != null) {
                        fwrs.set(i, tij);
                        fwrs.set(j, null);
                        ti = tij;
                    }
                }
            }
            List<LongTuple> f2 = scrubRules(fwrs);
            rf = f2.size() < fwrs.size();
            fwrs = f2;
        }
        LongTuple tmin = fwrs.get(0);
        for(LongTuple t: fwrs)
            if(t.y < tmin.x)
                tmin = t;
        //System.out.println(tmin);
        if(tmin.x > 0) return 0;
        return tmin.y+1;
    }

    /**
     * How many IPs are allowed by the blacklist?
     */
    private static int part2(final String[] args) {
        List<LongTuple> fwrs = readData(args);
        return -1;
    }

    private static List<LongTuple> readData(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<LongTuple> fwrs = new ArrayList<>();
        for(;sc.hasNext();) {
            String s = sc.next();
            String[] ft = s.split("-");
            long p = Long.parseLong(ft[0]), q = Long.parseLong(ft[1]);
            //if(p >= q) System.out.printf("---> %d %d\n", p, q);
            LongTuple t = LongTuple.createTuple(p, q);
            fwrs.add(t);
        }
        return fwrs;
    }

    private static LongTuple joinRanges(final LongTuple ra, final LongTuple rb) {
        if(ra.x >= rb.x && ra.x <= rb.y) {
            //System.out.printf("---> merge %s U %s\n", rb, ra);
            return LongTuple.createTuple(rb.x, Math.max(ra.y, rb.y));
        }
        if(ra.y >= rb.x && ra.y <= rb.y) {
            //System.out.printf("---> merge %s U %s\n", rb, ra);
            return LongTuple.createTuple(Math.min(ra.x, rb.x), rb.y);
        }
        if(rb.x >= ra.x && rb.x <= ra.y) {
            //System.out.printf("---> merge %s U %s\n", ra, rb);
            return LongTuple.createTuple(ra.x, Math.max(ra.y, rb.y));
        }
        if(rb.y >= ra.x && rb.y <= ra.y) {
            //System.out.printf("---> merge %s U %s\n", rb, ra);
            return LongTuple.createTuple(Math.min(ra.x, rb.x), ra.y);
        }
        if((ra.x-1) == rb.y) {
            //System.out.printf("---> join %s + %s\n", rb, ra);
            return LongTuple.createTuple(rb.x, ra.y);
        }
        if(ra.y == (rb.x-1)) {
            //System.out.printf("---> join %s + %s\n", ra, rb);
            return LongTuple.createTuple(ra.x, rb.y);
        }
        return null;
    }

    private static List<LongTuple> scrubRules(List<LongTuple> fwrs) {
        List<LongTuple> fwrs2 = new ArrayList<>();
        for(int i = 0, j = 0; i < fwrs.size(); ++i) {
            LongTuple t = fwrs.get(i);
            if(t != null) {
                fwrs2.add(t);
                ++j;
            }
        }
        return fwrs2;
    }
}
