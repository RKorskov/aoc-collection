package tld.localdomain.AoC.AoC_2016;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 4: Security Through Obscurity #
 *
 * Finally, you come across an information kiosk with a list of rooms. Of course, the list is encrypted and full of decoy data, but the instructions to decode the list are barely hidden nearby. Better remove the decoy data first.
 * Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.
 * A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. For example:
 *
 * -- aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
 * -- a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
 * -- not-a-real-room-404[oarel] is a real room.
 * -- totally-real-room-200[decoy] is not.
 *
 * Of the real rooms from the list above, the sum of their sector IDs is 1514.
 * What is the sum of the sector IDs of the real rooms?
 */
public class Day04 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        long sum = 0;
        for(;sc.hasNext();) {
            Room room = Room.decodeRoom(sc.next());
            if(isReal(room))
                sum += room.getSID();
        }
        return String.valueOf(sum);
    }

    private static boolean isReal(final Room room) {
        Map<Character,Integer> spectrum = new HashMap<>();
        for(Character c: room.getName().toCharArray()) {
            if(c == '-') continue;
            Integer v = spectrum.containsKey(c) ? spectrum.get(c)+1 : 1;
            spectrum.put(c, v);
        }
        List<CV> cv = new ArrayList<>();
        for(Character c: spectrum.keySet())
            cv.add(new CV(c, spectrum.get(c)));
        cv.sort(CV::compareTo);
        StringBuilder sb = new StringBuilder();
        for(CV k: cv)
            sb.append(k.c);
        return room.getChecksum().equals(sb.toString().substring(0,5));
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        for(;sc.hasNext();) {
            Room room = Room.decodeRoom(sc.next());
            if(!isReal(room)) continue;
            System.out.printf("%s  %d\n", room.decodeName(), room.getSID());
        }
        return null;
    }
}

final class Room {
    private final static Pattern pat = Pattern.compile("^([a-z\\-]+)-([0-9]+)\\[([a-z]{5})\\]$"); // totally-real-room-200[decoy]
    final String roomName, checksum;
    final int sid;

    private Room(String name, int sid, String csum) {
        this.sid = sid;
        roomName = name;
        checksum = csum;
    }

    public static Room decodeRoom(final String src) {
        Matcher mat = pat.matcher(src);
        if(!mat.find()) return null;
        int sid = Integer.parseInt(mat.group(2));
        String name = mat.group(1); //.replaceAll("-", "");
        return new Room(name, sid, mat.group(3));
    }

    public int getSID() { return sid; }
    public String getName() { return roomName; }
    public String getChecksum() {return checksum;}

    public String toString() {
        return String.format("name=\"%s\"  sid=%d  checksum=\"%s\"", roomName, sid, checksum);
    }

    public String decodeName() {
        int off26 = sid % 26;
        StringBuilder sb = new StringBuilder();
        for(char c: roomName.toCharArray()) {
            if(c == '-')
                sb.append(' ');
            else
                sb.append((char) ('a' + (c - 'a' + off26) % 26));
        }
        return sb.toString();
    }
}

final class CV implements Comparable<CV> {
    final char c;
    final int v;

    CV(char c, int v) {
        this.c = c;
        this.v = v;
    }

    public int compareTo(CV obj) {
        if(this == obj) return 0;
        if(v != obj.v) return obj.v - v;
        return c - obj.c;
    }
}