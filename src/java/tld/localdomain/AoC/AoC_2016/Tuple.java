package tld.localdomain.AoC.AoC_2016;

import java.util.Objects;

class Tuple {
    final int x, y;

    private Tuple(int x, int y) {
        this.x = x;
        this.y = y;
    }

    static Tuple createTuple(int x, int y) {
        return new Tuple(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple tuple = (Tuple) o;
        return x == tuple.x &&
                y == tuple.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString() {
        return String.format("(%d,%d)", x, y);
    }
}

class LongTuple {
    final long x, y;

    private LongTuple(long x, long y) {
        this.x = x;
        this.y = y;
    }

    static LongTuple createTuple(long x, long y) {
        return new LongTuple(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LongTuple tuple = (LongTuple) o;
        return x == tuple.x &&
                y == tuple.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString() {
        return String.format("(%d,%d)", x, y);
    }
}
