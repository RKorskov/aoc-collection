package tld.localdomain.AoC.AoC_2016;

/**
 * Day 13: A Maze of Twisty Little Cubicles
 *
 * Every location in this area is addressed by a pair of non-negative integers (x,y). Each such coordinate is either a wall or an open space. You can't move diagonally. The cube maze starts at 0,0 and seems to extend infinitely toward positive x and y; negative values are invalid, as they represent a location outside the building. You are in a small waiting area at 1,1.
 * While it seems chaotic, a nearby morale-boosting poster explains, the layout is actually quite logical. You can determine whether a given x,y coordinate will be a wall or an open space using a simple system:
 *
 *  * Find `x*x + 3*x + 2*x*y + y + y*y`.
 *  * Add the office designer's favorite number (your puzzle input, 1364).
 *  * Find the binary representation of that sum; count the number of bits that are 1.
 *    - If the number of bits that are 1 is even, it's an open space.
 *    - If the number of bits that are 1 is odd, it's a wall.
 *
 * What is the fewest number of steps required for you to reach 31,39?
 */
public class Day13 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        final int c = Integer.parseInt(args[0]);
        int x = 1, y = 1,
            trgx = Integer.parseInt(args[1]),
            trgy = Integer.parseInt(args[2]);
        return null;
    }

    private static String part2(final String[] args) {
        return null;
    }

    /**
     * x*x + 3*x + 2*x*y + y + y*y + C
     * Find the binary representation of that sum; count the number of bits that are 1.
     *  - If the number of bits that are 1 is even, it's an open space.
     *  - If the number of bits that are 1 is odd, it's a wall.
     * @return true if an open space
     */
    private static boolean fxyc(int x, int y, int c) {
        return (Long.bitCount(x * x + 3 * x + 2 * x * y + y + y * y + c) & 1) == 0;
    }
}
