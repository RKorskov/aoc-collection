package tld.localdomain.AoC.AoC_2016;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 21: Scrambled Letters and Hash
 *
 * The scrambling function is a series of operations (the exact list is provided in your puzzle input). Starting with the password to be scrambled, apply each operation in succession to the string. The individual operations behave as follows:
 *
 *  * swap position X with position Y means that the letters at indexes X and Y (counting from 0) should be swapped.
 *  * swap letter X with letter Y means that the letters X and Y should be swapped (regardless of where they appear in the string).
 *  * rotate left/right X steps means that the whole string should be rotated; for example, one right rotation would turn abcd into dabc.
 *  * rotate based on position of letter X means that the whole string should be rotated to the right based on the index of letter X (counting from 0) as determined before this instruction does any rotations. Once the index is determined, rotate the string to the right one time, plus a number of times equal to that index, plus one additional time if the index was at least 4.
 *  * reverse positions X through Y means that the span of letters at indexes X through Y (including the letters at X and Y) should be reversed in order.
 *  * move position X to position Y means that the letter which is at index X should be removed from the string, then inserted such that it ends up at index Y.
 */
public class Day21 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scrambler pwd = new Scrambler(args[1]);
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();)
            pwd.eval(sc.next());
        return pwd.toString();
    }

    /**
     * You scrambled the password correctly, but you discover that you can't actually modify the password file on the system. You'll need to un-scramble one of the existing passwords by reversing the scrambling process.
     * What is the un-scrambled version of the scrambled password fbgdceah?
     */
    private static String part2(final String[] args) {
        UnScrambler pwd = new UnScrambler(args[1]);
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> cmds = new ArrayList<>();
        for(;sc.hasNext();)
            cmds.add(sc.next());
        ListIterator<String> li = cmds.listIterator(cmds.size());
        while(li.hasPrevious())
            pwd.eval(li.previous());
        return pwd.toString();
    }

}

class Scrambler {
    private static Pattern pSwapPos = Pattern.compile("^swap position (\\d+) with position (\\d+)$"), // swap position X with position Y
            pSwapChar = Pattern.compile("^swap letter ([a-z]) with letter ([a-z])$"),
            pRotLR = Pattern.compile("^rotate (left|right) (\\d+) steps?$"), // WTF?! -- (step|step) intermix
            pRotPos = Pattern.compile("^rotate based on position of letter ([a-z])$"),
            pReverse = Pattern.compile("^reverse positions (\\d+) through (\\d+)$"),
            pMove = Pattern.compile("^move position (\\d+) to position (\\d+)$");
    private char[] pwd;

    Scrambler(char[] str) {
        pwd = Arrays.copyOf(str, str.length);
    }

    Scrambler(String str) {
        pwd = str.toCharArray();
    }

    public String toString() {
        return String.valueOf(pwd);
    }

    // eek!
    public boolean eval(final String cmd) {
        Matcher m = pMove.matcher(cmd);
        if(m.find()) {
            move(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pSwapPos.matcher(cmd);
        if(m.find()) {
            swap(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pSwapChar.matcher(cmd);
        if(m.find()) {
            swap(m.group(1).charAt(0), m.group(2).charAt(0));
            return true;
        }
        m = pRotLR.matcher(cmd);
        if(m.find()) {
            int rep = Integer.parseInt(m.group(2));
            if(m.group(1).equals("left")) rep = -rep;
            rotate(rep);
            return true;
        }
        m = pRotPos.matcher(cmd);
        if(m.find()) {
            rotatePosition(m.group(1).charAt(0));
            return true;
        }
        m = pReverse.matcher(cmd);
        if(m.find()) {
            reverse(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        return false;
    }

    /**
     * swap position X with position Y means that the letters at indexes X and Y (counting from 0) should be swapped
     */
    private void swap(final int x, final int y) {
        char c = pwd[x];
        pwd[x] = pwd[y];
        pwd[y] = c;
    }

    /**
     * swap letter X with letter Y means that the letters X and Y should be swapped (regardless of where they appear in the string)
     */
    private void swap(final char x, final char y) {
        int xpos = -1, ypos = -1;
        for(int i = 0; i < pwd.length && (xpos < 0 || ypos < 0); ++i) {
            if(pwd[i] == x && xpos < 0) xpos = i;
            if(pwd[i] == y && ypos < 0) ypos = i;
            // if(xpos >= 0 && ypos >= 0) break;
        }
        if(xpos < 0 || ypos < 0) return;
        swap(xpos, ypos);
    }

    /**
     * rotate left/right X steps means that the whole string should be rotated;
     * for example, one right rotation would turn abcd into dabc
     * @param rep right rotate on +n, left rotate on -n, no rotate on n==0
     */
    private void rotate(final int rep) {
        final int di = rep % pwd.length;
        char[] buf = Arrays.copyOf(pwd, pwd.length);
        for(int i = 0; i < pwd.length; ++i) {
            int j = i - di;
            if(j < 0)
                j += pwd.length;
            else if(j >= pwd.length)
                j %= pwd.length;
            pwd[i] = buf[j];
        }
    }

    /**
     * rotate based on position of letter X means that the whole string should be rotated to the right
     * based on the index of letter X (counting from 0) as determined before this instruction does any rotations.
     * Once the index is determined, rotate the string to the right one time, plus a number of times equal to that index,
     * plus one additional time if the index was at least 4
     */
    private void rotatePosition(final char x) {
        int xpos = -1;
        for(int i = 0; i < pwd.length; ++i) {
            if(pwd[i] == x) {
                xpos = i;
                break;
            }
        }
        if(x < 0) return;
        rotate(xpos + (xpos > 3 ? 2 : 1));
    }

    /**
     * reverse positions X through Y means that the span of letters at indexes X through Y
     * (including the letters at X and Y) should be reversed in order
     */
    private void reverse(final int x, final int y) {
        for(int i = x, j = y; i < j; ++i, --j)
            swap(i, j);
    }

    /**
     * move position X to position Y means that the letter which is at index X should be removed from the string,
     * then inserted such that it ends up at index Y
     */
    private void move(final int x, final int y) {
        if(x == y) return;
        char c = pwd[x];
        if(x < y)
            for(int i = x; i < pwd.length && i < y; ++i)
                pwd[i] = pwd[i+1];
        else // x > y
            for(int i = x; i > 0 && i > y; --i)
                pwd[i] = pwd[i-1];
        pwd[y] = c;
    }
}

class UnScrambler {
    private static Pattern pSwapPos = Pattern.compile("^swap position (\\d+) with position (\\d+)$"), // swap position X with position Y
            pSwapChar = Pattern.compile("^swap letter ([a-z]) with letter ([a-z])$"),
            pRotLR = Pattern.compile("^rotate (left|right) (\\d+) steps?$"), // WTF?! -- (step|step) intermix
            pRotPos = Pattern.compile("^rotate based on position of letter ([a-z])$"),
            pReverse = Pattern.compile("^reverse positions (\\d+) through (\\d+)$"),
            pMove = Pattern.compile("^move position (\\d+) to position (\\d+)$");
    private char[] pwd;

    UnScrambler(char[] str) {
        pwd = Arrays.copyOf(str, str.length);
    }

    UnScrambler(String str) {
        pwd = str.toCharArray();
    }

    public String toString() {
        return String.valueOf(pwd);
    }

    // eek!
    public boolean eval(final String cmd) {
        Matcher m = pMove.matcher(cmd);
        if(m.find()) {
            move(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pSwapPos.matcher(cmd);
        if(m.find()) {
            swap(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pSwapChar.matcher(cmd);
        if(m.find()) {
            swap(m.group(1).charAt(0), m.group(2).charAt(0));
            return true;
        }
        m = pRotLR.matcher(cmd);
        if(m.find()) {
            int rep = Integer.parseInt(m.group(2));
            if(m.group(1).equals("left")) rep = -rep;
            rotate(rep);
            return true;
        }
        m = pRotPos.matcher(cmd);
        if(m.find()) {
            rotatePosition(m.group(1).charAt(0));
            return true;
        }
        m = pReverse.matcher(cmd);
        if(m.find()) {
            reverse(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        return false;
    }

    /**
     * swap position X with position Y means that the letters at indexes X and Y (counting from 0) should be swapped
     */
    private void swap(final int x, final int y) {
        char c = pwd[x];
        pwd[x] = pwd[y];
        pwd[y] = c;
    }

    /**
     * swap letter X with letter Y means that the letters X and Y should be swapped (regardless of where they appear in the string)
     * fixme?
     */
    private void swap(final char x, final char y) {
        int xpos = -1, ypos = -1;
        for(int i = 0; i < pwd.length && (xpos < 0 || ypos < 0); ++i) {
            if(pwd[i] == x && xpos < 0) xpos = i;
            if(pwd[i] == y && ypos < 0) ypos = i;
            // if(xpos >= 0 && ypos >= 0) break;
        }
        if(xpos < 0 || ypos < 0) return;
        swap(xpos, ypos);
    }

    /**
     * rotate left/right X steps means that the whole string should be rotated;
     * for example, one right rotation would turn abcd into dabc
     * @param rep right rotate on -n, left rotate on +n, no rotate on n==0
     */
    private void rotate(final int rep) {
        final int di = rep % pwd.length;
        char[] buf = Arrays.copyOf(pwd, pwd.length);
        for(int i = 0; i < pwd.length; ++i) {
            int j = i + di;
            if(j < 0)
                j += pwd.length;
            else if(j >= pwd.length)
                j %= pwd.length;
            pwd[i] = buf[j];
        }
    }

    /**
     * rotate based on position of letter X means that the whole string should be rotated to the right
     * based on the index of letter X (counting from 0) as determined before this instruction does any rotations.
     * Once the index is determined, rotate the string to the right one time, plus a number of times equal to that index,
     * plus one additional time if the index was at least 4
     * fixme!
     */
    private void rotatePosition(final char x) {
        int xpos = -1;
        for(int i = 0; i < pwd.length; ++i) {
            if(pwd[i] == x) {
                xpos = i;
                break;
            }
        }
        if(x < 0) return;
        int rep = xpos + (xpos > 3 ? 2 : 1); // ?
        rotate(-rep);
    }

    /**
     * reverse positions X through Y means that the span of letters at indexes X through Y
     * (including the letters at X and Y) should be reversed in order
     */
    private void reverse(final int x, final int y) {
        for(int i = x, j = y; i < j; ++i, --j)
            swap(i, j);
    }

    /**
     * move position X to position Y means that the letter which is at index X should be removed from the string,
     * then inserted such that it ends up at index Y
     */
    private void move(final int y, final int x) {
        if(x == y) return;
        char c = pwd[x];
        if(x < y)
            for(int i = x; i < pwd.length && i < y; ++i)
                pwd[i] = pwd[i+1];
        else // x > y
            for(int i = x; i > 0 && i > y; --i)
                pwd[i] = pwd[i-1];
        pwd[y] = c;
    }
}
