package tld.localdomain.AoC.AoC_2016;

/**
 * Day 18: Like a Rogue
 *
 * For example, suppose you know the first row (with tiles marked by letters) and want to determine the next row (with tiles marked by numbers):
 *
 * ```text
 * ABCDE
 * 12345
 * ```
 *
 * The type of tile 2 is based on the types of tiles A, B, and C; the type of tile 5 is based on tiles D, E, and an imaginary "safe" tile. Let's call these three tiles from the previous row the left, center, and right tiles, respectively. Then, a new tile is a trap only in one of the following situations:
 *
 *  * Its left and center tiles are traps, but its right tile is not.
 *  * Its center and right tiles are traps, but its left tile is not.
 *  * Only its left tile is a trap.
 *  * Only its right tile is a trap.
 *
 * In any other situation, the new tile is safe.
 * Starting with the map in your puzzle input, in a total of 40 rows (including the starting row), how many safe tiles are there?
 * Your puzzle input is '.^^^^^.^^.^^^.^...^..^^.^.^..^^^^^^^^^^..^...^^.^..^^^^..^^^^...^.^.^^^^^^^^....^..^^^^^^.^^^.^^^.^^'.
 */
public class Day18 {
    /**
     *
     * @param args : {number_of_rows, starting_row}
     */
    public static void main(final String[] args) {
        System.out.println(part1(args));
        //System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        final int rows = Integer.parseInt(args[0]);
        boolean[] row = string2bool(args[1]);
        //row2string(row);
        int safe = countSafe(row);
        for(int j = 1; j < rows; ++j) {
            boolean[] next = new boolean[row.length];
            for(int i = 0; i < row.length; ++i) {
                next[i] = isSafe(row, i);
                if(next[i])
                    ++safe;
            }
            //row2string(next);
            row = next;
        }
        return String.valueOf(safe);
    }

    private static String part2(final String[] args) {
        final int rows = Integer.parseInt(args[0]);
        char[] row = args[1].toCharArray();
        int safe = 0;
        return String.valueOf(safe);
    }

    /**
     * Then, a new tile is a trap only in one of the following situations:
     *
     *  * Its left and center tiles are traps, but its right tile is not.
     *  * Its center and right tiles are traps, but its left tile is not.
     *  * Only its left tile is a trap.
     *  * Only its right tile is a trap.
     *
     * In any other situation, the new tile is safe.
     * @param row
     * @param pos
     * @return
     */
    private static boolean isSafe(final boolean[] row, final int pos) {
        final boolean c = row[pos];
        final boolean l = pos > 0 ? row[pos-1] : true;
        final boolean r = (pos+1) < row.length ? row[pos+1] : true;
        boolean trap = !l && !c && r || l && !c && !r || !l && c && r || l && c && !r;
        return !trap;
    }

    private static int countSafe(final boolean[] row) {
        int safe = 0;
        for(boolean t: row)
            if(t)
                ++safe;
        return safe;
    }

    private static boolean[] string2bool(final String src) {
        char[] crow = src.toCharArray();
        boolean[] brow = new boolean[crow.length];
        for(int i = 0; i < crow.length; ++i)
            brow[i] = crow[i] == '.';
        return brow;
    }

    private static void row2string(final boolean[] row) {
        for(boolean t: row)
            System.out.print(t?'.':'^');
        System.out.println();
    }
}
