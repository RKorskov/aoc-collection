package tld.localdomain.AoC.AoC_2016;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 10: Balance Bots
 *
 * You come upon a factory in which many robots are zooming around handing small microchips to each other.
 * Upon closer examination, you notice that each bot only proceeds when it has two microchips, and once it does, it gives each one to a different bot or puts it in a marked "output" bin. Sometimes, bots take microchips from "input" bins, too.
 * Inspecting one of the microchips, it seems like they each contain a single number; the bots must use some logic to decide what to do with each chip. You access the local control computer and download the bots' instructions (your puzzle input).
 * Some of the instructions specify that a specific-valued microchip should be given to a specific bot; the rest of the instructions indicate what a given bot should do with its lower-value or higher-value chip.
 *
 * For example, consider the following instructions:
 *
 * ```text
 * value 5 goes to bot 2
 * bot 2 gives low to bot 1 and high to bot 0
 * value 3 goes to bot 1
 * bot 1 gives low to output 1 and high to bot 0
 * bot 0 gives low to output 2 and high to output 0
 * value 2 goes to bot 2
 * ```
 *
 *  * Initially, bot 1 starts with a value-3 chip, and bot 2 starts with a value-2 chip and a value-5 chip.
 *  * Because bot 2 has two microchips, it gives its lower one (2) to bot 1 and its higher one (5) to bot 0.
 *  * Then, bot 1 has two microchips; it puts the value-2 chip in output 1 and gives the value-3 chip to bot 0.
 *  * Finally, bot 0 has two microchips; it puts the 3 in output 2 and the 5 in output 0.
 *
 * In the end, output bin 0 contains a value-5 microchip, output bin 1 contains a value-2 microchip, and output bin 2 contains a value-3 microchip. In this configuration, bot number 2 is responsible for comparing value-5 microchips with value-2 microchips.
 * Based on your instructions, what is the number of the bot that is responsible for comparing value-61 microchips with value-17 microchips?
 */
public class Day10 {
    public static void main(final String[] args) {
        //System.out.println(part1(args));
        System.out.println(BB.part1bb(args));
        System.out.println(part2(args));
    }

    final static Pattern pGive = Pattern.compile("^value (\\d+) goes to bot (\\d+)$"),
        pDest = Pattern.compile("bot (\\d+) gives low to (output|bot) (\\d+) and high to (output|bot) (\\d+)");

    private static int part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> cmds = new ArrayList<>();
        List<Tuple> botval = new ArrayList<>();
        BalanceSwarm hive = new BalanceSwarm();
        for(;sc.hasNext();) {
            String s = sc.next();
            Matcher m = pGive.matcher(s);
            if(m.find()) {
                boolean f = hive.addIfEmpty(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
                if(!f)
                    cmds.add(s);
            }
            m = pDest.matcher(s);
            if(m.find()) {
                int lo = Integer.parseInt(m.group(3)),
                    hi = Integer.parseInt(m.group(5));
                if(m.group(2).equals("output")) lo = -(lo+1);
                if(m.group(4).equals("output")) hi = -(hi+1);
                hive.update(Integer.parseInt(m.group(1)), lo, hi);
                //continue;
            }
        }
        return hive.eval(61, 17);
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        return null;
    }
}

class BalanceBot implements Consumer<Integer>, Supplier<Integer> {
    final static int __UNDEF = Integer.MIN_VALUE;
    Integer value;
    int id, toLow, toHigh;
    Consumer<Integer> targetLow, targetHigh;

    BalanceBot(int id) {
        this(id, __UNDEF, __UNDEF, __UNDEF);
    }

    BalanceBot(int id, int v) {
        this(id, v, __UNDEF, __UNDEF);
    }

    BalanceBot(int id, int v, int lo, int hi) {
        value = v == __UNDEF ? null : v;
        toLow = lo;
        toHigh = hi;
        targetLow = null;
        targetHigh = null;
    }

    public void setTargets(final int lo, final int hi) {
        toLow = lo;
        toHigh = hi;
    }

    public void setTargets(Consumer<Integer> lo, Consumer<Integer> hi) {
        targetLow = lo;
        targetHigh = hi;
    }

    public void setConsumerLow(Consumer<Integer> con) { targetLow = con; }
    public void setConsumerHigh(Consumer<Integer> con) { targetHigh = con; }

    public int getId() { return id; }
    public boolean haveValue() { return value != null; }
    public int getValue() { return value.intValue(); }
    public boolean isConsumersJoined() { return targetLow != null && targetHigh != null; }
    public int getConsumerIdLow() { return toLow; }
    public int getConsumerIdHigh() { return toHigh; }
    public Consumer<Integer> getConsumerLow() { return targetLow; }
    public Consumer<Integer> getConsumerHigh() { return targetHigh; }

    @Override
    public Integer get() {
        Integer t = Integer.valueOf(value);
        value = null;
        return t;
    }

    @Override
    public void accept(Integer n) { value = n; }
}

class BalanceSwarm {
    // List<BalanceBot> swarm;
    //Map<Integer,BalanceBot> swarm;
    Map<Integer,Consumer<Integer>> swarm;
    //Map<Integer,BinOut> outbins;

    BalanceSwarm() {
        //swarm = new ArrayList<>();
        swarm = new HashMap<>();
        //outbins = new HashMap<>();
    }

    /**
     * give the value (v) to the bot (id)
     */
    public void add(final int id, final int v) {
        BalanceBot bot = null;
        if (swarm.containsKey(id)) {
            Consumer<Integer> cin = swarm.get(id);
            if (cin instanceof BalanceBot)
                bot = (BalanceBot) cin;
        } else {
            bot = new BalanceBot(id, v);
            swarm.put(id, bot); // ? ... de-couple a bit
        }
        if (bot != null)
            bot.accept(v);
    }

    /**
     * give the value (v) to the bot (id) IFF the bot have no value stored
     */
    public boolean addIfEmpty(final int id, final int v) {
        BalanceBot bot = null;
        if (swarm.containsKey(id)) {
            Consumer<Integer> cin = swarm.get(id);
            if (cin instanceof BalanceBot)
                bot = (BalanceBot) cin;
        }
        else {
            bot = new BalanceBot(id, v);
            swarm.put(id, bot); // ? ... de-couple a bit
        }
        if (bot == null || bot.haveValue())
            return false;
        bot.accept(v);
        return true;
    }

    public void update(final int id, final int lo, final int hi) {
        /*
        Consumer<Integer> idLo, idHi;
        if(lo < 0) {
            BinOut t = new BinOut(+lo);
            outbins.put(+lo, t);
        }
        if(hi < 0) {
            BinOut t = new BinOut(+hi);
            outbins.put(+hi, t);
        }
        */
        if(!swarm.containsKey(lo)) {
            if(lo < 0)
                swarm.put(lo, new BinOut(+lo));
            else
                swarm.put(lo, new BalanceBot(lo));
        }
        if(!swarm.containsKey(hi)) {
            if(hi < 0)
                swarm.put(hi, new BinOut(+hi));
            else
                swarm.put(hi, new BalanceBot(hi));
        }
        if(swarm.containsKey(id)) {
            if(id >= 0) {
                Consumer<Integer> cin = swarm.get(id);
                if(cin instanceof BalanceBot) {
                    BalanceBot bot = (BalanceBot) cin;
                    bot.setTargets(lo, hi);
                }
            }
        }
        else {
            swarm.put(id, new BalanceBot(id, BalanceBot.__UNDEF, lo, hi)); // ? ... de-couple a bit
        }
    }

    public int eval(final int val0, final int val1) {
        for(Integer id: swarm.keySet()) {
            Consumer<Integer> cin = swarm.get(id);
            if(!(cin instanceof BalanceBot)) continue;
            BalanceBot bot = (BalanceBot)cin;
            if(!bot.haveValue()) continue;
            if(!bot.isConsumersJoined()) {
                if(bot.getConsumerLow() == null) {
                    int cid = bot.getConsumerIdLow();
                    Consumer<Integer> con = swarm.get(cid);
                    bot.setConsumerLow(con);
                }
                if(bot.getConsumerHigh() == null) {
                    int cid = bot.getConsumerIdHigh();
                    Consumer<Integer> con = swarm.get(cid);
                    bot.setConsumerHigh(con);
                }
            }
        }
        return -1;
    }
}

class BinOut implements Consumer<Integer> {
    int id;
    List<Integer> bin;

    BinOut(int n) {
        id = n;
        bin = new ArrayList<>();
    }

    @Override
    public void accept(Integer n) {
        bin.add(n);
    }
}

/**
 * bot 2 gives low to bot 1 and high to bot 0
 * NB! "output n" translates to "-n-1"
 */
class BB {
    final static int __UNDEF = Integer.MIN_VALUE;
    final int id, low, high;
    int val;

    BB(int i, int v, int l, int h) {
        id = i;
        val = v;
        low = l;
        high = h;
    }

    BB(int i, int l, int h) {
        id = i;
        low = l;
        high = h;
        val = __UNDEF;
    }

    public String toString() {
        return String.format("id %d  val %d  low %d  high %d", id, val, low, high);
    }

    /*
     * part1: dirty, but workable
     */
    public static int part1bb(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        Map<Integer,BB> idxbb = new HashMap<>(); // rules of comparision
        List<BBC> bbc = new ArrayList<>(); // chips to distribute
        parseDump(sc, idxbb, bbc);
        // feed
        List<BBC> bbc2 = new ArrayList<>();
        for(BBC bf: bbc) {
            BB bb = idxbb.get(bf.id);
            if(bb.val == BB.__UNDEF)
                bb.val = bf.val;
            else
                bbc2.add(bf);
        }
        bbc = bbc2;
        for(BBC bf: bbc) {
            giveTo(idxbb, bf.id, bf.val);
        }
        return -1;
    }

    private static void giveTo(Map<Integer,BB> idxbb, int id, int val) {
        if(id < 0) return;
        BB bb = idxbb.get(id);
        if(bb.val == BB.__UNDEF) {
            bb.val = val;
            return;
        }
        int toLow = Math.min(bb.val, val), toHigh = Math.max(bb.val, val);
        //
        // what is the number of the bot that is responsible for comparing value-61 microchips with value-17 microchips?
        if(toLow == 17 && toHigh == 61) {
            System.out.println(id);
            //return;
        }
        //
        bb.val = BB.__UNDEF; // ACID? ... pff!
        if(bb.low >= 0)
            giveTo(idxbb, bb.low, toLow);
        else
            System.out.printf("out %d  %d\n", -bb.low-1, toLow);
        if(bb.high >= 0)
            giveTo(idxbb, bb.high, toHigh);
        else
            System.out.printf("out %d  %d\n", -bb.high-1, toHigh);
    }

    private static void parseDump(final Scanner sc, final Map<Integer,BB> ibb, final List<BBC> bbc) {
        for(;sc.hasNext();) {
            String s = sc.next();
            Matcher m = Day10.pGive.matcher(s);
            if(m.find()) {
                int val = Integer.parseInt(m.group(1)),
                        id = Integer.parseInt(m.group(2));
                bbc.add(new BBC(id, val));
                continue;
            }
            m = Day10.pDest.matcher(s);
            if (m.find()) {
                int id = Integer.parseInt(m.group(1)),
                        lo = Integer.parseInt(m.group(3)),
                        hi = Integer.parseInt(m.group(5));
                if (m.group(2).equals("output")) lo = -lo-1;
                if (m.group(4).equals("output")) hi = -hi-1;
                BB bb = new BB(id, lo, hi);
                ibb.put(id, bb);
                //continue;
            }
        }
    }
}

/**
 * value 5 goes to bot 2
 */
class BBC {
    final int id, val;

    BBC(int i, int v) {
        id = i;
        val = v;
    }

    public String toString() {
        return String.format("id %d  val %d", id, val);
    }
}
