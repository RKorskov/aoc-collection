package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Day 25: Clock Signal
 */
public class Day25 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> cmds = new ArrayList<>();
        Assembunny usagi = new Assembunny();
        usagi.setReg("a", args.length > 1 ? Integer.parseInt(args[1]) : 0); // eek! fixme!
        for(;sc.hasNext();) {
            String cmd = sc.next().split(";")[0].trim();
            cmds.add(cmd);
        }
        for(int n = 0; usagi.getIP() < cmds.size() && n < 102400; ++n) {
            String cmd = cmds.get(usagi.getIP());
            usagi.hop(cmd);
        }
        return usagi.getOut();
    }
}
