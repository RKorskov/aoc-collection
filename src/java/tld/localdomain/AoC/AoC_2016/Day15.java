package tld.localdomain.AoC.AoC_2016;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * Day 15: Timing is Everything
 * LCQ?
 *
 * Disc #1 has 7 positions; at time=0, it is at position 0.
 * Disc #2 has 13 positions; at time=0, it is at position 0.
 * Disc #3 has 3 positions; at time=0, it is at position 2.
 * Disc #4 has 5 positions; at time=0, it is at position 2.
 * Disc #5 has 17 positions; at time=0, it is at position 0.
 * Disc #6 has 19 positions; at time=0, it is at position 7.
 *
 * (t+1)%7=0
 * (t+2)%13=0
 * (t+2)%3=0
 * (t+1)%5=0
 * (t+5)%17=0
 * (t+13)%19=0
 *
 * t%7=1
 * t%13=2
 * t%3=2
 * t%5=1
 * t%17=5
 * t%19=13
 */
public class Day15 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part1brute(args));
        System.out.println(part2(args));
    }

    private static String part1brute(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        int rc = -1;
        for (int t = 2; t < Integer.MAX_VALUE; t += 1) {
            if ((t+1) % 7 == 0 //
                    && (t+2) % 13 == 0 //
                    && (t+3+2) % 3 == 0 //
                    && (t+4+2) % 5 == 0 //
                    && (t+5) % 17 == 0 //
                    && (t+6+7) % 19 == 0) {
                rc = t;
                break;
            }
        }
        return String.valueOf(rc);
    }

    private static String part1cfuta(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        CompletableFuture<Integer> cfuta0 = CompletableFuture.supplyAsync(() -> moduloEval(2,3002, 3));
        CompletableFuture<Integer> cfuta1 = CompletableFuture.supplyAsync(() -> moduloEval(3002,6002, 3));
        CompletableFuture<Integer> cfuta2 = CompletableFuture.supplyAsync(() -> moduloEval(6002,9002, 3));
        CompletableFuture<Integer> cfuta3 = CompletableFuture.supplyAsync(() -> moduloEval(9002,11002, 3));
        CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(cfuta0, cfuta1, cfuta2, cfuta3);
        int rc = -1;
        try {
            combinedFuture.get();
            rc = Math.max(cfuta0.get().intValue(), cfuta1.get().intValue());
            rc = Math.max(rc, cfuta2.get().intValue());
            rc = Math.max(rc, cfuta3.get().intValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return String.valueOf(rc);
    }

    private static int moduloEval(int from, int to, int step) {
        int rc = -1;
        for (int t = from; t < to; t += step) {
            if (t % 7 != 1 //
                    || t % 13 != 2 //
                    || t % 3 != 2 //
                    || t % 5 != 1 //
                    || t % 17 != 5 //
                    || t % 19 != 13) continue;
            rc = t;
            break;
        }
        return rc;
    }

    private static String part1(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        return null;
    }

    private static String part2(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        return null;
    }
}

class IntRange {
    final int from, to, step;

    IntRange(int from, int to) {
        this(from, to, 1);
    }

    IntRange(int from, int to, int step) {
        this.from = from;
        this.to = to;
        this.step = step;
    }
}
