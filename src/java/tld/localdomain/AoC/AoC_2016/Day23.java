package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 23: Safe Cracking
 */
public class Day23 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> cmds = new ArrayList<>();
        Assembunny usagi = new Assembunny();
        usagi.setReg("a", 7);
        for(;sc.hasNext();) {
            String cmd = sc.next().split(";")[0].trim();
            cmds.add(cmd);
        }
        for(int n = 0; usagi.getIP() < cmds.size(); ++n) {
            String cmd = cmds.get(usagi.getIP());
            usagi.hop(cmd);
            if(usagi.isCallbackReady())
                ((Consumer<List<String>>)usagi.getCallback()).accept(cmds);
        }
        return usagi.toString();
    }

    /**
     * You're quite sure your logic is working correctly, so the only other thing is... you check the painting again. As it turns out, colored eggs are still eggs. Now you count 12.
     *
     * As you run the program with this new input, the prototype computer begins to overheat. You wonder what's taking so long, and whether the lack of any instruction more powerful than "add one" has anything to do with it. Don't bunnies usually multiply?
     */
    private static String part2(final String[] args) {
        // Pattern pat = Pattern.compile("inc ([a-d])");
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> cmds = new ArrayList<>();
        Assembunny usagi = new Assembunny();
        usagi.setReg("a", 12);
        //usagi.setMulF(true);
        for(;sc.hasNext();) {
            String cmd = sc.next().split(";")[0].trim();
            /*
            Matcher m = pat.matcher(cmd);
            if(m.find())
                cmd = String.format("mul %s", m.group(1));
             */
            cmds.add(cmd);
        }
        for(int n = 0; usagi.getIP() < cmds.size(); ++n) {
            String cmd = cmds.get(usagi.getIP());
            usagi.hop(cmd);
            if(usagi.isCallbackReady())
                ((Consumer<List<String>>)usagi.getCallback()).accept(cmds);
        }
        return usagi.toString();
    }
}
