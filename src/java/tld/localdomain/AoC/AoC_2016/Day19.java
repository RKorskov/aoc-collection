package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Day 19: An Elephant Named Joseph
 *
 * Each Elf brings a present. They all sit in a circle, numbered starting with position 1. Then, starting with the first Elf, they take turns stealing all the presents from the Elf to their left. An Elf with no presents is removed from the circle and does not take turns.
 * With the number of Elves given in your puzzle input, which Elf gets all the presents?
 * Your puzzle input is 3014387.
 */
public class Day19 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        int num = Integer.parseInt(args[0]);
        List<Tuple> elst = new ArrayList<>();
        for(int i = 1; i <= num; ++i)
            elst.add(Tuple.createTuple(i, 1));
        for(;elst.size() > 1;) {
            for(int i = 0; i < elst.size(); ++i) {
                Tuple p = elst.get(i);
                if(p.y < 1) continue;
                int j = i + 1;
                if(j >= elst.size()) j = 0;
                Tuple q = elst.get(j);
                elst.set(i, Tuple.createTuple(p.x, p.y + q.y));
                elst.set(j, Tuple.createTuple(q.x, 0));
                //System.out.printf("%d takes from %d of %d\n", p.x, q.x, q.y);
                ++i; // %-S
            }
            List<Tuple> erem = new ArrayList<>();
            for(Tuple t: elst)
                if(t.y > 0)
                    erem.add(t);
            elst = erem;
        }
        return String.valueOf(elst.get(0).x);
    }

    /**
     * Realizing the folly of their present-exchange rules, the Elves agree to instead steal presents from the Elf directly across the circle. If two Elves are across the circle, the one on the left (from the perspective of the stealer) is stolen from. The other rules remain unchanged: Elves with no presents are removed from the circle entirely, and the other elves move in slightly to keep the circle evenly spaced.
     * fixme! too slow!
     */
    private static String part2(final String[] args) {
        int num = Integer.parseInt(args[0]);
        List<Tuple> elst = new ArrayList<>(); // LinkedList<>();
        for(int i = 1; i <= num; ++i)
            elst.add(Tuple.createTuple(i, 1));
        for(;elst.size() > 1;) {
            for(int i = 0; i < elst.size(); ++i) {
                final int len = elst.size();
                int j = (i + len / 2) % len;
                Tuple p = elst.get(i);
                Tuple q = elst.get(j);
                elst.set(i, Tuple.createTuple(p.x, p.y + q.y));
                elst.remove(j);
            }
            System.out.printf("  %d\n", elst.size());
        }
        return String.valueOf(elst.get(0).x);
    }
}
