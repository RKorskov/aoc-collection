package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

/**
 * Day 3: Squares With Three Sides #
 *
 * Now that you can think clearly, you move deeper into the labyrinth of hallways and office furniture that makes up this part of Easter Bunny HQ. This must be a graphic design department; the walls are covered in specifications for triangles.
 * Or are they?
 * The design document gives the side lengths of each triangle it describes, but... 5 10 25? Some of these aren't triangles. You can't help but mark the impossible ones.
 * In a valid triangle, the sum of any two sides must be larger than the remaining side. For example, the "triangle" given above is impossible, because 5 + 10 is not larger than 25.
 * In your puzzle input, how many of the listed triangles are possible?
 */
public class Day03 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int cnt = 0;
        for(;sc.hasNext();) {
            int a = sc.nextInt(),
                b = sc.nextInt(),
                c = sc.nextInt();
            if(isTriangle(a, b, c))
                ++cnt;
        }
        return String.valueOf(cnt);
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int cnt = 0;
        for(;sc.hasNext();) {
            Int3Stack t0 = new Int3Stack(),
                    t1 = new Int3Stack(),
                    t2 = new Int3Stack();
            try {
                for (int i = 0; i < 3 && sc.hasNext(); ++i) {
                    int a = sc.nextInt(),
                            b = sc.nextInt(),
                            c = sc.nextInt();
                    t0.push(a);
                    t1.push(b);
                    t2.push(c);
                }
                for(Int3Stack i3s : new Int3Stack[] {t0, t1, t2}) {
                    int a = i3s.pop(), b = i3s.pop(), c = i3s.pop();
                    if (isTriangle(a, b, c))
                        ++cnt;
                }
            } catch(StackUnderflowException | StackOverflowException se) {se.printStackTrace();}
        }
        return String.valueOf(cnt);
    }

    private static boolean isTriangle(final int a, final int b, final int c) {
        return (a+b)>c && (a+c)>b && (b+c)>a;
    }
}

class Int3Stack {
    final static int SSIZE = 3;
    int[] seq;
    int len;

    Int3Stack() {
        seq = new int[SSIZE];
        len = 0;
    }

    public void push(final int n) throws StackOverflowException {
        if(len < SSIZE)
            seq[len++] = n;
        else
            throw new StackOverflowException("stack overflow");
    }

    public int pop() throws StackUnderflowException {
        if(len > 0)
            return seq[--len];
        else
            throw new StackUnderflowException("stack underflow");
    }
}

class StackOverflowException extends Exception {
    StackOverflowException(String msg) {
        super(msg);
    }
}

class StackUnderflowException extends Exception {
    StackUnderflowException(String msg) {
        super(msg);
    }
}