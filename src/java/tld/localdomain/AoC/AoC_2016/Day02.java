package tld.localdomain.AoC.AoC_2016;

import java.util.Scanner;

/**
 * Day 2: Bathroom Security
 * You arrive at Easter Bunny Headquarters under cover of darkness. However, you left in such a rush that you forgot to use the bathroom! Fancy office buildings like this one usually have keypad locks on their bathrooms, so you search the front desk for the code.
 * "In order to improve security," the document you find says, "bathroom codes will no longer be written down. Instead, please memorize and follow the procedure below to access the bathrooms."
 * The document goes on to explain that each button to be pressed can be found by starting on the previous button and moving to adjacent buttons on the keypad: U moves up, D moves down, L moves left, and R moves right. Each line of instructions corresponds to one button, starting at the previous button (or, for the first line, the "5" button); press whatever button you're on at the end of each line. If a move doesn't lead to a button, ignore it.
 * You can't hold it much longer, so you decide to figure out the code as you walk to the bathroom. You picture a keypad like this:

```
1 2 3
4 5 6
7 8 9
```

 * Suppose your instructions are:

```
ULL
RRDDD
LURDL
UUUUD
```

 * - You start at "5" and move up (to "2"), left (to "1"), and left (you can't, and stay on "1"), so the first button is 1.
 * - Starting from the previous button ("1"), you move right twice (to "3") and then down three times (stopping at "9" after two moves and ignoring the third), ending up with 9.
 * - Continuing from "9", you move left, up, right, down, and left, ending with 8.
 * - Finally, you move up four times (stopping at "2"), then down once, ending with 5.
 *
 * So, in this example, the bathroom code is 1985.
 *
 * Your puzzle input is the instructions from the document you found at the front desk. What is the bathroom code?
 */
public class Day02 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        Grid grid = new Grid();
        StringBuilder sb = new StringBuilder();
        for(;sc.hasNext();) {
            String cmd = sc.next();
            for(char c: cmd.toCharArray())
                grid.move(c);
            sb.append(grid.getKey());
        }
        return sb.toString();
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        OctaGrid grid = new OctaGrid();
        StringBuilder sb = new StringBuilder();
        for(;sc.hasNext();) {
            String cmd = sc.next();
            for(char c: cmd.toCharArray())
                grid.move(c);
            sb.append(grid.getKey());
        }
        return sb.toString();
    }

}

class Grid {
    int x, y;
    Grid() {
        x = 2;
        y = 1;
    }

    public int getKey() {
        return y * 3 + x;
    }

    public void move(final char dir) {
        int nx = x, ny = y;
        switch(dir) {
            case 'U': case 'u': --ny; break;
            case 'D': case 'd': ++ny; break;
            case 'L': case 'l': --nx; break;
            case 'R': case 'r': ++nx; break;
            default: return;
        }
        if(nx < 1 || nx > 3 || ny < 0 || ny > 2)
            return;
        x = nx;
        y = ny;
    }
}

/**
 * _ _ 1 _ _
 * _ 2 3 4 _
 * 5 6 7 8 9
 * _ A B C _
 * _ _ D _ _
 */
class OctaGrid {
    int x, y;
    OctaGrid() {
        x = 0;
        y = 2;
    }

    final static char[] KEYS = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D'};

    public char getKey() {
        final int h = x - 2, v = y - 2;
        int k = 6 + h;
        if(v != 0)
            k += Integer.signum(v) * (2 * (Math.abs(v) + 1)); // 1 2 --> 4 6
        return KEYS[k];
    }

    public void move(final char dir) {
        int nx = x, ny = y;
        switch(dir) {
            case 'U': case 'u': --ny; break;
            case 'D': case 'd': ++ny; break;
            case 'L': case 'l': --nx; break;
            case 'R': case 'r': ++nx; break;
            default: return;
        }
        if(isValidPosition(nx, ny)) {
            x = nx;
            y = ny;
        }
    }

    /**
     * Center of coordinates shift with distance check
     * @return true iff (nx,ny) is the valid (button) position
     */
    private static boolean isValidPosition(final int nx, final int ny) {
        final int h = nx - 2, v = ny - 2;
        final int s = Math.abs(h) + Math.abs(v);
        return s < 3;
    }
}
