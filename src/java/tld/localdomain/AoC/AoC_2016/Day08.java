package tld.localdomain.AoC.AoC_2016;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day08 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        Screen scr = new Screen();
        for(;sc.hasNext();)
            scr.eval(sc.next());
        return String.valueOf(scr.countLit());
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        Screen scr = new Screen();
        for(;sc.hasNext();)
            scr.eval(sc.next());
        return scr.toString();
    }
}

class Screen {
    final static int __SCR_WIDTH = 50, __SCR_HEIGHT = 6;
    final static Pattern pRect = Pattern.compile("^rect (\\d+)x(\\d+)$"),
            pRotRow = Pattern.compile("^rotate row y=(\\d+) by (\\d+)$"),
            pRotCol = Pattern.compile("^rotate column x=(\\d+) by (\\d+)$"),
            pCmd = Pattern.compile("^((rotate (row|column) [xy])|(rect))[ =](\\d+)(( by )|x)(\\d+)$");
    final int SCR_WIDTH, SCR_HEIGHT;
    boolean[][] scr;

    Screen() {
        this(__SCR_WIDTH, __SCR_HEIGHT);
    }

    Screen(int wdt, int hgt) {
        SCR_WIDTH = wdt;
        SCR_HEIGHT = hgt;
        scr = new boolean[SCR_HEIGHT][];
        for(int i = 0; i < SCR_HEIGHT; ++i)
            scr[i] = new boolean[SCR_WIDTH];
    }

    public void rect(final int w, final int h) {
        for(int i = 0; i < h; ++i)
            for(int j = 0; j < w; ++j)
                scr[i][j] = true;
    }

    public void rotRow(final int row, final int by) {
        final int off = by % SCR_WIDTH;
        if(off < 1) return;
        boolean[] newRow = new boolean[SCR_WIDTH];
        for(int i = 0, j = off; i < SCR_WIDTH; ++i, ++j) {
            if(j >= SCR_WIDTH) j = 0;
            newRow[j] = scr[row][i];
        }
        scr[row] = newRow;
    }

    public void rotCol(final int col, final int by) {
        final int off = by % SCR_HEIGHT;
        if(off < 1) return;
        boolean[] newCol = new boolean[SCR_HEIGHT];
        for(int i = 0, j = off; i < SCR_HEIGHT; ++i, ++j) {
            if(j >= SCR_HEIGHT) j = 0;
            newCol[j] = scr[i][col];
        }
        for(int i = 0; i < SCR_HEIGHT; ++i)
            scr[i][col] = newCol[i];
    }

    public int countLit() {
        int cnt = 0;
        for(int i = 0; i < SCR_HEIGHT; ++i)
            for(int j = 0; j < SCR_WIDTH; ++j)
                if(scr[i][j])
                    ++cnt;
        return cnt;
    }

    public boolean eval(final String cmd) {
        Matcher m = pRect.matcher(cmd);
        if(m.find()) {
            rect(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pRotRow.matcher(cmd);
        if(m.find()) {
            rotRow(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        m = pRotCol.matcher(cmd);
        if(m.find()) {
            rotCol(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
            return true;
        }
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < SCR_HEIGHT; ++i) {
            for (int j = 0; j < SCR_WIDTH; ++j)
                sb.append(scr[i][j] ? '*' : '.');
            sb.append('\n');
        }
        return sb.toString();
    }
}
