package tld.localdomain.AoC.AoC_2016;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Part 1
 *
 * The Document indicates that you should start at the given coordinates (where you just landed) and face North. Then, follow the provided sequence: either turn left (L) or right (R) 90 degrees, then walk forward the given number of blocks, ending at a new intersection.
 * There's no time to follow such ridiculous instructions on foot, though, so you take a moment and work out the destination. Given that you can only walk on the street grid of the city, how far is the shortest path to the destination?
 * For example:
 * -- Following R2, L3 leaves you 2 blocks East and 3 blocks North, or 5 blocks away.
 * -- R2, R2, R2 leaves you 2 blocks due South of your starting position, which is 2 blocks away.
 * -- R5, L5, R5, R3 leaves you 12 blocks away.
 * How many blocks away is Easter Bunny HQ?
 *
 * Part 2
 *
 * Then, you notice the instructions continue on the back of the Recruiting Document. Easter Bunny HQ is actually at the first location you visit twice.
 * For example, if your instructions are R8, R4, R4, R8, the first location you visit twice is 4 blocks away, due East.
 * How many blocks away is the first location you visit twice?
 */
public class Day01 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    public static String part1(final String[] args) {
        PC pc = new PC();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter(",?\\s+");
        for(;sc.hasNext();) {
            String cmd = sc.next();
            pc.exec1(cmd);
        }
        return String.valueOf(pc.getDistance());
    }

    public static String part2(final String[] args) {
        PC pc = new PC();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter(",?\\s+");
        for(;sc.hasNext();) {
            String cmd = sc.next();
            if(pc.exec2(cmd))
                return String.valueOf(pc.getDistance());
        }
        return String.valueOf(-pc.getDistance());
    }
}

enum Dir {
    N, E, S, W
}

class PC {
    int x, y;
    Dir head;
    static Dir[] dirs = {Dir.W, Dir.N, Dir.E, Dir.S, Dir.W, Dir.N};
    Set<Tuple> blocks = new HashSet<>(); // 2nd part

    PC() {
        x = 0;
        y = 0;
        head = Dir.N;
        blocks.add(Tuple.createTuple(0,0));
    }

    /**
     * cmd  direction-distance pair: L3, R5, ...
     */
    public void exec1(final String cmd) {
        switch(cmd.charAt(0)) {
            case 'R': case 'r': turnRight(); break;
            case 'L': case 'l': turnLeft(); break;
            default:
                System.out.printf("wut? skipped... '%s'\n", cmd);
                return;
        }
        final int off = Integer.parseInt(cmd.substring(1));
        switch(head) {
            case N: x += off; break;
            case S: x -= off; break;
            case W: y -= off; break;
            case E: y += off; //break;
        }
    }

    /**
     * cmd  direction-distance pair: L3, R5, ...
     */
    public boolean exec2(final String cmd) {
        switch(cmd.charAt(0)) {
            case 'R': case 'r': turnRight(); break;
            case 'L': case 'l': turnLeft(); break;
            default:
                System.out.printf("wut? skipped... '%s'\n", cmd);
                return false;
        }
        int dx = 0, dy = 0;
        switch(head) {
            case N: dx = +1; break;
            case S: dx = -1; break;
            case W: dy = -1; break;
            case E: dy = +1; //break;
        }
        final int off = Integer.parseInt(cmd.substring(1));
        for(int i = 0; i < off; ++i) {
            x += dx;
            y += dy;
            Tuple loc = Tuple.createTuple(x, y);
            if (blocks.contains(loc))
                return true;
            else
                blocks.add(loc);
        }
        return false;
    }

    public int getDistance() {
        return Math.abs(x) + Math.abs(y);
    }

    private void turnLeft() {
        head = dirs[head.ordinal()];
    }

    private void turnRight() {
        head = dirs[head.ordinal()+2];
    }
}
