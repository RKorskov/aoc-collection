package tld.localdomain.AoC.AoC_2016;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 7: Internet Protocol Version 7
 *
 * While snooping around the local network of EBHQ, you compile a list of IP addresses (they're IPv7, of course; IPv6 is much too limited). You'd like to figure out which IPs support TLS (transport-layer snooping).
 * An IP supports TLS if it has an Autonomous Bridge Bypass Annotation, or ABBA. An ABBA is any four-character sequence which consists of a pair of two different characters followed by the reverse of that pair, such as xyyx or abba. However, the IP also must not have an ABBA within any hypernet sequences, which are contained by square brackets.
 *
 * For example:
 *
 * - abba[mnop]qrst supports TLS (abba outside square brackets).
 * - abcd[bddb]xyyx does not support TLS (bddb is within square brackets, even though xyyx is outside square brackets).
 * - aaaa[qwer]tyui does not support TLS (aaaa is invalid; the interior characters must be different).
 * - ioxxoj[asdfgh]zxcvbn supports TLS (oxxo is outside square brackets, even though it's within a larger string).
 *
 * How many IPs in your puzzle input support TLS?
 */
public class Day07 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int cnt = 0;
        for(;sc.hasNext();) {
            String s = sc.next();
            IPv7 ip7 = IPv7.createAddress(s);
            if(ip7 == null) {
                System.err.println(s);
                continue;
            }
            if(ip7.isTLS())
                ++cnt;
        }
        return String.valueOf(cnt);
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int cnt = 0;
        for(;sc.hasNext();) {
            String s = sc.next();
            IPv7 ip7 = IPv7.createAddress(s);
            if(ip7 == null) {
                System.err.println(s);
                continue;
            }
            if(ip7.isSSL())
                ++cnt;
        }
        return String.valueOf(cnt);
    }
}

final class IPv7 {
    private static Pattern tls = Pattern.compile("([a-z])([a-z])\\2\\1"),
            ssl = Pattern.compile("([a-z])([a-z])\\1");
    final String addr;
    final boolean capableTLS, capableSSL;

    private IPv7(String str) {
        addr = str;
        capableTLS = __isTLS(addr);
        capableSSL = __isSSL(addr);
    }

    private static boolean __isTLS(final String str) {
        boolean mayBeTls = false;
        String[] chunks = str.split("[\\[\\]]");
        for(int i = 0; i < chunks.length; ++i) {
            Matcher m = tls.matcher(chunks[i]);
            if(!m.find()) continue;
            if(!m.group(1).equals(m.group(2))) {
                if((i & 1) == 0)
                    mayBeTls = true;
                else {
                    mayBeTls = false;
                    break;
                }
            }
        }
        return mayBeTls;
    }

    private static boolean __isSSL(final String str) {
        Set<String> aba = new HashSet<>(), bab = new HashSet<>();
        String[] chunks = str.split("[\\[\\]]");
        for(int i = 0; i < chunks.length; ++i) {
            Matcher m = ssl.matcher(chunks[i]);
            //if(!m.find()) continue;
            for(int pos = 0; m.find(pos); pos = m.start() + 1) {
                if (!m.group(1).equals(m.group(2))) {
                    if ((i & 1) == 0) {
                        String s = m.group(1) + m.group(2);
                        if (bab.contains(s))
                            return true;
                        aba.add(s);
                    } else {
                        String s = m.group(2) + m.group(1);
                        if (aba.contains(s))
                            return true;
                        bab.add(s);
                    }
                }
            }
        }
        return false;
    }

    public static IPv7 createAddress(final String str) {
        try {
            return new IPv7(str);
        }
        catch (Exception ex) {
            return null;
        }
    }

    public boolean isTLS() {return capableTLS;}
    public boolean isSSL() {return capableSSL;}

    public String toString() {
        return String.format("%s  TLS=%b  SSL=%b", addr, capableTLS, capableSSL);
    }
}
