package tld.localdomain.AoC.AoC_2016;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * Day 5: How About a Nice Game of Chess?
 *
 * You are faced with a security door designed by Easter Bunny engineers that seem to have acquired most of their security knowledge by watching hacking movies.
 * The eight-character password for the door is generated one character at a time by finding the MD5 hash of some Door ID (your puzzle input) and an increasing integer index (starting with 0).
 * A hash indicates the next character in the password if its hexadecimal representation starts with five zeroes. If it does, the sixth character in the hash is the next character of the password.
 *
 * For example, if the Door ID is abc:
 *
 * - The first index which produces a hash that starts with five zeroes is 3231929, which we find by hashing abc3231929; the sixth character of the hash, and thus the first character of the password, is 1.
 * - 5017308 produces the next interesting hash, which starts with 000008f82..., so the second character of the password is 8.
 * - The third time a hash starts with five zeroes is for abc5278568, discovering the character f.
 *
 * In this example, after continuing this search a total of eight times, the password is 18f47a30.
 * Given the actual Door ID, what is the password?
 * Your puzzle input is `abbhdwsy`.
 */
public class Day05 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        String doorID = sc.next();
        StringBuilder pwd = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            int len = 0;
            for(long n = 0; len < 8 && n < Long.MAX_VALUE; ++n) {
                String seed = doorID + n;
                md.update(seed.getBytes());
                byte[] hash = md.digest();
                if(hash[0] == 0 && hash[1] == 0 && (hash[2] & 0xF0) == 0) {
                    pwd.append(hash[2] < 10 ? (char)('0' + hash[2]) : (char)('a' + hash[2] - 10));
                    //System.out.printf("%d\n", hash[2]);
                    ++len;
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return pwd.toString();
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        String doorID = sc.next();
        char[] pwd = new char[8];
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            int len = 0;
            for(long n = 0; len < 8 && n < Long.MAX_VALUE; ++n) {
                String seed = doorID + n;
                md.update(seed.getBytes());
                byte[] hash = md.digest();
                if(hash[0] == 0 && hash[1] == 0 && (hash[2] & 0xF0) == 0) {
                    int pos = hash[2] & 0x0F;
                    if(pos > 7 || pwd[pos] != 0) continue;
                    int b = (hash[3] >> 4) & 0x0F;
                    char c = b < 10 ? (char)('0' + b) : (char)('a' + b - 10);
                    pwd[pos] = c;
                    ++len;
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new String(pwd);
    }
}
