package tld.localdomain.AoC.AoC_2016;

/**
 * Day 16: Dragon Checksum
 *
 * Start with an appropriate initial state (your puzzle input). Then, so long as you don't have enough data yet to fill the disk, repeat the following steps:
 *
 *  * Call the data you have at this point "a".
 *  * Make a copy of "a"; call this copy "b".
 *  * Reverse the order of the characters in "b".
 *  * In "b", replace all instances of 0 with 1 and all 1s with 0.
 *  * The resulting data is "a", then a single 0, then "b".
 *
 * The checksum for some given data is created by considering each non-overlapping pair of characters in the input data. If the two characters match (00 or 11), the next checksum character is a 1. If the characters do not match (01 or 10), the next checksum character is a 0. This should produce a new string which is exactly half as long as the original. If the length of the checksum is even, repeat the process until you end up with a checksum with an odd length.
 *
 * Your puzzle input is 11101000110010100.
 */
public class Day16 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static int __DISK1SIZE = 272,
            __DISK2SIZE = 3565_1584;
    private static String part1(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        String s = args[0];
        for(;s.length() < __DISK1SIZE;)
            s = dragon1(s);
        s = s.substring(0, __DISK1SIZE);
        return checksum(s);
    }

    private static String part2(final String[] args) {
        //Scanner sc = Utils.getScanner(args);
        return null;
    }

    private static String dragon1(String src) {
        StringBuilder sb = new StringBuilder(src);
        sb.append('0');
        sb.append(xor(src).reverse());
        return sb.toString();
    }

    private static StringBuilder xor(String src) {
        StringBuilder sb = new StringBuilder();
        for(char c: src.toCharArray())
            sb.append(c == '1' ? '0' : '1');
        return sb;
    }

    /**
     * The checksum for some given data is created by considering each non-overlapping pair of characters in the input data. If the two characters match (00 or 11), the next checksum character is a 1. If the characters do not match (01 or 10), the next checksum character is a 0. This should produce a new string which is exactly half as long as the original. If the length of the checksum is even, repeat the process until you end up with a checksum with an odd length.
     */
    private static String checksum(String src) {
        String csum = src;
        do {
            StringBuilder sb = new StringBuilder();
            char[] chrs = csum.toCharArray();
            for (int i = 0; i < chrs.length; i += 2)
                sb.append(chrs[i] == chrs[i + 1] ? '1' : '0');
            csum = sb.toString();
        } while((csum.length() & 1) == 0);
        return csum;
    }
}
