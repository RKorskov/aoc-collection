package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 12
 *
 *  - `cpy x y` copies x (either an integer or the value of a register) into register y.
 *  - `inc x` increases the value of register x by one.
 *  - `dec x` decreases the value of register x by one.
 *  - `jnz x y` jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
 *
 * Day 23
 *
 * `tgl x` toggles the instruction x away (pointing at instructions like jnz does: positive means forward; negative means backward):
 *
 * - For one-argument instructions, inc becomes dec, and all other one-argument instructions become inc.
 * - For two-argument instructions, jnz becomes cpy, and all other two-instructions become jnz.
 * - The arguments of a toggled instruction are not affected.
 * - If an attempt is made to toggle an instruction outside the program, nothing happens.
 * - If toggling produces an invalid instruction (like cpy 1 2) and an attempt is later made to execute that instruction, skip it instead.
 * - If tgl toggles itself (for example, if a is 0, tgl a would target itself and become inc a), the resulting instruction is not executed until the next time it is reached.
 *
 * Day 25
 *
 * - `out x` transmits x (either an integer or the value of a register) as the next value for the clock signal.
 */
public class Assembunny {
    AssembunnyRegFile regfile;
    AssembunnyALU alu;
    StringBuilder output = new StringBuilder();

    Assembunny() {
        regfile = new AssembunnyRegFile();
        alu = new ALUinc();
        alu.chainALU(new ALUdec());
        alu.chainALU(new ALUcpy());
        alu.chainALU(new ALUjnz());
        alu.chainALU(new ALUtgl());
        //alu.chainALU(new ALUmul());
        alu.chainALU(new ALUout());
        alu.chainALU(new ALUadd());
        alu.chainALU(new ALUnop());
        alu.chainALU(new ALUtrap()); // fail-trap "ALU", _always_ should be last in chain!
    }

    public int getIP() { return regfile.ip; }
    public String toString() { return String.format("a %d", regfile.regs[0]); }
    public void setReg(final String regn, final int val) { regfile.setRegByName(regn, val); }
    public String getOut() { return output.toString(); }
    public void setMulF(boolean f) { regfile.mulF = f; }

    public void hop(String cmd) {
        alu.eval(regfile, cmd);
        if(regfile.hasOut())
            output.append(regfile.getOut());
    }

    public boolean isCallbackReady() { return regfile.isCallbackReady(); }
    public Consumer<?> getCallback() { return regfile.getCallback(); }

    public int eval(String[] abprog) {
        List<String> cmds = Arrays.asList(abprog);
        for(; getIP() < cmds.size();) {
            final int ip = getIP();
            if(fastforward3(cmds, ip)) continue;
            alu.eval(regfile, cmds.get(ip));
            if(regfile.hasOut())
                output.append(regfile.getOut());
            if(isCallbackReady())
                ((Consumer<List<String>>)getCallback()).accept(cmds);
        }
        return regfile.regs[0];
    }

    /**
     * fast-forward code block:
     * (inc|dec) reg1
     * dec reg2
     * jnz reg2 -2
     * ... or:
     * dec reg1
     * (inc|dec) reg2
     * jnz reg1 -2
     */
    private static boolean fastforward3(final List<String> cmds, final int ip) {
        return false;
    }

    /**
     * fast-forward code block:
     * inc reg1
     * dec reg2
     * jnz reg2 -2
     * dec reg3
     * jnz reg3 -5
     */
    private static boolean fastforward5(final List<String> cmds, final int ip) {
        return false;
    }

    /**
     * Compile to byte-code.
     *  - nop
     *  - inc x increases the value of register x by one.
     *  - dec x decreases the value of register x by one.
     *  - tgl reg
     *  - out reg
     *  - cpy x y copies integer x into register y.
     *  - cpy x y copies the value of a register x into register y.
     *  - jnz x y jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
     *    jnz reg int
     *    jnz int reg
     *    jnz int int
     *    jnz reg reg
     */
    public static List<AssembunnyCmd> compile(final List<String> cmds) {
        List<AssembunnyCmd> lac = new ArrayList<>();
        for(String rawcmd : cmds) {
            String cmd = rawcmd.split(";")[0].trim();
            String[] nibs = cmd.split(" ");
            switch(nibs[0]) {
                case "nop": {
                    lac.add(new AssembunnyCmd(AssembunnyOP.NOP));
                    break;
                }
                case "inc": {
                    int reg = nibs[1].charAt(0) - 'a';
                    lac.add(new AssembunnyCmd(AssembunnyOP.INC, reg));
                    break;
                }
                case "dec": {
                    int reg = nibs[1].charAt(0) - 'a';
                    lac.add(new AssembunnyCmd(AssembunnyOP.DEC, reg));
                    break;
                }
                case "out": {
                    int reg = nibs[1].charAt(0) - 'a';
                    lac.add(new AssembunnyCmd(AssembunnyOP.OUT, reg));
                    break;
                }
                case "tgl": {
                    int reg = nibs[1].charAt(0) - 'a';
                    lac.add(new AssembunnyCmd(AssembunnyOP.TGL, reg));
                    break;
                }
                case "cpy": { // fixme! todo
                    boolean arg0reg, arg1reg;
                    int arg0, arg1;
                    if(nibs[1].charAt(0) >= 'a') {
                        arg0 = nibs[1].charAt(0) - 'a';
                        arg0reg = true;
                    }
                    else {
                        arg0 = Integer.parseInt(nibs[1]);
                        arg0reg = false;
                    }
                    if(nibs[2].charAt(0) >= 'a') {
                        arg1 = nibs[2].charAt(0) - 'a';
                        arg1reg = true;
                    }
                    else {
                        arg1 = Integer.parseInt(nibs[2]);
                        arg1reg = false;
                    }
                    AssembunnyOP cpt = AssembunnyOP.NOP;
                    if(arg0reg && arg1reg)
                        cpt = AssembunnyOP.CPR;
                    if(!arg0reg && arg1reg)
                        cpt = AssembunnyOP.CPI;
                    lac.add(new AssembunnyCmd(cpt, arg0, arg1));
                    break;
                }
                case "jnz": { // fixme! todo
                    boolean arg0reg, arg1reg;
                    int arg0, arg1;
                    if(nibs[1].charAt(0) >= 'a') {
                        arg0 = nibs[1].charAt(0) - 'a';
                        arg0reg = true;
                    }
                    else {
                        arg0 = Integer.parseInt(nibs[1]);
                        arg0reg = false;
                    }
                    if(nibs[2].charAt(0) >= 'a') {
                        arg1 = nibs[2].charAt(0) - 'a';
                        arg1reg = true;
                    }
                    else {
                        arg1 = Integer.parseInt(nibs[2]);
                        arg1reg = false;
                    }
                    AssembunnyOP jop = AssembunnyOP.NOP;
                    if(arg0reg && arg1reg)
                        jop = AssembunnyOP.JRR;
                    if(arg0reg && !arg1reg)
                        jop = AssembunnyOP.JRI;
                    if(!arg0reg && arg1reg)
                        jop = AssembunnyOP.JPR;
                    if(!arg0reg && !arg1reg)
                        jop = AssembunnyOP.JPI;
                    lac.add(new AssembunnyCmd(jop, arg0, arg1));
                    break;
                }
            }
        }
        return lac;
    }
}

enum AssembunnyOP {
    NOP,
    INC,
    DEC,
    TGL,
    OUT,
    CPI, // cpy int reg
    CPR, // cpy reg reg
    JPI, // jnz int int
    JPR, // jnz int reg
    JRI, // jnz reg int
    JRR  // jnz reg reg
}

class AssembunnyCmd {
    AssembunnyOP op;
    final int args;
    int arg0, arg1;
    boolean arg0type, arg1type; // t:reg, f:int

    AssembunnyCmd(AssembunnyOP op) { // nop
        this.op = op;
        args = 0;
        arg0 = Integer.MIN_VALUE;
        arg0type = false;
        arg1 = Integer.MIN_VALUE;
        arg1type = false;
    }

    AssembunnyCmd(AssembunnyOP op, int arg) { // inc dec out tgl
        this.op = op;
        args = 1;
        arg0 = arg;
        arg0type = true;
        arg1 = Integer.MIN_VALUE;
        arg1type = false;
    }

    AssembunnyCmd(AssembunnyOP op, int arg0, int arg1) { // cpy jnz
        this.op = op;
        args = 2;
        switch(op) {
            case JPR:
            case CPI: { // cpy int reg
                this.arg0 = arg0;
                arg0type = false;
                this.arg1 = arg1;
                arg1type = true;
                break;
            }
            case JRR:
            case CPR: { // cpy reg reg
                this.arg0 = arg0;
                arg0type = true;
                this.arg1 = arg1;
                arg1type = true;
                break;
            }
            case JPI: { // jnz int int
                this.arg0 = arg0;
                arg0type = false;
                this.arg1 = arg1;
                arg1type = false;
                break;
            }
            case JRI: { // jnz reg int
                this.arg0 = arg0;
                arg0type = true;
                this.arg1 = arg1;
                arg1type = false;
                break;
            }
        }
    }
}

class AssembunnyRegFile {
    int[] regs; // a..d
    int ip;
    boolean outF;
    int outVal;
    boolean mulF;
    private Consumer<?> callback;

    AssembunnyRegFile() {
        callback = null;
        regs = new int[4];
        ip = 0;
        outF = false;
        outVal = -1;
        mulF = false;
    }

    public int getRegByName(final String regn) {
        return regs[regn.charAt(0) - 'a'];
    }

    public void setRegByName(final String regn, final int val) {
        regs[regn.charAt(0) - 'a'] = val;
    }

    public int getIP() {return ip;}
    public void moveIPrel(final int off) { ip += off; }
    public boolean getMulF() { return mulF; }
    public boolean hasOut() { return outF; }

    public int getOut() {
        outF = false;
        return outVal;
    }

    public void out(final int val) {
        //System.out.print(val);
        outVal = val;
        outF = true;
    }

    public static boolean isRegName(final String str) {
        return str.length() == 1 && str.charAt(0) >= 'a' && str.charAt(0) <= 'd';
    }

    protected boolean isCallbackReady() { return callback != null; }

    protected void _registerCallback(Consumer<List<String>> func) {
        callback = func;
    }

    protected <T> void registerCallback(Consumer<T> func) {
        callback = func;
    }

    public Consumer<?> getCallback() {
        var t = callback;
        callback = null;
        return t;
    }
}

abstract class AssembunnyALU {
    AssembunnyALU nextALU;
    Pattern pCmd;
    public Pattern getPattern() { return pCmd; }
    public AssembunnyALU next() { return nextALU; }

    public void chainALU(final AssembunnyALU nextALU) {
        if(this.nextALU == null)
            this.nextALU = nextALU;
        else
            this.nextALU.chainALU(nextALU);
    }

    abstract public void eval(AssembunnyRegFile regs, String cmd);
}

class ALUinc extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("inc ([a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String reg = m.group(1);
        int t = regs.getRegByName(reg);
        regs.setRegByName(reg, regs.getMulF() ? (t * 2) : (t + 1));
        regs.moveIPrel(1);
    }
}

class ALUdec extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("dec ([a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String reg = m.group(1);
        int t = regs.getRegByName(reg);
        regs.setRegByName(reg, regs.getMulF() ? (t / 2) : (t - 1));
        regs.moveIPrel(1);
    }
}

class ALUcpy extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("cpy ([-]?\\d+|[a-d]) ([a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String dst = m.group(2),
                src = m.group(1);
        if(AssembunnyRegFile.isRegName(src)) {
            int t = regs.getRegByName(src);
            regs.setRegByName(dst, t);
        }
        else
            regs.setRegByName(dst, Integer.parseInt(src));
        regs.moveIPrel(1);
    }
}

class ALUjnz extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("jnz ([-]?\\d+|[a-d]) ([-]?\\d+|[a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String s = m.group(1);
        final int val = AssembunnyRegFile.isRegName(s) ? regs.getRegByName(s) : Integer.parseInt(s);
        if(val != 0) {
            s = m.group(2);
            final int off = AssembunnyRegFile.isRegName(s) ? regs.getRegByName(s) : Integer.parseInt(s);
            regs.moveIPrel(off);
        }
        else
            regs.moveIPrel(1);
    }
}

/**
 * Day 23
 *
 * `tgl x` toggles the instruction x away (pointing at instructions like jnz does: positive means forward; negative means backward):
 *
 * - For one-argument instructions, inc becomes dec, and all other one-argument instructions become inc.
 * - For two-argument instructions, jnz becomes cpy, and all other two-instructions become jnz.
 * - The arguments of a toggled instruction are not affected.
 * - If an attempt is made to toggle an instruction outside the program, nothing happens.
 * - If toggling produces an invalid instruction (like cpy 1 2) and an attempt is later made to execute that instruction, skip it instead.
 * - If tgl toggles itself (for example, if a is 0, tgl a would target itself and become inc a), the resulting instruction is not executed until the next time it is reached.
 */
class ALUtgl extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("tgl ([-]?\\d+|[a-d])"),
        pArg1 = Pattern.compile("^([a-z]{3}) ([a-z]|[-]?[0-9]+)$"),
        pArg2 = Pattern.compile("^([a-z]{3}) ([a-z]|[-]?[0-9]+) ([a-z]|[-]?[0-9]+)$");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String src = m.group(1);
        final int val = AssembunnyRegFile.isRegName(src) ? regs.getRegByName(src) : Integer.parseInt(src);
        // todo!
        //throw new NotImplementedException(String.format("tgl not implemented (yet!) : %d : %s", regs.getIP(), cmd));
        // ...
        final int ptr = regs.getIP() + val;
        if(ptr >= 0) {
            regs.registerCallback((List<String> cmds) -> {
                if (ptr < cmds.size()) {
                    System.err.printf("---> tgl of : %d : %s\n", ptr, cmds.get(ptr));
                    cmds.set(ptr, morph(cmds.get(ptr)));
                }
            });
        }
        regs.moveIPrel(1);
    }

    static String morph(final String cmd) {
        Matcher m = pArg1.matcher(cmd);
        if(m.find())
            return String.format(m.group(1).equals("inc") ? "dec %s" : "inc %s", m.group(2));
        m = pArg2.matcher(cmd);
        if(m.find())
            return String.format(m.group(1).equals("jnz") ? "cpy %s %s" : "jnz %s %s", m.group(2), m.group(3));
        return null;
    }
}

/**
 * Day 25
 *
 * - `out x` transmits x (either an integer or the value of a register) as the next value for the clock signal.
 */
class ALUout extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("out ([a-d]|[0-9]+)");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String reg = m.group(1);
        int t = regs.isRegName(reg) //
            ? regs.getRegByName(reg) //
            : Integer.parseInt(reg);
        regs.out(t);
        regs.moveIPrel(1);
    }
}

/**
 * Day 23.2
 *
 * You wonder what's taking so long, and whether the lack of any instruction more powerful than "add one"
 * has anything to do with it.
 * Don't bunnies usually multiply?
 */
class ALUmul extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("mul ([a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String reg = m.group(1);
        int t = regs.getRegByName(reg);
        regs.setRegByName(reg, t * t);
        regs.moveIPrel(1);
    }
}

class ALUtrap extends AssembunnyALU {
    public void eval(final AssembunnyRegFile regs, final String cmd) {
        System.err.printf("--> skip: %d : %s\n", regs.getIP(), cmd);
        regs.moveIPrel(1);
    }
}

class ALUnop extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("nop.*");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        regs.moveIPrel(1);
    }
}

/**
 * replacement of code such as:
 * inc a
 * dec c
 * jnz c -2
 */
class ALUadd extends AssembunnyALU {
    final static Pattern pCmd = Pattern.compile("add ([a-d]) ([a-d])");

    public void eval(final AssembunnyRegFile regs, final String cmd) {
        Matcher m = pCmd.matcher(cmd);
        if(!m.find()) {
            if(nextALU != null)
                nextALU.eval(regs, cmd);
            return;
        }
        String dst = m.group(1),
                off = m.group(2);
        regs.setRegByName(dst, regs.getRegByName(dst) + regs.getRegByName(off));
        regs.setRegByName(off, 0);
        regs.moveIPrel(3);
    }
}
