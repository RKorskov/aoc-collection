package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Day 24: Air Duct Spelunking
 *
 * You extract the duct layout for this area from some blueprints you acquired and
 * create a map with the relevant locations marked (your puzzle input).
 * 0 is your current location, from which the cleaning robot embarks;
 * the other numbers are (in no particular order) the locations the robot needs to visit at least once each.
 * Walls are marked as `#`, and open passages are marked as `.`.
 * Numbers behave like open passages.
 */
public class Day24 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        // System.out.println(part2(args));
    }

    private static int part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<String> rows = new ArrayList<>();
        for(;sc.hasNext();) {
            String row = sc.next();
            rows.add(row);
        }
        int[][] map = makemap(rows);
        //printMap(map);
        Tuple[] checkpoints = getCheckpoints(rows);
        //for(Tuple t : checkpoints) System.err.println(t);
        int[][] dist = evalDistances(map, checkpoints);
        //for(int i = 0; i < dist.length; ++i) System.err.println(Arrays.toString(dist[i]));
        // а теперь наш продажник^W робот отправится путешествовать...
        // todo: traveling salesman
        // List<> best_way;
        int best_way_time = findWayAround(dist, new int[] {0});
        return -1;
    }

    // матрица смежности
    private static int[][] evalDistances(final int[][] map, final Tuple[] checkpoints) {
        int[][] dist = new int[checkpoints.length][checkpoints.length];
        for(int i = 0; i < checkpoints.length; ++i) {
            Tuple a = checkpoints[i];
            if(a == null) continue;
            for(int j = i+1; j < checkpoints.length; ++j) {
                Tuple b = checkpoints[j];
                if(b == null) continue;
                int d = wavelength(map, a.x, a.y, b.x, b.y);
                dist[i][j] = d;
                dist[j][i] = d;
            }
        }
        return dist;
    }

    private static final int WALL = Integer.MIN_VALUE,
        PASSAGE = 0;
    /**
     * passage: 0,
     * checkpoint: -1..-9
     */
    private static int[][] makemap(List<String> rows) {
        // "edge" walls discount
        int[][] map = new int[rows.size()-2][rows.get(0).length()-2];
        for(int j = 1, y = 0; j < (rows.size()-1); ++j, ++y) {
            char[] row = rows.get(j).toCharArray();
            for (int i = 1, x = 0; i < (row.length-1); ++i, ++x) {
                switch(row[i]) {
                    case '#':
                        map[y][x] = WALL;
                        break;
                    case ' ':
                    case '.':
                        map[y][x] = PASSAGE;
                        break;
                    // checkpoint
                    default:
                        //map[y][x] = '0' - row[i];
                        map[y][x] = PASSAGE;
                }
            }
        }
        return map;
    }

    private static Tuple[] getCheckpoints(List<String> rows) {
        Tuple[] cps = new Tuple[8]; // 0..7
        for(int j = 1; j < (rows.size()-1); ++j) {
            char[] row = rows.get(j).toCharArray();
            for (int i = 1; i < row.length; ++i) {
                char c = row[i];
                if(c < '0' || c > '9') continue;
                int n = c - '0';
                cps[n] = Tuple.createTuple(i-1, j-1);
            }
        }
        // trim cps
        return cps;
    }

    /**
     * path finding by wave front
     * @return manhattan distance of A to B, or negative number if unreachable
     */
    private static int wavelength(final int[][] orig_map, final int ax, final int ay, final int bx, final int by) {
        if(orig_map[ay][ax] == WALL || orig_map[by][bx] == WALL) return -1; // oops...! :)
        int[][] map = new int[orig_map.length][];
        for(int j = 0; j < orig_map.length; ++j)
            map[j] = Arrays.copyOf(orig_map[j], orig_map[j].length);
        map[ay][ax] = 1;
        for(boolean cntF = true; cntF;) {
            cntF = false;
            for (int j = 0; j < map.length; ++j) {
                for (int i = 0; i < map[j].length; ++i) {
                    if (map[j][i] == WALL) continue;
                    if (map[j][i] != PASSAGE) {
                        // if(bx == i && by == j) return map[j][i];
                        continue;
                    }
                    int w = getCellWeight(map, i, j);
                    if (w < 1) continue;
                    if (bx == i && by == j) {
                        //printMap(map);
                        return w-1;
                    }
                    map[j][i] = w + 1;
                    cntF = true;
                }
            }
        }
        //printMap(map);
        return -1;
    }

    private static int getCellWeight(final int[][] map, final int x, final int y) {
        int val = -1;
        if(x > 0 && map[y][x-1] > 0)
            val = Math.max(val, map[y][x-1]);
        if((x+1) < map[y].length && map[y][x+1] > 0)
            val = Math.max(val, map[y][x+1]);
        if(y > 0 && map[y-1][x] > 0)
            val = Math.max(val, map[y-1][x]);
        if((y+1) < map.length && map[y+1][x] > 0)
            val = Math.max(val, map[y+1][x]);
        //System.err.printf("cell : %dx%d : %d\n", x, y, val);
        return val;
    }

    private static void printMap(final int[][] map) {
        for (int j = 0; j < map.length; ++j) {
            for (int i = 0; i < map[j].length; ++i) {
                char c = ' ';
                switch(map[j][i]) {
                    case WALL:
                        c = '#';
                        break;
                    case PASSAGE:
                        c = '.';
                        break;
                    default: {
                        if(map[j][i] >= 0)
                        if(map[j][i] < 10)
                            c = (char)('0' + map[j][i]); // 0..9
                        else {
                            if(map[j][i] < 36)
                                c = (char) ('a' + map[j][i] - 10); // 10..35
                            else
                                c = (char) ('A' + map[j][i] - 36); // 36....
                        }
                    }
                }
                System.out.print(c);
            }
            System.out.println();
        }
    }

    /**
     * traveling around graph, starting from skiplist[-1].
     */
    private static int findWayAround(final int[][] dist, final int[] skiplist) {
        final int startFrom = skiplist[skiplist.length-1];
        int best_way_time = Integer.MAX_VALUE;
        //int[] timeTravel = new int[dist.length];
        int[] skipPoints = Arrays.copyOf(skiplist, skiplist.length+1);// {startFrom, i};
        int nil = 0; // not-in-list
        for(int i = 0; i < skiplist.length; ++i)
            nil |= 1 << skiplist[i];
        final int mask = nil;
        if((dist.length - skiplist.length) <= 1) {
            for(int i = 0; i < dist.length; ++i)
                if((nil & 1 << i) == 0)
                    return dist[startFrom][i];
            return Integer.MAX_VALUE; // should not be...
        }
        for(int i = 0; i < dist.length; ++i) {
            if((mask & 1 << i) == 1) { // isIn(skiplist, i)
                //timeTravel[i] = Integer.MAX_VALUE;
                continue;
            }
            // timeTravel[i]
            int t = dist[startFrom][i] + findWayAround(dist, skipPoints);
            if(t < best_way_time)
                best_way_time = t;
        }
        return best_way_time;
    }

    private static boolean isIn(final int[] skiplist, final int n) {
        for(int p : skiplist)
            if(p == n)
                return true;
        return false;
    }
}
