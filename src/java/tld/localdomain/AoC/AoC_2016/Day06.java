package tld.localdomain.AoC.AoC_2016;

import java.util.Scanner;

/**
 * Day 6: Signals and Noise
 *
 * Something is jamming your communications with Santa. Fortunately, your signal is only partially jammed, and protocol in situations like this is to switch to a simple repetition code to get the message through.
 * In this model, the same message is sent repeatedly. You've recorded the repeating message signal (your puzzle input), but the data seems quite corrupted - almost too badly to recover. Almost.
 * All you need to do is figure out which character is most frequent for each position. For example, suppose you had recorded the following messages:
 *
 * eedadn
 * drvtee
 * eandsr
 * raavrd
 * atevrs
 * tsrnev
 * sdttsa
 * rasrtv
 * nssdts
 * ntnada
 * svetve
 * tesnvt
 * vntsnd
 * vrdear
 * dvrsen
 * enarar
 *
 * The most common character in the first column is e; in the second, a; in the third, s, and so on. Combining these characters returns the error-corrected message, easter.
 * Given the recording in your puzzle input, what is the error-corrected version of the message being sent?
 */
public class Day06 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static int MSG_LEN = 8;

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int[][] freq = new int[MSG_LEN][];
        for(int i = 0; i < MSG_LEN; ++i)
            freq[i] = new int[26];
        for(;sc.hasNext();) {
            String msg = sc.next();
            for(int i = 0; i < msg.length(); ++i) {
                char c = msg.charAt(i);
                ++freq[i][c - 'a'];
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < MSG_LEN; ++i) {
            int f = 0, c = 0;
            for(int j = 0; j < freq[i].length; ++j)
                if(freq[i][j] > f) {
                    f = freq[i][j];
                    c = j;
                }
            sb.append((char)('a' + c));
        }
        return sb.toString();
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        int[][] freq = new int[MSG_LEN][];
        for(int i = 0; i < MSG_LEN; ++i)
            freq[i] = new int[26];
        for(;sc.hasNext();) {
            String msg = sc.next();
            for(int i = 0; i < msg.length(); ++i) {
                char c = msg.charAt(i);
                ++freq[i][c - 'a'];
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < MSG_LEN; ++i) {
            int f = Integer.MAX_VALUE, c = 0;
            for(int j = 0; j < freq[i].length; ++j)
                if(freq[i][j] > 0 && freq[i][j] < f) {
                    f = freq[i][j];
                    c = j;
                }
            sb.append((char)('a' + c));
        }
        return sb.toString();
    }
}
