package tld.localdomain.AoC.AoC_2016;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 9: Explosives in Cyberspace #
 *
 * Wandering around a secure area, you come across a datalink port to a new part of the network. After briefly scanning it for interesting files, you find one file in particular that catches your attention. It's compressed with an experimental format, but fortunately, the documentation for the format is nearby.
 * The format compresses a sequence of characters. Whitespace is ignored. To indicate that some sequence should be repeated, a marker is added to the file, like (10x2). To decompress this marker, take the subsequent 10 characters and repeat them 2 times. Then, continue reading the file after the repeated data. The marker itself is not included in the decompressed output.
 * If parentheses or other characters appear within the data referenced by a marker, that's okay - treat it like normal data, not a marker, and then resume looking for markers after the decompressed section.
 * For example:
 *
 *  - ADVENT contains no markers and decompresses to itself with no changes, resulting in a decompressed length of 6.
 *  - A(1x5)BC repeats only the B a total of 5 times, becoming ABBBBBC for a decompressed length of 7.
 *  - (3x3)XYZ becomes XYZXYZXYZ for a decompressed length of 9.
 *  - A(2x2)BCD(2x2)EFG doubles the BC and EF, becoming ABCBCDEFEFG for a decompressed length of 11.
 *  - (6x1)(1x3)A simply becomes (1x3)A - the (1x3) looks like a marker, but because it's within a data section of another marker, it is not treated any differently from the A that comes after it. It has a decompressed length of 6.
 *  - X(8x2)(3x3)ABCY becomes X(3x3)ABC(3x3)ABCY (for a decompressed length of 18), because the decompressed data from the (8x2) marker (the (3x3)ABC) is skipped and not processed further.
 *
 * What is the decompressed length of the file (your puzzle input)? Don't count whitespace.
 */

public class Day09 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        StringBuilder sb = new StringBuilder();
        for(;sc.hasNext();)
            sb.append(BunnyPack.unpack(sc.next()));
        return sb.toString();
    }

    private static long part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        long cnt = 0;
        for(;sc.hasNext();)
            cnt += BunnyPack.unpack2(sc.next());
        return cnt;
    }
}

class BunnyPack {
    final static Pattern pPack = Pattern.compile("(\\((\\d+)x(\\d+)\\))"),
        pData = Pattern.compile("(.+)");

    public static String _unpack(final String src) {
        final StringBuilder sb = new StringBuilder();
        final Matcher m = pPack.matcher(src);
        for(int i = 0; i < src.length(); ++i) {
            char c = src.charAt(i);
            if(c != '(') {
                sb.append(c);
                continue;
            }
            if(!m.find(i)) {
                sb.append(c);
                continue;
            }
            final int len = Integer.parseInt(m.group(2)),
                    rep = Integer.parseInt(m.group(3));
            int pos = i + m.group(1).length();
            String sub = src.substring(pos, pos + len);
            for(int j = 0; j < rep; ++j)
                sb.append(sub);
            i = pos + len - 1;
        }
        return String.valueOf(sb.length());
    }

    public static int unpack(final String src) {
        final Matcher m = pPack.matcher(src);
        int cnt = 0;
        for(int i = 0; i < src.length(); ++i) {
            char c = src.charAt(i);
            if(c != '(') {
                ++cnt;
                continue;
            }
            if(m.find(i)) {
                final int len = Integer.parseInt(m.group(2)),
                        rep = Integer.parseInt(m.group(3)),
                        pos = i + m.group(1).length();
                cnt += len * rep;
                i = pos + len - 1;
            }
            else
                ++cnt;
        }
        return cnt;
    }

    public static long unpack2(final String src) {
        final Matcher m = pPack.matcher(src);
        long cnt = 0;
        for(int i = 0; i < src.length(); ++i) {
            char c = src.charAt(i);
            if(c != '(') {
                ++cnt;
                continue;
            }
            if(m.find(i)) {
                final int len = Integer.parseInt(m.group(2)),
                        rep = Integer.parseInt(m.group(3)),
                        pos = i + m.group(1).length();
                cnt += rep * unpack2(src.substring(pos, pos + len));
                i = pos + len - 1;
            }
            else
                ++cnt;
        }
        return cnt;
    }
}

