package tld.localdomain.AoC.AoC_2016;

import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 14: One-Time Pad
 * <p>
 * To generate keys, you first get a stream of random data by taking the MD5 of a pre-arranged salt (your puzzle input) and an increasing integer index (starting with 0, and represented in decimal); the resulting MD5 hash should be represented as a string of lowercase hexadecimal digits.
 * However, not all of these MD5 hashes are keys, and you need 64 new keys for your one-time pad. A hash is a key only if:
 * <p>
 * * It contains three of the same character in a row, like 777. Only consider the first such triplet in a hash.
 * * One of the next 1000 hashes in the stream contains that same character five times in a row, like 77777.
 * <p>
 * Considering future hashes for five-of-a-kind sequences does not cause those hashes to be skipped; instead, regardless of whether the current hash is a key, always resume testing for keys starting with the very next hash.
 * Your puzzle input is zpqevtbw.
 */
public class Day14 {
    public static void main(final String[] args) {
        //System.out.println(part1(args));
        //System.out.println(part1futa(args));
        //System.out.println(part2(args));
        /*
        {
            Instant start = Instant.now();
            String s = part1(args);
            Instant finish = Instant.now();
            System.out.printf("part1: %s (vs 16106) : time %d\n", s, Duration.between(start, finish).toMillis());
        }
        {
            Instant start = Instant.now();
            String s = part1futa(args);
            Instant finish = Instant.now();
            System.out.printf("part1futa: %s (vs 16106) : time %d\n", s, Duration.between(start, finish).toMillis());
        }
        {
            Instant start = Instant.now();
            String s = part2(args);
            Instant finish = Instant.now();
            System.out.printf("part2: %s (vs 22423) : time %d\n", s, Duration.between(start, finish).toMillis());
        }
        {
            Instant start = Instant.now();
            String s = part2futa(args);
            Instant finish = Instant.now();
            System.out.printf("part2futa: %s (vs 22423) : time %d\n", s, Duration.between(start, finish).toMillis());
        }
         */
        System.out.printf("part1 %s\n", measureTimeOf(Day14::part1, args));
        System.out.printf("part1futa %s\n", measureTimeOf(Day14::part1futa, args));
        System.out.printf("part2 %s\n", measureTimeOf(Day14::part2, args));
        System.out.printf("part2futa %s\n", measureTimeOf(Day14::part2futa, args));
    }

    private static String measureTimeOf(Function<String[],String> func, String[] args) {
        Instant start = Instant.now();
        String rs = func.apply(args);
        Instant finish = Instant.now();
        return String.format("'%s' in time %d\n", rs, Duration.between(start, finish).toMillis());
    }

    private static Pattern pat3 = Pattern.compile("(.)\\1{2}"),
            pat5 = Pattern.compile("(.)\\1{4}");

    private static String part1(final String[] args) {
        Map<Integer, String> mis = new HashMap<>();
        int key = 0, topCnt = 0;
        for (int n = 0; key < 64; ++n) {
            String khash = mis.get(n);
            if (khash == null) {
                if (n < topCnt) continue; // skip already known not-a-key
                khash = getMD5asString(args[0], n);
            }
            Matcher m3k = pat3.matcher(khash);
            if (!m3k.find()) continue;
            final char c3 = m3k.group(1).charAt(0);
            //System.out.printf("Candidate : n %d : p %d : c3 %c : %s\n", n, p, c3, hash);
            for (int i = n + 1; i < n + 1001; ++i) {
                String hash = mis.get(i);
                if (hash == null) {
                    if (i < topCnt) continue; // skip known not-a-key
                    hash = getMD5asString(args[0], i);
                    topCnt = i;
                }
                Matcher m3 = pat3.matcher(hash);
                if (!m3.find()) continue;
                if (!mis.containsKey(i)) mis.put(i, hash);
                Matcher m5 = pat5.matcher(hash);
                if (!m5.find()) continue;
                if (m5.group(1).charAt(0) == c3) {
                    ++key;
                    //System.out.printf("Key %d : index %d : %s\n", key, n, mis.get(n));
                    if (key == 64)
                        key = n;
                    break;
                }
            }
        }
        return String.valueOf(key);
    }

    private static String _part1(final String[] args) {
        Map<Integer, String> mis = new HashMap<>();
        int key = 0;
        for (int n = 0; key < 64; ++n) {
            String khash;
            if (mis.containsKey(n)) {
                khash = mis.get(n);
                if (khash == null) continue;
            } else
                khash = getMD5asString(args[0], n);
            Matcher m3k = pat3.matcher(khash);
            if (!m3k.find()) continue;
            if (!mis.containsKey(n)) mis.put(n, khash);
            final char c3 = m3k.group(1).charAt(0);
            for (int i = n + 1; i < n + 1001; ++i) {
                String hash;
                if (mis.containsKey(i)) {
                    hash = mis.get(i);
                    if (hash == null) continue;
                } else
                    hash = getMD5asString(args[0], i);
                Matcher m3 = pat3.matcher(hash);
                if (!m3.find()) continue;
                if (!mis.containsKey(i)) mis.put(i, hash);
                Matcher m5 = pat5.matcher(hash);
                if (!m5.find()) continue;
                if (m5.group(1).charAt(0) == c3) {
                    ++key;
                    //System.out.printf("Key %d : index %d : %s\n", key, n, mis.get(n));
                    if (key == 64)
                        key = n;
                    break;
                }
            }
        }
        return String.valueOf(key);
    }

    private static String part2(final String[] args) {
        Map<Integer, String> mis = new HashMap<>();
        int key = 0, topCnt = 0;
        for (int n = 0; key < 64; ++n) {
            String khash = mis.get(n);
            if (khash == null) {
                if (n < topCnt) continue; // skip already known not-a-key
                khash = getMD5asString(args[0], n, 1 + 2016);
            }
            Matcher m3k = pat3.matcher(khash);
            if (!m3k.find()) continue;
            final char c3 = m3k.group(1).charAt(0);
            for (int i = n + 1; i < n + 1001; ++i) {
                String hash = mis.get(i);
                if (hash == null) {
                    if (i < topCnt) continue; // skip already known not-a-key
                    hash = getMD5asString(args[0], i, 1 + 2016);
                    topCnt = i;
                }
                Matcher m = pat3.matcher(hash);
                if (!m.find()) continue;
                if (!mis.containsKey(i)) mis.put(i, hash);
                m = pat5.matcher(hash);
                if (!m.find()) continue;
                if (c3 == m.group(1).charAt(0)) {
                    ++key;
                    //System.out.printf("Key %d : index %d with %d : %s\n", key, n, i, mis.get(n));
                    if (key == 64)
                        key = n;
                    break;
                }
            }
        }
        return String.valueOf(key);
    }

    private static String getMD5asString(final String salt, final int n) {
        return getMD5(salt + n);
    }

    /**
     * stretch MD5-hash of (src+n) by rep times
     */
    private static String getMD5asString(final String src, final int n, final int rep) {
        String hash = src + n;
        for (int i = 0; i < rep; ++i)
            hash = getMD5(hash);
        return hash;
    }

    private static String getMD5(String src) {
        String hash;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] rawhash = md5.digest(src.getBytes());
            hash = byte2str(rawhash);
        } catch (NoSuchAlgorithmException ex) { // should not happen, ever
            hash = null;
            ex.printStackTrace();
        }
        return hash;
    }

    private static char hexdig[] = {'a', 'b', 'c', 'd', 'e', 'f'};

    public static String byte2str(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            byte b = bytes[i];
            int l = b & 0x0F;
            int h = (b >> 4) & 0x0F;
            if (h < 10)
                sb.append(h);
            else
                sb.append(hexdig[h - 10]);
            if (l < 10)
                sb.append(l);
            else
                sb.append(hexdig[l - 10]);
        }
        return sb.toString();
    }

    private static ExecutorService executor;

    private static String part1futa(final String[] args) {
        executor = Executors.newFixedThreadPool(24);
        Map<Integer, String> mis = new ConcurrentHashMap<>();
        int key = 0;
        int topCnt = 0;
        for (int n = 0; key < 64; ++n) {
            String hash = mis.get(n);
            if (hash == null) {
                if (n < topCnt) continue; // skip already known not-a-key
                hash = getMD5asString(args[0], n);
            }
            Matcher m3 = pat3.matcher(hash);
            if (!m3.find()) continue;
            final char c3 = m3.group(1).charAt(0);
            List<Future<String>> lfs = new ArrayList<>();
            for (int i = topCnt; i < n + 1001; ++i) { // compute-ahead
                topCnt = i;
                Future<String> fs = spamFutureHash(mis, args[0], i);
                if (fs != null)
                    lfs.add(fs);
            }
            topCnt = n + 1000;
            // wait 4 finish threads...
            for (; !lfs.isEmpty(); ) {
                try {
                    Thread.sleep(lfs.size() >> 4);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                List<Future<String>> lfs2wait = new ArrayList<>();
                for (Future<String> fs : lfs) {
                    if (!fs.isDone())
                        lfs2wait.add(fs);
                }
                lfs = lfs2wait;
            }
            // test collected hashes
            for (int i = n + 1; i < n + 1001; ++i) {
                hash = mis.get(i);
                if (hash == null) {
                    continue;
                }
                Matcher m5 = pat5.matcher(hash);
                if (!m5.find()) continue;
                if (c3 == m5.group(1).charAt(0)) {
                    ++key;
                    //System.out.printf("Key %d : index %d : %s\n", key, n, mis.get(n));
                    if (key == 64)
                        key = n;
                    break;
                }
            }
        }
        executor.shutdown();
        executor = null;
        return String.valueOf(key);
    }

    private static String part2futa(final String[] args) {
        executor = Executors.newFixedThreadPool(24);
        Map<Integer, String> mis = new ConcurrentHashMap<>();
        int key = 0;
        int topCnt = 0;
        for (int n = 0; key < 64; ++n) {
            String hash = mis.get(n);
            if (hash == null) {
                if (n < topCnt) continue; // skip already known not-a-key
                hash = getMD5asString(args[0], n, 1 + 2016);
            }
            Matcher m3 = pat3.matcher(hash);
            if (!m3.find()) continue;
            final char c3 = m3.group(1).charAt(0);
            //System.out.printf("testing key %d : %s\n", n, hash);
            List<Future<String>> lfs = new ArrayList<>();
            for (int i = topCnt; i < n + 1001; ++i) { // compute-ahead
                topCnt = i;
                Future<String> fs = spamFutureHash(mis, args[0], i, 1 + 2016);
                if (fs != null)
                    lfs.add(fs);
            }
            topCnt = n + 1000;
            // wait 4 finish threads...
            for (; !lfs.isEmpty(); ) {
                //System.out.printf("... wait for %d computations finish...\n", lfs.size());
                try {
                    Thread.sleep(lfs.size() >> 4);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                List<Future<String>> lfs2wait = new ArrayList<>();
                for (Future<String> fs : lfs) {
                    if (!fs.isDone())
                        lfs2wait.add(fs);
                }
                lfs = lfs2wait;
            }
            //System.out.printf("... hashes collected %d\n", mis.size());
            // test collected hashes
            for (int i = n + 1; i < n + 1001; ++i) {
                hash = mis.get(i);
                if (hash == null) {
                    //System.out.printf("hash is null! idx d%\n", i);
                    continue;
                }
                Matcher m5 = pat5.matcher(hash);
                if (!m5.find()) continue;
                if (c3 == m5.group(1).charAt(0)) {
                    ++key;
                    //System.out.printf("Key %d : index %d : %s\n", key, n, mis.get(n));
                    if (key == 64)
                        key = n;
                    break;
                }
            }
        }
        executor.shutdown();
        executor = null;
        return String.valueOf(key);
    }

    private static Future<String> spamFutureHash(Map<Integer, String> mis, String salt, int idx) {
        return spamFutureHash(mis, salt, idx, 1);
    }

    private static Future<String> spamFutureHash(Map<Integer, String> mis, String salt, int idx, int rep) {
        if (mis.get(idx) != null) return null;
        Callable<String> task = () -> {
            String hash = getMD5asString(salt, idx, rep);
            Matcher m3 = pat3.matcher(hash);
            if (m3.find()) {
                mis.put(idx, hash);
                return hash;
            }
            return null;
        };
        return executor.submit(task);
    }
}

class ComputeMD5 implements Runnable { // extends Thread
    String src, hash;

    ComputeMD5(String src) {
        this.src = src;
        hash = null;
    }

    public void run() {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] rawhash = md5.digest(src.getBytes());
            //BigInteger bi = new BigInteger(1, rawhash);
            //hash = bi.toString(16);
            hash = Day14.byte2str(rawhash);
        } catch (NoSuchAlgorithmException ex) { // should not happen, ever
            // hash = null;
            //ex.printStackTrace();
        }
    }
}
