package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 22: Grid Computing
 *
 * You gain access to a massive storage cluster arranged in a grid; each storage node is only connected to the four nodes directly adjacent to it (three if the node is on an edge, two if it's in a corner).
 *
 * You can directly access data only on node /dev/grid/node-x0-y0, but you can perform some limited actions on the other nodes:
 *
 *  * You can get the disk usage of all nodes (via df). The result of doing this is in your puzzle input.
 *  * You can instruct a node to move (not copy) all of its data to an adjacent node (if the destination node has enough space to receive the data). The sending node is left empty after this operation.
 *
 * Nodes are named by their position: the node named node-x10-y10 is adjacent to nodes node-x9-y10, node-x11-y10, node-x10-y9, and node-x10-y11.
 *
 * Before you begin, you need to understand the arrangement of data on these nodes. Even though you can only move data between directly connected nodes, you're going to need to rearrange a lot of the data to get access to the data you need. Therefore, you need to work out how you might be able to shift data around.
 *
 * To do this, you'd like to count the number of viable pairs of nodes. A viable pair is any two nodes (A,B), regardless of whether they are directly connected, such that:
 *
 *  * Node A is not empty (its Used is not zero).
 *  * Nodes A and B are not the same node.
 *  * The data on node A (its Used) would fit on node B (its Avail).
 *
 * How many viable pairs of nodes are there?
 */
public class Day22 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<Node> nodes = new ArrayList<>();
        for(;sc.hasNext();) {
            Node node = Node.createNode(sc.next());
            if(node != null)
                nodes.add(node);
        }
        int pairs = 0;
        for(int i = 0; i < nodes.size(); ++i) {
            for(int j = 0; j < nodes.size(); ++j) {
                if(i == j) continue;
                Node a = nodes.get(i),
                    b = nodes.get(j);
                if(a.getUsed() > 0 && b.isFit(a.getUsed())) ++pairs;
            }
        }
        return String.valueOf(pairs); //.toString();
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        List<Node> nodes = new ArrayList<>();
        for(;sc.hasNext();) {
            Node node = Node.createNode(sc.next());
            if(node != null)
                nodes.add(node);
        }
        return null; //.toString();
    }

}

class Node {
    // Filesystem              Size  Used  Avail  Use%
    // /dev/grid/node-x13-y17   89T   73T    16T   82%
    private static Pattern pat = Pattern.compile("^/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T");

    final int x, y, size;
    int used;

    Node(int x, int y, int size) {
        this(x, y, size, 0);
    }

    Node(int x, int y, int size, int used) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.used = used;
    }

    public static Node createNode(final String str) {
        Matcher m = pat.matcher(str);
        if(!m.find()) return null;
        int x = Integer.parseInt(m.group(1)),
            y = Integer.parseInt(m.group(2)),
            size = Integer.parseInt(m.group(3)),
            used = Integer.parseInt(m.group(4));
        return new Node(x, y, size, used);
    }

    public int getUsed() { return used; }
    public void setUsed(int used) { this.used = used; }
    public boolean isFit(final int newDataSize) { return size >= (used + newDataSize); }
}
