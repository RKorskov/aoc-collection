package tld.localdomain.AoC.AoC_2016;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class Utils {
    static Scanner getScanner(final String[] args) {
        Scanner sc = null;
        if(args[0].equals("-"))
            sc = new Scanner(System.in);
        else {
            Path fpin = Path.of(args[0]);
            if(Files.exists(fpin))
                try {
                    sc = new Scanner(fpin);
                } catch (IOException ex) {
                    sc = null;
                }
            if(sc == null)
                sc = new Scanner(args[0]);
        }
        sc.useDelimiter("\\s+");
        return sc;
    }
}
