package tld.localdomain.AoC.AoC_2016;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Day 12: Leonardo's Monorail
 *
 * The assembunny code you've extracted operates on four registers (a, b, c, and d) that start at 0 and can hold any integer. However, it seems to make use of only a few instructions:
 *
 *  - inc x increases the value of register x by one.
 *  - dec x decreases the value of register x by one.
 *  - cpy x y copies integer x into register y.
 *  - cpy x y copies the value of a register x into register y.
 *  - jnz x y jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
 *    jnz reg int
 *    jnz int reg
 *    jnz int int
 *
 * The jnz instruction moves relative to itself: an offset of -1 would continue at the previous instruction, while an offset of 2 would skip over the next instruction.
 * For example:
 *
 * cpy 41 a
 * inc a
 * inc a
 * dec a
 * jnz a 2
 * dec a
 *
 * The above code would set register a to 41, increase its value by 2, decrease its value by 1, and then skip the last dec a (because a is not zero, so the jnz a 2 skips it), leaving register a at 42. When you move past the last instruction, the program halts.
 * After executing the assembunny code in your puzzle input, what value is left in register a?
 */
public class Day12 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static String part1(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        Assembunny usagi = new Assembunny();
        List<String> cmds = new ArrayList<>();
        for(;sc.hasNext();) {
            String cmd = sc.next().split(";")[0].trim();
            cmds.add(cmd);
        }
        for(;usagi.getIP() < cmds.size();) {
            String cmd = cmds.get(usagi.getIP());
            usagi.hop(cmd);
        }
        return usagi.toString();
    }

    private static String part2(final String[] args) {
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        Assembunny usagi = new Assembunny();
        List<String> cmds = new ArrayList<>();
        for(;sc.hasNext();) {
            String cmd = sc.next().split(";")[0].trim();
            cmds.add(cmd);
        }
        usagi.regfile.setRegByName("c", 1);
        for(;usagi.getIP() < cmds.size();) {
            String cmd = cmds.get(usagi.getIP());
            usagi.hop(cmd);
        }
        return usagi.toString();
    }
}
