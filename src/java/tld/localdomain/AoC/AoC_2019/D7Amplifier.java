// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-13 08:07:16 roukoru>

import java.util.Arrays;

class D7Amplifier {
    Intcode 电脑;
    private D7Amplifier(){;}

    public long eval(final long[] prog_in) {
        return 电脑.eval(prog_in);
    }

    public static D7Amplifier create(final long[] prog) {
        D7Amplifier 功放 = new D7Amplifier();
        工仿.电脑 = D9Intcode.create(prog);
        return 功放;
    }
}
