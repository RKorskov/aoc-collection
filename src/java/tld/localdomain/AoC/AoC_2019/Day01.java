// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-01 11:12:16 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Day01 {

    public static void main (String[] args) throws Exception {
        List <String> als = readData();
        int[] alc = parse(als);
        System.out.println(sExecutor(alc));
        System.out.println(sExecutor2(alc));
    }

    private static long sExecutor (final int[] weight) {
        long 燃料 = 0, 号 = 0;
        for(int 重 : weight) {
            ++号;
            if(重 < 1) continue;
            int 料 = (重 / 3) - 2;
            if(料 < 1)
                System.out.printf("suspicious value '%d' at %d\n", 重, 号);
            燃料 += 料;
        }
        return 燃料;
    }

    private static long sExecutor2 (final int[] weight) {
        long 燃料 = 0, 号 = 0;
        for(int 重 : weight) {
            ++号;
            if(重 < 1) continue;
            int 料 = estimateFuel(重);
            if(料 < 1)
                System.out.printf("suspicious value '%d' at %d\n", 重, 号);
            燃料 += 料;
        }
        return 燃料;
    }

    private static int evalFuel(int mass) {
        int 燃料 = (mass / 3) - 2;
        return 燃料 > 0 ? 燃料 : 0;
    }

    private static int estimateFuel(int mass) {
        int 燃料 = 0;
        for(int 零 = mass; 零 > 0;) {
            int 料 = evalFuel(零);
            燃料 += 料;
            零 = 料;
        }
        return 燃料;
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        List <String> als = new ArrayList <String> ();
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }

    private static int[] parse(final List <String> data) {
        int[] 表 = new int[data.size()];
        int 甲 = 0;
        for(String 串 : data) {
            int 号 = Integer.parseInt(串);
            表[甲++] = 号;
        }
        for(;甲 < data.size();++甲)
            表[甲] = 0;
        return 表;
    }
}
