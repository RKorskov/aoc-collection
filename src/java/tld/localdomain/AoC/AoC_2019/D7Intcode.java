// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-09 09:07:34 roukoru>

import java.util.Arrays;

class D7Intcode implements Intcode {
    long[] 程序源;
    boolean overwrite;
    private D7Intcode(){;}

    public long eval(final long[] prog_in) {
        long 值出 = -1;
        int 号来 = 0;
        long[] 程序;
        if(overwrite)
            程序 = 程序源;
        else
            程序 = Arrays.copyOf(程序源, 程序源.length);
        REPL: for(int 标 = 0; 程序[标] != 99; 标 += 2) {
            int 地向 = (int)程序[标+3],
                地甲 = (int)程序[标+1],
                地乙 = (int)程序[标+2];
            int 令 = ((int)程序[标]) % 100;
            long 甲 = (程序[标] / 100) % 10 == 1 ? 地甲 //
                : (地甲 < 程序.length ? 程序[地甲] : Long.MIN_VALUE),
                乙 = (程序[标] / 1000) % 10 == 1 ? 地乙 //
                : (地乙 < 程序.length ? 程序[地乙] : Long.MIN_VALUE);
            switch(令) {
            case 1: // ADD:
                程序[地向] = 甲 + 乙;
                标 += 2;
                break;
            case 2: // MUL:
                程序[地向] = 甲 * 乙;
                标 += 2;
                break;
            case 3: // IN:
                程序[地甲] = prog_in[号来++];
                break;
            case 4: // OUT:
                值出 = 甲;
                break;
            case 5: // JNZ
                if(甲 != 0)
                    标 = (int)乙 - 2;
                else
                    ++标;
                break;
            case 6: // JZ
                if(甲 == 0)
                    标 = (int)乙 - 2;
                else
                    ++标;
                break;
            case 7: // CMPL
                程序[地向] = 甲 < 乙 ? 1 : 0;
                标 += 2;
                break;
            case 8: // CMP
                程序[地向] = 甲 == 乙 ? 1 : 0;
                标 += 2;
                break;
            case 99: // HALT:
                break REPL;
            default:
                return -1;
            }
        }
        return 值出;
    }

    public void enableOverwrite() {
        overwrite = true;
    }

    public static D7Intcode create(final long[] prog) {
        D7Intcode 电脑 = new D7Intcode();
        电脑.程序源 = Arrays.copyOf(prog, prog.length);
        电脑.overwrite = false;
        return 电脑;
    }
}
