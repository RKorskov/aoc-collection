// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-23 12:50:01 roukoru>

import java.util.Scanner;
import java.util.Arrays;

public class Day09 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        // System.out.println(Arrays.toString(al));
        sExecutor1(al, 数河);
        // System.out.println(sExecutor1(al, 数河));
        // System.out.println(sExecutor2(al, 数河));
    }

    private static long sExecutor1 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -1;
        Intcode 电脑 = D9Intcode.create(prog);
        long rc = 电脑.eval(prog_in);
        for(long 号 : 电脑.getOutput())
            System.out.printf("%d ", 号);
        System.out.println();
        return rc;
    }

    private static long sExecutor2 (final long[] prog, final int[] prog_in) {
        /*
        long[] 程序;
        for(int 名词 = 0; 名词 < 100; ++名词) {
            for(int 动词 = 0; 动词 < 100; ++动词) {
                程序 = Arrays.copyOf(prog, prog.length);
                eval(程序, prog_in);
                if(程序[0] == 19690720)
                    return 名词 * 100 + 动词;
            }
        }
        */
        return -1;
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}
