// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2020-01-03 14:21:13 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Objects;

/*
 * Universal Orbit Map
 */
public class Day06 {

    public static void main (String[] args) throws Exception {
        List <String> alo = readData();
        D6Tree 木 = parseData(alo);
        if(木 == null)
            return;
        reValuateTree(木);
        // System.out.println(木);
        System.out.println(sExecutor(木));
        System.out.println(sExecutor2(木));
    }

    private static long sExecutor (final D6Tree d6t) {
        return d6t.sum();
    }

    private static long sExecutor2 (final D6Tree d6t) {
        return -1;
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> 串表 = new ArrayList <String>();
        //器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() > 0)
                串表.add(串);
        }
        return 串表;
    }

    private static Deque <String[]> parseAsDeque(final List <String> sdata) {
        Deque <String[]> 叶卡 = new ArrayDeque <String[]> ();
        for(String 串 : sdata) {
            String[] 父子 = 串.split("[)]");
            if(父子.length > 1)
                叶卡.add(父子);
        }
        return 叶卡;
    }

    private static void buildMap(Map <String,D6Tree> 叶堆,
                                 Deque <String[]> 叶列) {
        for(int 号 = 叶列.size(); 号 > 0; --号) {
            String[] 父子 = 叶列.pop();
            String 父 = 父子[0],
                子 = 父子[1];
            if(叶堆.containsKey(父)) {
                叶列.addLast(父子);
            }
            else {
                D6Tree 叶 = D6Tree.createLeaf(子);
                if(叶 == null) continue;
                boolean 新 = true;
                for(String 本 : 叶堆.keySet()) {
                    D6Tree 星 = 叶堆.get(本);
                    if(星.add(叶, 父)) {
                        新 = false;
                        break;
                    }
                }
                if(新)
                    叶堆.put(父, 叶);
            }
        }
    }

    private static void optimizeMap(Map <String,D6Tree> 叶堆) {
        Set <String> 卡 = new HashSet <String> ();
        for(String 父 : 叶堆.keySet()) {
            if(卡.contains(父)) continue;
            D6Tree 节父 = 叶堆.get(父);
            for(String 子 : 叶堆.keySet()) {
                if(卡.contains(子) || 父.equals(子)) continue;
                D6Tree 叶子 = 叶堆.get(子);
                if(节父.add(叶子, 子))
                    卡.add(子);
            }
        }
        for(String 键 : 卡)
            叶堆.remove(键);
    }

    private static D6Tree parseData(final List <String> sdata) {
        D6Tree 天林 = null;
        Map <String,D6Tree> 叶堆 = new HashMap <String,D6Tree> ();
        Deque <String[]> 叶列 = parseAsDeque(sdata);
        int 长 = Integer.MAX_VALUE;
        for(;叶堆.size() > 1 || 叶列.size() > 0;) {
            buildMap(叶堆, 叶列);
            optimizeMap(叶堆);
            if(长 <= 叶堆.size()) {
                System.out.printf("possible loop detected, exiting.\n");
                break;
            }
            长 = 叶堆.size();
        }
        return 叶堆.values().toArray(new D6Tree[0])[0];
    }

    private static void reValuateTree(final D6Tree tree) {
        if(tree == null) return;
        tree.重 = tree.父 == null ? 1 : (tree.父.getOrbitIdx()+1);
        if(tree.孩子 != null)
            for(D6Tree 叶 : tree.孩子)
                reValuateTree(叶);
    }
}

class D6Orbituary {
    D6Tree 星林;
    List <String> 星球;
}

class D6Tree {
    private static int WEIGHT_UNDEF = Integer.MIN_VALUE;
    D6Tree 父;
    String 名;
    List <D6Tree> 孩子; // set吗
    int 重;
    private D6Tree(){;}

    /**
     * attach leaf to _this_ node
     */
    public void attach(final D6Tree leaf) {
        if(leaf == null) return;
        if(孩子 == null)
            孩子 = new ArrayList <D6Tree> ();
        孩子.add(leaf);
        leaf.父 = this;
    }

    /**
     * add leaf to tree,
     * if parent not found, leaf not attached
     */
    public boolean add(final D6Tree leaf) {
        if(leaf == null) return false;
        if(leaf.父 == this || leaf.父 == null) {
            this.attach(leaf);
            return true;
        }
        return false;
    }

    /**
     * add leaf to tree,
     * if parent not found, leaf not attached
     * @param tag label of this leaf
     */
    public boolean add(final D6Tree leaf, final String parentag) {
        if(leaf == null) return false;
        if(名.equals(parentag)) {
            this.attach(leaf);
            return true;
        }
        if(孩子 == null || 孩子.size() < 1)
            return false;
        for(D6Tree 子 : 孩子)
            if(子.add(leaf, parentag))
                return true;
        return false;
    }

    public int numberLeafs() {
        if(孩子 == null || 孩子.size() < 1)
            return 0;
        int 甲 = 孩子.size();
        for(D6Tree 子 : 孩子)
            甲 += 子.numberLeafs();
        return 甲;
    }

    public boolean isLeaf() {
        return 孩子 == null || 孩子.size() < 1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(名);
        sb.append('(');
        sb.append(重 >= 0 ? 重 : '-');
        sb.append(')');
        if(孩子 != null) {
            sb.append(":[");
            boolean 指 = false;
            for(D6Tree 子 : 孩子) {
                if(指)
                    sb.append(' ');
                else
                    指 = true;
                sb.append(子);
            }
            sb.append(']');
        }
        return sb.toString();
    }

    public int getOrbitIdx() {return 重;}

    /**
     * returns distance from (to) root
     */
    public int getWeight() {
        return -1;
    }

    public int sum() {
        int 加 = 重;
        if(孩子 != null && 孩子.size() > 0)
            for(D6Tree 叶 : 孩子)
                加 += 叶.sum();
        return 加;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof D6Tree)) return false;
        D6Tree 木 = (D6Tree) obj;
        if(this == 木) return true;
        if(父 != 木.父 || !名.equals(木.名)) return false;
        if(孩子 == 木.孩子) return true;
        for(D6Tree 子 : 孩子) {
            boolean 不好 = true;
            for(D6Tree 叶: 木.孩子)
                if(子.equals(叶)) {
                    不好 = false;
                    break;
                }
            if(不好) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(名, 父, 孩子);
    }

    public static D6Tree createTree() {
        D6Tree 点 = new D6Tree();
        点.父 = null; // I am the Root!
        点.孩子 = null;
        点.名 = null;
        return 点;
    }

    public static D6Tree createTree(final String tag) {
        D6Tree 点 = new D6Tree();
        点.父 = null; // I am The Root!
        点.孩子 = null;
        点.名 = tag;
        点.重 = 0;
        return 点;
    }

    public static D6Tree createLeaf(final D6Tree parent, final String tag) {
        D6Tree 点 = new D6Tree();
        点.父 = parent;
        点.孩子 = null;
        点.名 = tag;
        点.重 = parent.getOrbitIdx() == WEIGHT_UNDEF ? //
            WEIGHT_UNDEF : (parent.getOrbitIdx() + 1);
        return 点;
    }

    public static D6Tree createLeaf(final String tag) {
        D6Tree 点 = new D6Tree();
        点.父 = null;
        点.孩子 = null;
        点.名 = tag;
        点.重 = WEIGHT_UNDEF;
        return 点;
    }
}

