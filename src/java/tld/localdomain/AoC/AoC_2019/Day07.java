// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-09 10:00:01 roukoru>

import java.util.Scanner;

public class Day07 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        if(args.length > 0) {
            int[] 数河 = new int[args.length];
            int 号 = 0;
            for(String 串 : args)
                数河[号++] = Integer.parseInt(串);
            System.out.println(testExecutor(al, 数河));
        }
        else
            System.out.println(sExecutor1(al));
            System.out.println(sExecutor2(al));
    }

    private static long testExecutor (final long[] prog, final int[] phases) {
        if(prog.length < 3) return -1;
        D7Amplifier 程序 = D7Amplifier.createAmplifier(prog);
        long 总 = 0;
        for(int p : phases) {
            long[] d = {p, 总};
            总 = 程序.eval(d);
            System.out.printf("%d %d\n", p, 总);
        }
        return 总;
    }

    private static long sExecutor1 (final long[] prog) {
        if(prog.length < 3) return -1;
        D7Amplifier 功放 = D7Amplifier.createAmplifier(prog);
        long 大总 = 0;
        final int 从 = 0, 向 = 5;
        for(int 零 = 从; 零 < 向; ++零)
            for(int 甲 = 从; 甲 < 向; ++甲) {
                if(甲 == 零) continue;
                for(int 乙 = 从; 乙 < 向; ++乙) {
                    if(乙 == 零 || 乙 == 甲) continue;
                    for(int 丙 = 从; 丙 < 向; ++丙) {
                        if(丙 == 零 || 丙 == 甲 || 丙 == 乙) continue;
                        for(int 丁  = 从; 丁 < 向; ++丁) {
                            if(丁 == 零 || 丁 == 甲 || 丁 == 乙 || 丁 == 丙)
                                continue;
                            long 总 = 0;
                            int[] 表 = {零, 甲, 乙, 丙, 丁};
                            for(int p : 表) {
                                long[] d = {p, 总};
                                总 = 功放.eval(d);
                            }
                            if(总 > 大总)
                                大总 = 总;
                        }
                    }
                }
            }
        return 大总;
    }

    private static long sExecutor2 (final long[] prog) {
        if(prog.length < 3) return -1;
        D7Amplifier[] 功放 = new D7Amplifier[5];
        for(int 甲 = 0; 甲 < 5; ++甲) {
            功放[甲] = D7Amplifier.createAmplifier(prog);
            功放[甲].enableOverwrite();
        }
        long 大总 = 0;
        final int 从 = 5, 向 = 10;
        for(int 零 = 从; 零 < 向; ++零)
            for(int 甲 = 从; 甲 < 向; ++甲) {
                if(甲 == 零) continue;
                for(int 乙 = 从; 乙 < 向; ++乙) {
                    if(乙 == 零 || 乙 == 甲) continue;
                    for(int 丙 = 从; 丙 < 向; ++丙) {
                        if(丙 == 零 || 丙 == 甲 || 丙 == 乙) continue;
                        for(int 丁  = 从; 丁 < 向; ++丁) {
                            if(丁 == 零 || 丁 == 甲 || 丁 == 乙 || 丁 == 丙)
                                continue;
                            long 总 = 0;
                            final int[] 表 = {零, 甲, 乙, 丙, 丁};
                            for(int i = 0; i < 5; ++i) {
                                long[] d = {表[i], 总};
                                总 = 功放[i].eval(d);
                            }
                            if(总 > 大总)
                                大总 = 总;
                        }
                    }
                }
            }
        return 大总;
    }


    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}
