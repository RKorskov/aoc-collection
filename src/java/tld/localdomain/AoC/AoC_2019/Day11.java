// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2020-01-14 08:49:47 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Supplier;

public class Day11 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        // System.out.println(Arrays.toString(al));
        System.out.println(sExecutor1(al, 数河));
        //System.out.println(sExecutor2(al, 数河));
    }

    private static long sExecutor1 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        // Map <D11XY,Boolean> 图 = new HashMap <D11XY,Boolean> ();
        long 回数 = 电脑.eval(new long[0]);
        long[] 回词 = 电脑.getOutput();
        System.out.println(回数);
        System.out.println(Arrays.toString(回词));
        return 回数;
    }

    private static long sExecutor2 (final long[] prog, final int[] prog_in) {
        return -1;
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}

enum D11Color {
    BLACK, WHITE;
    public static D11Color getColor(long clr) {
        return clr == 0 ? D11Color.BLACK : D11Color.WHITE;
    }
}

enum D11TurnDir {
    LEFT, RIGHT;
    public static D11TurnDir getDir(long dir) {
        return dir == 0 ? D11TurnDir.LETF : D11TurnDir.RIGHT;
    }
}

class D11XY {
    final int x, y;
    D11Color 色; // free payload

    D11XY(int x, int y) {
        this.x = x;
        this.y = y;
        色 = D11Color.BLACK;
    }

    public int hashCode() {
        return Objects.hash(x, y);
    }

    public boolean equals(final Object obj) {
        if(obj == null || !(obj instanceof D11XY)) return false;
        D11XY 点 = (D11XY)obj;
        return this == 点 || x == 点.x && y == 点.y;
    }
}

class D11PR {
    int x, y;
    Intcode 电脑;
    Set <D11XY> 图;

    /**
     * The program uses input instructions to access the robot's
     * camera. Provide:
     * 0 if the robot is over a black panel or
     * 1 if the robot is over a white panel.
     */
    public long inputHandler() {
        long[] 数语 = 电脑.getOutput();
        // или же лучше 2 последних?
        D11Color 色 = D11Color.getColor(语数[0]);
        D11TurnDir 方 = D11TurnDir.getDir(语数[1]);
        return -1L;
    }

    public static D11PR create(final long[] prog) {
        D11PR drone = new D11PR();
        drone.图 = new HashSet <D11XY> ();
        drone.电脑 = D9Intcode.create(prog);
        drone.电脑.enableRetainMemory();
        Supplier <Long> sup = ()->drone.inputHandler();
        drone.电脑.registerInputSource(sup);
        drone.x = 0;
        drone.y = 0;
        return drone;
    }
}
