// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-26 01:10:40 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Slam Shuffle
 */
public class Day22 {

    public static void main (String[] args) throws Exception {
        List <String> 串表 = readData();
        List <D22SCardOp> 令表 = parseData(串表);
        long[] 数河 = args2long(args);
        long 卡号 = 数河 != null && 数河.length > 0 ? 数河[0] : 7,
            座长 = 数河 != null && 数河.length > 1 ? 数河[1] : 10;
        // System.out.println(sExecutor0(令表, 卡号));
        // System.out.println(sExecutor1(令表));
        // System.out.println(sExecutor2(令表));
        // System.out.println(sExecutor3(令表, 卡号, 座长));
        System.out.println(sExecutor4(令表, (int)卡号, (int)座长));
    }

    private static int sExecutor0(final List <D22SCardOp> copl,
                                  final int stackSize) {
        if(stackSize < 1) return -1;
        D22SCards 卡座 = D22SCards.create(stackSize);
        REPL:
        for(D22SCardOp 令 : copl)
            卡座.manage(令);
        // System.out.println(卡座);
        return 卡座.getIdx(2019);
    }

    /**
     * After shuffling your factory order deck of 10007 cards,
     * what is the position of card 2019?
     */
    private static long sExecutor1(final List <D22SCardOp> copl) {
        D22SCardsL 卡座 = D22SCardsL.create(2019, 10007);
        for(D22SCardOp 令 : copl)
            卡座.manage(令);
        return 卡座.getIdx();
    }

    private static final long SEX2_REPL = 101_7415_8207_6661L,
        SEX2_SIZE = 119315717514047L;

    /**
     * 1. factory order deck of 119315717514047 cards;
     * 2. apply complete shuffle process to the deck 101741582076661 times in a row;
     * 3. what number is on the card that ends up in position 2020?
     */
    private static long sExecutor2(final List <D22SCardOp> copl) {
        D22SCardsL 卡座 = D22SCardsL.create(2020, SEX2_SIZE);
        long 号 = -1;
        REPL:
        for(号 = 0; 号 < 10; ++号) { // SEX2_REPL
            for(D22SCardOp 令 : copl)
                卡座.manage(令);
            System.out.printf("%d %d\n", 号, 卡座.getIdx());
        }
        return 卡座.getIdx();
    }

    private static long sExecutor3(final List <D22SCardOp> copl,
                                   final long cardIdx,
                                   final long stackSize) {
        if(stackSize < cardIdx) return -1;
        D22SCardsL 卡座 = D22SCardsL.create(cardIdx, stackSize);
        long 号 = -1;
        REPL:
        for(号 = 0; 号 < 53; ++号) { // SEX2_REPL
            for(D22SCardOp 令 : copl)
                卡座.manage(令);
            System.out.printf("%d %d\n", 号, 卡座.getIdx());
        }
        return 卡座.getIdx();
    }

    private static int sExecutor4(final List <D22SCardOp> copl,
                                  final int cardIx,
                                  final int stackSize) {
        if(stackSize < cardIx) return -1;
        D22SCards 卡座 = D22SCards.create(stackSize);
        REPL:
        for(int 号 = 0; 号 < 53; ++号) {
            for(D22SCardOp 令 : copl)
                卡座.manage(令);
            System.out.printf("pos %d of card %d;",
                              卡座.getIdx(cardIx), cardIx);
            System.out.printf("  pos %d of card %d\n",
                              cardIx, 卡座.卡座[cardIx]);
        }
        return 卡座.getIdx(2019);
    }

    private static List <D22SCardOp> parseData(final List <String> sdata) {
        List <D22SCardOp> 表 = new ArrayList <D22SCardOp> (sdata.size());
        for(String 串 : sdata) {
            D22SCardOp op = D22SCardOp.create(串);
            表.add(op);
        }
        return 表;
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> 串表 = new ArrayList <String>();
        器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() > 0)
                串表.add(串);
        }
        return 串表;
    }

    private static int[] args2int(final String[] args) {
        int[] 数河 = null;
        if(args.length > 0) {
            数河 = new int[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Integer.parseInt(args[滴]);
        }
        return 数河;
    }

    private static long[] args2long(final String[] args) {
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        return 数河;
    }
}

enum D22SCardOperation {
    NEW, CUT, INC;
}

/*
 * cut 9126
 * deal with increment 67
 * deal into new stack
 * cut -4398
 */
class D22SCardOp {
    private final int val;
    private final D22SCardOperation cop;

    private D22SCardOp(D22SCardOperation op) {
        this(op, 0);
    }

    private D22SCardOp(D22SCardOperation op, int val) {
        this.val = val;
        this.cop = op;
    }

    private D22SCardOp(D22SCardOperation op, String sval) {
        this(op, Integer.parseInt(sval));
    }

    public int getValue() {return val;}
    public D22SCardOperation getOperation() {return cop;}

    public static D22SCardOp create(final String str) throws IllegalArgumentException {
        String[] ops = str.split(" ");
        if(ops.length > 1 && ops[0].equals("cut"))
            return new D22SCardOp(D22SCardOperation.CUT, ops[1]);
        if(ops.length > 3) {
            if(ops[2].equals("new"))
                return new D22SCardOp(D22SCardOperation.NEW);
            if(ops[2].equals("increment"))
                return new D22SCardOp(D22SCardOperation.INC, ops[3]);
        }
        throw new IllegalArgumentException(str);
    }
}

class D22SCards {
    public final static int __STACK_SIZE = 10007;
    int[] 卡座;

    D22SCards() {
        this(__STACK_SIZE);
    }

    private D22SCards(final int sizeof) {
        卡座 = new int[sizeof > 0 ? sizeof : __STACK_SIZE]; // IAE exception?
        for(int 甲 = 0; 甲 < 卡座.length; 卡座[甲] = 甲++);
    }

    public void manage(final D22SCardOp op) {
        switch(op.getOperation()) {
        case NEW:
            dealNew();
            break;
        case INC:
            dealInc(op.getValue());
            break;
        case CUT:
            cut(op.getValue());
            //break;
        }
    }

    public int getIdx(final int cardId) {
        for(int 标 = 0; 标 < 卡座.length; ++标)
            if(卡座[标] == cardId)
                return 标;
        return -1;
    }

    /**
     * reverse order
     */
    public void dealNew() {
        for(int 首 = 0, 尾 = 卡座.length - 1; 首 < 尾 ; ++首, --尾)
            swap(首, 尾);
    }

    private void swap(final int p, final int q) {
        int 甲 = 卡座[p];
        卡座[p] = 卡座[q];
        卡座[q] = 甲;
    }

    final static int __NO_CARD = -1;

    /**
     * To deal with increment N, lay out all of the cards individually
     * in a long line. Deal the top card into the leftmost
     * position. Then, move N positions to the right and deal the next
     * card there. If you would move into a position past the end of
     * the space on your table, wrap around and keep counting from the
     * leftmost card again. Continue this process until you run out of
     * cards.
     */
    public void dealInc(final int step) {
        int[] 表 = new int[卡座.length];
        Arrays.fill(表, __NO_CARD);
        for(int 标 = 0, 位 = 0; 标 < 卡座.length;
            ++标, 位 = (位 + step) % 表.length) {
            for(;表[位] != __NO_CARD; 位 = (位 + 1) % 表.length);
            表[位] = 卡座[标];
        }
        卡座 = 表;
    }

    /**
     * To cut N cards, take the top N cards off the top of the deck
     * and move them as a single unit to the bottom of the deck,
     * retaining their order.
     *
     * If N is negative, cut (the absolute value of) N cards from the
     * bottom of the deck onto the top.
     */
    public void cut(final int pos) {
        int[] 表 = new int[卡座.length];
        if(pos > 0) {
            int 标, 位;
            for(标 = 0, 位 = 卡座.length - pos; 标 < pos; ++标, ++位)
                表[位] = 卡座[标];
            for(位 = 0; 标 < 卡座.length; ++标, ++位)
                表[位] = 卡座[标];
            卡座 = 表;
        }
        if(pos < 0) {
            int 位, 标;
            for(标 = 卡座.length + pos, 位 = 0; 标 < 卡座.length; ++标, ++位)
                表[位] = 卡座[标];
            for(标 = 0; 标 < (卡座.length + pos); ++标, ++位)
                表[位] = 卡座[标];
            卡座 = 表;
        }
    }

    public String toString() {
        return Arrays.toString(卡座);
    }

    public static D22SCards create(final int sizeof) {
        return new D22SCards(sizeof);
    }

    public static D22SCards create() {
        return new D22SCards();
    }
}

class D22SCardsL {
    public final static int __STACK_SIZE = 10007;
    long 卡号, 座长, 卡生号;

    private D22SCardsL(final long cardIdx, final long sizeof) {
        卡号 = cardIdx;
        卡生号 = cardIdx;
        座长 = sizeof > 0 ? sizeof : __STACK_SIZE;
    }

    public void manage(final D22SCardOp op) {
        switch(op.getOperation()) {
        case NEW:
            dealNew();
            break;
        case INC:
            dealInc(op.getValue());
            break;
        case CUT:
            cut(op.getValue());
            //break;
        }
    }

    public long getIdx() {
        return 卡号;
    }

    /**
     * reverse order
     */
    public void dealNew() {
        卡号 = 座长 - 卡号 - 1;
    }

    /**
     * To deal with increment N, lay out all of the cards individually
     * in a long line. Deal the top card into the leftmost
     * position. Then, move N positions to the right and deal the next
     * card there. If you would move into a position past the end of
     * the space on your table, wrap around and keep counting from the
     * leftmost card again. Continue this process until you run out of
     * cards.
     */
    public void dealInc(final int step) {
        卡号 = 卡号 * step % 座长;
    }

    /**
     * To cut N cards, take the top N cards off the top of the deck
     * and move them as a single unit to the bottom of the deck,
     * retaining their order.
     *
     * If N is negative, cut (the absolute value of) N cards from the
     * bottom of the deck onto the top.
     */
    public void cut(final int pos) {
        卡号 -= pos;
        if(卡号 < 0)
            卡号 += 座长;
        else if(卡号 >= 座长)
            卡号 -= 座长;
    }

    public String toString() {
        return String.valueOf(卡号);
    }

    public static D22SCardsL create(final long cardIdx, final long sizeof) {
        return new D22SCardsL(cardIdx, sizeof);
    }

    public static D22SCardsL create(final long cardIdx) {
        return new D22SCardsL(cardIdx, __STACK_SIZE);
    }
}
