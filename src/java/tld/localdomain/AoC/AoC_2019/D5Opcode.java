// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-09 19:05:19 roukoru>

enum D5Opcode {
    NOP(0),
    ADD(1),
    MUL(2),
    IN(3),
    OUT(4),
    JNZ(5),
    JZ(6),
    CPL(7),
    CMP(8),
    MVB(9),
    HALT(99);

    int 指令号;
    D5Opcode(int n) {指令号 = n;}
    public int getId() {return 指令号;}
}
