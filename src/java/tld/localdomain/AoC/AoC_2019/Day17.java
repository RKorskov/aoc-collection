// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-25 12:15:14 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
// import java.util.Set;
// import java.util.HashSet;
import java.util.Deque;
import java.util.ArrayDeque;

/*
 * Set and Forget
 */
public class Day17 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        // System.out.println(Arrays.toString(al));
        System.out.println(sExecutor1(al, 数河));
        // System.out.println(sExecutor2(al, 数河));
    }

    private static long sExecutor1 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        Intcode 电脑 = D9Intcode.create(prog);
        电脑.eval(prog_in);
        long[] 数表 = 电脑.getOutput();
        printGrid(数表);
        System.out.println("--------------------------------");
        // List <D17XY> 矩阵 = getGridList(数表); D17XY 本 = buildGridTree(矩阵);
        byte[][] 矩阵 = getGridArray(数表);
        printGrid(矩阵);
        int 回号 = 0, n = 0;
        for(int 列 = 0; 列 < 矩阵.length; ++列) {
            for(int 行 = 0; 行 < 矩阵[列].length; ++行) {
                if(isCrossing(矩阵, 行, 列)) {
                    回号 += 行 * 列;
                    System.out.printf("%d  %d %d\n", ++n, 行, 列);
                }
            }
        }
        return 回号;
    }

    private static long sExecutor2 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        Intcode 电脑 = D9Intcode.create(prog);
        //电脑.enableRetainMemory();
        return -1;
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

    private static boolean isCrossing(final byte[][] grid,
                                      final int x, final int y) {
        if(isSpace(grid, x, y) //
           || isSpace(grid, x+1, y)
           || isSpace(grid, x-1, y) //
           || isSpace(grid, x, y+1) //
           || isSpace(grid, x, y-1))
            return false;
        return true;
    }

    private static boolean isSpace(final byte[][] grid,
                                   final int x, final int y) {
        if(x < 0 || y < 0 //
           || y >= grid.length || x >= grid[y].length)
            return true;
        return grid[y][x] < 1;
    }

    private static byte[][] getGridArray(final long[] 数表) {
        int 列 = 0, 行 = 0, 丈 = 0;
        for(long 号 : 数表) {
            switch((int)号) {
            case 10:
                ++列;
                丈 = Math.max(丈, 行);
                行 = 0;
                break;
            default:
                ++行;
            }
        }
        byte[][] 矩阵 = new byte[列-1][]; // trailing CR
        for(int 号 = 0; 号 < 矩阵.length; ++号)
            矩阵[号] = new byte[丈];
        列 = 0;
        行 = 0;
        for(long 号 : 数表) {
            switch((int)号) {
            case 10:
                ++列;
                行 = 0;
                break;
            case 35: // #
                矩阵[列][行++] = 1;
                break;
            case 46: // .
                矩阵[列][行++] = 0;
                break;
            default: // ^v<>
                矩阵[列][行++] = 1;
            }
        }
        return 矩阵;
    }

    /*
    private static List <D17XY> getGridList(final long[] 数表) {
        int 列 = 0, 行 = 0;
        List <D17XY> 矩阵 = new ArrayList <D17XY> ();
        for(long 号 : 数表)
            switch((int)号) {
            case 10:
                ++列;
                行 = 0;
                break;
            case 35:
                D17XY 点 = new D17XY(行, 列);
                if(点 != null)
                    矩阵.add(点);
            default:
                ++行;
            }
        return 矩阵;
    }

    private static D17XY buildGridTree(final List <D17XY> grid) {
        Deque <D17XY> 卡堆 = new ArrayDeque <D17XY> (grid);
        List <D17XY> xways = new ArrayList <D17XY> ();
        for(;卡堆.size() > 1;) {
            D17XY 首 = 卡堆.poll();
            if(首.numLinks() > 3) {
                xways.add(首); // auxillary list of 4-way crossings
                continue; // all connected, remove it from queue
            }
            D17XY 尾 = 卡堆.peek();
            if(首.connect(尾)) {
                if(首.numLinks() > 3) {
                    xways.add(首); // auxillary list of 4-way crossings
                }
                else
                    卡堆.addLast(首);
            }
        }
        return 卡堆.peek(); // .poll() fine too
    }
    */

    private static void printGrid(final long[] 数表) {
        for(long 号 : 数表) {
            char 字 = '.';
            switch((int)号) {
            case 35:
                字 = '#';
                break;
            case 46:
                字 = '.';
                break;
            case 10:
                字 = '\n';
                break;
            default:
                字 = (char)号;
            }
            System.out.print(字);
        }
    }

    private static void printGrid(final byte[][] grid) {
        for(int 列 = 0; 列 < grid.length; ++列) {
            for(int 行 = 0; 行 < grid[列].length; ++行) {
                char 字;
                if(isSpace(grid, 行, 列))
                    字 = '.';
                else if(isCrossing(grid, 行, 列))
                    字 = 'X';
                else
                    字 = '#';
                System.out.print(字);
            }
            System.out.println();
        }
    }
}

/*
class D17XY extends D11XY {
    D17XY 北, 南, 东, 西;
    D17XY(int x, int y) {
        super(x, y);
        北 = null;
        南 = null;
        东 = null;
        西 = null;
    }

    public int numLinks() {
        int 关 = 0;
        if(北 != null) ++关;
        if(南 != null) ++关;
        if(东 != null) ++关;
        if(西 != null) ++关;
        return 关;
    }

    public boolean connect(final D17XY dst) {
        final int dx = x - dst.x,
            dy = y - dst.y;
        if(Math.abs(dx) > 1 || Math.abs(dy) > 1)
            return false;
        boolean 数回 = false;
        if(dx == 0) {
            if(dy == 1 && 南 == null) {
                南 = dst;
                数回 = true;
            }
            else if(dy == -1 && 北 == null) {
                北 = dst;
                数回 = true;
            }
        }
        if(dy == 0) {
            if(dx == 1 && 东 == null) {
                东 = dst;
                数回 = true;
            }
            else if(dx == -1 && 西 == null) {
                西 = dst;
                数回 = true;
            }
        }
        return 数回;
    }

}
*/
