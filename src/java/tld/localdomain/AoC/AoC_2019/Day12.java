// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-21 16:41:05 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day12 {

    public static void main (String[] args) throws Exception {
        List <String> 串表 = readData();
        List <D12Moon> 第十二月 = parse(串表);
        int 步号 = args.length > 0 ? Integer.parseInt(args[0]) : 4;
        List <D12Moon> 新月 = new ArrayList <D12Moon> ();
        for(D12Moon 月 : 第十二月)
            新月.add(月.cpy());
        System.out.println(sExecutor1(新月, 步号));
        System.out.println(sExecutor2(第十二月));
    }

    private static long sExecutor1 (final List <D12Moon> moons,
                                    final int steps) {
        for(int 步 = 0; 步 < steps; ++步)
            move_moons(moons);
        long 力 = 0;
        for(D12Moon 月 : moons)
            力 += 月.getEnergy();
        return 力;
    }

    /**
     * all thanks for a tip about LCM...
     */
    private static long sExecutor2 (final List <D12Moon> moons) {
        D12Moon[] 组月 = new D12Moon[moons.size()];
        for(int 甲 = 0; 甲 < moons.size(); ++甲)
            组月[甲] = moons.get(甲).cpy();
        long 步, cx, cy, cz;
        for(步 = 1, cx = -1, cy = -1, cz = -1; 步 < Long.MAX_VALUE; ++步) {
            move_moons(moons);
            boolean nx = true, ny = true, nz = true;
            for(int 标 = 0; 标 < 组月.length; ++标) {
                D12Moon 月 = moons.get(标);
                nx &= checkX(组月[标], 月);
                ny &= checkY(组月[标], 月);
                nz &= checkZ(组月[标], 月);
            }
            if(nx && cx < 0) {
                cx = 步;
                System.out.printf("by x : %d\n", cx);
            }
            if(ny && cy < 0) {
                cy = 步;
                System.out.printf("by y : %d\n", cy);
            }
            if(nz && cz < 0) {
                cz = 步;
                System.out.printf("by z : %d\n", cz);
            }
            if(cx > 0 && cy > 0 && cz > 0) break;
        }
        return evalLCM(cx, cy, cz);
    }

    /**
     * least common multipe
     */
    private static long evalLCM(final long a, final long b, final long c) {
        long gcd = evalGCD(a, b);
        long lcm = (a / gcd) * b;
        gcd = evalGCD(lcm, c);
        return  (lcm / gcd) * c;
    }

    /**
     * greatest common divisor
     */
    private static long evalGCD(final long a, final long b) {
        // x=qy+r, x<y
        long y = Math.max(Math.abs(a), Math.abs(b)),
            x = Math.min(Math.abs(a), Math.abs(b));
        for(long r = 1; r > 0;) {
            r = y % x;
            y = x;
            x = r;
        }
        return y;
    }

    private static boolean checkX(D12Moon oldMoon, D12Moon newMoon) {
        return oldMoon.vx == newMoon.vx && oldMoon.x == newMoon.x;
    }

    private static boolean checkY(D12Moon oldMoon, D12Moon newMoon) {
        return oldMoon.vy == newMoon.vy && oldMoon.y == newMoon.y;
    }

    private static boolean checkZ(D12Moon oldMoon, D12Moon newMoon) {
        return oldMoon.vz == newMoon.vz && oldMoon.z == newMoon.z;
    }

    private static void move_moons(final List <D12Moon> moons) {
        for(int 甲 = 0; 甲 < (moons.size() - 1); ++甲)
            for(int 乙 = 甲 + 1; 乙 < moons.size(); ++乙)
                apply_gravity(moons.get(甲), moons.get(乙));
        for(D12Moon 月 : moons)
            月.apply_velocity();
    }

    private static void apply_gravity(final D12Moon moon0,
                                      final D12Moon moon1) {
        if(moon0.x != moon1.x) {
            if(moon0.x < moon1.x) {
                ++moon0.vx;
                --moon1.vx;
            }
            else {
                --moon0.vx;
                ++moon1.vx;
            }
        }
        if(moon0.y != moon1.y) {
            if(moon0.y < moon1.y) {
                ++moon0.vy;
                --moon1.vy;
            }
            else {
                --moon0.vy;
                ++moon1.vy;
            }
        }
        if(moon0.z != moon1.z) {
            if(moon0.z < moon1.z) {
                ++moon0.vz;
                --moon1.vz;
            }
            else {
                --moon0.vz;
                ++moon1.vz;
            }
        }
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> 串表 = new ArrayList <String>();
        器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() > 0)
                串表.add(串);
        }
        return 串表;
    }

    private final static Pattern 月面 = Pattern.compile("<x=(?<x>[+-]?\\d+), y=(?<y>[+-]?\\d+), z=(?<z>[+-]?\\d+)>");

    /**
     * <x=-1, y=0, z=2>
     * <x=2, y=-10, z=-7>
     * <x=4, y=-8, z=8>
     * <x=3, y=5, z=-1>
     */
    private static List <D12Moon> parse(final List <String> data) {
        List <D12Moon> 表 = new ArrayList <D12Moon> ();
        for(String 串 : data) {
            Matcher m = 月面.matcher(串);
            if(m == null || m.find() == false) continue;
            int x = Integer.parseInt(m.group("x")),
                y = Integer.parseInt(m.group("y")),
                z = Integer.parseInt(m.group("z"));
            D12Moon 月 = D12Moon.create(x, y, z);
            if(月 != null)
                表.add(月);
        }
        return 表;
    }
}

class D12Moon {
    int x, y, z, vx, vy, vz;
    private D12Moon(){;}

    public void apply_velocity() {
        x += vx;
        y += vy;
        z += vz;
    }

    public long getEnergy() {
        return (Math.abs(x) + Math.abs(y) + Math.abs(z)) //
            * (Math.abs(vx) + Math.abs(vy) + Math.abs(vz));
    }

    public boolean cmpPos(final D12Moon moon) {
        if(moon == null) return false;
        return moon.x == x && moon.y == y && moon.z == z //
            && moon.vx == vx && moon.vy == vy && moon.vz == vz;
    }

    public D12Moon cpy() {
        D12Moon 月 = new D12Moon();
        月.x = x;
        月.y = y;
        月.z = z;
        月.vx = vx;
        月.vy = vy;
        月.vz = vz;
        return 月;
    }

    public String toString() {
        return String.format("%+2d %+2d %+2d : %+2d %+2d %+2d", x, y, z, vx, vy, vz);
    }

    public static D12Moon create() {
        D12Moon 月 = new D12Moon();
        月.x = 0;
        月.y = 0;
        月.z = 0;
        月.vx = 0;
        月.vy = 0;
        月.vz = 0;
        return 月;
    }

    public static D12Moon create(final int x, final int y, final int z) {
        D12Moon 月 = new D12Moon();
        月.x = x;
        月.y = y;
        月.z = z;
        月.vx = 0;
        月.vy = 0;
        月.vz = 0;
        return 月;
    }
}
