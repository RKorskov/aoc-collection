// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-23 23:55:14 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ArrayDeque;
import java.util.Deque;

public class Day23 {

    public static void main (String[] args) throws Exception {
        List <String> 串表 = readData();
        List <> 令表 = parseData(串表);
        System.out.println(sExecutor1(令表));
        System.out.println(sExecutor2(令表));
    }

    private static int sExecutor1(final List <> data) {
        return -1;
    }

    private static int sExecutor2(final List <> data) {
        return -1;
    }

    private static List <> parseData(final List <String> sdata) {
        List <> 表 = new ArrayList <> (sdata.size());
        for(String 串 : sdata)
            ;
        return 表;
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> 串表 = new ArrayList <String>();
        器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() > 0)
                串表.add(串);
        }
        return 串表;
    }
}

class D23Packet {
    final int dst, x, y;
    D23Packet(int dst, int x, int y) {
        this.dst = dst;
        this.x = x;
        this.y = y;
    }
}

class D23NIC /* extends D9Intcode */ {
    private static int __nextAddr = 0;
    private D23NIC(){;}

    D9Intcode 电脑;
    int 称;
    Deque <D23Packet> 出列, 来列;
    boolean firstRun;

    public void run() {
        // stub
        if(firstRun)
            firstRun = false;
        ;
    }

    public static D23NIC create() {
        return create(__nextAddr++);
    }

    public static D23NIC create(final int addr) {
        D23NIC nic = new D23NIC();
        nic.电脑 = D9Intcode.create();
        nic.电脑.enableRetainMemory();
        if(addr < __nextAddr)
            throw new IllegalArgumentException("requested address too low");
        else
            __nextAddr = addr+1;
        nic.称 = addr;
        nic.出列 = new ArrayDeque <D23Packet>();
        nic.来列 = new ArrayDeque <D23Packet>();
        nic.电脑.addALU(new NicInALU());
        nic.电脑.addALU(new NicOutALU());
        nic.firstRun = true;
        return nic;
    }

}

/**
 * Opcode 4 outputs the value of its only parameter. For example, the
 * instruction 4,50 would output the value at address 50;
 */
class NicOutALU extends AbstractALU implements ALU {
    NicOutALU() {ALU_OP = D5Opcode.OUT;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1);
        inst.out(inst.getAt(IP+1, m1)); // here
        return IP+2;
    }

    public boolean equals(final Object obj) {
        if(obj == null || !(obj instanceof NicOutALU)) return false;
        NicOutALU alu = (NicOutALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 3 takes a single integer as input and saves it to the
 * address given by its only parameter. For example, the instruction
 * 3,50 would take an input value and store it at address 50;
 */
class NicInALU extends AbstractALU implements ALU {
    NicInALU() {ALU_OP = D5Opcode.IN;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        long v1 = inst.getIn(); // fixme: here!
        D9MemMode m1 = getMemMode(inst, IP, 1);
        int a1 = (int)inst.getAt(IP+1);
        inst.setAt(a1, v1, m1);
        return IP+2;
    }

    public boolean equals(final Object obj) {
        if(obj == null || !(obj instanceof NicOutALU)) return false;
        NicInALU alu = (NicInALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}
