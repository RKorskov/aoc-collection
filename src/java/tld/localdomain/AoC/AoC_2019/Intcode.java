// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2020-01-14 08:47:01 roukoru>

import java.util.function.Supplier;

interface Intcode {
    long eval(final long[] prog_in);
    void enableRetainMemory();
    void registerInputSource(Supplier <Long> s_in);
    long[] getOutput();
    // void flushOutput(); // ?
}
