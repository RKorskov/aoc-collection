// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-09 09:09:38 roukoru>

import java.util.Arrays;

/**
 * implementation Intecode computer, day 5
 */
class D5Intcode implements Intcode {
    long[] 程序;
    boolean debug;
    private D5Intcode(){;}

    public long eval(final long[] prog_in) {
        long 值出 = -1;
        int 号来 = 0;
        for(int 标 = 0; 程序[标] != 99; 标 += 2) {
            int 地向 = (int)程序[标+3],
                地甲 = (int)程序[标+1],
                地乙 = (int)程序[标+2];
            int 令 = ((int)程序[标]) % 100;
            long 甲 = (程序[标] / 100) % 10 == 1 ? 地甲 //
                : (地甲 < 程序.length ? 程序[地甲] : Long.MIN_VALUE),
                乙 = (程序[标] / 1000) % 10 == 1 ? 地乙 //
                : (地乙 < 程序.length ? 程序[地乙] : Long.MIN_VALUE);
            String scmd = debug ? String.format("%d:  ", 标) : null;
            int a1 = (int)(程序[标] / 100) % 10,
                a2 = (int)(程序[标] / 1000) % 10;
            switch(令) {
            case 1: // ADD:
                程序[地向] = 甲 + 乙;
                标 += 2;
                break;
            case 2: // MUL:
                程序[地向] = 甲 * 乙;
                标 += 2;
                break;
            case 3: // IN:
                程序[地甲] = prog_in[号来++];
                if(debug) scmd += String.format("in [%d] ; %d", 地甲, 程序[地甲]);
                break;
            case 4: // OUT:
                值出 = 甲;
                if(debug) {
                    System.out.printf("---[ %d > %d\n", 标, 甲);
                    if(a1 == 1)
                        scmd += String.format("out %d", 甲);
                    else
                        scmd += String.format("out [%d] ; %d", 地甲, 甲);
                }
                break;
            case 5: // JNZ
                if(甲 != 0)
                    标 = (int)乙 - 2;
                else
                    ++标;
                if(debug) {
                    if(a1 == 1)
                        scmd += String.format("jnz %d", 甲);
                    else
                        scmd += String.format("jnz [%d]", 地甲);
                    if(a2 == 1)
                        scmd = String.format("%s %d ; %d ", scmd, 乙, 甲);
                    else
                        scmd = String.format("%s [%d] ; %d %d", scmd, 地乙, 甲, 乙);
                }
                break;
            case 6: // JZ
                if(甲 == 0)
                    标 = (int)乙 - 2;
                else
                    ++标;
                if(debug) {
                    if(a1 == 1)
                        scmd += String.format("jz %d", 甲);
                    else
                        scmd += String.format("jz [%d]", 地甲);
                    if(a2 == 1)
                        scmd = String.format("%s %d ; %d", scmd, 乙, 甲);
                    else
                        scmd = String.format("%s [%d] ; %d %d", scmd, 地乙, 甲, 乙);
                }
                break;
            case 7: // CMPL
                程序[地向] = 甲 < 乙 ? 1 : 0;
                标 += 2;
                if(debug) {
                    if(a1 == 1)
                        scmd += String.format("cmpl %d", 甲);
                    else
                        scmd += String.format("cmpl [%d]", 地甲);
                    if(a2 == 1)
                        scmd = String.format("%s %d [%d] ; %d", scmd, 乙, 地向, 甲);
                    else
                        scmd = String.format("%s [%d] [%d] ; %d", scmd, 地乙, 地向, 甲, 乙);
                }
                break;
            case 8: // CMP
                程序[地向] = 甲 == 乙 ? 1 : 0;
                标 += 2;
                if(debug) {
                    if(a1 == 1)
                        scmd += String.format("cmp %d", 甲);
                    else
                        scmd += String.format("cmp [%d]", 地甲);
                    if(a2 == 1)
                        scmd = String.format("%s %d [%d] ; %d", scmd, 乙, 地向, 甲);
                    else
                        scmd = String.format("%s [%d] [%d] ; %d %d", scmd, 地乙, 地向, 甲, 乙);
                }
                break;
            case 99: // HALT:
            default:
                System.out.println("Oops!");
                return -1;
            }
            if(debug)
                System.out.println(scmd);
        }
        return 值出;
    }

    public static D5Intcode create(final long[] prog) {
        D5Intcode 电脑 = new D5Intcode();
        电脑.程序 = Arrays.copyOf(prog, prog.length);
        电脑.debug = false;
        return 电脑;
    }
}
