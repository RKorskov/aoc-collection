// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-24 15:36:22 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 * Planet of Discord
 */
public class Day24 {

    public static void main(String[] args) throws Exception {
        byte[][] 天田 = readDataF();
        int[] 数河 = args2int(args);
        printField(天田);
        System.out.println(sExecutor1(天田, 数河));
        System.out.println(sExecutor2(天田, 数河));
    }

    private static int sExecutor1(final byte[][] field, final int[] args) {
        byte[][] 田 = new byte[field.length][];
        for(int 列 = 0; 列 < field.length; ++列)
            田[列] = Arrays.copyOf(field[列], field[列].length);
        int n = args.length > 0 && args[0] > 0 ? args[0] : Integer.MAX_VALUE;
        for(int 步 = 0; 步 < n; ++步) {
            田 = roundOfLife(田);
            long 级 = evalFieldRating(田);
            System.out.printf("\n%d: %016X\n", 步, 级);
            printField(田);
            break;
        }
        return -1;
    }

    private static int sExecutor2(final byte[][] field, final int[] args) {
        return -1;
    }

    /**
     * 5x5 field should fit into long (and even int!)
     */
    private static long evalFieldRating(final byte[][] field) {
        long 回号 = 0;
        for(int 列 = 0; 列 < field.length; ++列)
            for(int 行 = 0; 行 < field[列].length; ++行) {
                回号 <<= 1;
                if(field[列][行] != SPACE)
                    回号 |= 1;
            }
        return 回号;
    }

    /**
     * Each minute, The bugs live and die based on the number of bugs
     * in the four adjacent tiles:
     *
     * 1. A bug dies (becoming an empty space) unless there is exactly
     * one bug adjacent to it.
     *
     * 2. An empty space becomes infested with a bug if exactly one or
     * two bugs are adjacent to it.
     *
     * Otherwise, a bug or empty space remains the same. Tiles on the
     * edges of the grid have fewer than four adjacent tiles; the
     * missing tiles count as empty space. This process happens in
     * every location simultaneously.
     */
    private static byte[][] roundOfLife(final byte[][] field) {
        byte[][] 田 = new byte[field.length][];
        for(int 列 = 0; 列 < field.length; ++列) {
            田[列] = new byte[field[列].length];
            for(int 行 = 0; 行 < field[列].length; ++行) {
                int 虫 = countBugs(field, 行, 列);
                switch(虫) {
                case 1:
                    田[列][行] = BUG;
                    break;
                case 2:
                    田[列][行] = SPACE;
                    break;
                default:
                    田[列][行] = field[列][行];
                }
            }
        }
        return 田;
    }

    private static int countBugs(final byte[][] field,
                                 final int x, final int y) {
        return checkCell(field, x-1, y) + checkCell(field, x+1, y) //
            + checkCell(field, x, y-1) + checkCell(field, x, y+1);
    }

    private static int checkCell(final byte[][] field,
                                 final int x, final int y) {
        if(x < 0 || y < 0 || y >= field.length || x >= field[y].length)
            return 0;
        return field[y][x];
    }

    final private static byte BUG = 1,
        SPACE = 0;

    /**
     * 5x5 grid
     * ###..
     * #...#
     * .#.##
     * ##.#.
     * #.###
     */
    private static byte[][] readDataF() {
        Scanner 器 = new Scanner(System.in);
        器.useDelimiter("\\n");
        List <byte[]> 表 = new ArrayList <byte[]> ();
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() < 1) continue;
            byte[] 行 = new byte[串.length()];
            for(int 标 = 0; 标 < 串.length(); ++标) {
                char 字 = 串.charAt(标);
                switch(字) {
                case '.':
                    // 行[标] = SPACE;
                    break;
                case '#':
                default:
                    行[标] = BUG;
                    break;
                }
            }
            表.add(行);
        }
        return 表.toArray(new byte[0][]);
    }

    private static int[] args2int(final String[] args) {
        int[] 数河 = null;
        if(args.length > 0) {
            数河 = new int[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Integer.parseInt(args[滴]);
        }
        return 数河;
    }

    private static void printField(final byte[][] field) {
        for(int 列 = 0; 列 < field.length; ++列) {
            for(int 行 = 0; 行 < field[列].length; ++行) {
                char 字 = field[列][行] == SPACE ? '.' : '#';
                System.out.print(字);
            }
            System.out.println();
        }
    }
}
