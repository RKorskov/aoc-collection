// -*- mode: c; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-12 18:18:02 roukoru>

#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#define STEPS 1000
#define NUM_MOONS 4

typedef struct JMoon {
  long x, y, z, vx, vy, vz;
} Moon;

void apply_gravity(Moon *moon0, Moon *moon1) {
  if(moon0->x != moon1->x) {
    if(moon0->x < moon1->x) {
      ++(moon0->vx);
      --(moon1->vx);
    }
    else {
      --(moon0->vx);
      ++(moon1->vx);
    }
  }
  if(moon0->y != moon1->y) {
    if(moon0->y < moon1->y) {
      ++(moon0->vy);
      --(moon1->vy);
    }
    else {
      --(moon0->vy);
      ++(moon1->vy);
    }
  }
  if(moon0->z != moon1->z) {
    if(moon0->z < moon1->z) {
      ++(moon0->vz);
      --(moon1->vz);
    }
    else {
      --(moon0->vz);
      ++(moon1->vz);
    }
  }
}

void apply_velocity(Moon *moon) {
  moon->x += moon->vx;
  moon->y += moon->vy;
  moon->z += moon->vz;
}

/*
 * moons - pointer on array of Moon
 * num - length of array of moons
 */
void move_moons(Moon *moons, int num) {
  for(int i = 0; i < (NUM_MOONS-1); ++i)
    for(int j = i+1; j < NUM_MOONS; ++j)
      apply_gravity(&moons[i], &moons[j]);
  for(int i = 0; i < NUM_MOONS; ++i)
    apply_velocity(&moons[i]);
}

long get_energy(Moon *moon) {
  long p = abs(moon->x) + abs(moon->y) + abs(moon->z),
    k = abs(moon->vx) + abs(moon->vy) + abs(moon->vz);
  return p * k;
}

/*
 * result: 14780
 */
unsigned long executor1(Moon *moons, int num_moons) {
  Moon sim[num_moons];
  memcpy(sim, moons, sizeof(Moon) * num_moons);
  for(int i = 0; i < STEPS; ++i)
    move_moons(sim, num_moons);
  unsigned long rl = 0;
  for(int i = 0; i < num_moons; ++i)
    rl += get_energy(&sim[i]);
  return rl;
}

int mooncmp(Moon *m0, Moon *m1) {
  if(m0 == NULL || m1 == NULL) return 0;
  return m0 == m1 //
    || m0->x == m1->x && m0->y == m1->y && m0->z == m1->z //
    && m0->vx == m1->vx && m0->vy == m1->vy && m0->vz == m1->vz;
}

int moonscmp(Moon *moons, Moon *sim, int num_moons) {
  int s, i;
  for(s = 0, i = 0; i < num_moons; ++i)
    s += mooncmp(&moons[i], &sim[i]);
  return s;
}

unsigned long executor2(Moon *moons, int num_moons) {
  Moon sim[num_moons];
  memcpy(sim, moons, sizeof(Moon) * num_moons);
  unsigned long n;
  for(n = 1; n > 0; ++n) {
    move_moons(sim, num_moons);
    if(moonscmp(moons, sim, num_moons) == num_moons)
      break;
  }
  return ++n;
}

void print_moon(Moon *moon) {
  printf("%+3li %+3li %+3li : %+3li %+3li %+3li\n",
         moon->x, moon->y, moon->z, moon->vx, moon->vy, moon->vz);
}

int main() {
  Moon jms[NUM_MOONS] = {
                         {3,2,-6,0,0,0}, // steps 1000  energy 14780
                         {-13,18,10,0,0,0},
                         {-8,-1,13,0,0,0},
                         {5,10,4,0,0,0}
                         /*
                         {-1,0,2,0,0,0}, // steps 10  energy 179  sim 2772
                         {2,-10,-7,0,0,0},
                         {4,-8,8,0,0,0},
                         {3,5,-1,0,0,0}
                         */
                         /*
                         {-8,-10,0,0,0,0}, // steps 100 energy 1940
                         {5,5,10,0,0,0},
                         {2,-7,3,0,0,0},
                         {9,-8,-3,0,0,0},
                         */
  };
  // for(int i = 0; i < NUM_MOONS; ++i) print_moon(&jms[i]);
  printf("part 1 : %lu\n", executor1(jms, NUM_MOONS));
  printf("part 2 : %lu\n", executor2(jms, NUM_MOONS));
  // for(int i = 0; i < NUM_MOONS; ++i) print_moon(&jms[i]);
  return 0;
}
