// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2020-01-14 08:49:02 roukoru>

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.function.Supplier;

/**
 * после добавления нового обработчика ALU, его нужно прицепить в
 * D9Intcode.create через .addALU(brand_new_ALU)
 */
class D9Intcode implements Intcode {
    long[] 程序源;
    long[] 程序;
    boolean retainMem;
    int 基,
        数舟;
    long[] 数河, 数出;
    List <Long> 数话;
    Map <Integer,Long> 大书;
    Set <ALU> chainALU; // не Set, а PriorityQueue для on-fly оптимизации
    Supplier <Long> dataInput;
    
    private D9Intcode(){;}

    /**
     * non-iteractive runs prog until if terminates,
     * uses prog_in as input source
     */
    public long eval(final long[] prog_in) {
        long 值出 = -1;
        int 号来 = 0;
        prepareProg(prog_in);
        REPL: for(int 标 = 0; 标 >= 0 && 标 < 程序.length;) {
            try {
                int op = (int)程序[标] % 100;
                for(ALU alu : chainALU) {
                    int 报 = alu.eval(this, 标, op);
                    if(报 < 0) {
                        值出 = -1;
                        break REPL;
                    }
                    if(报 != 标) {
                        标 = 报;
                        break;
                    }
                }
            }
            catch (IntcodeException iex) {
                if(iex instanceof IntcodeHaltException) {
                    值出 = 数话 == null || 数话.size() < 1 ? 0 //
                        : 数话.get(数话.size() - 1);
                }
                else {
                    System.out.printf("IntcodeException at %d : %s\n", 标, iex);
                    iex.printStackTrace();
                    值出 = -1;
                }
                break;
            }
        }
        progOutput2LArray();
        return 值出;
    }

    public void enableRetainMemory() {
        retainMem = true;
    }

    public void registerInputSource(Supplier <Long> s_in) {
        dataInput = s_in;
    }

    /**
     * reset existing output queue
     */
    public void flushOutput() {
        数语.clear();
    }

    /**
     * returns copy of an output as long[],
     * and resets output queue
     */
    public long[] getOutput() {
        progOutput2LArray();
        if(数出 != null) {
            long[] 回数 = Arrays.copyOf(数出, 数出.length);
            数出 = null;
            return 回数;
        }
        else
            return new long[0];
    }

    /**
     * copies output as long[],
     * and resets output queue
     */
    private void progOutput2LArray() {
        if(数话 == null || 数话.size() < 1) {
            数出 = null;
            return;
        }
        数出 = new long[数话.size()];
        for(int 标 = 0; 标 < 数话.size(); ++标)
            数出[标] = 数话.get(标);
        flushOutput();
    }

    /**
     * @return [ptr]
     */
    protected long getAt(final int ptr) throws IntcodeException {
        if(ptr < 0)
            throw new IntcodeException //
                (String.format("negative memory pointer (%d)", ptr));
        if(ptr >= 程序.length)
            return getLargeMem(ptr);
        else
            return 程序[ptr];
    }

    /**
     * @return [rawptr]
     */
    protected long getAt(final int rawptr, final D9MemMode mmode)
        throws IntcodeException {
        long ptr = 程序[rawptr];
        switch(mmode) {
        case DIRECT: // direct pointer
            break;
        case CONSTANT:
            return ptr;
            //break;
        case RELATIVE: // relative pointer
            ptr += 基;
            break;
        default:
            throw new IntcodeException //
                (String.format("invalid memory access mode (%d)", mmode));
        }
        if(ptr < 0)
            throw new IntcodeException //
                (String.format("negative memory pointer (%d)", ptr));
        if(ptr >= 程序.length)
            return getLargeMem((int)ptr);
        else
            return 程序[(int)ptr];
    }

    /**
     * @param  ptr memory pointer
     * @param  val new value at [ptr]
     */
    protected void setAt(final int ptr, final long val)
        throws IntcodeException {
        if(ptr < 0)
            throw new IntcodeException(String.format("negative memory pointer (%d)", ptr));
        if(ptr >= 程序.length)
            setLargeMem(ptr, val);
        else
            程序[ptr] = val;
    }

    /**
     * @param  ptr memory pointer
     * @param  val new value at [ptr]
     */
    protected void setAt(final int rawptr, final long val,
                         final D9MemMode mmode)
        throws IntcodeException {
        int ptr = rawptr;
        switch(mmode) {
        case DIRECT: // pointer
            break;
        case RELATIVE: // relative pointer
            ptr += 基;
            break;
        default:
            throw new IntcodeException //
                (String.format("invalid memory access mode (%d)", mmode));
        }
        if(ptr < 0)
            throw new IntcodeException(String.format("negative memory pointer (%d)", ptr));
        if(ptr >= 程序.length)
            setLargeMem(ptr, val);
        else
            程序[ptr] = val;
    }

    /**
     * The computer's available memory should be much larger than the
     * initial program. Memory beyond the initial program starts with
     * the value 0 and can be read or written like any other
     * memory.
     * It is invalid to try to access memory at a negative address,
     * though.
     */
    private long getLargeMem(final int ptr) {
        if(大书 == null)
            return 0;
        if(大书.containsKey(ptr))
            return 大书.get(ptr);
        return 0;
    }

    private void setLargeMem(final int ptr, final long val) {
        if(大书 == null)
            大书 = new HashMap <Integer,Long> ();
        大书.put(ptr, val);
    }

    protected void out(final long val) {
        数话.add(val);
    }

    protected long getIn() throws IntcodeException {
        if(dataInput != null)
            return dataInput.get();
        if(数舟 >= 数河.length)
            throw new IntcodeException("out of input");
        return 数河[数舟++];
    }

    protected int addBase(final int inc) {
        基 += inc;
        return 基;
    }

    private void prepareProg(final long[] prog_in) {
        if(prog_in == null || prog_in.length < 1)
            数河 = new long[0];
        else
            数河 = Arrays.copyOf(prog_in, prog_in.length);
        if(数话 == null)
            数话 = new ArrayList <Long> ();
        if(retainMem)
            程序 = 程序源;
        else { // reset state(s)
            程序 = Arrays.copyOf(程序源, 程序源.length);
            数舟 = 0; // reset input pointer
            基 = 0; // reset base
            数出 = null;
            数话.clear();
        }
    }

    protected void addALU(ALU nextALU) {
        if(chainALU == null)
            chainALU = new HashSet <ALU>();
        chainALU.add(nextALU);
    }

    public static D9Intcode create(final long[] prog) {
        D9Intcode 电脑 = new D9Intcode();
        电脑.程序源 = Arrays.copyOf(prog, Integer.max(prog.length, 128));
        电脑.retainMem = false;
        电脑.基 = 0;
        电脑.大书 = null;
        电脑.dataInput = null;
        电脑.chainALU = null;
        电脑.addALU(new AddALU());
        电脑.addALU(new MulALU());
        电脑.addALU(new JzALU());
        电脑.addALU(new JnzALU());
        电脑.addALU(new CmpALU());
        电脑.addALU(new CmplALU());
        电脑.addALU(new OutALU());
        电脑.addALU(new InALU());
        电脑.addALU(new MvbALU());
        电脑.addALU(new NopALU());
        电脑.addALU(new HaltALU());
        return 电脑;
    }
}

class IntcodeException extends Exception {
    String 信;
    IntcodeException(String msg) {
        super(msg);
        信 = msg;
    }

    public String toString() {
        return 信;
    }
}

class IntcodeHaltException extends IntcodeException {
    String 信;
    IntcodeHaltException(int ip) {
        super(String.format("normal HALT at %d", ip));
        信 = String.format("normal HALT at %d", ip);
    }

    public String toString() {
        return 信;
    }
}

interface ALU {
    int eval(D9Intcode intcode, int ip, int op) throws IntcodeException;
    void addALU(ALU nextALU);
}

abstract class AbstractALU implements ALU {
    ALU nextALU = null;
    protected D5Opcode ALU_OP;

    public void addALU(ALU nextALU) {
        if(nextALU == null)
            this.nextALU = nextALU;
        else
            this.nextALU.addALU(nextALU);
    }

    private final static D9MemMode[] dmm = {D9MemMode.DIRECT,
                                            D9MemMode.CONSTANT,
                                            D9MemMode.RELATIVE};

    protected static D9MemMode getMemMode(final D9Intcode inst,
                                          final int IP, final int argId)
        throws IntcodeException {
        switch(argId) {
            /*
        case 0: // opcode itself
            return dmm[(int)(inst.getAt(IP) % 100)];
            */
        case 1:
            return dmm[(int)(inst.getAt(IP) / 100 % 10)];
        case 2:
            return dmm[(int)(inst.getAt(IP) / 1000 % 10)];
        case 3:
            return dmm[(int)(inst.getAt(IP) / 10000 % 10)];
        }
        throw new IntcodeException //
            (String.format("invalid memory access mode at %d as %d",
                           IP, argId));
    }

    protected D5Opcode getOp() {return ALU_OP;}

    public abstract int eval(D9Intcode intcode, int IP, int op) throws IntcodeException;
}

class NopALU extends AbstractALU implements ALU {
    NopALU() {ALU_OP = D5Opcode.NOP;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op == ALU_OP.getId())
            return IP+1;
        if(nextALU == null)
            return IP;
        return nextALU.eval(inst, IP, op);
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof NopALU)) return false;
        NopALU alu = (NopALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

class HaltALU extends AbstractALU implements ALU {
    HaltALU() {ALU_OP = D5Opcode.HALT;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op == ALU_OP.getId()) {
            throw new IntcodeHaltException(IP);
        }
        if(nextALU == null)
            return IP;
        return nextALU.eval(inst, IP, op);
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof HaltALU)) return false;
        HaltALU alu = (HaltALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 1 adds together numbers read from two positions and stores
 * the result in a third position. The three integers immediately
 * after the opcode tell you these three positions - the first two
 * indicate the positions from which you should read the input values,
 * and the third indicates the position at which the output should be
 * stored.
 */
class AddALU extends AbstractALU implements ALU {
    AddALU() {ALU_OP = D5Opcode.ADD;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2),
            m3 = getMemMode(inst, IP, 3);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        int o = (int)inst.getAt(IP+3);
        inst.setAt(o, v1 + v2, m3);
        return IP+4;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof AddALU)) return false;
        AddALU alu = (AddALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 2 works exactly like opcode 1, except it multiplies the two
 * inputs instead of adding them. Again, the three integers after the
 * opcode indicate where the inputs and outputs are, not their values.
 */
class MulALU extends AbstractALU implements ALU {
    MulALU() {ALU_OP = D5Opcode.MUL;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2),
            m3 = getMemMode(inst, IP, 3);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        int o = (int)inst.getAt(IP+3);
        inst.setAt(o, v1 * v2, m3);
        return IP+4;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof MulALU)) return false;
        MulALU alu = (MulALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 8 is equals: if the first parameter is equal to the second
 * parameter, it stores 1 in the position given by the third
 * parameter. Otherwise, it stores 0.
 *
 * странный выбор констант для t/nil ...
 */
class CmpALU extends AbstractALU implements ALU {
    CmpALU() {ALU_OP = D5Opcode.CMP;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2),
            m3 = getMemMode(inst, IP, 3);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        int o = (int)inst.getAt(IP+3);
        inst.setAt(o, v1 == v2 ? 1 : 0, m3);
        return IP+4;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof CmpALU)) return false;
        CmpALU alu = (CmpALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 7 is less than: if the first parameter is less than the
 * second parameter, it stores 1 in the position given by the third
 * parameter. Otherwise, it stores 0.
 *
 * странный выбор констант для t/nil ...
 */
class CmplALU extends AbstractALU implements ALU {
    CmplALU() {ALU_OP = D5Opcode.CPL;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2),
            m3 = getMemMode(inst, IP, 3);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        int o = (int)inst.getAt(IP+3);
        inst.setAt(o, v1 < v2 ? 1 : 0, m3);
        return IP+4;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof CmplALU)) return false;
        CmplALU alu = (CmplALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 4 outputs the value of its only parameter. For example, the
 * instruction 4,50 would output the value at address 50;
 */
class OutALU extends AbstractALU implements ALU {
    OutALU() {ALU_OP = D5Opcode.OUT;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1);
        inst.out(inst.getAt(IP+1, m1));
        return IP+2;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof OutALU)) return false;
        OutALU alu = (OutALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 3 takes a single integer as input and saves it to the
 * address given by its only parameter. For example, the instruction
 * 3,50 would take an input value and store it at address 50;
 */
class InALU extends AbstractALU implements ALU {
    InALU() {ALU_OP = D5Opcode.IN;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        long v1 = inst.getIn();
        D9MemMode m1 = getMemMode(inst, IP, 1);
        int a1 = (int)inst.getAt(IP+1);
        inst.setAt(a1, v1, m1);
        return IP+2;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof InALU)) return false;
        InALU alu = (InALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 6 is jump-if-false: if the first parameter is zero, it sets
 * the instruction pointer to the value from the second
 * parameter. Otherwise, it does nothing.
 */
class JzALU extends AbstractALU implements ALU {
    JzALU() {ALU_OP = D5Opcode.JZ;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        if(v1 == 0)
            return (int)v2;
        return IP+3;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof JzALU)) return false;
        JzALU alu = (JzALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 5 is jump-if-true: if the first parameter is non-zero, it
 * sets the instruction pointer to the value from the second
 * parameter. Otherwise, it does nothing.
 */
class JnzALU extends AbstractALU implements ALU {
    JnzALU() {ALU_OP = D5Opcode.JNZ;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1),
            m2 = getMemMode(inst, IP, 2);
        long v1 = inst.getAt(IP+1, m1),
            v2 = inst.getAt(IP+2, m2);
        if(v1 != 0)
            return (int)v2;
        return IP+3;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof JnzALU)) return false;
        JnzALU alu = (JnzALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

/**
 * Opcode 9 adjusts the relative base by the value of its only
 * parameter. The relative base increases (or decreases, if the value
 * is negative) by the value of the parameter;
 */
class MvbALU extends AbstractALU implements ALU {
    MvbALU() {ALU_OP = D5Opcode.MVB;}

    public int eval(final D9Intcode inst, final int IP, final int op)
        throws IntcodeException {
        if(op != ALU_OP.getId()) {
            if(nextALU == null)
                return IP;
            return nextALU.eval(inst, IP, op);
        }
        D9MemMode m1 = getMemMode(inst, IP, 1);
        long v1 = inst.getAt(IP+1, m1);
        inst.addBase((int)v1);
        return IP+2;
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof MvbALU)) return false;
        MvbALU alu = (MvbALU)obj;
        if(alu == this) return true;
        return alu.getOp() == ALU_OP;
    }
}

enum D9IntcodeState {
    READY,
    WAIT_IN,
    WAIT_OUT,
    TERMINATED
}
