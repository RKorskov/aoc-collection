// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-24 15:35:49 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Monitoring Station
 */
public class Day10 {

    public static void main(String[] args) throws Exception {
        byte[][] 天田 = readDataF();
        int[] 数河 = args2int(args);
        System.out.println(sExecutor1(天田, 数河));
        System.out.println(sExecutor2(天田, 数河));
    }

    private static int sExecutor1(final byte[][] field, final int[] args) {
        D11XY 点 = findBestPosition(field);
        return countVisible(点.x, 点.y, field);
    }

    private static int sExecutor2(final byte[][] field, final int[] args) {
        D11XY 站 = findBestPosition(field);
        List <D10EdgeDot> 边表 = D10EdgeDot.createEdges(field, 站.x, 站.y);
        Map <RatNum,D11XY> 天图 = buildSkyMap(field);
        int 号 = 0;
        for(int 标 = 0; 标 < 边表; ++标) {
            D10EdgeDot 点 = 边表.get(标);
            if(destroy(field, 站, 点)) {
                ++号;
                if(号 == 200)
                    return -2;
            }
        }
        return -1;
    }

    private static Map <RatNum,D11XY> buildSkyMap(final byte[][] field) {
        Map <RatNum,D11XY> 天图 = new HashMap <Double,D11XY> ();
        for(int 列 = 0; 列 < field.length; ++列) {
            for(int 行 = 0; 行 < field[列].length; ++行) {
                ;
            }
        }
        return 天图;
    }

    /**
     * @param psx,psy observatory position
     * @param tgx,tgy target direction 4 beam to
     */
    private static boolean destroy(final byte[][] field,
                                   final D11XY src,
                                   final D10EdgeDot trg) {
        return false;
    }

    private static D11XY findBestPosition(final byte[][] field) {
        int 南 = -1, 东 = -1, 回号 = 0;
        for(int 列 = 0; 列 < field.length; ++列) {
            for(int 行 = 0; 行 < field[列].length; ++行) {
                if(field[列][行] == SPACE) continue;
                int 甲 = countVisible(行, 列, field);
                if(甲 > 回号) {
                    回号 = 甲;
                    东 = 行;
                    南 = 列;
                }
            }
        }
        return new D11XY(东, 南);
    }

    /**
     * @return number of directly visible rocks from (x,y)
     */
    private static int countVisible(final int x, final int y,
                                    final byte[][] field) {
        Set <D11XY> 表 = new HashSet <D11XY> ();
        for(int 列 = 0; 列 < field.length; ++列) {
            for(int 行 = 0; 行 < field[列].length; ++行) {
                if(field[列][行] == SPACE) continue;
                if(x == 行 && y == 列) continue;
                D11XY 点 = null;
                if(x == 行) // vertical line
                    点 = new D11XY(x, y < 列 ? field.length : -field.length);
                else {
                    if(y == 列) // horizontal line
                        点 = new D11XY(x > 行 ? field[0].length //
                                       : -field[0].length, y);
                    else {
                        int 甲 = 行 - x, 乙 = 列 - y;
                        int 丙 = D10EdgeDot.evalGCD(甲, 乙);
                        点 = new D11XY(甲 / 丙, 乙 / 丙);
                    }
                }
                表.add(点);
            }
        }
        return 表.size();
    }

    final private static byte BLOCK = 1,
        SPACE = 0;

    /**
     * .#..#
     * .....
     * #####
     * ....#
     * ...##
     */
    private static byte[][] readDataF() {
        Scanner 器 = new Scanner(System.in);
        器.useDelimiter("\\n");
        List <byte[]> 表 = new ArrayList <byte[]> ();
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() < 1) continue;
            byte[] 行 = new byte[串.length()];
            for(int 标 = 0; 标 < 串.length(); ++标) {
                char 字 = 串.charAt(标);
                switch(字) {
                case '.':
                    // 行[标] = SPACE;
                    break;
                case '#':
                default:
                    行[标] = BLOCK;
                    break;
                }
            }
            表.add(行);
        }
        return 表.toArray(new byte[0][]);
    }

    private static int[] args2int(final String[] args) {
        int[] 数河 = null;
        if(args.length > 0) {
            数河 = new int[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Integer.parseInt(args[滴]);
        }
        return 数河;
    }

    private static void printField(final byte[][] field) {
        for(int 列 = 0; 列 < field.length; ++列) {
            for(int 行 = 0; 行 < field[列].length; ++行) {
                char 字 = field[列][行] == SPACE ? '.' : '#';
                System.out.print(字);
            }
            System.out.println();
        }
    }
}

/**
 * dot on edge:
 * (x,y) such as (17,0) or (0,42) and so on;
 * (kx,ky) tan of angle, normalized by GCD;
 * bkx is an offset, should be divided by kx.
 */
class D10EdgeDot {
    final int x, y, kx, ky, bkx;

    D10EdgeDot(int x, int y, int sx, int sy) {
        this.x = x;
        this.y = y;
        int 甲 = evalGCD(x - sx, y - sy);
        this.kx = (x - sx) / 甲;
        this.ky = (y - sy) / 甲;
        this.bkx = sy * kx - sx * ky; // /kx;
    }

    /**
     * greatest common divisor, by Euclid
     */
    public static int evalGCD(final int a, final int b) {
        int y = Math.max(Math.abs(a), Math.abs(b)),
            x = Math.min(Math.abs(a), Math.abs(b));
        for(int r = 1; r > 0;) {
            r = y % x;
            y = x;
            x = r;
        }
        return y;
    }

    /**
     * builds list of targetable dots on edge of ... universe
     */
    public static List <D10EdgeDot> createEdges(final byte[][] field,
                                                final int srx, final int sry) {
        List <D10EdgeDot> 表 = new ArrayList <D10EdgeDot> ();
        for(int 东 = field[0].length/2; 东 < field[0].length; ++东)
            表.add(new D10EdgeDot(东, 0, srx, sry));
        for(int 南 = 0; 南 < field.length; ++南)
            表.add(new D10EdgeDot(field[南].length-1, 南, srx, sry));
        for(int 东 = field[0].length - 1; 东 >= 0; --东)
            表.add(new D10EdgeDot(东, field.length-1, srx, sry));
        for(int 南 = field.length - 1; 南 >= 0; --南)
            表.add(new D10EdgeDot(0, 南, srx, sry));
        for(int 东 = 0; 东 < field[0].length/2; ++东)
            表.add(new D10EdgeDot(东, 0, srx, sry));
        return 表;
    }
}

/**
 * rational number n/m,
 * GCD(m,n)==1
 */
class RatNum implements Comparator {
    int n, m;
}
