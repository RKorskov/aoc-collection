// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-20 01:29:03 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class Day19 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        // System.out.println(Arrays.toString(al));
        // System.out.println(sExecutor1(al, 数河));
        System.out.println(sExecutor2(al, 数河));
    }

    private static long sExecutor1 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        Intcode 电脑 = D9Intcode.create(prog);
        int 回号 = 0;
        for(int 列 = 0; 列 < 50; ++列)
            for(int 行 = 0; 行 < 50; ++行)
                回号 += (int)电脑.eval(new long[] {行, 列});
        return 回号;
    }

    private static int SCAN_LINE = 1_000_000, // jump of ((very) strong) faith
        SPOT_SIZE = 100;

    private static long sExecutor2 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        Intcode 电脑 = D9Intcode.create(prog);
        int 左 = 0, 右 = 0;
        左 = findEdgeFromLeft(电脑, SCAN_LINE); // System.out.println(左);
        右 = findEdgeFromRight(电脑, SCAN_LINE); // System.out.println(左);
        // y = kx
        double kL, kR;
        kL = (double)SCAN_LINE / 左;
        kR = (double)SCAN_LINE / 右;
        // System.out.printf("%f %f\n", kL, kR);
        /*
         * а теперь, точки пересечения с диагональю! y=-x+b
         * {y+99 = kL*x} && {y = kR*(x+99)};
         * kR*(x+99) + 99 = kL*x;
         * kR*x+kR*99 + 99 = kL*x;
         * (kR+1)*99 = (kL-kR)*x;
         * x = (kR+1)*99 / (kL-kR);
         * y = kR*(x+99);
         */
        double x = 99 * (kR+1) / (kL-kR),
            y = kR*(x + 99);
        System.out.printf("kL %f  kR %f\n", kL, kR);
        System.out.printf(" x %f   y %f\n", x, y);
        /*
         * 1e5: kL 1.748802  kR 1.395907
         * 1e6: kL 1.748830  kR 1.395892
         * x 672  y 1076
         * 6722477 too low
         */
        return (long)x * 10_000 + (long)y;
    }

    private static int findEdgeFromLeft(final Intcode 电脑, final int onLineY) {
        // binary search MUCH faster, but is if worth it here?
        for(int 行 = 0; 行 <= onLineY; ++行) {
            int 点 = (int)电脑.eval(new long[] {行, onLineY});
            if(点 == 1)
                return 行;
        }
        return -1;
    }

    private static int findEdgeFromRight(final Intcode 电脑,
                                         final int onLineY) {
        // binary search MUCH faster, but is if worth it here?
        for(int 行 = onLineY; 行 > 0 ; --行) {
            int 点 = (int)电脑.eval(new long[] {行, onLineY});
            if(点 == 1)
                return 行;
        }
        return -1;
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}
