// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-03 22:50:35 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Day03 {

    public static void main(String[] args) throws Exception {
        List <String> als = readData();
        List <D3Wire> alw = parseRead(als);
        System.out.println(sExecutor1(alw));
        System.out.println(sExecutor2(alw));
    }

    private static int sExecutor1(final List <D3Wire> d3wires) {
        final D3Wire 甲链 = d3wires.get(0),
            乙链 = d3wires.get(1);
        List <D3Node> 路口 = findIntersections(甲链, 乙链);
        int 好好 = Integer.MAX_VALUE;
        for(D3Node 点 : 路口) {
            int 号 = 点.getDistance();
            if(号 < 好好 && 号 > 0)
                好好 = 号;
        }
        return 好好;
    }

    private static int sExecutor2(final List <D3Wire> d3wires) {
        final D3Wire 甲链 = d3wires.get(0),
            乙链 = d3wires.get(1);
        List <D3Node> 路口 = findIntersections(甲链, 乙链);
        int 好好 = Integer.MAX_VALUE;
        for(D3Node 点 : 路口) {
            final int 甲总 = 甲链.getDistance(点.东, 点.南),
                乙总 = 乙链.getDistance(点.东, 点.南),
                总 = 甲总 + 乙总;
            if(总 < 好好)
                好好 = 总;
        }
        return 好好;
    }

    private static List <D3Node> findIntersections(final D3Wire 甲链,
                                                   final D3Wire 乙链) {
        List <D3Node> 路口 = new ArrayList <D3Node> ();
        for(int 甲 = 1, 甲东 = 0, 甲南 = 0; 甲 < 甲链.链.size(); ++甲) {
            D3Node 甲节 = 甲链.链.get(甲);
            boolean 甲横 = 甲南 == 甲节.南;
            for(int 乙 = 1, 乙东 = 0, 乙南 = 0; 乙 < 乙链.链.size(); ++乙) {
                D3Node 乙节 = 乙链.链.get(乙);
                boolean 乙横 = 乙南 == 乙节.南;
                if(甲横 != 乙横) {
                    if(甲横) {
                        int 首 = Integer.max(甲东, 甲节.东),
                            尾 = Integer.min(甲东, 甲节.东),
                            左 = Integer.min(乙南, 乙节.南),
                            右 = Integer.max(乙南, 乙节.南);
                        if(首 >= 乙东 && 尾 <= 乙东 //
                           && 左 <= 甲南 && 右 >= 甲南 //
                           && !(乙东 == 0 && 甲南 == 0)) {
                            D3Node 点 = D3Node.createNode(乙东, 甲南);
                            if(点 != null)
                                路口.add(点);
                        }
                    }
                    else {
                        int 首 = Integer.max(甲南, 甲节.南),
                            尾 = Integer.min(甲南, 甲节.南),
                            左 = Integer.min(乙东, 乙节.东),
                            右 = Integer.max(乙东, 乙节.东);
                        if(首 >= 乙南 && 尾 <= 乙南 //
                           && 左 <= 甲东 && 右 >= 甲东 //
                           && !(甲东 == 0 && 乙南 == 0)) {
                            D3Node 点 = D3Node.createNode(甲东, 乙南);
                            if(点 != null)
                                路口.add(点);
                        }
                    }
                }
                乙东 = 乙节.东;
                乙南 = 乙节.南;
            }
            甲东 = 甲节.东;
            甲南 = 甲节.南;
        }
        return 路口;
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        List <String> als = new ArrayList <String> ();
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }

    private static List <D3Wire> parseRead(final List <String> sdata) {
        List <D3Wire> 链 = new ArrayList <D3Wire> (sdata.size());
        for(String 串 : sdata) {
            D3Wire 节 = D3Wire.createWire(串);
            if(节 != null)
                链.add(节);
        }
        return 链;
    }
}

class D3Wire {
    List <D3Node> 链;
    private D3Wire(){;}

    public void add(final D3Node d3n) {
        链.add(d3n);
    }

    public void add(final int x, final int y) {
        D3Node 节 = D3Node.createNode(x,y);
        if(节 != null)
            链.add(节);
    }

    /**
     * get distance by-wire from (0,0) to (dx,dy)
     */
    public int getDistance(final int dx, final int dy) {
        int 长 = 0;
        int 尾东 = 0, 尾南 = 0;
        for(D3Node 节 : 链) {
            final int 首东 = 节.东, 首南 = 节.南;
            final int 右 = Math.max(尾东, 首东),
                左 = Math.min(尾东, 首东),
                上 = Math.max(尾南, 首南),
                下 = Math.min(尾南, 首南);
            if(左 <= dx && 右 >= dx && 下 <= dy && 上 >= dy) {
                长 += Math.abs(dx - 尾东) + Math.abs(dy - 尾南);
                break;
            }
            else
                长 += Math.abs(首东 - 尾东) + Math.abs(首南 - 尾南);
            尾东 = 首东;
            尾南 = 首南;
        }
        return 长;
    }

    public static D3Wire createWire() {
        D3Wire d3w = new D3Wire();
        d3w.链 = new ArrayList <D3Node> ();
        return d3w;
    }

    /**
     * U7,R6,D4,L4
     */
    public static D3Wire createWire(final String wstr) {
        D3Wire d3w = new D3Wire();
        d3w.链 = new ArrayList <D3Node> ();
        String[] 节子 = wstr.split(",");
        int 东 = 0, 南 = 0;
        d3w.add(东, 南);
        for(String 串 : 节子) {
            int 号 = Integer.parseInt(串.substring(1));
            switch(串.charAt(0)) {
            case 'L':
                东 -= 号;
                break;
            case 'R':
                东 += 号;
                break;
            case 'U':
                南 += 号;
                break;
            case 'D':
                南 -= 号;
                break;
            default:
                System.out.printf("wrong direction '%s'\n", 串);
                continue;
            }
            d3w.add(东, 南);
        }
        return d3w;
    }
}

class D3Node {
    int 南, 东;
    private D3Node(){;}

    /**
     * Manhattan distance
     */
    public int getDistance() {
        return 东 + 南;
    }

    public String toString() {
        return String.format("(%d, %d)", 东, 南);
    }

    public static D3Node createNode(final int x, final int y) {
        D3Node 节 = new D3Node();
        节.南 = y;
        节.东 = x;
        return 节;
    }
}

