// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-02 12:10:43 roukoru>

import java.util.Scanner;
import java.util.Arrays;

public class Day02 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        System.out.println(sExecutor(al));
        System.out.println(sExecutor2(al));
    }

    /**
     * Once you have a working computer, the first step is to restore
     * the gravity assist program (your puzzle input) to the "1202
     * program alarm" state it had just before the last computer
     * caught fire. To do this, before running the program, replace
     * position 1 with the value 12 and replace position 2 with the
     * value 2.
     */
    private static long sExecutor (final long[] prog) {
        if(prog.length < 3) return -1;
        long[] 程序 = Arrays.copyOf(prog, prog.length);
        程序[1] = 12;
        程序[2] = 2;
        eval(程序);
        return 程序[0];
    }

    private static long sExecutor2 (final long[] prog) {
        long[] 程序;
        for(int 名词 = 0; 名词 < 100; ++名词) {
            for(int 动词 = 0; 动词 < 100; ++动词) {
                程序 = Arrays.copyOf(prog, prog.length);
                程序[1] = 名词;
                程序[2] = 动词;
                eval(程序);
                if(程序[0] == 19690720)
                    return 名词 * 100 + 动词;
            }
        }
        return -1;
    }

    private static void eval(final long[] prog) {
        for(int 标 = 0; prog[标] != 99; 标 += 4) {
            int 向 = (int)prog[标+3],
                甲 = (int)prog[标+1],
                乙 = (int)prog[标+2];
            switch((int)prog[标]) {
            case 1: // add
                prog[向] = prog[甲] + prog[乙];
                break;
            case 2: // mul
                prog[向] = prog[甲] * prog[乙];
                break;
            case 99:
                System.out.println("Oops!");
                return;
            }
        }
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}
