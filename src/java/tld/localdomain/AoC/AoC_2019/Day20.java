// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-20 14:36:33 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day20 {

    public static void main (String[] args) throws Exception {
        List <String> 串表 = readData();
        System.out.println(sExecutor1());
        System.out.println(sExecutor2());
    }

    private static int sExecutor1(final D20Maze maze) {
        return -1;
    }

    private static int sExecutor2(final D20Maze maze) {
        return -1;
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> 串表 = new ArrayList <String>();
        器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串.length() > 0)
                串表.add(串);
        }
        return 串表;
    }

}

class D20Maze {
}
