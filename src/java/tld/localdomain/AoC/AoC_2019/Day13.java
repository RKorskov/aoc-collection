// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-14 01:21:45 roukoru>

import java.util.Scanner;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;

public class Day13 {

    public static void main (String[] args) throws Exception {
        long[] al = readData();
        long[] 数河 = null;
        if(args.length > 0) {
            数河 = new long[args.length];
            for(int 滴 = 0; 滴 < 数河.length; ++滴)
                数河[滴] = Long.parseLong(args[滴]);
        }
        // System.out.println(Arrays.toString(al));
        System.out.println(sExecutor1(al, 数河));
        System.out.println(sExecutor2(al, 数河));
    }

    private static long sExecutor1 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        Intcode 电脑 = D9Intcode.create(prog);
        电脑.eval(prog_in);
        long[] 数数 = 电脑.getOutput();
        //System.out.println(Arrays.toString(数数));
        return blockCount(数数);
    }

    private static long sExecutor2 (final long[] prog, final long[] prog_in) {
        if(prog.length < 3) return -2;
        prog[0] = 2; // set it to 2 to play for free
        Intcode 电脑 = D9Intcode.create(prog);
        电脑.enableRetainMemory();
        for(int bc = 1; bc > 0;) {
            电脑.eval(prog_in);
            long[] 数数 = 电脑.getOutput();
            bc = blockCountQ(数数);
        }
        return -1;
    }

    private static int blockCount(final long[] 数数) {
        if(数数 == null || 数数.length < 3) return 0;
        Set <D11XY> 东西 = new HashSet <D11XY> ();
        for(int 标 = 0; 标 < 数数.length; 标 += 3) {
            if(数数[标+2] != 2) continue; // block tile
            D11XY 点 = new D11XY((int)数数[标], (int)数数[标+1]);
            if(点 != null)
                东西.add(点);
        }
        return 东西.size();
    }

    private static int blockCountQ(final long[] 数数) {
        if(数数 == null || 数数.length < 3) return 0;
        int 回数 = 0;
        for(int 标 = 0; 标 < 数数.length; 标 += 3) {
            if(数数[标] == -1 && 数数[标+1] == 0) {
                System.out.println(数数[标+2]);
                continue;
            }
            if(数数[标+2] == 2) // block tile
                ++回数;
        }
        return 回数;
    }

    private static long[] readData() {
        String 串 = (new Scanner(System.in)).next();
        String[] 甲 = 串.split(",");
        long[] 号表 = new long[甲.length];
        for(int 乙 = 0; 乙 < 甲.length; ++乙)
            号表[乙] = Long.parseLong(甲[乙]);
        return 号表;
    }

}
