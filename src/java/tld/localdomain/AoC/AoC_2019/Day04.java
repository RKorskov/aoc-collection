// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-04 09:28:59 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Day04 {

    public static void main(final String[] args) throws Exception {
        String[] 串 = args[0].split("-");
        int 甲 = Integer.parseInt(串[0]),
            乙 = Integer.parseInt(串[1]);
        System.out.println(sExecutor1(甲, 乙));
        System.out.println(sExecutor2(甲, 乙));
    }

    private static int sExecutor1(final int n0, final int n1) {
        int 好好 = 0;
        for(int 号 = n0; 号 <= n1; ++号) {
            if(testPasswd(号))
                ++好好;
        }
        return 好好;
    }

    private static int sExecutor2(final int n0, final int n1) {
        int 好好 = 0;
        for(int 号 = n0; 号 <= n1; ++号) {
            if(testPasswd2(号))
                ++好好;
        }
        return 好好;
    }

    private static boolean testPasswd(final int npwd) {
        if(npwd < 111111) return false;
        String 密码 = String.valueOf(npwd);
        char 前字 = 密码.charAt(0);
        boolean 好好 = true,
            两 = false;
        for(int 号 = 1; 号 < 密码.length(); ++号) {
            char 字 = 密码.charAt(号);
            if(字 < 前字) {
                好好 = false;
                break;
            }
            if(字 == 前字)
                两 = true;
            前字 = 字;
        }
        return 好好 & 两;
    }

    final static int PLATEAU_DIGIT = 0,
        PLATEAU_LENGTH = 1;

    private static boolean testPasswd2(final int npwd) {
        if(npwd < 111111) return false;
        int[][] 字板 = new int[10][];
        for(int 甲 = 0; 甲 < 10; 字板[甲++] = new int[2]);
        String 密码 = String.valueOf(npwd);
        char 前字 = 密码.charAt(0);
        boolean 好好 = true;
        for(int 号 = 1; 号 < 密码.length(); ++号) {
            char 字 = 密码.charAt(号);
            if(字 < 前字) {
                好好 = false;
                break;
            }
            if(字 == 前字) {
                int 甲 = 字 - '0';
                字板[甲][PLATEAU_DIGIT] = 甲;
                ++字板[甲][PLATEAU_LENGTH];
            }
            前字 = 字;
        }
        boolean 两 = false;
        for(int[] 高原: 字板) {
            if(高原[PLATEAU_LENGTH] == 1)
                两 = true;
        }
        return 好好 & 两;
    }
}
