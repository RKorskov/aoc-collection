// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-08 12:48:42 roukoru>

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Day08 {

    public static void main (String[] args) throws Exception {
        int x = 25, y = 6;
        if(args.length > 0)
            x = Integer.parseInt(args[0]);
        if(args.length > 1)
            y = Integer.parseInt(args[1]);
        List <byte[][]> 图画 = readParseData(x, y);
        System.out.printf("%d重图画\n", 图画.size());
        System.out.println(sExecutor1(图画));
        System.out.println(sExecutor2(图画));
    }

    private static long sExecutor1 (final List <byte[][]> image) {
        int 画号0 = Integer.MAX_VALUE, 画12 = 0;
        for(byte[][] 层 : image) {
            int 号1 = 0, 号2 = 0, 号0 = 0;
            for(int 南 = 0; 南 < 层.length; ++南)
                for(int 东 = 0; 东 < 层[南].length; ++东)
                    switch(层[南][东]) {
                    case 0:
                        ++号0;
                        break;
                    case 1:
                        ++号1;
                        break;
                    case 2:
                        ++号2;
                        break;
                    }
            if(号0 < 画号0) {
                画号0 = 号0;
                画12 = 号1 * 号2;
            }
        }
        return 画12;
    }

    private static long sExecutor2 (final List <byte[][]> image) {
        int 画号0 = Integer.MAX_VALUE, 画12 = 0;
        byte[][] 图画 = newLayer(image.get(0)[0].length, image.get(0).length);
        for(int 南 = 0; 南 < 图画.length; ++南)
            Arrays.fill(图画[南], (byte)2);
        for(byte[][] 层 : image) {
            for(int 南 = 0; 南 < 层.length; ++南)
                for(int 东 = 0; 东 < 层[南].length; ++东)
                    switch(层[南][东]) {
                    case 0:
                    case 1:
                        if(图画[南][东] == 2)
                            图画[南][东] = 层[南][东];
                        break;
                    case 2:  // skip;
                        break;
                    }
        }
        printPixmap(图画);
        return 0;
    }

    private static void printPixmap(final byte[][] image) {
        for(int 南 = 0; 南 < image.length; ++南) {
            for(int 东 = 0; 东 < image[南].length; ++东) {
                byte 字 = image[南][东];
                if(字 == 0) 字 = ' ';
                else 字 += '0';
                System.out.print((char)字);
            }
            System.out.println();
        }
    }

    private static List <byte[][]> readParseData(final int imgWidth,
                                                 final int imgHeight) {
        Scanner sc = new Scanner(System.in);
        if(!sc.hasNext()) return null;
        byte[] inb = sc.next().getBytes();
        List <byte[][]> als = new ArrayList <byte[][]> ();
        byte[][] layer = newLayer(imgWidth, imgHeight);
        als.add(layer);
        int 东 = 0, 南 = 0;
        for(byte i : inb) {
            byte 墨 = (byte)(i - '0');
            if(东 >= imgWidth) {
                东 = 0;
                ++南;
            }
            if(南 >= imgHeight) {
                东 = 0;
                南 = 0;
                layer = newLayer(imgWidth, imgHeight);
                als.add(layer);
            }
            layer[南][东++] = 墨;
        }
        return als;
    }

    private static byte[][] newLayer(final int imgWidth, final int imgHeight) {
        byte[][] layer = new byte[imgHeight][];
        for(int 甲 = 0; 甲 < imgHeight; ++甲)
            layer[甲] = new byte[imgWidth];
        return layer;
    }

    private static int[] parse(final List <String> data) {
        int[] 表 = new int[data.size()];
        int 甲 = 0;
        for(String 串 : data) {
            int 号 = Integer.parseInt(串);
            表[甲++] = 号;
        }
        for(;甲 < data.size();++甲)
            表[甲] = 0;
        return 表;
    }
}
