// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-21 16:34:36 roukoru>

import java.util.Objects;

class D11XY {
    final int x, y;

    D11XY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    D11XY() {x = 0; y = 0;}

    public int hashCode() {
        return Objects.hash(x, y);
    }

    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof D11XY)) return false;
        D11XY oxy = (D11XY) obj;
        if(oxy == this) return true;
        if(oxy.x == x && oxy.y == y) return true;
        return false;
    }

    public String toString() {
        return String.format("(%d,%d)", x, y);
    }
}
