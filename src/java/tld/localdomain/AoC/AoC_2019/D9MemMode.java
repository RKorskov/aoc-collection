// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-11 10:42:55 roukoru>

enum D9MemMode {
    DIRECT,   // 0
    CONSTANT, // 1
    RELATIVE;  // 2
}
