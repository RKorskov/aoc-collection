// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-27 10:11:58 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day06 {

    public static void main (String[] args) throws Exception {
        List <String> als = readData();
        List <GCmd> alc = parse(als);
        System.out.println(sExecutor(alc));
        System.out.println(sExecutor2(alc));
    }

    private static int sExecutor (final List <GCmd> args) {
        D6Grid d6g = D6Grid.createGrid(1000);
        for(GCmd 命 : args)
            d6g.apply(命);
        return d6g.countLit();
    }

    private static long sExecutor2 (final List <GCmd> args) {
        D6Grid d6g = D6Grid.createGrid(1000);
        for(GCmd 命 : args)
            d6g.apply2(命);
        return d6g.countLit2();
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        List <String> als = new ArrayList <String> ();
        sc.useDelimiter("\\n");
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }

    private static List <GCmd> parse(final List <String> data) {
        List <GCmd> 表命 = new ArrayList <GCmd> (data.size());
        for(String 串 : data) {
            GCmd 命 = GCmd.parse(串);
            if(命 != null)
                表命.add(命);
        }
        return 表命;
    }
}

class GCmd {
    /**
     * turn off 301,3 through 808,453
     * turn on 351,678 through 951,908
     * toggle 720,196 through 897,994
     */
    public static Pattern pat = Pattern.compile //
        ("(?<act>turn off|turn on|toggle) ((?<x0>\\d+),(?<y0>\\d+)) through ((?<xe>\\d+),(?<ye>\\d+))");

    int x0,y0, xe,ye;
    Action act;
    private GCmd(){;}

    public static GCmd parse(final String str) {
        Matcher m = pat.matcher(str);
        if(!m.find())
            return null;
        GCmd 命 = new GCmd();
        命.x0 = Integer.parseInt(m.group("x0"));
        命.y0 = Integer.parseInt(m.group("y0"));
        命.xe = Integer.parseInt(m.group("xe"));
        命.ye = Integer.parseInt(m.group("ye"));
        switch(m.group("act")) {
        case "turn off":
            命.act = Action.OFF;
            break;
        case "turn on":
            命.act = Action.ON;
            break;
        case "toggle":
            命.act = Action.TOGGLE;
            break;
        default:
            命 = null;
        }
        return 命;
    }
}

enum Action {
    ON, OFF, TOGGLE;
}

class D6Grid {
    final private static int LBITLEN = 64;
    int 长;
    int[] 矩阵;
    private D6Grid(){;}

    public void on(final int x, final int y) {
        int 地址 = y * 长 + x;
        ++矩阵[地址];
    }

    public void off(final int x, final int y) {
        int 地址 = y * 长 + x;
        矩阵[地址] = 0;
    }

    public void flip(final int x, final int y) {
        int 地址 = y * 长 + x;
        if(矩阵[地址] == 0)
            矩阵[地址] = 1;
        else
            矩阵[地址] = 0;
    }

    public void off2(final int x, final int y) {
        int 地址 = y * 长 + x;
        if(矩阵[地址] > 0)
            --矩阵[地址];
    }

    public void flip2(final int x, final int y) {
        int 地址 = y * 长 + x;
        ++矩阵[地址];
        ++矩阵[地址];
    }

    public int get(final int x, final int y) {
        int 地址 = y * 长 + x;
        return 矩阵[地址];
    }

    public void apply(GCmd cmd) {
        for(int 南 = cmd.y0; 南 <= cmd.ye; ++南)
            for(int 东 = cmd.x0; 东 <= cmd.xe; ++东)
                switch(cmd.act) {
                case ON:
                    on(东, 南);
                    break;
                case OFF:
                    off(东, 南);
                    break;
                case TOGGLE:
                    // default:
                    flip(东, 南);
                }
    }

    public void apply2(GCmd cmd) {
        for(int 南 = cmd.y0; 南 <= cmd.ye; ++南)
            for(int 东 = cmd.x0; 东 <= cmd.xe; ++东)
                switch(cmd.act) {
                case ON:
                    on(东, 南);
                    break;
                case OFF:
                    off2(东, 南);
                    break;
                case TOGGLE:
                    // default:
                    flip2(东, 南);
                }
    }

    public int countLit() {
        int 灯号 = 0;
        for(int 南 = 0; 南 < 1000; ++南)
            for(int 东 = 0; 东 < 1000; ++东)
                if(get(东, 南) > 0)
                    ++灯号;
        return 灯号;
    }

    public long countLit2() {
        long 灯号 = 0;
        for(int 南 = 0; 南 < 1000; ++南)
            for(int 东 = 0; 东 < 1000; ++东)
                灯号 += get(东, 南);
        return 灯号;
    }

    /**
     * square grid of size之size
     */
    public static D6Grid createGrid(final int size) {
        D6Grid 矩 = new D6Grid();
        矩.长 = size;
        矩.矩阵 = new int[size * size];
        return 矩;
    }
}
