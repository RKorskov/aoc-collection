package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 15: Science for Hungry People
 *
 * Today, you set out on the task of perfecting your milk-dunking
 * cookie recipe. All you have to do is find the right balance of
 * ingredients.
 * Your recipe leaves room for exactly 100 teaspoons of
 * ingredients. You make a list of the remaining ingredients you could
 * use to finish the recipe (your puzzle input) and their properties
 * per teaspoon:
 *
 *  - capacity (how well it helps the cookie absorb milk)
 *  - durability (how well it keeps the cookie intact when full of milk)
 *  - flavor (how tasty it makes the cookie)
 *  - texture (how it improves the feel of the cookie)
 *  - calories (how many calories it adds to the cookie)
 *
 * You can only measure ingredients in whole-teaspoon amounts
 * accurately, and you have to be accurate so you can reproduce your
 * results in the future. The total score of a cookie can be found by
 * adding up each of the properties (negative totals become 0) and
 * then multiplying together everything except calories.
 * For instance, suppose you have these two ingredients:
 *
 * Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
 * Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
 *
 * Then, choosing to use 44 teaspoons of butterscotch and 56 teaspoons
 * of cinnamon (because the amounts of each ingredient must add up to
 * 100) would result in a cookie with the following properties:
 *
 *  - A capacity of 44*-1 + 56*2 = 68
 *  - A durability of 44*-2 + 56*3 = 80
 *  - A flavor of 44*6 + 56*-2 = 152
 *  - A texture of 44*3 + 56*-1 = 76
 *
 * Multiplying these together (68 * 80 * 152 * 76, ignoring calories
 * for now) results in a total score of 62842880, which happens to be
 * the best score possible given these ingredients. If any properties
 * had produced a negative total, it would have instead become zero,
 * causing the whole score to multiply to zero.
 * Given the ingredients in your kitchen and their properties, what is
 * the total score of the highest-scoring cookie you can make?
 */
public class Day15 {
    public static void main(final String[] args) {
        Ingredient[] aoi = readParseInput(args);
        System.out.println(part1(aoi));
        System.out.println(part2(aoi));
    }

    private static Ingredient[] readParseInput(final String[] args) {
        List<Ingredient> lois = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            Ingredient p = Ingredient.create(src);
            //System.err.println(p);
            if(p != null)
                lois.add(p);
        }
        if(lois.size() < 4) return null;
        return lois.toArray(new Ingredient[0]);
    }

    final static int TOTAL = 100;

    /**
     * Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2
     * Sprinkles: capacity -3, durability 3, flavor 0, texture 0, calories 9
     * Candy: capacity -1, durability 0, flavor 4, texture 0, calories 1
     * Chocolate: capacity 0, durability 0, flavor -2, texture 2, calories 8
     */
    private static long part1(final Ingredient[] aoi) {
        long bestScore = 0;
        for(int 甲 = 0; 甲 <= TOTAL; ++甲) {
            for(int 乙 = 0; 乙 <= (TOTAL-甲); ++乙) {
                for(int 丙=0; 丙 <= (TOTAL-甲-乙); ++丙) {
                    int 丁 = TOTAL - 甲 - 乙 - 丙;
                    long s = evalScore(aoi, 甲, 乙, 丙, 丁);
                    // if(s > 0) System.out.printf("%d : %d %d %d %d\n", s, 甲, 乙, 丙, 丁);
                    if(s > bestScore) {
                        bestScore = s;
                    }
                }
            }
        }
        return bestScore;
        // return evalScore(aoi, 0, 100);
    }

    /*
    private static int evalScore(Ingredient[] aoi, int ip, int upto) {
        if(ip >= aoi.length) return 1; // !0, see later
        int r = 1;
        for(int a = 0; a <= upto; ++a) {
            evalScore(aoi, ip+1, upto - a);
            int v = aoi[ip];
        }
        return r;
    }
    */

    private static long evalScore(final Ingredient[] aoi, final int... vargs) {
        long cap = 0, dur = 0, fla = 0, tex = 0;
        for(int i = 0; i < vargs.length; ++i) {
            cap += aoi[i].getCap() * vargs[i];
            dur += aoi[i].getDur() * vargs[i];
            fla += aoi[i].getFla() * vargs[i];
            tex += aoi[i].getTex() * vargs[i];
        }
        if(cap < 1 || dur < 1 || fla < 1 || tex < 1)
            return 0;
        long total = cap * dur * fla * tex;
        return total;
    }

    /**
     * Given the ingredients in your kitchen and their properties,
     * what is the total score of the highest-scoring cookie you can
     * make with a calorie total of 500?
     */
    private static long part2(final Ingredient[] aoi) {
        long bestScore = 0;
        for(int 甲 = 0; 甲 <= TOTAL; ++甲) {
            for(int 乙 = 0; 乙 <= (TOTAL-甲); ++乙) {
                for(int 丙=0; 丙 <= (TOTAL-甲-乙); ++丙) {
                    int 丁 = TOTAL - 甲 - 乙 - 丙;
                    long cal = 0;
                    cal += aoi[0].getCal() * 甲;
                    cal += aoi[1].getCal() * 乙;
                    cal += aoi[2].getCal() * 丙;
                    cal += aoi[3].getCal() * 丁;
                    if(cal != 500) continue;
                    long s = evalScore(aoi, 甲, 乙, 丙, 丁);
                    // if(s > 0) System.out.printf("%d : %d %d %d %d\n", s, 甲, 乙, 丙, 丁);
                    if(s > bestScore) {
                        bestScore = s;
                    }
                }
            }
        }
        return bestScore;
    }
}

class Ingredient {
    final String name;
    final int capacity, durability, flavor, texture, calories;

    private Ingredient(String name, int capacity, int durability, int flavor, int texture, int calories) {
        this.name = name;
        this.capacity = capacity;
        this.durability = durability;
        this.flavor = flavor;
        this.texture = texture;
        this.calories = calories;
    }

    public int getCap() { return capacity; }
    public int getDur() { return durability; }
    public int getFla() { return flavor; }
    public int getTex() { return texture; }
    public int getCal() { return calories; }

    public String toString() {
        return String.format("%-16s %d %d %d %d %d",
                             name, capacity, durability, flavor, texture, calories);
    }

    /**
     * Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2
     */
    private final static Pattern pat
        = Pattern.compile("(\\w+): capacity (-?\\d+), durability (-?\\d+), flavor (-?\\d+), texture (-?\\d+), calories (-?\\d+)");

    public static Ingredient create(final String src) {
        Matcher m = pat.matcher(src);
        if(!m.find()) return null;
        String name = m.group(1);
        int capacity = Integer.parseInt(m.group(2)), durability = Integer.parseInt(m.group(3)),
            flavor = Integer.parseInt(m.group(4)), texture = Integer.parseInt(m.group(5)),
            calories = Integer.parseInt(m.group(6));
        return new Ingredient(name, capacity, durability, flavor, texture, calories);
    }

    public static Ingredient create(String name, int capacity, int durability, int flavor, int texture, int calories) {
        return new Ingredient(name, capacity, durability, flavor, texture, calories);
    }
}
