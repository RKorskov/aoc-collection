// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-26 20:43:24 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;

public class Day02 {

    public static void main (String[] args) throws Exception {
        sExecutor(args);
    }

    private static void sExecutor (final String[] args) {
        Scanner sc = new Scanner(System.in);
        long 片页 = 0;
        long 长带 = 0;
        for(;sc.hasNext();) {
            String 字串 = sc.next();
            String[] 甲乙丙 = 字串.split("x");
            final int 甲 = Integer.parseInt(甲乙丙[0]),
                乙 = Integer.parseInt(甲乙丙[1]),
                丙 = Integer.parseInt(甲乙丙[2]);
            final int 甲地 = 甲 * 乙,
                乙地 = 乙 * 丙,
                丙地 = 丙 * 甲,
                量 = 甲 * 乙 * 丙;
            int 长 = 2 * (甲 < 乙 ? (甲 + Integer.min(丙, 乙)) //
                          : (乙 + Integer.min(丙, 甲)));
            int 小地 = min(甲地, 乙地, 丙地);
            int 大地 = 2 * (甲地 + 乙地 + 丙地);
            片页 += 大地 + 小地;
            长带 += 长 + 量;
        }
        System.out.printf("%d片页\n%d长带\n", 片页, 长带);
    }

    public static int min(final int a, final int b, final int... rest) {
        int m = Integer.min(a, b);
        for(int i = 0; i < rest.length; ++i)
            m = Integer.min(m, rest[i]);
        return m;
    }
}
