package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 18: Like a GIF For Your Yard #
 *
 * After the million lights incident, the fire code has gotten
 * stricter: now, at most ten thousand lights are allowed. You arrange
 * them in a 100x100 grid.
 *
 * Never one to let you down, Santa again mails you instructions on
 * the ideal lighting configuration. With so few lights, he says,
 * you'll have to resort to animation.
 *
 * Start by setting your lights to the included initial configuration
 * (your puzzle input). A `#` means "on", and a `.` means "off".
 *
 * Then, animate your grid in steps, where each step decides the next
 * configuration based on the current one. Each light's next state
 * (either on or off) depends on its current state and the current
 * states of the eight lights adjacent to it (including
 * diagonals). Lights on the edge of the grid might have fewer than
 * eight neighbors; the missing ones always count as "off".
 *
 * The state a light should have next is based on its current state
 * (on or off) plus the number of neighbors that are on:
 *
 * - A light which is on stays on when 2 or 3 neighbors are on, and
 *   turns off otherwise.
 * - A light which is off turns on if exactly 3 neighbors are on, and
 *   stays off otherwise.
 *
 * All of the lights update simultaneously; they all consider the same
 * current state before moving to the next.
 *
 * In your grid of 100x100 lights, given your initial configuration,
 * how many lights are on after 100 steps?
 */
public class Day18 {
    public static void main(final String[] args) {
        boolean[][] 格 = readParseInput(args);
        System.out.println(part1(格));
        System.out.println(part2(格));
    }

    private static boolean[][] readParseInput(final String[] args) {
        List<boolean[]> grid = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            List<Boolean> row = new ArrayList<>();
            //System.err.println(src);
            boolean[] brow = new boolean[src.length()];
            int i = 0;
            for(char c : src.toCharArray())
                brow[i++] = c == '#';
            grid.add(brow);
        }
        boolean[][] rlb = new boolean[grid.size()][];
        for(int i = 0; i < grid.size(); ++i)
            rlb[i] = grid.get(i);
        return rlb;
    }

    /**
     */
    private static int part1(final boolean[][] 格) {
        boolean[][] grid = 格;
        for(int rep = 0; rep < 100; ++rep) {
            boolean[][] ngrid = new boolean[grid.length][];
            for(int j = 0; j < grid.length; ++j) {
                ngrid[j] = new boolean[grid[j].length];
                for(int i = 0; i < grid[j].length; ++i)
                    ngrid[j][i] = isOn(grid, i, j);
            }
            grid = ngrid;
        }
        int ras = 0;
        for(int j = 0; j < grid.length; ++j)
            for(int i = 0; i < grid[j].length; ++i)
                if(grid[j][i])
                    ++ras;
        return ras;
    }

    /**
     * - A light which is on stays on when 2 or 3 neighbors are on, and
     *   turns off otherwise.
     * - A light which is off turns on if exactly 3 neighbors are on, and
     *   stays off otherwise.
     */
    private static boolean isOn(final boolean[][] 格,
                                final int x0, final int y0) {
        int cnt = 0;
        if(peek(格, x0-1, y0)) ++cnt;
        if(peek(格, x0+1, y0)) ++cnt;
        if(peek(格, x0,   y0-1)) ++cnt;
        if(peek(格, x0,   y0+1)) ++cnt;
        if(peek(格, x0-1, y0-1)) ++cnt;
        if(peek(格, x0+1, y0-1)) ++cnt;
        if(peek(格, x0-1, y0+1)) ++cnt;
        if(peek(格, x0+1, y0+1)) ++cnt;
        return 格[y0][x0] ? (cnt == 2 || cnt == 3) : cnt == 3;
    }

    private static boolean peek(final boolean[][] 格,
                                final int x, final int y) {
        if(x < 0 || y < 0 || y >= 格.length || x >= 格[y].length)
            return false;
        return 格[y][x];
    }

    /**
     * ... you notice that something's wrong with the grid of lights
     * you bought: four lights, one in each corner, are stuck on and
     * can't be turned off.
     */
    private static int part2(final boolean[][] 格) { 
        boolean[][] grid = 格;
        for(int rep = 0; rep < 100; ++rep) {
            boolean[][] ngrid = new boolean[grid.length][];
            for(int j = 0; j < grid.length; ++j) {
                ngrid[j] = new boolean[grid[j].length];
                for(int i = 0; i < grid[j].length; ++i)
                    ngrid[j][i] = isOn(grid, i, j);
            }
            ngrid[0][0] = true;
            ngrid[0][ngrid[0].length-1] = true;
            ngrid[ngrid.length-1][0] = true;
            ngrid[ngrid.length-1][ngrid[ngrid.length-1].length-1] = true;
            grid = ngrid;
        }
        int ras = 0;
        for(int j = 0; j < grid.length; ++j)
            for(int i = 0; i < grid[j].length; ++i)
                if(grid[j][i])
                    ++ras;
        return ras;
    }
}
