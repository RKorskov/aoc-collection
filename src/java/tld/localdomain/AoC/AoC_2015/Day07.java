// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-29 00:30:43 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * нормализация элементов (у меня это '1 AND ...'):
 * `sed -i -e 's/^1 /one /g;' day07.txt; printf "1 -> one\n" >> day07.txt`
 *
 * также, нужно посмотреть, какой выход подключен к 'a',
 * и именно его подать аргументом для main (у меня был lx)
 */
public class Day07 {

    public static void main (String[] args) throws Exception {
        List <String> als = readData();
        List <CNode> alc = parse(als);
        Map <String,CNode> 电键 = new HashMap <String,CNode> (als.size());
        for(CNode 键 : alc)
            电键.put(键.出口, 键);
        for(CNode 键 : alc)
            for(CNode 子 : alc)
                if(键.出口.equals(子.进口0) || 键.出口.equals(子.进口1))
                    键.第妹.add(子);
        System.out.println(sExecutor(电键, args[0]));
        System.out.println(sExecutor2(电键, args[0]));
    }

    private static int sExecutor (final Map <String,CNode> circuit,
                                  final String arg) {
        for(int RF = 0, CF = -1; CF != RF && RF < circuit.size();) {
            CF = RF; RF = 0;
            for(CNode 键 : circuit.values()) {
                if(键.isActive()) {++RF; continue;}
                GOp gop = 键.getGOp();
                String 进口0 = 键.getIn0(),
                    进口1 = 键.getIn1();
                if(进口0 == null) continue; // WTF?
                CNode 键0 = circuit.get(进口0);
                if(!键0.isActive()) continue; // in0 not ready -- skip
                CNode 键1 = 进口1 == null ? null : circuit.get(进口1);
                switch(gop) {
                case NOT:
                    键.__setOutLevel(~键0.getOutLevel());
                    break;
                case LSHIFT:
                    键.__setOutLevel(键0.getOutLevel() << 键.getNArg());
                    break;
                case RSHIFT:
                    键.__setOutLevel(键0.getOutLevel() >> 键.getNArg());
                    break;
                case OR:
                    if(键1.isActive())
                        键.__setOutLevel(键0.getOutLevel() | 键1.getOutLevel());
                    break;
                case AND:
                    if(键1.isActive())
                        键.__setOutLevel(键0.getOutLevel() & 键1.getOutLevel());
                }
                if(键.isActive()) ++RF;
            }
        }
        // for(CNode 键 : circuit.values()) System.out.printf("%s %d\n", 键.getOut(), 键.getOutLevel());
        return circuit.get(arg).getOutLevel();
    }

    private static int sExecutor2 (final Map <String,CNode> circuit,
                                   final String arg) {
        int lvl1 = circuit.get(arg).getOutLevel();
        for(CNode 键 : circuit.values())
            键.reset();
        circuit.get("b").__setOutLevel(lvl1);
        // for(CNode 进口 : circuit.values()) System.out.println(进口);
        // for(CNode 进口 : inlets.values()) System.out.println(进口);
        for(CNode 键 : circuit.values()) System.out.printf("%s %d\n", 键.getOut(), 键.getOutLevel());
        return sExecutor(circuit, arg);
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        List <String> als = new ArrayList <String> ();
        sc.useDelimiter("\\n");
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }

    private static List <CNode> parse(final List <String> data) {
        List <CNode> 表节 = new ArrayList <CNode> (data.size());
        for(String 串 : data) {
            CNode 节 = CNode.parse(串);
            if(节 != null)
                表节.add(节);
        }
        return 表节;
    }
}

class CNode {
    /**
     * 123 -> x
     * lx -> a
     * x AND y -> d
     * 1 AND am -> an
     * x OR y -> e
     * x LSHIFT 2 -> f
     * y RSHIFT 1 -> g
     * NOT y -> i
     */
    String 进口0, 进口1, 出口;
    GOp op;
    int narg, // numerical argument (shift, set)
        gval; // gate resulting value of op(in0,in1) on out
    Set <CNode> 第妹; // set connected to out nodes

    final public static int NAN = 0xDEADBEEF;
    final private static Pattern patBiOp = Pattern.compile
        ("(?<in0>[a-z]+) (?<op>OR|AND) (?<in1>[a-z]+) -> (?<out>[a-z]+)"),
        patShift = Pattern.compile
        ("(?<in0>[a-z]+) (?<op>[LR]SHIFT) (?<narg>[0-9]+) -> (?<out>[a-z]+)"),
        patNot = Pattern.compile("NOT (?<in0>[a-z]+) -> (?<out>[a-z]+)"),
        patSet = Pattern.compile("(?<narg>[0-9]+) -> (?<out>[a-z]+)");

    private CNode(){;}

    /**
     * out hold a valid value
     */
    public boolean isActive() {return gval != NAN;}
    public int getOutLevel() {return gval;}
    public GOp getGOp() {return op;}
    public String getOut() {return 出口;}
    public int getNArg() {return narg;}

    public void reset() {
        if(op == GOp.SET) {
            if(出口.equals("one"))
                gval = 1;
            else
                gval = 0;
        }
        else
            gval = NAN;
    }

    public String getIn0() {
        if(op == GOp.SET)
            return null;
        return 进口0;
    }

    public String getIn1() {
        if(op == GOp.OR || op == GOp.AND)
            return 进口1;
        return null;
    }

    public void __setOutLevel(final int lvl) {
        gval = lvl;
    }

    public String toString() {
        String 串;
        switch(op) {
        case SET:
            串 = String.format("(setq %s %d)", 出口, narg);
            break;
        case NOT:
            串 = String.format("(setq %s (not %s))", 出口, 进口0);
            break;
        case LSHIFT:
            串 = String.format("(setq %s (lshift %s %d))", 出口, 进口0, narg);
            break;
        case RSHIFT:
            串 = String.format("(setq %s (rshift %s %d))", 出口, 进口0, narg);
            break;
        case OR:
            串 = String.format("(setq %s (or %s %s))", 出口, 进口0, 进口1);
            break;
        case AND:
            串 = String.format("(setq %s (and %s %s))", 出口, 进口0, 进口1);
            break;
        default:
            串 = "(Bad OP)";
        }
        return 串;
    }

    public static CNode parse(final String snode) {
        CNode 节 = parseBiOp(snode);
        if(节 == null) 节 = parseShift(snode);
        if(节 == null) 节 = parseNot(snode);
        if(节 == null) 节 = parseSet(snode);
        if(节 != null)
            节.第妹 = new HashSet <CNode> ();
        return 节;
    }

    private static CNode parseBiOp(final String snode) {
        Matcher m = patBiOp.matcher(snode);
        if(!m.find()) return null;
        CNode 节 = new CNode();
        节.gval = NAN;
        节.narg = NAN;
        节.进口0 = m.group("in0");
        节.进口1 = m.group("in1");
        节.出口 = m.group("out");
        switch(m.group("op")) {
        case "OR":
            节.op = GOp.OR;
            break;
        case "AND":
            节.op = GOp.AND;
            break;
        default:
            节 = null;
        }
        return 节;
    }

    private static CNode parseShift(final String snode) {
        Matcher m = patShift.matcher(snode);
        if(!m.find()) return null;
        CNode 节 = new CNode();
        节.gval = NAN;
        节.narg = Integer.parseInt(m.group("narg"));
        节.进口0 = m.group("in0");
        节.进口1 = null;
        节.出口 = m.group("out");
        switch(m.group("op")) {
        case "LSHIFT":
            节.op = GOp.LSHIFT;
            break;
        case "RSHIFT":
            节.op = GOp.RSHIFT;
            break;
        default:
            节 = null;
        }
        return 节;
    }

    private static CNode parseNot(final String snode) {
        Matcher m = patNot.matcher(snode);
        if(!m.find()) return null;
        CNode 节 = new CNode();
        节.gval = NAN;
        节.narg = NAN;
        节.进口0 = m.group("in0");
        节.进口1 = null;
        节.出口 = m.group("out");
        节.op = GOp.NOT;
        return 节;
    }

    private static CNode parseSet(final String snode) {
        Matcher m = patSet.matcher(snode);
        if(!m.find()) return null;
        CNode 节 = new CNode();
        节.gval = Integer.parseInt(m.group("narg"));
        节.narg = 节.gval;
        节.进口0 = null;
        节.进口1 = null;
        节.出口 = m.group("out");
        节.op = GOp.SET;
        return 节;
    }
}

/**
 * Gate Operation
 */
enum GOp {
    OR, // (or 进口0 进口1)
    AND, // (and 进口0 进口)
    LSHIFT, RSHIFT, // (shift 进口0 narg)
    NOT, // (not 进口0)
    SET // (setq 出口 narg)
}
