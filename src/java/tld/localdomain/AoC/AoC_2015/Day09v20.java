// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-02 21:42:14 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 9: All in a Single Night
 *
 * Every year, Santa manages to deliver all of his presents in a single night.
 * This year, however, he has some new locations to visit; his elves have provided him the distances between every pair of locations. He can start and end at any two (different) locations he wants, but he must visit each location exactly once. What is the shortest distance he can travel to achieve this?
 * For example, given the following distances:
 *
 * London to Dublin = 464
 * London to Belfast = 518
 * Dublin to Belfast = 141
 *
 * The possible routes are therefore:
 *
 * Dublin -> London -> Belfast = 982
 * London -> Dublin -> Belfast = 605
 * London -> Belfast -> Dublin = 659
 * Dublin -> Belfast -> London = 659
 * Belfast -> Dublin -> London = 605
 * Belfast -> London -> Dublin = 982
 *
 * The shortest of these is London -> Dublin -> Belfast = 605, and so the answer is 605 in this example.
 *
 * What is the distance of the shortest route?
 */
public class Day09v20 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    private static int part1(String[] args) {
        RoadMap roadmap = RoadMap.create(args);
        return -1;
    }

    private static int part2(String[] args) {
        return -1;
    }
}

/**
 * матрица смежности с названиями пунктов назначения
 */
class RoadMap {
    final static Pattern rPat = Pattern.compile("^(.+) to (.+) = (\\d+)$");

    String[] locations;
    int[][] roadmap;

    public static RoadMap create(final String[] args) {
        List<Route> routes = new ArrayList<>();
        Set<String> locNameSet = new HashSet<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\\v");
        for(int cnt = -1; sc.hasNext();) {
            Route r = new Route(sc.next());
            locNameSet.add(r.from);
            locNameSet.add(r.to);
            routes.add(r);
        }
        RoadMap rm = new RoadMap();
        rm.locations = locNameSet.toArray(new String[0]);
        Arrays.sort(rm.locations);
        rm.roadmap = new int[rm.locations.length][];
        for(int i = 0; i < rm.roadmap.length; ++i) {
            rm.roadmap[i] = new int[rm.roadmap.length];
            Arrays.fill(rm.roadmap[i], -1);
            rm.roadmap[i][i] = 0;
        }
        for(Route r : routes) {
            int i = Arrays.binarySearch(rm.locations, r.from),
                    j = Arrays.binarySearch(rm.locations, r.from);
            rm.roadmap[i][j] = r.dist;
            rm.roadmap[j][i] = r.dist;
        }
        return null;
    }
}

class Route {
    final static Pattern rPat = Pattern.compile("^(.+) to (.+) = (\\d+)$");
    final String from, to;
    final int dist;

    Route(String from, String to, int dist) {
        this.from = from;
        this.to = to;
        this.dist = dist;
    }

    Route(String src) {
        Matcher m = rPat.matcher(src);
        m.find();
        from = m.group(1);
        to = m.group(2);
        dist = Integer.parseInt(m.group(3));
    }
}