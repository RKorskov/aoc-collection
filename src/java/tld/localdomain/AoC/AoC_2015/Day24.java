package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 24: It Hangs in the Balance
 *
 * Santa has provided you a list of the weights of every package he
 * needs to fit on the sleigh. The packages need to be split into
 * three groups of exactly the same weight, and every package has to
 * fit. The first group goes in the passenger compartment of the
 * sleigh, and the second and third go in containers on either
 * side. Only when all three groups weigh exactly the same amount will
 * the sleigh be able to fly. Defying physics has rules, you know!
 *
 * Of course, that's not the only problem. The first group - the one
 * going in the passenger compartment - needs as few packages as
 * possible so that Santa has some legroom left over. It doesn't
 * matter how many packages are in either of the other two groups, so
 * long as all of the groups weigh the same.
 *
 * Furthermore, Santa tells you, if there are multiple ways to arrange
 * the packages such that the fewest possible are in the first group,
 * you need to choose the way where the first group has the smallest
 * quantum entanglement to reduce the chance of any
 * "complications". The quantum entanglement of a group of packages is
 * the product of their weights, that is, the value you get when you
 * multiply their weights together. Only consider quantum entanglement
 * if the first group has the fewest possible number of packages in it
 * and all groups weigh the same amount.
 *
 * What is the quantum entanglement of the first group of packages in
 * the ideal configuration?
 */
public class Day24 {
    public static void main(final String[] args) {
        List<GPack> gifts = readParseInput(args);
        System.out.println(part1(gifts));
        System.out.println(part2(gifts));
    }

    private static List<GPack> readParseInput(final String[] args) {
        List<GPack> gifts = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        for(;sc.hasNextInt();) {
            final int n = sc.nextInt();
            gifts.add(new GPack(n));
        }
        /*
        GPack[] ri = new int[gifts.size()];
        for(int i = 0; i < gifts.size(); ++i)
            ri[i] = new GPack(gifts.get(i));
        return ri;
        */
        return gifts;
    }

    /**
     * What is the quantum entanglement of the first group of packages
     * in the ideal configuration?
     */
    private static int part1(final List<GPack> gifts) {
        final int sum3 = gifts.stream().mapToInt(g->g.weight).sum() / 3;
        Deque<GPack> head = new ArrayDeque<>(),
            left = new ArrayDeque<>(), right = new ArrayDeque<>();
        Deque<GPack> unused = new ArrayDeque<>(gifts),
            discard = new ArrayDeque<>();
        collect(unused, head, sum3);
        return sum3;
    }

    /**
     * attemps to collect gpile with exact grossWeight from unused,
     * placing processed elements to discard (for later re-use, if
     * any)
     */
    private static Deque<GPack> collect(Deque<GPack> unused,
                                        // Deque<GPack> discard,
                                        Deque<GPack> gpile,
                                        final int grossWeight) {
        if(unused == null || unused.isEmpty()) return;
        Deque<GPack> discard = new ArrayDeque<>();
        for(;!unused.isEmpty();) {
            GPack gp = unused.pop();
            gpile.addLast(gp);
            final int weight = gpile.stream().mapToInt(g->g.weight).sum();
            if(weight == grossWeight) {
                // return discard;
                break;
            }
            if(weight < grossWeight)
                collect(unused, gpile, grossWeight);
            if(weight > grossWeight) {
                GPack dp = gpile.removeLast();
                discard.add();
            }
        }
        return discard;
    }

    /**
     */
    private static int part2(final List<GPack> gifts) {
        final int sum3 = gifts.stream().mapToInt(g->g.weight).sum() / 3;
        return sum3;
    }
}

class GPack {
    final int weight, gpid;
    GPack(int weight) {
        this.weight = weight;
        this.gpid = GPack.getNewID();
    }

    public static int getNewID() {
        return idgen.getID();
    }

    private static __IDGen idgen = new __IDGen();
    private static class __IDGen {
        private int id_counter = 0; // long is overkill for such small task
        public int getID() { return ++id_counter; }
    }
}
