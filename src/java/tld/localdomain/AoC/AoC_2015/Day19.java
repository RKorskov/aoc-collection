package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 19: Medicine for Rudolph
 *
 * However, the machine has to be calibrated before it can be
 * used. Calibration involves determining the number of molecules that
 * can be generated in one step from a given starting point.
 *
 * The machine replaces without regard for the surrounding
 * characters. For example, given the string H2O, the transition H =>
 * OO would result in OO2O.
 *
 * Your puzzle input describes all of the possible replacements and,
 * at the bottom, the medicine molecule for which you need to
 * calibrate the machine. How many distinct molecules can be created
 * after all the different ways you can do one replacement on the
 * medicine molecule?
 */
public class Day19 {
    public static void main(final String[] args) {
        List<String> 格 = readParseInput(args);
        List<Rule> rules = new ArrayList<>();
        String src = null;
        for(String s : 格) {
            Rule rule = Rule.create(s);
            if(rule != null)
                rules.add(rule);
            else
                src = s;
        }
        System.out.println(part1(src, rules));
        System.out.println(part2(src, rules));
    }

    /**
     * Al => ThRnFAr
     * B => BCa
     *
     * CRnCaCaCaSiRnBPTiMgArSi...W
     */
    private static List<String> readParseInput(final String[] args) {
        List<String> grid = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src != null && src.length() > 0)
                grid.add(src);
        }
        return grid;
    }

    /**
     * How many distinct molecules can be created after all the
     * different ways you can do one replacement on the medicine
     * molecule?
     */
    private static int part1(final String src, final List<Rule> ruleset) {
        Set<String> rps = new HashSet<>();
        for(Rule rule : ruleset) {
            for(Matcher m = Pattern.compile(rule.src).matcher(src);
                m.find();) {
                String dst = src.substring(0, m.start()) //
                    + rule.dst //
                    + src.substring(m.end());
                rps.add(dst);
            }
        }
        return rps.size();
    }

    /**
     * Molecule fabrication always begins with just a single electron,
     * e, and applying replacements one at a time, just like the ones
     * during calibration.
     *
     * How long will it take to make the medicine? Given the available
     * replacements and the medicine molecule in your puzzle input,
     * what is the fewest number of steps to go from e to the medicine
     * molecule?
     */
    private static int part2(final String src, final List<Rule> ruleset) {
        int ras = -1;
        return ras;
    }
}

class Rule {
    final String src, dst;

    //private Rule() {;}
    private Rule(String src, String dst) {
        this.src = src;
        this.dst = dst;
    }

    public String toString() {
        return String.format("%s => %s", src, dst);
    }

    private final static Pattern pat = Pattern.compile("^(\\w+) => (\\w+)$");

    public static Rule create(final String src) {
        Matcher m = pat.matcher(src);
        if(m.find())
            return new Rule(m.group(1), m.group(2));
        return null; // exception?
    }
}
