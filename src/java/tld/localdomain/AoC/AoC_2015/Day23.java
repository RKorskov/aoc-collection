package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Day 23: Opening the Turing Lock
 *
 * The registers are named a and b, can hold any non-negative integer,
 * and begin with a value of 0. The instructions are as follows:
 *
 * - `hlf r` sets register r to half its current value, then continues
 *   with the next instruction.
 * - `tpl r` sets register r to triple its current value, then continues
 *   with the next instruction.
 * - `inc r` increments register r, adding 1 to it, then continues with
 *   the next instruction.
 * - `jmp offset` is a jump; it continues with the instruction offset
 *   away relative to itself.
 * - `jie r, offset` is like jmp, but only jumps if register r is even
 *   ("jump if even").
 * - `jio r, offset` is like jmp, but only jumps if register r is 1
 *   ("jump if one", not odd).
 *
 * All three jump instructions work with an offset relative to that
 * instruction. The offset is always written with a prefix + or - to
 * indicate the direction of the jump (forward or backward,
 * respectively).
 *
 * The program exits when it tries to run an instruction beyond the
 * ones defined.
 *
 * What is the value in register b when the program in your puzzle
 * input is finished executing?
 */
public class Day23 {
    public static void main(final String[] args) {
        Cmd[] prog = readParseData(args);
        System.out.println(part1(prog));
        System.out.println(part2(prog));
    }

    private static Cmd[] readParseData(final String[] args) {
        List<Cmd> prog = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            Cmd cmd = Cmd.parse(src);
            if(cmd != null) prog.add(cmd);
        }
        return prog.toArray(new Cmd[0]);
    }

    /**
     * What is the value in register b when the program in your puzzle
     * input is finished executing?
     */
    private static int part1(final Cmd[] prog) {
        SoC2015 soc = new SoC2015(prog);
        soc.eval();
        return soc.regs.b;
    }

    /**
     * what is the value in register b after the program is finished
     * executing if register a starts as 1 instead?
     */
    private static int part2(final Cmd[] prog) {
        SoC2015 soc = new SoC2015(prog);
        soc.regs.a = 1; // eek!
        soc.eval();
        return soc.regs.b;
    }
}

/*
enum CmdSet {
    HLFA, TPLA, INCA, JIEA, JIOA,
    HLFB, TPLB, INCB, JIEB, JIOB,
    JMP
}
*/
enum CmdSet {
    HLF, TPL, INC, JIE, JIO,
    JMP
}

class RegFile {
    int a, b;
    int ip;

    RegFile() {
        a = 0;
        b = 0;
        ip = 0;
    }

    public String toString() {
        return String.format("r0 %d  r1 %d  ip %d", a, b, ip);
    }
}

class SoC2015 {
    RegFile regs;
    Cmd[] prog;
    CmdEvalHandler cmdEvalHandler;

    SoC2015(Cmd[] prog) {
        regs = new RegFile();
        cmdEvalHandler = new CmdHandlerHLF();
        cmdEvalHandler.addHandler(new CmdHandlerTPL());
        cmdEvalHandler.addHandler(new CmdHandlerINC());
        cmdEvalHandler.addHandler(new CmdHandlerJIE());
        cmdEvalHandler.addHandler(new CmdHandlerJIO());
        cmdEvalHandler.addHandler(new CmdHandlerJMP());
        this.prog = prog;
    }

    public int eval() {
        for(regs.ip = 0;
            regs.ip > -1 && regs.ip < prog.length;
            ++regs.ip) {
            // System.err.printf("%d : %s ; %s\n", regs.ip, prog[regs.ip], regs);
            cmdEvalHandler.eval(regs, prog[regs.ip]);
        }
        // System.err.println(regs);
        return regs.b;
    }
}

class Cmd {
    final CmdSet cmd;
    final int arg0, arg1;

    private Cmd(CmdSet cmd, int arg0, int arg1) {
        this.cmd = cmd;
        this.arg0 = arg0;
        this.arg1 = arg1;
    }

    private Cmd(CmdSet cmd, int arg0) {
        this(cmd, arg0, -1);
    }

    public static Cmd parse(final String src) {
        String[] sch = src.trim().split(",?\\s+");
        Cmd cmd = null;
        String arg = sch[1].toUpperCase();
        switch(sch[0].toUpperCase()) {
        case "HLF":
            cmd = new Cmd(CmdSet.HLF, arg.equals("A") ? 0 : 1);
            break;
        case "TPL":
            cmd = new Cmd(CmdSet.TPL, arg.equals("A") ? 0 : 1);
            break;
        case "INC":
            cmd = new Cmd(CmdSet.INC, arg.equals("A") ? 0 : 1);
            break;
        case "JMP":
            cmd = new Cmd(CmdSet.JMP, Integer.parseInt(sch[1]));
            break;
        case "JIE":
            cmd = new Cmd(CmdSet.JIE, arg.equals("A") ? 0 : 1, Integer.parseInt(sch[2]));
            break;
        case "JIO":
            cmd = new Cmd(CmdSet.JIO, arg.equals("A") ? 0 : 1, Integer.parseInt(sch[2]));
            break;
        default:
            //return null;
            throw new NotImplementedException(String.format("%s", src));
        }
        return cmd;
    }

    public String toString() {
        if(cmd == CmdSet.JIE || cmd == CmdSet.JIO)
            return String.format("%s r%d %+d", cmd, arg0, arg1);
        if(cmd == CmdSet.JMP)
            return String.format("%s %+d", cmd, arg0);
        return String.format("%s r%d", cmd, arg0);
    }
}

abstract class CmdEvalHandler {
    CmdEvalHandler next;

    CmdEvalHandler() { next = null; }
    CmdEvalHandler(CmdEvalHandler next) { this.next = next; }

    public void addHandler(final CmdEvalHandler nextCmdEvalHandler) {
        if(next == null)
            next = nextCmdEvalHandler;
        else
            next.addHandler(nextCmdEvalHandler);
    }

    public int eval(RegFile regs, final Cmd cmd) {
        if(next != null)
            next.eval(regs, cmd);
        throw new NotImplementedException(String.format("%d", regs.ip));
    }
}

/**
 * `hlf r` sets register r to half its current value, then continues
 * with the next instruction.
 */
class CmdHandlerHLF extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.HLF) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        if(cmd.arg0 == 0)
            regs.a /= 2;
        else
            regs.b /= 2;
        return 0;
    }
}

/**
 * `tpl r` sets register r to triple its current value, then continues
 * with the next instruction.
 */
class CmdHandlerTPL extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.TPL) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        if(cmd.arg0 == 0)
            regs.a *= 3;
        else
            regs.b *= 3;
        return 0;
    }
}

/**
 * `inc r` increments register r, adding 1 to it, then continues with
 * the next instruction.
 */
class CmdHandlerINC extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.INC) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        if(cmd.arg0 == 0)
            ++regs.a;
        else
            ++regs.b;
        return 0;
    }
}

/**
 * `jmp offset` is a jump; it continues with the instruction offset
 * away relative to itself.
 */
class CmdHandlerJMP extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.JMP) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        regs.ip += cmd.arg0 - 1;
        return cmd.arg0;
    }
}

/**
 * `jie r, offset` is like jmp, but only jumps if register r is even
 * ("jump if even").
 */
class CmdHandlerJIE extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.JIE) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        int rval = cmd.arg0 == 0 ? regs.a : regs.b;
        if((rval & 1) == 0)
            regs.ip += cmd.arg1 - 1;
        return cmd.arg1;
    }
}

/**
 * `jio r, offset` is like jmp, but only jumps if register r is 1
 * ("jump if one", not odd).
 */
class CmdHandlerJIO extends CmdEvalHandler {
    public int eval(RegFile regs, final Cmd cmd) {
        if(cmd.cmd != CmdSet.JIO) {
            if(next == null)
                throw new NotImplementedException(String.format("%d : %s", regs.ip, cmd));
            else
                return next.eval(regs, cmd);
        }
        int rval = cmd.arg0 == 0 ? regs.a : regs.b;
        if(rval == 1)
            regs.ip += cmd.arg1 - 1;
        return cmd.arg1;
    }
}
