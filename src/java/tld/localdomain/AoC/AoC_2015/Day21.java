package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Day 21: RPG Simulator 20XX
 *
 * In this game, the player (you) and the enemy (the boss) take turns
 * attacking. The player always goes first. Each attack reduces the
 * opponent's hit points by at least 1. The first character at or
 * below 0 hit points loses.
 *
 * Damage dealt by an attacker each turn is equal to the attacker's
 * damage score minus the defender's armor score. An attacker always
 * does at least 1 damage.
 *
 * Your damage score and armor score both start at zero. They can be
 * increased by buying items in exchange for gold. You start with no
 * items and have as much gold as you need. Your total damage or armor
 * is equal to the sum of those stats from all of your items. You have
 * 100 hit points.
 *
 * Here is what the item shop is selling:
 *
 * Weapons:    Cost  Damage  Armor
 * Dagger        8     4       0
 * Shortsword   10     5       0
 * Warhammer    25     6       0
 * Longsword    40     7       0
 * Greataxe     74     8       0
 *
 * Armor:      Cost  Damage  Armor
 * Leather      13     0       1
 * Chainmail    31     0       2
 * Splintmail   53     0       3
 * Bandedmail   75     0       4
 * Platemail   102     0       5
 *
 * Rings:      Cost  Damage  Armor
 * Damage +1    25     1       0
 * Damage +2    50     2       0
 * Damage +3   100     3       0
 * Defense +1   20     0       1
 * Defense +2   40     0       2
 * Defense +3   80     0       3
 *
 * You must buy exactly one weapon; no dual-wielding. Armor is
 * optional, but you can't use more than one. You can buy 0-2 rings
 * (at most one for each hand). You must use any items you buy. The
 * shop only has one of each item, so you can't buy, for example, two
 * rings of Damage +3.
 *
 * You have 100 hit points. The boss's actual stats are in your puzzle
 * input. What is the least amount of gold you can spend and still win
 * the fight?
 */
public class Day21 {
    public static void main(final String[] args) {
        Actor[] actors = readParseData(args);
        System.out.println(part1(actors));
        System.out.println(part2(actors)); // 282-
    }

    /**
     * @return Actor[] {pc, boss}
     */
    private static Actor[] readParseData(final String[] args) {
        if(args.length < 1)
            return new Actor[] {new Actor(100, -1, -1), new Actor(103, 9, 2)};
        Actor boss = new Actor(Integer.parseInt(args[0]),
                               Integer.parseInt(args[1]),
                               Integer.parseInt(args[2]));
        if(args.length == 3) {
            return new Actor[] {new Actor(100, -1, -1), boss};
        }
        if(args.length == 4) {
            return new Actor[] {new Actor(Integer.parseInt(args[3]), -1, -1),
                boss};
        }
        return new Actor[] {new Actor(Integer.parseInt(args[3]),
                          Integer.parseInt(args[4]),
                          Integer.parseInt(args[5])),
                boss};
    }

    private static int BOSS_IDX = 1, PC_IDX = 0;

    private static Item[]
        WEAPONS = {
            new Item("-", 0,0,0),
            new Item("dagger",  8, 4, 0),
            new Item("ssword", 10, 5, 0),
            new Item("hammer", 25, 6, 0),
            new Item("lsword", 40, 7, 0),
            new Item("gaxe",   74, 8, 0),
        },
        ARMORS = {
            new Item("-", 0,0,0),
            new Item("leather", 13, 0, 1),
            new Item("chain",   31, 0, 2),
            new Item("splint",  53, 0, 3),
            new Item("banded",  75, 0, 4),
            new Item("plate",  102, 0, 5),
        },
        RINGS = {
            new Item("-", 0,0,0),
            new Item("dmg1", 25, 1, 0),
            new Item("dmg2", 50, 2, 0),
            new Item("dmg3",100, 3, 0),
            new Item("ac1",  20, 0, 1),
            new Item("ac2",  40, 0, 2),
            new Item("ac3",  80, 0, 3),
        };

    /**
     * bHP : 103
     * bDmg: 9
     * bAC : 2
     *
     * bHP_i -= pDmg - bAC;
     * pHP_i -= bDmg - pAC;
     */
    private static int part1(final Actor[] actors) {
        int bestSet = Integer.MAX_VALUE;
        int n = 0;
        for(int wpnIdx = 0; wpnIdx < WEAPONS.length; ++wpnIdx) {
            for(int acIdx = 0; acIdx < ARMORS.length; ++acIdx) {
                for(int rrIdx = 0; rrIdx < RINGS.length; ++rrIdx) {
                    for(int rlIdx = 0; rlIdx < RINGS.length; ++rlIdx) {
                        if(rrIdx == rlIdx && rlIdx > 0) continue;
                        Actor boss = Actor.copy(actors[BOSS_IDX]);
                        final int ac = ARMORS[acIdx].ac
                            + RINGS[rrIdx].ac
                            + RINGS[rlIdx].ac;
                        final int dmg = WEAPONS[wpnIdx].dmg
                            + RINGS[rrIdx].dmg
                            + RINGS[rlIdx].dmg;
                        Actor pc = new Actor(actors[PC_IDX].hp, dmg, ac);
                        if(simulate(pc, boss) == false) continue;
                        int cost = WEAPONS[wpnIdx].cost
                            + ARMORS[acIdx].cost
                            + RINGS[rrIdx].cost
                            + RINGS[rlIdx].cost;
                        if(cost < bestSet)
                            bestSet = cost;
                    }
                }
            }
        }
        return bestSet;
    }

    /**
     * simulate fight with boss,
     * @return true if player win.
     *
     * The player (you) and the enemy (the boss) take turns
     * attacking. The player always goes first. Each attack reduces
     * the opponent's hit points by at least 1. The first character at
     * or below 0 hit points loses.
     */
    private static boolean simulate(final Actor pc, final Actor boss) {
        int n = 0;
        for(boolean turnPC = true; pc.hp > 0 && boss.hp > 0; turnPC = !turnPC) {
            if(turnPC)
                boss.hp -= Math.max(pc.dmg - boss.ac, 1);
            else
                pc.hp -= Math.max(boss.dmg - pc.ac, 1);
            //System.out.printf("%d: %d - %d\n", ++n, pc.hp, boss.hp);
        }
        return pc.hp > 0;
    }

    /**
     * What is the most amount of gold you can spend and still lose the fight?
     */
    private static int part2(final Actor[] actors) {
        int bestSet = Integer.MIN_VALUE;
        int n = 0;
        for(int wpnIdx = 0; wpnIdx < WEAPONS.length; ++wpnIdx) {
            for(int acIdx = 0; acIdx < ARMORS.length; ++acIdx) {
                for(int rrIdx = 0; rrIdx < RINGS.length; ++rrIdx) {
                    for(int rlIdx = 0; rlIdx < RINGS.length; ++rlIdx) {
                        if(rrIdx == rlIdx && rrIdx > 0) continue;
                        Actor boss = Actor.copy(actors[BOSS_IDX]);
                        final int ac = ARMORS[acIdx].ac
                            + RINGS[rrIdx].ac
                            + RINGS[rlIdx].ac;
                        final int dmg = WEAPONS[wpnIdx].dmg
                            + RINGS[rrIdx].dmg
                            + RINGS[rlIdx].dmg;
                        Actor pc = new Actor(actors[PC_IDX].hp, dmg, ac);
                        int cost = WEAPONS[wpnIdx].cost
                            + ARMORS[acIdx].cost
                            + RINGS[rrIdx].cost
                            + RINGS[rlIdx].cost;
                        if(simulate(pc, boss) == true) continue;
                        if(cost > bestSet)
                            bestSet = cost;
                    }
                }
            }
        }
        return bestSet;
    }
}

class Item {
    final int cost, ac, dmg;
    final String name;

    Item(String name, int cost, int dmg, int ac) {
        this.name = name;
        this.cost = cost;
        this.ac = ac;
        this.dmg = dmg;
    }
}
