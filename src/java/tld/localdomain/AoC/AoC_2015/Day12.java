package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 12: JSAbacusFramework.io #
 *
 * They have a JSON document which contains a variety of things:
 * arrays ([1,2,3]), objects ({"a":1, "b":2}), numbers, and
 * strings. Your first job is to simply find all of the numbers
 * throughout the document and add them together.
 *
 * - `[1,2,3]` and `{"a":2,"b":4}` both have a sum of 6.
 * - `[[[3]]]` and `{"a":{"b":4},"c":-1}` both have a sum of 3.
 * - `{"a":[-1,1]}` and `[-1,{"a":1}]` both have a sum of 0.
 * - `[]` and `{}` both have a sum of 0.
 *
 * You will not encounter any strings containing numbers.
 *
 * What is the sum of all numbers in the document?
 */
public class Day12 {
    public static void main(final String[] args) {
        JSAF jsaf = readParseData(args);
        System.out.println(part1(jsaf));
        System.out.println(part2(jsaf));
    }

    private static JSAF readParseData(final String[] args) {
        List<String> raw = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next().trim();
            if(src != null && src.length() > 0)
                raw.add(src);
        }
        JSAF rj = new JSAF();
        rj.raw = raw.toArray(new String[0]);
        return rj;
    }

    final static Pattern pNum = Pattern.compile("([+-]?\\d+)");

    /**
     * What is the sum of all numbers in the document?
     */
    private static int part1(final JSAF jsaf) {
        return sumOf(jsaf.raw[0]);
    }

    private static int sumOf(final String str) {
        Matcher m = pNum.matcher(str);
        int rsum = 0;
        for(rsum = 0; m.find();)
            rsum += Integer.parseInt(m.group(1));
        return rsum;
    }

    /**
     * Ignore any object (and all of its children) which has any
     * property with the value "red". Do this only for objects
     * ({...}), not arrays ([...]).
     *
     * - [1,2,3] still has a sum of 6.
     * - [1,{"c":"red","b":2},3] now has a sum of 4, because the
     *   middle object is ignored.
     * - {"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because
     *   the entire structure is ignored.
     * - [1,"red",5] has a sum of 6, because "red" in an array has no
     *   effect.
     */
    private static int part2(final JSAF jsaf) {
        JObj jo = jsaf.parse();
        return jo.sumWhite();
    }

    private static int sumOffRed(final String str,
                                 final int countFrom, final int countTo) {
        if((countTo - countFrom) < 2) return 0; // skip empty?
        int sum = 0;
        for(int cur = 0, pos = 0; cur < str.length(); ++cur) {
            if(str.charAt(cur) == '{') {
                int joHead = cur;
                int joTail = findJObjEnd(str, cur);
                cur = joTail;
                sum += sumOffRed(str, joHead, joTail);
            }
        }
        return sum;
    }

    /**
     * searches end of json-object.
     * str.charAt(startFrom) == '{'
     *
     * @return closing bracket position
     */
    private static int findJObjEnd(final String str, final int startFrom) {
        for(int cnt = 0, cur = startFrom; cur < str.length(); ++cur) {
            if(str.charAt(cur) == '{')
                ++cnt;
            if(str.charAt(cur) == '}') {
                --cnt;
                if(cnt < 1)
                    return cur;
            }
        }
        return -1;
    }
}

/**
 * They have a JSON document which contains a variety of things:
 * arrays ([1,2,3]), objects ({"a":1, "b":2}), numbers, and
 * strings.
 *
 * For example:
 *
 * - `[1,2,3]` and `{"a":2,"b":4}` both have a sum of 6.
 * - `[[[3]]]` and `{"a":{"b":4},"c":-1}` both have a sum of 3.
 * - `{"a":[-1,1]}` and `[-1,{"a":1}]` both have a sum of 0.
 * - `[]` and `{}` both have a sum of 0.
 *
 * You will not encounter any strings containing numbers.
 */
class JSAF {
    String[] raw;

    public JObj parse() {
        StringBuilder sb = new StringBuilder();
        for(String s : raw)
            sb.append(s);
        String src = sb.toString();
        return JObj.parse(src);
    }
}

class JObj {
    String raw, jstr;
    List<JObj> childs;

    JObj() {
        raw = null;
        childs = null;
        jstr = null;
    }

    JObj(String jsrc) {
        raw = jsrc;
        childs = null;
        jstr = null;
    }

    public String toString() {
        if(jstr != null)
            return jstr;
        return raw;
    }

    private static final Pattern pNum = Pattern.compile("([+-]?\\d+)");

    public int sumWhite() {
        if(jstr.matches(".*:\"red\".*"))
            return 0;
        Matcher m = pNum.matcher(jstr);
        int rsum = 0;
        for(rsum = 0; m.find();)
            rsum += Integer.parseInt(m.group(1));
        if(childs != null)
        for(JObj kid : childs)
            rsum += kid.sumWhite();
        return rsum;
    }

    public static JObj parse(final String jsrc) {
        // todo
        //System.err.printf("parsing '%s'\n", jsrc);
        JObj rjo = new JObj(jsrc);
        StringBuilder sb = new StringBuilder();
        for(int i = 0, bc = 0, cp = 0; i < jsrc.length(); ++i) {
            char c = jsrc.charAt(i);
            if(c == '{') {
                ++bc;
                // if(bc == 1) ...; // start of top-level json object
                // if(bc > 2) ... ; // child object
                if(bc == 2) {
                    sb.append(jsrc.substring(cp, i));
                    cp = i;
                }
            }
            if(c == '}') {
                --bc;
                if(bc < 1) { // end of top-level json object
                    sb.append(jsrc.substring(cp, i+1));
                    break;
                }
                if(bc == 1) { // end of level-1 child object
                    // end of this jobject, return
                    //JObj child = new JObj(jsrc.substring(cp, i+1));
                    JObj child = JObj.parse(jsrc.substring(cp, i+1));
                    if(child != null) {
                        if(rjo.childs == null)
                            rjo.childs = new ArrayList<>();
                        rjo.childs.add(child);
                        cp = i+1;
                    }
                }
            }
        }
        rjo.jstr = sb.toString();
        return rjo;
    }

    /**
     * searches end of json-object.
     * str.charAt(startFrom) == '{'
     *
     * @return closing bracket position
     */
    private static int findJObjEnd(final String str, final int startFrom) {
        for(int cnt = 0, cur = startFrom; cur < str.length(); ++cur) {
            if(str.charAt(cur) == '{')
                ++cnt;
            if(str.charAt(cur) == '}') {
                --cnt;
                if(cnt < 1)
                    return cur;
            }
        }
        return -1;
    }
}
