// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-12-02 21:42:14 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day09 {

    public static void main(String[] args) throws Exception {
        List <String> als = readData();
        // Map <String,D9Site> msr = parse2map(als);
        // Set <D9Route> sr = parse2set(als);
        D9Matrix msr = D9Matrix.createMatrix(als); // матрица смежности
        System.out.println(sExecutor(msr));
        System.out.println(sExecutor2(msr));
    }

    /*
     * Tristram
     * AlphaCentauri
     * Snowdin
     * Tambi
     * Faerun
     * Norrath
     * Straylight
     * Arbre
     */

    /**
     * Dijkstra's algorithm. sorta of.
     * en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     */
    private static int sExecutor0(final D9Matrix d9matr) {
        int 好好 = 0;
        //System.out.println(d9matr);
        //for(D9Site 邑 : d9sites.values()) {System.out.println(邑);}
        // Set <Integer> 队列 = new HashSet <Integer> ();
        Deque <Integer> 队列 = new ArrayDeque <Integer> (),
            大路 = new ArrayDeque <Integer> (),
            还地 = new ArrayDeque <Integer> ();
        for(int 甲 = 0; 甲 < d9matr.名子.length; ++甲) 队列.push(甲);
        for(;!队列.isEmpty();) {
            int 步 = 队列.pop();
            if(大路.isEmpty()) {
                大路.add(步);
                continue;
            }
            if(大路.contains(步)) { // wtf?
                还地.add(步);
                continue;
            }
            int 长 = d9matr.getDistance(大路.peek(), 步);
            if(长 < 1) {
                队列.addLast(步);
                continue;
            }
            大路.add(步);
        }
        return 好好;
    }

    private static int sExecutor2(final D9Matrix d9sites) {
        int 好好 = 0;
        return 好好;
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        List <String> als = new ArrayList <String> ();
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }

    private static Pattern ROUTE = Pattern.compile("(?<from>[a-zA-Z]+) to (?<to>[a-zA-Z]+) = (?<dist>[0-9]+)");

    private static Set <D9Route> parse2set(final List <String> dists) {
        Set <D9Route> 路网 = new HashSet <D9Route> ();
        for(String 串 : dists) {
            D9Route 路 = D9Route.createRoute(串);
            if(路 != null)
                路网.add(路);
        }
        return 路网;
    }

    private static Map <String,D9Site> parse2map(final List <String> dists) {
        Map <String,D9Site> d9s = new HashMap <String,D9Site> ();
        for(String 串 : dists) {
            Matcher m = ROUTE.matcher(串);
            if(!m.find()) continue;
            String 从 = m.group("from"),
                向 = m.group("to");
            int 长 = Integer.parseInt(m.group("dist"));
            if(d9s.containsKey(从)) {
                d9s.get(从).add(向, 长);
            }
            else {
                D9Site 邑 = D9Site.createSite(从, 向, 长);
                if(邑 != null)
                    d9s.put(从, 邑);
            }
        }
        return d9s;
    }

    private static int sExecutor(final D9Matrix d9matr) {
        int[] 速路 = null;
        int 速时 = Integer.MAX_VALUE;
        for(int 首 = 0; 首 < d9matr.名子.length; ++首) {
            int[] 路 = new int[d9matr.名子.length];
            Arrays.fill(路, -1);
            路[0] = 首;
            int 甲 = getWay(d9matr, );
        }
        return -1;
    }

    private static int getWay(final D9Matrix d9matr,
                              final Deque <Integer> track) {
        if(track.size() >= d9matr.size())
            return evalTrackDist(d9matr, track);
        Deque <Integer> 速路 = null;
        int 速时 = Integer.MAX_VALUE, 好号 = -1;
        for(int 首 = 0; 首 < d9matr.size(); ++首) {
            if(track.contains(首)) continue;
            Deque <Integer> 新路 = track.clone();
            新路.addLast(首);
            int 长 = getWay(d9matr, 新路);
            if(长 > 0 && 长 < 速时) {
                好号 = 首;
                速时 = 长;
                速路 = 新路;
            }
        }
        if(好号 > 0) {
            // track.addLast(好号);
        }
        else
            return -1; // WTF?
        return getWay(d9matr, track);
    }

    private static int evalTrackDist(final D9Matrix d9matr, final Deque<Integer> track) {
        // todo and so on
        return -1;
    }
}

class D9Site {
    String 名子;
    Set <D9Route> 目地;
    private D9Site(){;}

    /**
     * add route to set
     */
    public void add(final D9Route d9r) {
        目地.add(d9r);
    }

    /**
     * add route to set
     */
    public void add(final String dest, final int dist) {
        D9Route 路 = D9Route.createRoute(名子, dest, dist);
        if(路 != null)
            目地.add(路);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(名子);
        sb.append(" : {");
        boolean 甲 = false;
        for(D9Route 路 : 目地) {
            if(甲)
                sb.append(", ");
            else
                甲 = true;
            sb.append(路.向);
            sb.append(":");
            sb.append(String.valueOf(路.长));
        }
        sb.append('}');
        return sb.toString();
    }

    /**
     * empty site
     */
    public static D9Site createSite(final String name) {
        D9Site 邑 = new D9Site();
        邑.名子 = name;
        邑.目地 = new HashSet <D9Route> ();
        return 邑;
    }

    /*
     * site from route
     */
    public static D9Site createSite(final String name, final String dest,
                                    final int dist) {
        D9Site 邑 = new D9Site();
        邑.名子 = name;
        邑.目地 = new HashSet <D9Route> ();
        D9Route 路 = D9Route.createRoute(name, dest, dist);
        邑.目地.add(路);
        return 邑;
    }
}

/**
 * bi-directional route
 */
class D9Route {
    String 从, 向;
    int 长;

    private static Pattern DIST = Pattern.compile("(?<from>[a-zA-Z]+) to (?<to>[a-zA-Z]+) = (?<dist>[0-9]+)");

    private D9Route() {;}

    public int length() {return 长;}

    public String toString() {
        return String.format("%s : %s : %d", 从, 向, 长);
    }

    /**
     * Faerun to Arbre = 144
     * Norrath to Straylight = 115
     */
    public static D9Route createRoute(final String str) {
        Matcher m = DIST.matcher(str);
        if(!m.find()) return null;
        D9Route 路 = new D9Route();
        路.从 = m.group("from");
        路.向 = m.group("to");
        路.长 = Integer.parseInt(m.group("dist"));
        return 路;
    }

    public static D9Route createRoute(final String from, final String dest,
                                      final int dist) {
        D9Route 路 = new D9Route();
        路.从 = from;
        路.向 = dest;
        路.长 = dist;
        return 路;
    }
}

class D9Matrix {
    String[] 名子;
    int[][] 路网;

    private static Pattern ROUTE = Pattern.compile("(?<from>[a-zA-Z]+) to (?<to>[a-zA-Z]+) = (?<dist>[0-9]+)");
    private D9Matrix(){;}

    public int getDistance(final int src, final int dst) {
        return 路网[src][dst];
    }

    public int size() { return 名子.length; }

    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        boolean 甲 = false;
        for(String 串 : 名子) {
            if(甲)
                sb.append(", ");
            else {
                甲 = true;
                sb.append(' ');
            }
            sb.append(串);
        }
        if(名子.length > 0)
            sb.append(' ');
        sb.append(']');
        for(int 行 = 0; 行 < 路网.length; ++行) {
            sb.append('\n');
            sb.append(行);
            sb.append(": [");
            甲 = false;
            for(int 列 = 0; 列 < 路网[行].length; ++列) {
                if(甲)
                    sb.append(" ");
                else
                    甲 = true;
                sb.append(路网[行][列]);
                sb.append(" ");
            }
            sb.append(']');
        }
        return sb.toString();
    }

    public static D9Matrix createMatrix(final List <String> data) {
        Set <String> 邑 = new HashSet <String> ();
        Set <D9Route> 组网 = new HashSet <D9Route> ();
        D9Matrix 路表 = new D9Matrix();
        for(String 串 : data) {
            D9Route 路 = D9Route.createRoute(串);
            if(路 == null) continue;
            组网.add(路);
            邑.add(路.从);
            邑.add(路.向);
        }
        if(邑.size() < 1) return null;
        路表.名子 = 邑.toArray(new String[0]);
        路表.路网 = new int[路表.名子.length][];
        for(int 甲 = 0; 甲 < 路表.名子.length; ++甲)
            路表.路网[甲] = new int[路表.名子.length];
        for(D9Route 路 : 组网) {
            int 从 = getPlaceId(路表.名子, 路.从),
                向 = getPlaceId(路表.名子, 路.向);
            路表.路网[从][向] = 路.长;
            路表.路网[向][从] = 路.长;
        }
        return 路表;
    }

    private static int getPlaceId(final String[] list, final String str) {
        for(int 甲 = 0; 甲 < list.length; ++甲)
            if(list[甲].equals(str))
                return 甲;
        return -1;
    }
}
