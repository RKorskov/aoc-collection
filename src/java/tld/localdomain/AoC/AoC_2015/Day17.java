package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 17: No Such Thing as Too Much #
 *
 * The elves bought too much eggnog again - 150 liters this time. To
 * fit it all into your refrigerator, you'll need to move it into
 * smaller containers. You take an inventory of the capacities of the
 * available containers.
 *
 * For example, suppose you have containers of size 20, 15, 10, 5, and
 * 5 liters. If you need to store 25 liters, there are four ways to do
 * it:
 *
 * - 15 and 10
 * - 20 and 5 (the first 5)
 * - 20 and 5 (the second 5)
 * - 15, 5, and 5
 *
 * Filling all containers entirely, how many different combinations of
 * containers can exactly fit all 150 liters of eggnog?
 */
public class Day17 {
    public static void main(final String[] args) {
        int[] 容器 = readParseInput(args);
        System.out.println(part1(容器));
        System.out.println(part2(容器));
    }

    private static int[] readParseInput(final String[] args) {
        List<Integer> loi = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        //sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            int p = Integer.parseInt(src);
            //System.err.println(p);
            loi.add(p);
        }
        int[] rli = new int[loi.size()];
        for(int i = 0; i < loi.size(); ++i)
            rli[i] = loi.get(i);
        Arrays.sort(rli);
        return rli;
    }

    final static int VOLUME = 150;

    /**
     */
    private static int part1(final int[] 容器) {
        int ras = -1;
        ras = fill(容器, 0, VOLUME);
        return ras;
    }

    /**
     * While playing with all the containers in the kitchen, another
     * load of eggnog arrives! The shipping and receiving department
     * is requesting as many containers as you can spare.
     *
     * Find the minimum number of containers that can exactly fit all
     * 150 liters of eggnog. How many different ways can you fill that
     * number of containers and still hold exactly 150 litres?
     */
    private static int part2(final int[] aas) {
        int ras = -1;
        return ras;
    }

    private static int fill(final int[] jugs, final int cur, final int vol) {
        if(cur >= jugs.length) return 0;
        int rv = 0;
        for(int i = cur; i < jugs.length; ++i) {
            int rem = vol - jugs[i];
            if(rem == 0)
                ++rv;
            else
                if(rem > 0 && i < (jugs.length-1))
                    rv += fill(jugs, i+1, rem);
        }
        return rv;
    }
}
