// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-29 08:55:12 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.security.MessageDigest;

public class Day04 {

    public static void main (String[] args) throws Exception {
        System.out.printf("零5: %d\n", sExecutor5(args[0]));
        System.out.printf("零6: %d\n", sExecutor6(args[0]));
    }

    private static int sExecutor5 (final String skey) {
        for(int 号 = 0; 号 < Integer.MAX_VALUE; ++号) {
            String 串 = skey + String.valueOf(号);
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.reset();
                byte[] h5 = md.digest(串.getBytes());
                if(h5[0] == 0 && h5[1] == 0 && (h5[2] & 0xF0) == 0)
                    return 号;
            }
            catch (Exception ex) { ex.printStackTrace(); }
        }
        return -1;
    }

    private static int sExecutor6 (final String skey) {
        for(int 号 = 0; 号 < Integer.MAX_VALUE; ++号) {
            String 串 = skey + String.valueOf(号);
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.reset();
                byte[] h5 = md.digest(串.getBytes());
                if(h5[0] == 0 && h5[1] == 0 && h5[2] == 0)
                    return 号;
            }
            catch (Exception ex) { ex.printStackTrace(); }
        }
        return -1;
    }
}
