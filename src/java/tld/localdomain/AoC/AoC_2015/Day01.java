// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-26 20:43:47 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;

public class Day01 {

    public static void main (String[] args) throws Exception {
        sExecutor(args);
    }

    private static void sExecutor (final String[] args) {
        String 字串 = (new Scanner(System.in)).next();
        int 楼层 = 0;
        boolean basement = true;
        for(int 号 = 0; 号 < 字串.length(); ++号) {
            char 字 = 字串.charAt(号);
            switch(字) {
            case '(':
                ++楼层;
                break;
            case ')':
                --楼层;
                if(basement && 楼层 < 0) {
                    basement = false;
                    System.out.printf("basement reached on %d step\n", 号+1);
                }
                break;
            default:
                System.out.printf("Oops! Illegal char '%c' at %d\n", 字, 号);
            }
        }
        System.out.printf("第%d楼层\n", 楼层);
    }
}
