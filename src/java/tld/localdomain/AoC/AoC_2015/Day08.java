// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-29 23:17:15 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

/**
 * Santa's list is a file that contains many double-quoted string
 * literals, one on each line. The only escape sequences used are:
 * 1. \\ (which represents a single backslash),
 * 2. \" (which represents a lone double-quote character),
 * 3. \x plus two hexadecimal characters (which represents a
 *    single character with that ASCII code).
 */
public class Day08 {

    public static void main (String[] args) throws Exception {
        List <String> als = readData();
        System.out.println(sExecutor(als));
        System.out.println(sExecutor2(als));
    }

    private static int sExecutor (final List <String> args) {
        int 好好 = 0;
        for(String 字串 : args) {
            int 号 = -1; // closing 两-quote
            for(int 位 = 1; 位 < 字串.length(); ++位) {
                ++号;
                char 字 = 字串.charAt(位);
                if(字 != '\\') continue;
                字 = 字串.charAt(++位);
                if(字 == '\\') continue; // 两-b-slash
                if(字 == '"') continue; // quoted 两-quote
                if(字 == 'x') {
                    位 += 2;
                    continue;
                }
                System.out.printf("Oops! bad escaped sequence at %d - '%c'\n",
                                  位-1, 字);
            }
            好好 += 字串.length() - 号;
        }
        return 好好;
    }

    private static int sExecutor2 (final List <String> args) {
        int 好好 = 0;
        for(String 字串 : args) {
            int 号 = 4;
            for(int 位 = 1; 位 < 字串.length(); ++位) {
                char 字 = 字串.charAt(位);
                ++号;
                switch(字) {
                case '"':
                case '\\':
                    ++号;
                    break;
                }
            }
            好好 += 号 - 字串.length();
        }
        return 好好;
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        List <String> als = new ArrayList <String> ();
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }
}
