// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-27 07:20:23 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day05 {

    public static void main (String[] args) throws Exception {
        List <String> als = readData();
        System.out.println(sExecutor(als));
        System.out.println(sExecutor2(als));
    }

    private static Pattern VOWELS = //
        Pattern.compile("[aeiou].*[aeiou].*[aeiou]"),
        C2 = Pattern.compile("(.)\\1"),
        BAN2C = Pattern.compile("(ab)|(cd)|(pq)|(xy)");

    private static int sExecutor (final List <String> args) {
        int 好好 = 0;
        for(String 字串 : args) {
            if(VOWELS.matcher(字串).find() && //
               C2.matcher(字串).find() //
               && !BAN2C.matcher(字串).find())
                ++好好;
        }
        return 好好;
    }

    private static Pattern TPAIR = Pattern.compile("(..).*\\1"),
        XYX = Pattern.compile("(.).\\1");

    private static int sExecutor2 (final List <String> args) {
        int 好好 = 0;
        for(String 字串 : args) {
            if(TPAIR.matcher(字串).find() && XYX.matcher(字串).find())
                ++好好;
        }
        return 好好;
    }

    private static List <String> readData() {
        Scanner sc = new Scanner(System.in);
        List <String> als = new ArrayList <String> ();
        for(;sc.hasNext();)
            als.add(sc.next());
        return als;
    }
}
