package tld.localdomain.AoC.AoC_2015;

import java.util.List;

/**
 * Day 21, 22 - actors
 */
class Actor {
    int hp, mp;
    final int dmg, ac;
    List<Effect> effects; // active effects

    Actor(int hp, int dmg, int ac) {
        this.hp = hp;
        this.ac = ac;
        this.dmg = dmg;
        this.mp = 0;
    }

    public static Actor copy(final Actor src) {
        return new Actor(src.hp, src.dmg, src.ac);
    }
}

class Effect {
    int duration;
}
