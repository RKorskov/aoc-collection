// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2019-11-26 23:11:17 roukoru>

package tld.localdomain.AoC.AoC_2015;

import java.util.Scanner;
import java.util.HashSet;
import java.util.Set;

public class Day03 {

    public static void main (String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        final String 字串 = sc.next();
        final char[] dirs = 字串.toCharArray();
        System.out.printf("%d户家\n", sExecutor(dirs));
        System.out.printf("%d户家\n", roboSExecutor(dirs));
    }

    private static int sExecutor (final char[] dirs) {
        Set <D2House> 胡同 = new HashSet <D2House> ();
        胡同.add(D2House.createHouse(0,0));
        int 东 = 0, 南 = 0, 号 = 0;
        for(char c : dirs) {
            switch(c) {
            case '^':
                ++南;
                break;
            case 'v':
                --南;
                break;
            case '<':
                --东;
                break;
            case '>':
                ++东;
                break;
            default:
                System.out.printf("Oops! Illegal char '%c' at %d\n", c, 号);
                continue;
            }
            ++号;
            D2House 家 = D2House.createHouse(东, 南);
            if(!胡同.contains(家))
                胡同.add(家);
        }
        return 胡同.size();
    }

    private static int roboSExecutor (final char[] dirs) {
        Set <D2House> 胡同 = new HashSet <D2House> ();
        胡同.add(D2House.createHouse(0,0));
        D2Drone s2d = D2Drone.createDrone(),
            r2d = D2Drone.createDrone();
        int 号 = 0;
        for(char c : dirs) {
            D2Drone rs2d = (号&1) == 0 ? s2d : r2d;
            if(!rs2d.move(c))
                System.out.printf("Oops! Illegal char '%c' at %d\n", c, 号);
            ++号;
            D2House 家 = D2House.createHouse(rs2d.东, rs2d.南);
            if(!胡同.contains(家))
                胡同.add(家);
        }
        return 胡同.size();
    }

    public static int min(final int a, final int b, final int... rest) {
        int m = Integer.min(a, b);
        for(int i = 0; i < rest.length; ++i)
            m = Integer.min(m, rest[i]);
        return m;
    }
}

class D2House {
    int x, y;
    private D2House(){;}

    public boolean equals(Object obj) {
        if(!(obj instanceof D2House))
            return false;
        D2House d2h = (D2House) obj;
        return d2h.x == x && d2h.y == y;
    }

    public int hashCode() {
        return (x & 0xFFFF) + (y << 16);
    }

    public static D2House createHouse(final int x, final int y) {
        D2House 家 = new D2House();
        家.x = x;
        家.y = y;
        return 家;
    }
}

class D2Drone {
    int 南, 东;
    private D2Drone() {;}

    public boolean move(final char dir) {
        switch(dir) {
        case '^':
            ++南;
            break;
        case 'v':
            --南;
            break;
        case '<':
            --东;
            break;
        case '>':
            ++东;
            break;
        default:
            return false;
        }
        return true;
    }

    public static D2Drone createDrone() {
        D2Drone d2d = new D2Drone();
        d2d.南 = 0;
        d2d.东 = 0;
        return d2d;
    }
}
