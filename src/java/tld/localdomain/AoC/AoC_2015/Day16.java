package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 16: Aunt Sue

 * Your Aunt Sue has given you a wonderful gift, and you'd like to
 * send her a thank you card. However, there's a small problem: she
 * signed it "From, Aunt Sue".
 *
 * You have 500 Aunts named "Sue".
 *
 * So, to avoid sending the card to the wrong person, you need to
 * figure out which Aunt Sue (which you conveniently number 1 to 500,
 * for sanity) gave you the gift. You open the present and, as luck
 * would have it, good ol' Aunt Sue got you a My First Crime Scene
 * Analysis Machine! Just what you wanted. Or needed, as the case may
 * be.
 *
 * The My First Crime Scene Analysis Machine (MFCSAM for short) can
 * detect a few specific compounds in a given sample, as well as how
 * many distinct kinds of those compounds there are. According to the
 * instructions, these are what the MFCSAM can detect:
 *
 * - children, by human DNA age analysis.
 * - cats. It doesn't differentiate individual breeds.
 * - Several seemingly random breeds of dog: samoyeds, pomeranians, akitas, and vizslas.
 * - goldfish. No other kinds of fish.
 * - trees, all in one group.
 * - cars, presumably by exhaust or gasoline or something.
 * - perfumes, which is handy, since many of your Aunts Sue wear a few kinds.
 *
 * In fact, many of your Aunts Sue have many of these. You put the
 * wrapping from the gift into the MFCSAM. It beeps inquisitively at
 * you a few times and then prints out a message on ticker tape:
 *
 * children: 3
 * cats: 7
 * samoyeds: 2
 * pomeranians: 3
 * akitas: 0
 * vizslas: 0
 * goldfish: 5
 * trees: 3
 * cars: 2
 * perfumes: 1
 *
 * You make a list of the things you can remember about each Aunt
 * Sue. Things missing from your list aren't zero - you simply don't
 * remember the value.
 *
 * What is the number of the Sue that got you the gift?
 */
public class Day16 {
    public static void main(final String[] args) {
        AuntSue[] aas = readParseInput(args);
        System.out.println(part1(aas));
        System.out.println(part2(aas));
    }

    private static AuntSue[] readParseInput(final String[] args) {
        List<AuntSue> loas = new ArrayList<>();
        Scanner sc = Utils.getScanner(args);
        sc.useDelimiter("\n");
        for(;sc.hasNext();) {
            String src = sc.next();
            AuntSue p = AuntSue.create(src);
            //System.err.println(p);
            if(p != null)
                loas.add(p);
        }
        if(loas.size() < 4) return null;
        return loas.toArray(new AuntSue[0]);
    }

    /**
     * What is the number of the Sue that got you the gift?
     */
    private static int part1(final AuntSue[] aas) {
        final AuntSue agift = new AuntSue(0, 3, 7, 2, 3, 0, 0, 5, 3, 2, 1);
        int ras = -1;
        for(AuntSue as : aas)
            if(agift.cmp(as))
                ras = as.getId();
                //System.out.println(as.getId());
        return ras;
    }

    /**
     * In particular, the cats and trees readings indicates that there
     * are greater than that many (due to the unpredictable nuclear
     * decay of cat dander and tree pollen), while the pomeranians and
     * goldfish readings indicate that there are fewer than that many
     * (due to the modial interaction of magnetoreluctance).
     */
    private static int part2(final AuntSue[] aas) {
        final AuntSue agift = new AuntSue(0, 3, 7, 2, 3, 0, 0, 5, 3, 2, 1);
        int ras = -1;
        for(AuntSue as : aas) {
            if(agift.cmpRanged(as))
                ras = as.getId();
        }
        return ras;
    }

}

/**
 * Note: this class has a natural ordering that is inconsistent with equals.
 *
 * These are what the MFCSAM can detect:
 *
 * - children, by human DNA age analysis.
 * - cats. It doesn't differentiate individual breeds.
 * - Several seemingly random breeds of dog: samoyeds, pomeranians, akitas, and vizslas.
 * - goldfish. No other kinds of fish.
 * - trees, all in one group.
 * - cars, presumably by exhaust or gasoline or something.
 * - perfumes, which is handy, since many of your Aunts Sue wear a few kinds.
 *
 * Things missing from your list aren't zero - you simply don't
 * remember the value.
 */
class AuntSue /* implements Comparable */ {
    final static int UNDEF = -1;
    final int id,
        children,
        cats,
        samoyeds, pomeranians, akitas, vizslas,
        goldfish,
        trees,
        cars,
        perfumes;

    // private
    AuntSue(int id, int ch, int ct,
            int sa, int po, int ak, int vi,
            int go, int tr, int cr, int pe) {
        this.id = id;
        children = ch;
        cats = ct;
        samoyeds = sa;
        pomeranians = po;
        akitas = ak;
        vizslas = vi;
        goldfish = go;
        trees = tr;
        cars = cr;
        perfumes = pe;
    }

    public int getId() { return id; }

    public boolean cmp(AuntSue aso) {
        return fuzzyCmp(children, aso.children) == 0
            && fuzzyCmp(cats, aso.cats) == 0
            && fuzzyCmp(samoyeds, aso.samoyeds) == 0
            && fuzzyCmp(pomeranians, aso.pomeranians) == 0
            && fuzzyCmp(akitas, aso.akitas) == 0
            && fuzzyCmp(vizslas, aso.vizslas) == 0
            && fuzzyCmp(goldfish, aso.goldfish) == 0
            && fuzzyCmp(trees, aso.trees) == 0
            && fuzzyCmp(cars, aso.cars) == 0
            && fuzzyCmp(perfumes, aso.perfumes) == 0;
    }

    /**
     * In particular, the cats and trees readings indicates that there
     * are greater than that many (due to the unpredictable nuclear
     * decay of cat dander and tree pollen), while the pomeranians and
     * goldfish readings indicate that there are fewer than that many
     * (due to the modial interaction of magnetoreluctance).
     */
    public boolean cmpRanged(AuntSue aso) {
        return fuzzyCmpLt(cats, aso.cats) == -1
            && fuzzyCmpLt(trees, aso.trees) == -1
            && fuzzyCmpGt(pomeranians, aso.pomeranians) == 1
            && fuzzyCmpGt(goldfish, aso.goldfish) == 1
            && fuzzyCmp(children, aso.children) == 0
            && fuzzyCmp(samoyeds, aso.samoyeds) == 0
            && fuzzyCmp(akitas, aso.akitas) == 0
            && fuzzyCmp(vizslas, aso.vizslas) == 0
            && fuzzyCmp(cars, aso.cars) == 0
            && fuzzyCmp(perfumes, aso.perfumes) == 0;
    }

    /**
     * fuzzy
     * compareTo(p, q)
     * if value undefined (i.e., negative), returns '0'
     */
    private static int fuzzyCmp(final int p, final int q) {
        if(p == q)
            return 0;
        if(p < 0)
            return 0;
        if(q < 0)
            return 0;
        return p < q ? -1 : 1;
    }

    /**
     * fuzzy
     * p -gt q
     * if value undefined (i.e., negative), returns '1'
     */
    private static int fuzzyCmpGt(final int p, final int q) {
        if(p < 0)
            return 1;
        if(q < 0)
            return 1;
        if(p == q)
            return 0;
        return p > q ? 1 : -1;
    }

    /**
     * fuzzy
     * p -lt q
     * if value undefined (i.e., negative), returns '-1'
     */
    private static int fuzzyCmpLt(final int p, final int q) {
        if(p < 0)
            return -1;
        if(q < 0)
            return -1;
        if(p == q)
            return 0;
        return p < q ? -1 : 1;
    }

    public boolean equals(Object obj) {
        if(!(obj instanceof AuntSue)) return false;
        AuntSue aso = (AuntSue)obj;
        return id == aso.id &&
            children == aso.children &&
            cats == aso.cats &&
            samoyeds == aso.samoyeds &&
            pomeranians == aso.pomeranians &&
            akitas == aso.akitas &&
            vizslas == aso.vizslas &&
            goldfish == aso.goldfish &&
            trees == aso.trees &&
            cars == aso.cars &&
            perfumes == aso.perfumes;
    }

    public int hashCode() {
        return Objects.hash(id, children, cats, samoyeds, pomeranians, akitas,
                            vizslas, goldfish, trees, cars, perfumes);
    }

    final static Pattern pat
        = Pattern.compile("^Sue (\\d+): (\\w+): (\\d+), (\\w+): (\\d+), (\\w+): (\\d+)$");

    /**
     * Sue 11: goldfish: 1, perfumes: 4, cars: 6
     */
    public static AuntSue create(final String src) {
        int ch = UNDEF, ct = UNDEF,
            sa = UNDEF, po = UNDEF, ak = UNDEF, vi = UNDEF,
            go = UNDEF, tr = UNDEF, cr = UNDEF, pe = UNDEF;
        Matcher m = pat.matcher(src);
        if(!m.find()) return null;
        int id = Integer.parseInt(m.group(1));
        for(int i = 2; i < m.groupCount(); i += 2) {
            switch(m.group(i)) {
            case "children":
                ch = Integer.parseInt(m.group(i + 1));
                break;
            case "cats":
                ct = Integer.parseInt(m.group(i + 1));
                break;
            case "goldfish":
                go = Integer.parseInt(m.group(i + 1));
                break;
            case "trees":
                tr = Integer.parseInt(m.group(i + 1));
                break;
            case "cars":
                cr = Integer.parseInt(m.group(i + 1));
                break;
            case "perfumes":
                pe = Integer.parseInt(m.group(i + 1));
                break;
            case "samoyeds":
                sa = Integer.parseInt(m.group(i + 1));
                break;
            case "pomeranians":
                po = Integer.parseInt(m.group(i + 1));
                break;
            case "akitas":
                ak = Integer.parseInt(m.group(i + 1));
                break;
            case "vizslas":
                vi = Integer.parseInt(m.group(i + 1));
                break;
            }
        }
        return new AuntSue(id, ch, ct, sa, po, ak, vi, go, tr, cr, pe);
    }
}
