package tld.localdomain.AoC.AoC_2015;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Day 20: Infinite Elves and Infinite Houses
 *
 * To keep the Elves busy, Santa has them deliver some presents by
 * hand, door-to-door. He sends them down a street with infinite
 * houses numbered sequentially: 1, 2, 3, 4, 5, and so on.
 *
 * Each Elf is assigned a number, too, and delivers presents to houses
 * based on that number:
 *
 * - The first Elf (number 1) delivers presents to every house: 1, 2,
 *   3, 4, 5, ....
 * - The second Elf (number 2) delivers presents to every second
 *   house: 2, 4, 6, 8, 10, ....
 * - Elf number 3 delivers presents to every third house: 3, 6, 9, 12,
 *   15, ....
 *
 * There are infinitely many Elves, numbered starting with 1. Each Elf
 * delivers presents equal to ten times his or her number at each
 * house.
 *
 * What is the lowest house number of the house to get at least as
 * many presents as the number in your puzzle input?
 *
 * Your puzzle input is `34000000`.
 */
public class Day20 {
    public static void main(final String[] args) {
        System.out.println(part1(args));
        System.out.println(part2(args));
    }

    /**
     */
    private static int part1(final String[] args) {
        final int NUMO_PRESO = args.length > 0 ? Integer.parseInt(args[0]) : 3400_000;
        int ras = -1;
        // fixme! _too_ slow!
        for(int n = 1; n <= NUMO_PRESO; ++n) {
            int np = numoPreso(n);
            if(np >= NUMO_PRESO) {
                ras = n;
                break;
            }
        }
        return ras;
    }

    /**
     * np(n) = 1 + n%2==0 + n%3==0 + ...
     */
    private static int numoPreso(final int n) {
        int np = 1;
        for(int i = 2; i <= n; ++i)
            if((n % i) == 0)
                np += i;
        return np;
    }

    private static int pn(final int np) {
        for(int i = 1, rem = np; i <= np; ++i) {
            ;
        }
        return -1;
    }

    /**
     * The Elves decide they don't want to visit an infinite number of
     * houses. Instead, each Elf will stop after delivering presents
     * to 50 houses. To make up for it, they decide to deliver
     * presents equal to eleven times their number at each house.
     *
     * With these changes, what is the new lowest house number of the
     * house to get at least as many presents as the number in your
     * puzzle input?
     */
    private static int part2(final String[] args) {
        final int NUMO_PRESO = args.length > 0 ? Integer.parseInt(args[0]) : 3400_0000;
        int ras = -1;
        // fixme! _too_ slow!
        for(int n = 1; n <= NUMO_PRESO; ++n) {
            int np = numoPreso50(n);
            if(np >= NUMO_PRESO) {
                ras = n;
                break;
            }
        }
        return ras;
    }

    /**
     * np10(n) = 10 + n%2==0 + n%3==0 + ...
     */
    private static int numoPreso50(final int n) {
        int np = 0;
        for(int i = 1; i <= n; ++i)
            if(i * 50 <= n)
                if((n % i) == 0)
                    np += i * 10;
        return np;
    }
}
