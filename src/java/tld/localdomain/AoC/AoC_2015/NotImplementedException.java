package tld.localdomain.AoC.AoC_2015;

/**
 * https://stackoverflow.com/questions/2329358/is-there-anything-like-nets-notimplementedexception-in-java
 */
public class NotImplementedException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public NotImplementedException() {}
    public NotImplementedException(final String msg) { System.err.println(msg); }
}
