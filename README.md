-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2019-11-26 13:04:06 roukoru>

[Advent of Code](https://adventofcode.com)

my AoC solutions, remade from scratch and grouped together.
